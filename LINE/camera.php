<!DOCTYPE html>
<html lang="en">

<head>
    <title>霍恩關懷雲-掃描器</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/fontawesome-free-6.0.0-web/css/all.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/line/util.css">
    <link rel="stylesheet" type="text/css" href="../css/line/main.css">
    <style type="text/css">
        .logo {
            text-align: center;
            margin-bottom: 20px;
            padding-left: 15px;
        }

        .text {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 24px;
            color: #04cecf;
            font-weight: 800;
            margin-bottom: 5px;
            text-align: center;
            margin-top: 5px;
        }

        .color {
            font-weight: 500;
            color: #04cecf;
        }

        .form2 {
            font-size: 17px;
            font-weight: 800;
            color: #F26060;
        }

        #checkbox {
            height: 22px;
            width: 22px;
            vertical-align: text-bottom;
        }

        .box {
            width: 100%;
            background: #eee;
            border: 1px solid transparent;
            margin-bottom: 15px;
            margin-top: auto;
            margin-right: auto;
            margin-left: auto;
        }

        .border-img {
            -moz-border-image: -moz-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            -webkit-border-image: -webkit-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            border-image: linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            border-image-slice: 1;
        }

        .select {
            border: 1px solid #F3F3F3;
        }

        .form-control {
            border: 1px solid #CCCCCC;
            width: 100%;
            font-size: 16px;
            color: #999;
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 5px;
            margin-bottom: 30px;
            display: inline;
        }

        .form-control:hover {
            border: 1px solid #CCCCCC;
        }

        .green {
            color: #008C9B;
        }

        .red {
            color: #F33030;
        }

        .lightgreen {
            color: lightgreen;
        }

        .record-text {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            padding-top: 5px;
            padding-bottom: 5px;
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: #DBDBDB;
            text-align: left;
            margin-top: 3px;
            margin-bottom: 3px;
            font-weight: 500;
            color: #666;
        }

        .record-text-1 {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: left;
            margin-top: 3px;
            margin-bottom: 8px;
            font-weight: 500;
            color: #666;
            display: inline;
        }

        .line {
            border-top-width: 1px;
            border-top-style: dotted;
            border-top-color: #999999;
            margin-top: 17px;
            margin-bottom: 17px;
        }

        .form-control {
            border: 1px solid #CCCCCC;
            width: 100%;
            font-size: 16px;
            color: #999;
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 5px;
            margin-bottom: 12px;
        }

        .form-control:hover {
            border: 1px solid #CCCCCC;
            background-color: #F9F9F9;
        }

        ::-ms-input-placeholder {
            /* Microsoft Edge */
            color: red;
        }

        .orange {
            color: #FFB11B;
        }

        .preview-container {
            width: 300px;
            height: 300px;
            overflow: hidden;
            border: 3px dotted #ccc;
            margin: 0px auto;
        }
    </style>

    <!--===============================================================================================-->
</head>

<body>
    <div class="row limiter">
        <div class="col-md-4 col-lg-12">
            <div class="container-docter">
                <div class="wrap-docter">
                    <!--title-->
                    <div class="col-md-12">
                        <p class="text">掃描器</p>
                    </div>
                    <div class="box border-img"></div>
                    <div style="display:inline-block;margin-top: 70px;width:100%">
                        <div class="record-text-1">
                            <span class="green"><b>掃描器</b></span>
                        </div>
                        <div>
                            <video id="preview" width="320" height="300"></video>
                        </div>
                    </div>
                    <div style="display:inline-block;margin-top: 20px;width:100%">
                        <div class="record-text-1">
                            <span class="green"><b>掃碼結果</b></span>
                        </div>
                        <div>
                            <span id="content"></span>
                        </div>
                    </div>
                    <div style="display:inline-block;margin-top: 90px;width:100%"></div>
                </div>
            </div>
        </div>
    </div>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
    <script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/bootstrap/js/popper.js"></script>
    <script src="../css/vendor/line/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/daterangepicker/moment.min.js"></script>
    <script src="../css/vendor/line/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/jquery-qrcode-master/jquery.qrcode.min.js"></script>
    <!--===============================================================================================-->
    <script src="../js/line/main.js"></script>
    <script>
        // 開啟一個新的掃描:宣告變數scanner，在html<video>標籤id為preview的地方開啟相機預覽。
        // Notice:這邊注意一定要用<video>的標籤才能使用。
        let scanner = new Instascan.Scanner({
            video: document.getElementById('preview')
        });
        //content承載掃描結果，透過console.log顯示。
        scanner.addListener('scan', function(content) {
            document.getElementById('content').innerHTML = content;
            setTimeout(function() {
                location.href = content + '&userSid=<?php echo $_GET['userSid']; ?>';
            }, 2000);
        });
        //設定相機
        Instascan.Camera.getCameras().then(function(cameras) {
            if (cameras.length > 1) {
                scanner.start(cameras[1]);
            }
            //取得設備的相機數目
            if (cameras.length > 0) {
                ///若設備相機數目大於0 則先開啟第0個相機
                //self.activeCameraId = cameras[0].id;
                scanner.start(cameras[0]);
            } else {
                //若設備沒有相機數量則顯示"No cameras found";
                //這裡自行判斷要寫什麼
                console.error('No cameras found.');
            }
        }).catch(function(e) {
            console.log(e);
        });
        /*var app = new Vue({
            el: '#app',
            data: {
                scanner: null,
                activeCameraId: null,
                cameras: [],
                scans: []
            },
            mounted: function() {
                var self = this;
                self.scanner = new Instascan.Scanner({
                    video: document.getElementById('preview'),
                    scanPeriod: 5
                });
                self.scanner.addListener('scan', function(content, image) {
                    self.scans.unshift({
                        date: +(Date.now()),
                        content: content
                    });
                });
                Instascan.Camera.getCameras().then(function(cameras) {
                    self.cameras = cameras;
                    if (cameras.length > 0) {
                        self.activeCameraId = cameras[0].id;
                        self.scanner.start(cameras[0]);
                    } else {
                        console.error('No cameras found.');
                    }
                }).catch(function(e) {
                    console.error(e);
                });
            },
            methods: {
                formatName: function(name) {
                    return name || '(unknown)';
                },
                selectCamera: function(camera) {
                    this.activeCameraId = camera.id;
                    this.scanner.start(camera);
                }
            }
        });*/
    </script>

</body>

</html>