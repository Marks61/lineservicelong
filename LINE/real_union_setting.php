<!DOCTYPE html>
<html lang="en">

<head>
    <title>霍恩關懷雲-申請實聯制</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/bootstrap/css/bootstrap.min.css">

    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="../admin/css/colorlib/vendor/sweetalert2/dist/sweetalert2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/line/util.css">
    <link rel="stylesheet" type="text/css" href="../css/line/main.css">
    <script src="https://www.google.com/recaptcha/api.js?render=6Lcc0ekeAAAAAILG-vmZAKdkUCkvkwx9NP8xzuIS"></script>
    <style type="text/css">
        .logo {
            text-align: center;
            margin-bottom: 20px;
            padding-left: 15px;
        }

        .text {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 24px;
            color: #04cecf;
            font-weight: 800;
            margin-bottom: 5px;
            text-align: center;
            margin-top: 5px;
        }

        .color {
            font-weight: 500;
            color: #04cecf;
        }

        .form2 {
            font-size: 17px;
            font-weight: 800;
            color: #F26060;
        }

        #checkbox {
            height: 22px;
            width: 22px;
            vertical-align: text-bottom;
        }

        .box {
            width: 100%;
            background: #eee;
            border: 1px solid transparent;
            margin-bottom: 15px;
            margin-top: auto;
            margin-right: auto;
            margin-left: auto;
        }

        .border-img {
            -moz-border-image: -moz-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            -webkit-border-image: -webkit-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            border-image: linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            border-image-slice: 1;
        }

        .select {
            border: 1px solid #F3F3F3;
        }

        .form-control {
            border: 1px solid #CCCCCC;
            width: 100%;
            font-size: 16px;
            color: #999;
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 5px;
            margin-bottom: 30px;
            display: inline;
        }

        .form-control:hover {
            border: 1px solid #CCCCCC;
        }

        .green {
            color: #008C9B;
        }

        .red {
            color: #F33030;
        }

        .record-text {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            padding-top: 5px;
            padding-bottom: 5px;
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: #DBDBDB;
            text-align: left;
            margin-top: 3px;
            margin-bottom: 3px;
            font-weight: 500;
            color: #666;
        }

        .record-text-1 {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: left;
            margin-top: 3px;
            margin-bottom: 8px;
            font-weight: 500;
            color: #666;
            display: inline;
        }

        .line {
            border-top-width: 1px;
            border-top-style: dotted;
            border-top-color: #999999;
            margin-top: 17px;
            margin-bottom: 17px;
        }

        .form-control {
            border: 1px solid #CCCCCC;
            width: 100%;
            font-size: 16px;
            color: #999;
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 5px;
            margin-bottom: 12px;
        }

        .form-control:hover {
            border: 1px solid #CCCCCC;
            background-color: #F9F9F9;
        }

        ::-ms-input-placeholder {
            /* Microsoft Edge */
            color: red;
        }
    </style>

    <!--===============================================================================================-->
</head>

<body>
    <div class="row limiter">
        <div class="col-md-4 col-lg-12">
            <div class="container-docter">
                <div class="wrap-docter">
                    <!--title-->
                    <div class="col-md-12">
                        <p class="text">申請實聯制</p>
                    </div>
                    <div class="box border-img"></div>
                    <!--title-->
                    <!--list-->
                    <div style="display:inline-block;margin-top: 30px;width:100%">
                        <div class="record-text-1">
                            <span class="green"><b>申請人電話號碼</b></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter Phone">
                            <input class="input100" type="text" name="phone" id="phone" placeholder="請輸入手機號碼，查詢管理qrcode使用。">
                            <span class="focus-input100" data-placeholder=""></span>
                        </div>
                    </div>
                    <div style="display:inline-block;margin-top: 15px;width:100%">
                        <div class="record-text-1">
                            <span class="green"><b>申請人</b></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter user">
                            <input class="input100" type="text" name="user" id="user" placeholder="請輸入姓名">
                            <span class="focus-input100" data-placeholder=""></span>
                        </div>
                    </div>
                    <div style="display:inline-block;margin-top: 15px;width:100%">
                        <div class="record-text-1">
                            <span class="green"><b>申請人電子信箱</b></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter Email">
                            <input class="input100" type="text" name="email" id="email" placeholder="請輸入電子信箱">
                            <span class="focus-input100" data-placeholder=""></span>
                        </div>
                    </div>
                    <div style="display:inline-block;margin-top: 15px;width:100%">
                        <div class="record-text-1">
                            <span class="green"><b>店名</b></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter location">
                            <input class="input100" type="text" name="location" id="location" placeholder="請輸入店家名稱">
                            <span class="focus-input100" data-placeholder=""></span>
                        </div>
                    </div>
                    <div style="display:inline-block;margin-top: 15px;width:100%">
                        <div class="record-text-1">
                            <span class="green"><b>店家住址</b></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter address">
                            <input class="input100" type="text" name="address" id="address" placeholder="請輸入店家地址">
                            <span class="focus-input100" data-placeholder=""></span>
                        </div>
                    </div>
                    <div style="display:inline-block;margin-top: 15px;width:100%">
                        <div class="record-text-1">
                            <span class="green"><b>店家連絡電話</b></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter contact">
                            <input class="input100" type="text" name="contact" id="contact" placeholder="請輸入店家連絡電話">
                            <span class="focus-input100" data-placeholder=""></span>
                        </div>
                    </div>
                    <div style="display:inline-block;margin-top: 15px;width:100%">
                        <div class="record-text-1">
                            <span class="green"><b>分店數量</b></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter number">
                            <input class="input100" type="number" name="number" id="number" placeholder="請輸入分店數量">
                            <span class="focus-input100" data-placeholder=""></span>
                        </div>
                    </div>
                    <div style="display:inline-block;margin-top: 15px;width:100%"></div>
                    <div class="wrap-docter-form-btn">
                        <div class="docter-form-bgbtn"></div>
                        <button class="docter-form-btn" onclick="poster();">
                            <input type="button" id="btn_login"> 送出申請
                        </button>
                    </div>
                    <div style="display:inline-block;margin-top: 50px;width:100%"></div>
                    <input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse">
                    <div class="text-center p-t-10">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
    <script src="../css/vendor/line/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/bootstrap/js/popper.js"></script>
    <script src="../css/vendor/line/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/daterangepicker/moment.min.js"></script>
    <script src="../css/vendor/line/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="../admin/css/colorlib/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="../js/line/main.js"></script>
    <script>
        $(document).ready(function() {});

        function poster() {
            var request = $.ajax({
                url: "../new_api/real_union_create.php",
                method: "POST",
                data: {
                    user: $('#user').val(),
                    phone: $('#phone').val(),
                    location: $('#location').val(),
                    address: $('#address').val(),
                    contact: $('#contact').val(),
                    number: $('#number').val(),
                    email: $('#email').val(),
                    recaptcha: document.getElementById('recaptchaResponse').value
                },
                dataType: "text"
            });
            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                console.log(arr);
                if (arr['success'] == "1") {
                    Swal.fire(
                        "完成", //標題 
                        "申請已經送出。請登入店家管理系統下載QrCode。", //訊息內容(可省略)
                        "success" //圖示(可省略) success/info/warning/error/question
                        //圖示範例：https://sweetalert2.github.io/#icons
                    );
                    //alert('LINE服務已經開通，請關閉本畫面。');
                } else if (arr['success'] == '2') {
                    Swal.fire(
                        "完成", //標題 
                        "申請已經送出，請繼續完成店家帳號的資料。<a href='http://127.0.0.1/horn/lineservice/LINE/real_union_userData.php?id=" + arr['userSid'] + "' >點我完成店家資料。</a>", //訊息內容(可省略)
                        "success" //圖示(可省略) success/info/warning/error/question
                        //圖示範例：https://sweetalert2.github.io/#icons
                    ).then(function(result) {
                        if (result.value) {
                            location.href = "http://127.0.0.1/horn/lineservice/LINE/real_union_userData.php?id=" + arr['userSid'];
                        }
                    });
                } else {
                    Swal.fire(
                        "錯誤", //標題 
                        arr['message']['ch'], //訊息內容(可省略)
                        "error" //圖示(可省略) success/info/warning/error/question
                        //圖示範例：https://sweetalert2.github.io/#icons
                    );
                }
                // console.log(arr);
            });
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
            // console.log(isChecked);

        }

        grecaptcha.ready(function() {
            grecaptcha.execute('6Lcc0ekeAAAAAILG-vmZAKdkUCkvkwx9NP8xzuIS', {
                action: 'submit'
            }).then(function(token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>

</body>

</html>