<?php
if (isset($_POST['error'])) {
  echo "error:" . $_POST['error'];
  echo "<br>";
  echo "error_description:" . $_POST['error_description'];
  echo "<br>";
  exit;
};

$Push_Content['grant_type'] = "authorization_code";
$Push_Content['code'] = $_POST['code'];
$Push_Content['redirect_uri'] = "https://2ef7-2001-b011-e605-32ee-c56f-17b7-a817-6569.ap.ngrok.io/horn/lineservice/LINE/lineNotifyCallback.php";
$Push_Content['client_id'] = "WfPqvHcz3GJ0WYfJtvCHLs";
$Push_Content['client_secret'] = "89aJsPvIaNvFOj5F8jQJ7ao2Gtx4PouZuijz1LxeUAH";
// Auth Line Official Connect Step-1
$HTTP_Request_Header = getallheaders();
$PassCallBackCheck = 0;
if (isset($HTTP_Request_Header['Origin'])) {
  if ($HTTP_Request_Header['Origin'] == "https://notify-bot.line.me") {
    $PassCallBackCheck++;
  };
};
if (isset($HTTP_Request_Header['Referer'])) {
  $ExplodeReferer = explode("&", parse_url($HTTP_Request_Header['Referer'], PHP_URL_QUERY));
  if (Count($ExplodeReferer) > 2) {
    if ($ExplodeReferer[1] == "client_id=" . $Push_Content['client_id']) {
      $PassCallBackCheck++;
    };
  };
};
/*if ($PassCallBackCheck != 2) {
   header('HTTP/1.1 403 Forbidden');
   exit;
  };*/
// Auth Line Official Connect Step-1

$ch = curl_init("https://notify-bot.line.me/oauth/token");
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($Push_Content));
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  'Content-Type: application/x-www-form-urlencoded'
));
$response_json_str = curl_exec($ch);
curl_close($ch);
$response = json_decode($response_json_str, true);
var_dump($response['access_token']);
exit;
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title></title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../css/vendor/line/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/vendor/line/fontawesome-free-6.0.0-web/css/all.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../css/vendor/line/animate/animate.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../css/vendor/line/css-hamburgers/hamburgers.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../css/vendor/line/animsition/css/animsition.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../css/vendor/line/select2/select2.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../css/vendor/line/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" type="text/css" href="../admin/css/colorlib/vendor/sweetalert2/dist/sweetalert2.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../css/line/util.css">
  <link rel="stylesheet" type="text/css" href="../css/line/main.css">
  <style type="text/css">
    .logo {
      text-align: center;
      margin-bottom: 20px;
      padding-left: 15px;
    }

    .text {
      font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
      font-size: 24px;
      color: #04cecf;
      font-weight: 800;
      margin-bottom: 5px;
      text-align: center;
      margin-top: 5px;
    }

    .color {
      font-weight: 500;
      color: #04cecf;
    }

    .form2 {
      font-size: 17px;
      font-weight: 800;
      color: #F26060;
    }

    #checkbox {
      height: 22px;
      width: 22px;
      vertical-align: text-bottom;
    }

    .box {
      width: 100%;
      background: #eee;
      border: 1px solid transparent;
      margin-bottom: 15px;
      margin-top: auto;
      margin-right: auto;
      margin-left: auto;
    }

    .border-img {
      -moz-border-image: -moz-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
      -webkit-border-image: -webkit-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
      border-image: linear-gradient(top left, #04cecf 0%, #ffffff 100%);
      border-image-slice: 1;
    }

    .select {
      border: 1px solid #F3F3F3;
    }

    .form-control {
      border: 1px solid #CCCCCC;
      width: 100%;
      font-size: 16px;
      color: #999;
      padding-top: 5px;
      padding-bottom: 5px;
      border-radius: 5px;
      margin-bottom: 30px;
      display: inline;
    }

    .form-control:hover {
      border: 1px solid #CCCCCC;
    }

    .green {
      color: #008C9B;
    }

    .red {
      color: #F33030;
    }

    .record-text {
      font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
      font-size: 18px;
      padding-top: 5px;
      padding-bottom: 5px;
      border-bottom-width: 1px;
      border-bottom-style: solid;
      border-bottom-color: #DBDBDB;
      text-align: left;
      margin-top: 3px;
      margin-bottom: 3px;
      font-weight: 500;
      color: #666;
    }

    .record-text-1 {
      font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
      font-size: 18px;
      padding-top: 5px;
      padding-bottom: 5px;
      text-align: left;
      margin-top: 3px;
      margin-bottom: 8px;
      font-weight: 500;
      color: #666;
      display: inline;
    }

    .line {
      border-top-width: 1px;
      border-top-style: dotted;
      border-top-color: #999999;
      margin-top: 17px;
      margin-bottom: 17px;
    }

    .form-control {
      border: 1px solid #CCCCCC;
      width: 100%;
      font-size: 16px;
      color: #999;
      padding-top: 5px;
      padding-bottom: 5px;
      border-radius: 5px;
      margin-bottom: 12px;
    }

    .form-control:hover {
      border: 1px solid #CCCCCC;
      background-color: #F9F9F9;
    }

    ::-ms-input-placeholder {
      /* Microsoft Edge */
      color: red;
    }

    .wrap-docter {
      width: 100%;
    }
  </style>

  <!--===============================================================================================-->
</head>

<body>
  <form id="form" action="/NotifyReset.php?userSid=<?php echo $_GET['state']; ?>" method="POST">
    <input style="display: none;" id="access_token" value="access_token" value="<?php echo $response['access_token']; ?>">
  </form>
  <!--===============================================================================================-->
  <script src="../css/vendor/line/jquery/jquery-3.2.1.min.js"></script>
  <!--===============================================================================================-->
  <script src="../css/vendor/line/animsition/js/animsition.min.js"></script>
  <!--===============================================================================================-->
  <script src="../css/vendor/line/bootstrap/js/popper.js"></script>
  <script src="../css/vendor/line/bootstrap/js/bootstrap.min.js"></script>
  <!--===============================================================================================-->
  <script src="../css/vendor/line/select2/select2.min.js"></script>
  <!--===============================================================================================-->
  <script src="../css/vendor/line/daterangepicker/moment.min.js"></script>
  <script src="../css/vendor/line/daterangepicker/daterangepicker.js"></script>
  <!--===============================================================================================-->
  <script src="../css/vendor/line/countdowntime/countdowntime.js"></script>
  <!--===============================================================================================-->
  <script src="../admin/css/colorlib/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="../js/line/main.js"></script>
  <script>
    window.onload = function() {
      oAuth2();
    };

    function oAuth2() {
      var request = $.ajax({
        url: "../new_api/account_opening_notify_service.php",
        method: "POST",
        data: {
          Uid: '<?php echo $_POST['state']; ?>',
          token: '<?php echo $response['access_token']; ?>'
        },
        dataType: "text"
      });
      request.done(function(dat) {
        var data = JSON.parse(dat);
        var arr = [];
        for (var key in data) {
          arr[key] = data[key];
        }
        if (arr['success'] == 1) {
          //second = 0;
          Swal.fire(
            "完成", //標題 
            "已經完成Notify功能綁定，系統將發送測試訊息。", //訊息內容(可省略)
            "success" //圖示(可省略) success/info/warning/error/question
            //圖示範例：https://sweetalert2.github.io/#icons
          );
        } else if (arr['success'] == 2) {
          Swal.fire(
            "錯誤", //標題 
            arr['message'], //訊息內容(可省略)
            "info" //圖示(可省略) success/info/warning/error/question
            //圖示範例：https://sweetalert2.github.io/#icons
          ).then(function(result) {
            if (result.value) {
              $('#form').submit();
            }
          });
        }
        // console.log(arr);
      });
      request.fail(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
      });
      // console.log(isChecked);

    }
  </script>

</body>

</html>