<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <title>霍恩關懷雲-實聯制歷史查詢</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/fontawesome-free-6.0.0-web/css/all.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->

    <link rel="stylesheet" type="text/css" href="../css/line/util.css">
    <link rel="stylesheet" type="text/css" href="../css/line/main.css">
    <style type="text/css">
        .logo {
            text-align: center;
            margin-bottom: 20px;
            padding-left: 15px;
        }


        .line {
            border-top-width: 1px;
            border-top-style: solid;
            border-top-color: #DBDBDB;
        }

        .text {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 26px;
            color: #04cecf;
            font-weight: 800;
            margin-bottom: 5px;
            text-align: center;
            margin-top: 5px;
        }

        .color {
            font-weight: 500;
            color: #04cecf;
        }

        .title {
            width: 100%;
            margin-bottom: 50px;
            padding-top: 1px;
            padding-bottom: 1px;
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: #18B5CB;
        }


        .header {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 22px;
            text-align: center;
            padding-top: 8px;
            padding-bottom: 8px;
            background-color: #F0F0F0;
            font-weight: 500;
            color: #666;
        }

        .header-1 {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            text-align: center;
            font-weight: 500;
            color: #999;
            margin-bottom: 10px;
        }

        .card {
            margin-bottom: 20px;
        }

        .record-text {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            padding-top: 5px;
            padding-bottom: 5px;
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: #DBDBDB;
            text-align: center;
            margin-top: 3px;
            margin-bottom: 3px;
            font-weight: 500;
            color: #666;
        }

        .record-text-1 {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: center;
            margin-top: 3px;
            margin-bottom: 3px;
            font-weight: 500;
            color: #666;
        }

        .green {
            color: #008C9B;
        }

        .red {
            color: #F33030;
        }

        .box {
            width: 100%;
            background: #eee;
            border: 1px solid transparent;
            margin-bottom: 35px;
            margin-top: auto;
            margin-right: auto;
            margin-left: auto;
        }

        .border-img {
            -moz-border-image: -moz-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            -webkit-border-image: -webkit-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            border-image: linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            border-image-slice: 1;
        }

        .hidden {
            display: none;
        }

        .show {
            display: inline-block;
        }

        body {
            overflow-y: auto;
        }
    </style>

    <!--===============================================================================================-->
</head>

<body>
    <div class="row limiter">
        <div class="col-md-12 col-lg-12">
            <input type="text" style="display: none;" id="sid" value="" />
            <div class="container-docter">
                <div class="wrap-docter">
                    <!--search bar
                    <div class="title">
                    </div>-->
                    <div class="col-md-12">
                        <p class="text">實聯制個人歷史紀錄查詢</p>
                    </div>
                    <div class="box border-img"></div>
                    <div class="mb-3">
                        <div>
                            <i class="green fas fa-calendar-alt" style="padding-right:4px;"></i>
                            <span style="padding-right: 4px;">
                                <select id="dateSelect" name="dateSelect" class="form-control col-lg-12 hidden" style="width: 187.2px;">
                                    <option value="1">今日</option>
                                    <option value="3">3日內</option>
                                    <option value="5" <?php echo $_GET['day'] == 5 ? 'selected' : ''; ?>>5日內</option>
                                    <option value="10" <?php echo $_GET['day'] == 10 ? 'selected' : ''; ?>>10日內</option>
                                    <option value="20" <?php echo $_GET['day'] == 20 ? 'selected' : ''; ?>>20日內</option>
                                    <option value="28" <?php echo $_GET['day'] == 28 ? 'selected' : ''; ?>>28日內</option>
                                </select>
                                <span id="date" style="color: rgb(150, 146, 146);">5日內(****/**/** ~
                                    ****/**/**)</span>
                            </span>
                            <i id="downButton" onclick="selectOpen();" class="green fas fa-chevron-circle-down"></i>
                            <i id="upButton" onclick="closeSelect();" class="green fas fa-chevron-circle-up hidden"></i>
                        </div>
                    </div>

                    <!--search bar-->

                    <div class="header-1">內容</div>
                    <!--record block-->
                    <div id="info" class="hidden">
                    </div>
                    <table class="table custom-table hidden" id="noData">
                        <thead>
                        </thead>
                        <tbody>
                            <tr scope="row">
                                <td>查無資料</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="text-center p-t-47">
            </div>
        </div>
    </div>
    </div>
    </div>

    <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
    <script src="../css/vendor/line/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/bootstrap/js/popper.js"></script>
    <script src="../css/vendor/line/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/daterangepicker/moment.min.js"></script>
    <script src="../css/vendor/line/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="../js/line/main.js"></script>
    <script>
        var date = new Date();
        $(document).ready(function() {
            console.log(date);
            var startdate = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();
            var endDate = date.addDays($('#dateSelect').val());
            endDate = endDate.getFullYear() + '/' + (endDate.getMonth() + 1) + '/' + endDate.getDate();
            $('#date').text($('#dateSelect').val() + '日內(' + endDate + ' ~ ' + startdate + ')');
            getInfo(endDate, startdate);
        });

        function open(id, num) {
            $("#" + id).attr("size", num);
        }

        function selectOpen() {
            $('#date').addClass('hidden');
            $('#dateSelect').removeClass('hidden');
            $('#dateSelect').addClass('show');
            $('#downButton').addClass('hidden');
            $('#upButton').removeClass('hidden');
            $('#dateSelect').attr('size', 7);
            // open('dateSelect', 6);
        }

        Date.prototype.addDays = function(days) {
            this.setDate(this.getDate() - parseInt(days - 1));
            return this;
        };

        function closeSelect() {
            $('#date').removeClass('hidden');
            $('#dateSelect').addClass('hidden');
            $('#dateSelect').removeClass('show');
            $('#downButton').removeClass('hidden');
            $('#upButton').addClass('hidden');
            $('#dateSelect').attr('size', 0);

            date = new Date();
            startdate = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();
            endDate = date.addDays($('#dateSelect').val());
            endDate = endDate.getFullYear() + '/' + (endDate.getMonth() + 1) + '/' + endDate.getDate();
            $('#date').text($('#dateSelect').val() + '日內(' + endDate + ' ~ ' + startdate + ')');
            // open('dateSelect', 6);
        }

        function getInfo(endDate, startdate) {
            var request = $.ajax({
                url: "../new_api/real_union_search.php",
                method: "POST",
                data: {
                    startdate: endDate,
                    endDate: startdate,
                    userSid: '<?php echo $_GET['userSid']; ?>'
                },
                dataType: "text"
            });
            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                if (arr['success'] == 1) {
                    if (arr['num'] <= 0) {
                        $('#nodata').removeClass('hidden');
                    } else {
                        var text = '';
                        for (var i = 0; i <= (arr['num'] - 1); i++) {
                            var listDate = new Date(arr['row'][i]['in_time']);
                            text += '<div class="card">';
                            text += '<div class="header">';
                            text += '<span>';
                            text += '日期/時間';
                            text += '</span>';
                            text += '</div>';
                            text += '<div class="record-text">';
                            text += '<span class="green">' + listDate.getFullYear() + ' - ' + ((listDate.getMonth() + 1) <= 9 ? '0' + (listDate.getMonth() + 1) : (listDate.getMonth() + 1)) + ' - ' + listDate.getDate() + '/' + (listDate.getHours() <= 9 ? '0' + listDate.getHours() : listDate.getHours()) + ':' + (listDate.getMinutes() <= 9 ? '0' + listDate.getMinutes() : listDate.getMinutes()) + '</span>';
                            text += '</div>';
                            text += '<div class="header-1">';
                            text += '<span>';
                            text += '店家代碼';
                            text += '</span>';
                            text += '</div>';
                            text += '<div class="record-text-1">';
                            text += '<span>' + arr['row'][i]['token'] + '</span>';
                            text += '</div>';
                            text += '<div class="header-1">';
                            text += '<span>';
                            text += '店家名稱';
                            text += '</span>';
                            text += '</div>';
                            text += '<div class="record-text-1">';
                            text += '<span>' + arr['row'][i]['name'] + '</span>';
                            text += '</div>';
                            text += '<div class="header-1">';
                            text += '<span>';
                            text += '店家地址';
                            text += '</span>';
                            text += '</div>';
                            text += '<div class="record-text-1">';
                            text += '<span>' + arr['row'][i]['address'] + '</span>';
                            text += '</div>';
                            text += '</div>';
                        }
                        $('#info').html('');
                        $('#info').html(text);
                        $('#info').removeClass('hidden');
                    }
                }
                // console.log(arr);
            });
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
            // console.log(isChecked);

        }
    </script>
</body>

</html>