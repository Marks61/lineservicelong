<!DOCTYPE html>
<html lang="en">

<head>
    <title>霍恩關懷雲-驗證中</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/fontawesome-free-6.0.0-web/css/all.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/line/util.css">
    <link rel="stylesheet" type="text/css" href="../css/line/main.css">
    <style type="text/css">
        .logo {
            text-align: center;
            margin-bottom: 20px;
            padding-left: 15px;
        }

        .text {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 24px;
            color: #04cecf;
            font-weight: 800;
            margin-bottom: 5px;
            text-align: center;
            margin-top: 5px;
        }

        .color {
            font-weight: 500;
            color: #04cecf;
        }

        .form2 {
            font-size: 17px;
            font-weight: 800;
            color: #F26060;
        }

        #checkbox {
            height: 22px;
            width: 22px;
            vertical-align: text-bottom;
        }

        .box {
            width: 100%;
            background: #eee;
            border: 1px solid transparent;
            margin-bottom: 15px;
            margin-top: auto;
            margin-right: auto;
            margin-left: auto;
        }

        .border-img {
            -moz-border-image: -moz-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            -webkit-border-image: -webkit-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            border-image: linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            border-image-slice: 1;
        }

        .select {
            border: 1px solid #F3F3F3;
        }

        .form-control {
            border: 1px solid #CCCCCC;
            width: 100%;
            font-size: 16px;
            color: #999;
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 5px;
            margin-bottom: 30px;
            display: inline;
        }

        .form-control:hover {
            border: 1px solid #CCCCCC;
        }

        .green {
            color: #008C9B;
        }

        .red {
            color: #F33030;
        }

        .lightgreen {
            color: lightgreen;
        }

        .record-text {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            padding-top: 5px;
            padding-bottom: 5px;
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: #DBDBDB;
            text-align: left;
            margin-top: 3px;
            margin-bottom: 3px;
            font-weight: 500;
            color: #666;
        }

        .record-text-1 {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: left;
            margin-top: 3px;
            margin-bottom: 8px;
            font-weight: 500;
            color: #666;
            display: inline;
        }

        .line {
            border-top-width: 1px;
            border-top-style: dotted;
            border-top-color: #999999;
            margin-top: 17px;
            margin-bottom: 17px;
        }

        .form-control {
            border: 1px solid #CCCCCC;
            width: 100%;
            font-size: 16px;
            color: #999;
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 5px;
            margin-bottom: 12px;
        }

        .form-control:hover {
            border: 1px solid #CCCCCC;
            background-color: #F9F9F9;
        }

        ::-ms-input-placeholder {
            /* Microsoft Edge */
            color: red;
        }

        .orange {
            color: #FFB11B;
        }

        /*loading*/
        .loading {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            margin-top: 10px;
        }

        .loading__inner {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            background-color: #008C9B;
            margin-left: 10px;
        }

        .loading__inner_big {
            width: 25px;
            height: 25px;
        }
    </style>

    <!--===============================================================================================-->
</head>

<body>
    <div class="row limiter">
        <div class="col-md-4 col-lg-12">
            <div class="container-docter">
                <div class="wrap-docter">
                    <!--title-->
                    <div class="col-md-12">
                        <p class="text">驗證中</p>
                    </div>
                    <div class="box border-img"></div>
                    <!--title-->
                    <!--list-->
                    <div style="display:inline-block;margin-top: 180px;width:100%">
                        <div class="record-text-1">
                            <span class="green"><b></b><i class="fas fa-chevron-circle-down green"></i></span>
                        </div>
                        <div class="loading row" id="loading_1">
                            <div class="col-md-12 lightgreen text-center">
                                <span><i class="fas fa-check-circle fa-4x"></i></span>
                            </div>
                            <div class="green text-center">
                                <h5><b>請確認電腦畫面已經完成登入，並關閉此畫面。</b></h5>
                            </div>
                        </div>
                        <div class="loading row" id="loading_3">
                            <div class="col-md-12 red text-center">
                                <span><i class="fas fa-unlink fa-4x"></i></span>
                            </div>
                            <div class="green text-center">
                                <h5><b>登入失敗或是QR Code已經失效，請關閉此畫面。</b></h5>
                            </div>
                        </div>
                        <div class="loading row" id="loading_4">
                            <div class="col-md-12 red text-center">
                                <span><i class="fas fa-unlink fa-4x"></i></span>
                            </div>
                            <div class="green text-center">
                                <h5><b>LINE帳號沒有與本服務完成綁定。</b></h5>
                            </div>
                        </div>
                        <div class="loading row" id="loading_5">
                            <div class="col-md-12 red text-center">
                                <span><i class="fas fa-unlink fa-4x"></i></span>
                            </div>
                            <div class="green text-center">
                                <h5><b>與服務連線失敗，請稍後再試。</b></h5>
                            </div>
                        </div>
                        <div class="loading" id="loading_2">
                            <i class="loading__inner loading__inner_big"></i>
                            <i class="loading__inner"></i>
                            <i class="loading__inner"></i>
                            <i class="loading__inner"></i>
                            <i class="loading__inner"></i>
                        </div>
                    </div>
                    <div style="display:inline-block;margin-top:250px;width:100%"></div>
                    <div class="text-center p-t-10">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
    <script src="../css/vendor/line/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/bootstrap/js/popper.js"></script>
    <script src="../css/vendor/line/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/daterangepicker/moment.min.js"></script>
    <script src="../css/vendor/line/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/jquery-qrcode-master/jquery.qrcode.min.js"></script>
    <!--===============================================================================================-->
    <script src="../js/line/main.js"></script>
    <script>
        var close_status = true;
        var inner = $('.loading__inner');
        var i = 0;
        $(document).ready(function() {
            $('#loading_1').hide();
            $('#loading_3').hide();
            $('#loading_4').hide();
            $('#loading_5').hide();
            setInterval(() => {
                i++;
                if (i > 4) {
                    i = 0;
                };
                inner.removeClass('loading__inner_big');
                $(inner[i]).addClass('loading__inner_big');
            }, 500);
            vail_status();
        });

        function close_window() {
            if (close_status)
                window.close();
        }

        function vail_status() {
            var request = $.ajax({
                url: "../new_api/account_login_qrcode_status.php",
                method: "POST",
                data: {
                    token: '<?php echo $_GET['token']; ?>',
                    sid: '<?php echo $_GET['userSid']; ?>'
                },
                dataType: "text"
            });
            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                if (arr['success'] == 1) {
                    $('#loading_1').show();
                } else if (arr['success'] == -1) {
                    $('#loading_4').show();
                } else if (arr['success'] == -2) {
                    $('#loading_3').show();
                }
                $('#loading_2').hide();
                // console.log(arr);
            });
            request.fail(function(jqXHR, textStatus) {
                $('#loading_2').hide();
                $('#loading_5').show();
                //alert("Request failed: " + textStatus);
            });
            // console.log(isChecked);

        }
    </script>

</body>

</html>