<!DOCTYPE html>
<html lang="en">

<head>
    <title>霍恩關懷雲-找回密碼</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/fontawesome-free-6.0.0-web/css/all.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/line/util.css">
    <link rel="stylesheet" type="text/css" href="../css/line/main.css">
    <style type="text/css">
        .logo {
            text-align: center;
            margin-bottom: 20px;
            padding-left: 15px;
        }

        .text {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 24px;
            color: #04cecf;
            font-weight: 800;
            margin-bottom: 5px;
            text-align: center;
            margin-top: 5px;
        }

        .color {
            font-weight: 500;
            color: #04cecf;
        }

        .form2 {
            font-size: 17px;
            font-weight: 800;
            color: #F26060;
        }

        #checkbox {
            height: 22px;
            width: 22px;
            vertical-align: text-bottom;
        }

        .box {
            width: 100%;
            background: #eee;
            border: 1px solid transparent;
            margin-bottom: 15px;
            margin-top: auto;
            margin-right: auto;
            margin-left: auto;
        }

        .border-img {
            -moz-border-image: -moz-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            -webkit-border-image: -webkit-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            border-image: linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            border-image-slice: 1;
        }

        .select {
            border: 1px solid #F3F3F3;
        }

        .form-control {
            border: 1px solid #CCCCCC;
            width: 100%;
            font-size: 16px;
            color: #999;
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 5px;
            margin-bottom: 30px;
            display: inline;
        }

        .form-control:hover {
            border: 1px solid #CCCCCC;
        }

        .green {
            color: #008C9B;
        }

        .red {
            color: #F33030;
        }

        .lightgreen {
            color: lightgreen;
        }

        .record-text {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            padding-top: 5px;
            padding-bottom: 5px;
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: #DBDBDB;
            text-align: left;
            margin-top: 3px;
            margin-bottom: 3px;
            font-weight: 500;
            color: #666;
        }

        .record-text-1 {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: left;
            margin-top: 3px;
            margin-bottom: 8px;
            font-weight: 500;
            color: #666;
            display: inline;
        }

        .line {
            border-top-width: 1px;
            border-top-style: dotted;
            border-top-color: #999999;
            margin-top: 17px;
            margin-bottom: 17px;
        }

        .form-control {
            border: 1px solid #CCCCCC;
            width: 100%;
            font-size: 16px;
            color: #999;
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 5px;
            margin-bottom: 12px;
        }

        .form-control:hover {
            border: 1px solid #CCCCCC;
            background-color: #F9F9F9;
        }

        ::-ms-input-placeholder {
            /* Microsoft Edge */
            color: red;
        }

        .orange {
            color: #FFB11B;
        }
    </style>

    <!--===============================================================================================-->
</head>

<body>
    <div class="row limiter">
        <div class="col-md-4 col-lg-12">
            <div class="container-docter">
                <div class="wrap-docter">
                    <!--title-->
                    <div class="col-md-12">
                        <p class="text">找回密碼</p>
                    </div>
                    <div class="box border-img"></div>
                    <!--title-->
                    <!--list-->
                    <div style="display:inline-block;margin-top: 70px;width:100%">
                        <div class="record-text-1">
                            <span class="green"><b>電子信箱驗證</b></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter Email">
                            <input class="input100" type="text" name="email" id="email" placeholder="請輸入信箱">
                            <span class="focus-input100" data-placeholder=""></span>
                        </div>
                    </div>
                    <div style="display:inline-block;margin-top: 30px;width:100%">
                        <div class="record-text-1">
                            <span class="green"><b>一次性 QR Code驗證</b><span style="font-size: 12px;" id="qrcode-status" class="text-right"></span>
                        </div>
                        <div id="qrcode" style="display:inline-block;margin-left: 100px;width:100%;display:none;">
                        </div>
                        <div id="qrcode-time" class="green" style="display:inline-block;margin-left: 145px;width:100%">
                            <span></span>
                        </div>
                    </div>
                    <div style="display:inline-block;margin-top: 10px;width:100%"></div>
                    <div class="wrap-docter-form-btn">
                        <div class="docter-form-bgbtn"></div>
                        <button class="docter-form-btn" onclick="poster();">
                            <input type="button" id="btn_login"> 發送重設信函
                        </button>
                    </div>
                    <div style="display:inline-block;margin-top: 50px;width:100%"></div>
                    <div class="text-center p-t-10">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
    <script src="../css/vendor/line/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/bootstrap/js/popper.js"></script>
    <script src="../css/vendor/line/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/daterangepicker/moment.min.js"></script>
    <script src="../css/vendor/line/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/jquery-qrcode-master/jquery.qrcode.min.js"></script>
    <!--===============================================================================================-->
    <script src="../js/line/main.js"></script>
    <script>
        var second = 300;
        var second_text = '';
        var qrcode_status = false;
        var qrcode_life_function = setInterval(qrcode_life, 1000);
        var qrcode_token = '';
        var userName = '';
        var userId = 0;
        $(document).ready(function() {
            get_qrcode();
            setInterval(function() {
                if (second > 0) {
                    second--;
                }
            }, 1000);
        });

        function qrcode_life() {
            $('#qrcode').show();
            qrcode_sstatus_check();
            var time = second % 60;
            second_text = (Math.floor((second / 60))) + ':' + (time >= 10 ? time : '0' + time);
            $('#qrcode-time span').html(second_text + '<span onclick="qrcode_reload()">  <i i class="fas fa-undo"></i></span>');

            if (second > 0 && !qrcode_status) {
                $('#qrcode-status').removeClass('lightgreen');
                $('#qrcode-status').removeClass('red');
                $('#qrcode-status').addClass('orange');
                $('#qrcode-status').html('<i class="fas fa-exclamation-circle"></i>驗證中</span>');
            } else if (second > 0 && qrcode_status) {
                clearInterval(qrcode_life_function);
                $('#qrcode-status').removeClass('orange');
                $('#qrcode-status').removeClass('red');
                $('#qrcode-status').addClass('lightgreen');
                $('#qrcode-status').html('<i class="fas fa-check-circle"></i>已驗證</span>');
            } else if (second == 0) {
                clearInterval(qrcode_life_function);
                qrcode_time_out();
                $('#qrcode-status').removeClass('orange');
                $('#qrcode-status').removeClass('lightgreen');
                $('#qrcode-status').addClass('red');
                $('#qrcode-status').html('<i class="fas fa-times-circle"></i>未通過或已經過期</span>');
            }
        }

        function qrcode_reload() {
            get_qrcode();
            if (second == 0) {
                var qrcode_life_function = setInterval(qrcode_life, 1000);
            }
        }

        function get_qrcode() {
            $('#qrcode').html('');
            var request = $.ajax({
                url: "../new_api/account_login_qrcode_create.php"/*?userSid=<?php //echo $_GET['userSid']; ?>"*/,
                method: "GET",
            });
            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                if (arr['success']) {
                    $('#qrcode').qrcode({
                        width: 120,
                        height: 120,
                        render: 'table',
                        text: arr['url']
                    });
                    qrcode_token = arr['token'];
                    second = 300;
                }
                // console.log(arr);
            });
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
            // console.log(isChecked);

        }

        function qrcode_time_out() {
            var request = $.ajax({
                url: "../new_api/account_login_qrcode_timeout.php",
                method: "POST",
                data: {
                    token: qrcode_token
                },
                dataType: "text"
            });
            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                if (arr['success']) {
                    second = 0;
                }
                // console.log(arr);
            });
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
            // console.log(isChecked);
        }

        function qrcode_sstatus_check() {
            var request = $.ajax({
                url: "../new_api/account_login_qrcode_check.php",
                method: "POST",
                data: {
                    token: qrcode_token
                },
                dataType: "text"
            });
            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                if (arr['success'] == 1) {
                    qrcode_status = true;
                    $('#email').val(arr['email']);
                    userName = arr['name'];
                    userId = arr['id'];
                }
                // console.log(arr);
            });
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
            // console.log(isChecked);
        }

        function poster() {
            if (!qrcode_status) {
                var request2 = $.ajax({
                    url: "../new_api/account_reset_get_emailInfo.php",
                    method: "POST",
                    data: {
                        email: $('#email').val()
                    },
                    dataType: "text",
                    async: false
                });
                request2.done(function(dat) {
                    var data = JSON.parse(dat);
                    var arr = [];
                    for (var key in data) {
                        arr[key] = data[key];
                    }
                    if (arr['success']) {
                        userName = arr['name'];
                        userId = arr['id'];
                    } else {
                        alert(arr['message']['ch']);
                    }
                    // console.log(arr);
                });
                request2.fail(function(jqXHR, textStatus) {
                    alert("Request failed: " + textStatus);
                });
                // console.log(isChecked);

            }
            var request = $.ajax({
                url: "../new_api/account_poster_reset_mail.php",
                method: "POST",
                data: {
                    email: $('#email').val(),
                    name: userName,
                    account: userId
                },
                dataType: "text",
                async: false
            });
            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                if (arr['success'] == 1) {
                    alert('密碼重設信已經寄出!!');
                } else {
                    alert(arr['message']['ch']);
                }
                // console.log(arr);
            });
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
            // console.log(isChecked);
        }
    </script>

</body>

</html>