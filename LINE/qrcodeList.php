<?php
//session_start();
require_once('../new_api/config/config.php');

$realUnion = new RealUnion();
$page = !isset($_POST['page']) ? 1 : $_POST['page'];
$num = ($page == 1 ? 0 : (0 + (10 * ($page - 1))));
$file = $realUnion->qrocdeList();
$views_data = array();
for ($i = 1; $i <= 10; $i++) {
    if (isset($file['data'][$num])) {
        array_push($views_data, $file['data'][$num]);
        $num++;
    } else {
        break;
    }
}
$view = 10;

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>實聯制Qrcode管理店家版</title>
    <!-- Bootstrap core CSS -->
    <link href="https://bootstrap.hexschool.com/docs/4.2/dist/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/fontawesome-free-6.0.0-web/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="../admin/css/colorlib/vendor/sweetalert2/dist/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        /*page item*/
        .page_number {
            display: inline-block;

        }

        .page_number nav ul .page-item:first-child .page-link {
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
            border-color: #08192D;
        }

        .page_number nav ul .page-item:last-child .page-link {
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
            border-color: #08192D;
        }

        .page_number nav ul .page-item .page-link {
            background-color: #ffffff;
            color: #aaaaaa;
            border: 1px solid #dddddd;
            width: 40px;
            height: 40px;
            -webkit-box-shadow: 0 1px #aaaaaa;
            box-shadow: 0 1px #aaaaaa;
            border-color: #08192D;
        }

        .page_number nav ul .page-item .page-link:hover {
            background-color: #A64DD6;
            color: #ffffff;
            border: 1px solid #A64DD6;
            -webkit-box-shadow: 0;
            box-shadow: 0;
            height: 39px;
            -webkit-transform: translateY(1px);
            transform: translateY(1px);
            border-color: #08192D;
        }

        .page_number nav ul .page-item .page-link:focus {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .page_number nav ul .page-item .page-link span {
            top: 2px;
        }

        .page_number nav ul .page-item.disabled .page-link {
            background-color: #eeeeee;
        }

        .page_number nav ul .page-item.active .page-link {
            background-color: #9646C2;
            color: #ffffff;
            border: 1px solid #9646C2;
            border-color: #08192D;
        }

        .page_number nav ul .page-item.active .page-link:hover {
            height: 40px;
            -webkit-transform: translateY(0px);
            transform: translateY(0px);
        }

        .page_number {
            font-weight: bold;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="https://bootstrap.hexschool.com/docs/4.2/examples/pricing/pricing.css" rel="stylesheet">
</head>

<body>
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 border-bottom shadow-sm" style="background-color:#08192D;">
        <h5 class="my-0 mr-md-auto text-light font-weight-normal">實聯制Qrcode <small>店家版</small></h5>
        <div class="text-right"><button class="btn btn-light border border-success" onclick="package_download();"><i class="fa-solid fa-box"></i>打包(全數)下載</button></div>
    </div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    </div>

    <div class="container-fluid rows">
        <div class="card-deck mb-4 text-center qrcode" <?php echo $file['num'] == 0 ? 'style="display:none;"' : ''; ?>>
            <?php
            foreach ($views_data as $data) {
            ?>
                <div class="card mb-4 shadow-sm">
                    <div class="card-header" style="background-color: #6bafcc;">
                        <h4 class="my-0 font-weight-normal">#<?php echo $data['id']; ?><?php echo $data['is_use'] == 0 ? '(已停用)' : '' ?></h4>
                    </div>
                    <div class="card-body" style="background-color:#FCFAF2;">
                        <h1 class="card-title pricing-card-title">
                            <div id="qrcode_<?php echo $data['id']; ?>">
                            </div>
                            <div style="display: none;" id="qrcodebg_<?php echo $data['id']; ?>">
                            </div>
                        </h1>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li>場所代碼：<?php echo $data['token']; ?></li>
                            <li>場所地址：<?php echo $data['address']; ?></li>
                            <li>場所名稱：<?php echo $data['name']; ?></li>
                        </ul>
                        <button type="button" class="btn btn-md btn-block btn-primary" onclick="download('<?php echo $data['id']; ?>','<?php echo $data['token']; ?>');">下載</button>
                        <button type="button" class="btn btn-md btn-block btn-primary" onclick="edit('<?php echo $data['id']; ?>');">編輯</button>
                        <button type="button" class="btn btn-md btn-block btn-warning" onclick="stop('<?php echo $data['id']; ?>','<?php echo $data['is_use']; ?>');"><?php echo $data['is_use'] == 0 ? '啟用' : '停用' ?></button>
                        <button type="button" class="btn btn-md btn-block btn-light text-danger border border-danger" onclick="deleter('<?php echo $data['id']; ?>');">刪除</button>
                    </div>
                </div>
            <?php
            }
            ?>
            <?php
            foreach ($file['data'] as $data) {
            ?>
                <div class="card mb-4 shadow-sm downloadqrcode" style="display: none;">
                    <div class="card-body">
                        <h1 class="card-title pricing-card-title">
                            <div style="display: none;" id="qrcodedownload_<?php echo $data['id']; ?>">
                            </div>
                        </h1>
                    </div>
                </div>
            <?php
            }
            ?>

        </div>
        <div class="card mb-12 text-center shadow-sm" <?php echo $file['num'] != 0 ? 'style="display:none;"' : ''; ?>>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">找不到資料。</h1>
            </div>
        </div>
        <footer class="pt-4 my-md-5 pt-md-5 border-top">
            <div class="row">
                <div class="col-12 col-md col-lg-12 col-md-12 text-center text-dark">
                    <h4>共<?php echo $file['num']; ?>筆資料</h4>
                    <div class="page_number">
                        <nav aria-label="Page navigation">
                            <ul class="pagination">
                                <?php
                                if ($file['num'] <= 10 || $page == 1) {
                                ?>
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" aria-label="PreviousStart">
                                            <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                                        </a>
                                    </li>
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                                        </a>
                                    </li>
                                <?php
                                } else {
                                ?>
                                    <li class="page-item ">
                                        <a class="page-link" href="javascript:setpg('1')" aria-label="PreviousStart">
                                            <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                                        </a>
                                    </li>
                                    <li class="page-item ">
                                        <a class="page-link" href="javascript:setpg('<?php echo $page - 1; ?>')" aria-label="Previous">
                                            <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
                                <?php
                                if ($file['num'] <= $view) {
                                ?>
                                    <li class="page-item active"><a class="page-link " id="page1" href="#">1</a></li>
                                    <?php
                                } else if ($file['num'] <= ($view * 2)) {
                                    if ($page == 1) {
                                    ?>
                                        <li class="page-item active"><a class="page-link " id="page1" href="javascript:setpg(1)">1</a></li>
                                        <li class="page-item"><a class="page-link" id="page2" href="javascript:setpg('2')">2</a></li>
                                    <?php
                                    } else {
                                    ?>
                                        <li class="page-item "><a class="page-link " id="page1" href="javascript:setpg(1)">1</a></li>
                                        <li class="page-item active"><a class="page-link" id="page2" href="javascript:setpg('2')">2</a></li>
                                    <?php
                                    }
                                } else if ($file['num'] > ($view * 2)) {
                                    if ($file['num'] >= ($view * 2) && $page == 1) {
                                    ?>
                                        <li class="page-item active"><a class="page-link " id="page1" href="javascript:setpg(1)">1</a></li>
                                        <li class="page-item"><a class="page-link" id="page2" href="javascript:setpg('2')">2</a></li>
                                        <li class="page-item"><a class="page-link" id="page3" href="javascript:setpg('3')">3</a></li>
                                    <?php
                                    } else if ($page == 2) {
                                    ?>
                                        <li class="page-item "><a class="page-link " id="page1" href="javascript:setpg(1)">1</a></li>
                                        <li class="page-item active"><a class="page-link" id="page2" href="javascript:setpg('2')">2</a></li>
                                        <li class="page-item"><a class="page-link" id="page3" href="javascript:setpg('3')">3</a></li>
                                    <?php
                                    } else if (($page * $view) < $file['num'] && $page >= 3 && ($page * $view) <= $file['num']) {
                                    ?>
                                        <li class="page-item "><a class="page-link " id="page1" href="javascript:setpg('<?php echo $page - 1; ?>')"><?php echo $page - 1; ?></a></li>
                                        <li class="page-item active"><a class="page-link" id="page2" href="javascript:setpg('<?php echo $page; ?>')"><?php echo $page; ?></a></li>
                                        <li class="page-item"><a class="page-link" id="page3" href="javascript:setpg('<?php echo $page + 1; ?>')"><?php echo $page + 1; ?></a></li>
                                    <?php
                                    } else if ((($page + 1) * $view) >= $file['num']) {
                                    ?>
                                        <li class="page-item "><a class="page-link " id="page1" href="javascript:setpg('<?php echo $page - 2; ?>')"><?php echo $page - 2; ?></a></li>
                                        <li class="page-item "><a class="page-link " id="page1" href="javascript:setpg('<?php echo $page - 1; ?>')"><?php echo $page - 1; ?></a></li>
                                        <li class="page-item active"><a class="page-link" id="page2" href="javascript:setpg('<?php echo $page; ?>')"><?php echo $page; ?></a></li>
                                    <?php
                                    }
                                }

                                if (floor($file['num'] / ($view) + 1 == $page)) {
                                    ?>
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                                        </a>
                                    </li>
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" aria-label="NextEnd">
                                            <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                                        </a>
                                    </li>
                                <?php
                                }
                                if (($page * $view) >= $file['num']) {
                                ?>
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                                        </a>
                                    </li>
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" aria-label="NextEnd">
                                            <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                                        </a>
                                    </li>
                                <?php
                                } else {
                                ?>
                                    <li class="page-item">
                                        <a class="page-link" href="javascript:setpg('<?php echo $page + 1; ?>')" aria-label="Next">
                                            <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                                        </a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="javascript:setpg('<?php echo ($page * $view) <= $file['num'] && ($file['num'] % $view) != 0 ? floor($file['num'] / $view) + 1 : floor($file['num'] / $view); ?>')" aria-label="NextEnd">
                                            <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
                            </ul>
                        </nav>
                    </div>
                    <form method="post" style="display: none;" action="/horn/lineservice/LINE/qrcodeList.php" id="pageChange">
                        <input type="number" id="page" name="page">
                    </form>
                    <div class="modal" tabindex="-1" id="edit_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">編輯資料</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group row">
                                        <label for="name" class="mb-1 col-sm-3 col-form-label">店家名稱</label>
                                        <div class="col-sm-9"> <input type="text" class="form-control mb-2" id="name" name="name"></div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="address" class="mb-1 col-sm-3 col-form-label">店家地址</label>
                                        <div class="col-sm-9"> <input type="text" class="form-control mb-2" id="address" name="address"></div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" onclick="javascript:setId = 0;" data-bs-dismiss="modal">關閉</button>
                                    <button type="button" class="btn btn-primary" onclick="save();">儲存</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md text-center">
                    <img class="mb-2" src="../CSS/gold_big2.png" alt="" width="24" height="32">
                    <small class="d-block mb-3 text-muted">&copy; <?php echo date('Y', time()); ?></small>
                </div>
            </div>
        </footer>
    </div>
    <script src="../css/vendor/line/jquery/jquery-3.2.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="../css/vendor/line/jquery-qrcode-master/jquery.qrcode.min.js"></script>
    <script src="../css/vendor/line/jszip-master/dist/jszip.min.js"></script>
    <script src="../css/vendor/line/jszip-master/vendor/FileSaver.js"></script>
    <script src="../admin/css/colorlib/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script>
        var jzip = new JSZip();
        var setId = 0;
        $(document).ready(function() {
            <?php
            foreach ($views_data as $data) {
            ?>
                $('.qrcode #qrcode_<?php echo $data['id']; ?>').qrcode({
                    width: 120,
                    height: 120,
                    text: <?php echo '\'http://' . $_SERVER['HTTP_HOST'] . '/horn/lineservice/LINE/real_union.php?token=' . $data['token'] . '\''; ?>
                });
                $('.qrcode #qrcodebg_<?php echo $data['id']; ?>').qrcode({
                    width: 960,
                    height: 960,
                    text: <?php echo '\'http://' . $_SERVER['HTTP_HOST'] . '/horn/lineservice/LINE/real_union.php?token=' . $data['token'] . '\''; ?>
                });
            <?php
            }
            ?>
            //qrcode_onload();
        });

        function download(id, filename) {
            var link = document.createElement('a');
            var file = document.getElementById('qrcodebg_' + id);
            var canvas = file.getElementsByTagName('canvas');
            link.download = filename + '.png';
            link.href = canvas[0].toDataURL().replace("image/png", "image/octet-stream");
            link.click();

            var img = jzip.folder("images");
            img.file("smile.png", link.href.replace(/^data:image\/(png|jpg|octet-stream);base64,/, ''), {
                base64: true
            });
            img.file("smile2.png", link.href.replace(/^data:image\/(png|jpg|octet-stream);base64,/, ''), {
                base64: true
            });
            //jzip.file("smile3.jpeg", base64_val);
            jzip.generateAsync({
                    type: "blob"
                })
                .then(function(content) {
                    // see FileSaver.js
                    saveAs(content, "example.zip");
                });
        }

        function package_download() {
            var img = jzip.folder("images");
            <?php
            foreach ($file['data'] as $data) {
            ?>
                $('.downloadqrcode #qrcodedownload_<?php echo $data['id']; ?>').qrcode({
                    width: 960,
                    height: 960,
                    text: <?php echo '\'http://' . $_SERVER['HTTP_HOST'] . '/horn/lineservice/LINE/real_union.php?token=' . $data['token'] . '\''; ?>
                });
                var link_<?php echo $data['id']; ?> = document.createElement('a');
                var file_<?php echo $data['id']; ?> = document.getElementById('qrcodedownload_<?php echo $data['id']; ?>');
                var canvas_<?php echo $data['id']; ?> = file_<?php echo $data['id']; ?>.getElementsByTagName('canvas');
                link_<?php echo $data['id']; ?>.download = '_<?php echo $data['token']; ?>' + '.png';
                link_<?php echo $data['id']; ?>.href = canvas_<?php echo $data['id']; ?>[0].toDataURL().replace("image/png", "image/octet-stream");
                img.file("<?php echo $data['token']; ?>.png", link_<?php echo $data['id']; ?>.href.replace(/^data:image\/(png|jpg|octet-stream);base64,/, ''), {
                    base64: true
                });
            <?php
            }
            ?>
            //jzip.file("smile3.jpeg", base64_val);
            jzip.generateAsync({
                    type: "blob"
                })
                .then(function(content) {
                    // see FileSaver.js
                    saveAs(content, "<?php echo md5($_SESSION['username'] . date('YMDHis', time())); ?>.zip");
                });

        }

        function setpg(page) {
            $('#pageChange #page').val(page);
            $('#pageChange').submit();
        }

        function edit(id) {
            var request = $.ajax({
                url: "../new_api/real_union_qrcode_get.php",
                method: "POST",
                data: {
                    id: id
                },
                dataType: "text"
            });
            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                if (arr['success']) {
                    $('#name').val(arr['name']);
                    $('#address').val(arr['address']);
                    setId = id;
                    $('#edit_modal').modal('show');
                } else {
                    Swal.fire(
                        "錯誤", //標題 
                        arr['message']['ch'], //訊息內容(可省略)
                        "error" //圖示(可省略) success/info/warning/error/question
                        //圖示範例：https://sweetalert2.github.io/#icons
                    );
                }
                // console.log(arr);
            });
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
            // console.log(isChecked);

        }

        function save() {
            var request = $.ajax({
                url: "../new_api/real_union_qrcode_edit.php",
                method: "POST",
                data: {
                    id: setId,
                    name: $('#name').val(),
                    address: $('#address').val()
                },
                dataType: "text"
            });
            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                if (arr['success']) {
                    setpg('<?php echo $page; ?>');
                } else {
                    Swal.fire(
                        "錯誤", //標題 
                        arr['message']['ch'], //訊息內容(可省略)
                        "error" //圖示(可省略) success/info/warning/error/question
                        //圖示範例：https://sweetalert2.github.io/#icons
                    );
                }
                // console.log(arr);
            });
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
            // console.log(isChecked);
        }

        function stop(id, status) {
            Swal.fire({
                title: status == 0 ? '確認啟用項目' : '確認停用項目',
                html: status == 0 ? '確認啟用QrCode' : '確認停用QrCode',
                showCancelButton: true,
                icon: 'question'
            }).then(function(result) {
                if (result.value) {
                    status == 0 ? return_action(id) : stop_action(id);
                }
            });
        }

        function return_action(id) {
            var request = $.ajax({
                url: "../new_api/real_union_qrcode_open.php",
                method: "POST",
                data: {
                    id: id
                },
                dataType: "text"
            });
            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                if (arr['success']) {
                    setpg('<?php echo $page; ?>');
                } else {
                    Swal.fire(
                        "錯誤", //標題 
                        arr['message']['ch'], //訊息內容(可省略)
                        "error" //圖示(可省略) success/info/warning/error/question
                        //圖示範例：https://sweetalert2.github.io/#icons
                    );
                }
                // console.log(arr);
            });
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
            // console.log(isChecked);
        }

        function stop_action(id) {
            var request = $.ajax({
                url: "../new_api/real_union_qrcode_stop.php",
                method: "POST",
                data: {
                    id: id
                },
                dataType: "text"
            });
            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                if (arr['success']) {
                    setpg('<?php echo $page; ?>');
                } else {
                    Swal.fire(
                        "錯誤", //標題 
                        arr['message']['ch'], //訊息內容(可省略)
                        "error" //圖示(可省略) success/info/warning/error/question
                        //圖示範例：https://sweetalert2.github.io/#icons
                    );
                }
                // console.log(arr);
            });
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
            // console.log(isChecked);
        }

        function deleter(id) {
            Swal.fire({
                title: '確認刪除項目',
                html: '確認刪除QrCode?此操作將無法復原。',
                showCancelButton: true,
                icon: 'warning'
            }).then(function(result) {
                if (result.value) {
                    delete_action(id);
                }
            });
        }

        function delete_action(id) {
            var request = $.ajax({
                url: "../new_api/real_union_qrcode_delete.php",
                method: "POST",
                data: {
                    id: id
                },
                dataType: "text"
            });
            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                if (arr['success']) {
                    setpg('<?php echo $page; ?>');
                } else {
                    Swal.fire(
                        "錯誤", //標題 
                        arr['message']['ch'], //訊息內容(可省略)
                        "error" //圖示(可省略) success/info/warning/error/question
                        //圖示範例：https://sweetalert2.github.io/#icons
                    );
                }
                // console.log(arr);
            });
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
            // console.log(isChecked);
        }
    </script>
</body>

</html>