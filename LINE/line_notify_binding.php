<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/fontawesome-free-6.0.0-web/css/all.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/vendor/line/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/line/util.css">
    <link rel="stylesheet" type="text/css" href="../css/line/main.css">
    <style type="text/css">
        .logo {
            text-align: center;
            margin-bottom: 20px;
            padding-left: 15px;
        }

        .text {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 24px;
            color: #04cecf;
            font-weight: 800;
            margin-bottom: 5px;
            text-align: center;
            margin-top: 5px;
        }

        .color {
            font-weight: 500;
            color: #04cecf;
        }

        .form2 {
            font-size: 17px;
            font-weight: 800;
            color: #F26060;
        }

        #checkbox {
            height: 22px;
            width: 22px;
            vertical-align: text-bottom;
        }

        .box {
            width: 100%;
            background: #eee;
            border: 1px solid transparent;
            margin-bottom: 15px;
            margin-top: auto;
            margin-right: auto;
            margin-left: auto;
        }

        .border-img {
            -moz-border-image: -moz-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            -webkit-border-image: -webkit-linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            border-image: linear-gradient(top left, #04cecf 0%, #ffffff 100%);
            border-image-slice: 1;
        }

        .select {
            border: 1px solid #F3F3F3;
        }

        .form-control {
            border: 1px solid #CCCCCC;
            width: 100%;
            font-size: 16px;
            color: #999;
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 5px;
            margin-bottom: 30px;
            display: inline;
        }

        .form-control:hover {
            border: 1px solid #CCCCCC;
        }

        .green {
            color: #008C9B;
        }

        .red {
            color: #F33030;
        }

        .record-text {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            padding-top: 5px;
            padding-bottom: 5px;
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: #DBDBDB;
            text-align: left;
            margin-top: 3px;
            margin-bottom: 3px;
            font-weight: 500;
            color: #666;
        }

        .record-text-1 {
            font-family: Arial, Helvetica, sans-serif, "微軟正黑體", "蘋方-繁";
            font-size: 18px;
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: left;
            margin-top: 3px;
            margin-bottom: 8px;
            font-weight: 500;
            color: #666;
            display: inline;
        }

        .line {
            border-top-width: 1px;
            border-top-style: dotted;
            border-top-color: #999999;
            margin-top: 17px;
            margin-bottom: 17px;
        }

        .form-control {
            border: 1px solid #CCCCCC;
            width: 100%;
            font-size: 16px;
            color: #999;
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 5px;
            margin-bottom: 12px;
        }

        .form-control:hover {
            border: 1px solid #CCCCCC;
            background-color: #F9F9F9;
        }

        ::-ms-input-placeholder {
            /* Microsoft Edge */
            color: red;
        }

        .wrap-docter {
            width: 100%;
        }
    </style>

    <!--===============================================================================================-->
</head>

<body>

    <!--===============================================================================================-->
    <script src="../css/vendor/line/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/bootstrap/js/popper.js"></script>
    <script src="../css/vendor/line/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/daterangepicker/moment.min.js"></script>
    <script src="../css/vendor/line/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="../css/vendor/line/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="../js/line/main.js"></script>
    <script>
        window.onload = function() {
            oAuth2();
        };

        function oAuth2() {
            var URL = 'https://notify-bot.line.me/oauth/authorize?';
            URL += 'response_type=code';
            URL += '&client_id=WfPqvHcz3GJ0WYfJtvCHLs';
            URL += '&redirect_uri=https://2ef7-2001-b011-e605-32ee-c56f-17b7-a817-6569.ap.ngrok.io/horn/lineservice/LINE/lineNotifyCallback.php';
            URL += '&scope=notify';
            URL += '&state=<?php echo $_GET['userSid']; ?>;';
            URL += '&response_mode=form_post';
            window.location.href = URL;
        }
    </script>

</body>

</html>