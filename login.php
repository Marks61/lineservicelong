<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>帳號登入</title>
  <!--===============================================================================================-->
  <script src="	https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="	https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
  <script src="css/vendor/select2.min.js"></script>
  <script src="css/vendor/tilt.jquery.min.js"></script>
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/vendor/select2.min.css">
  <!--===============================================================================================-->
  <style type="text/css">
    body {
      color: #000;
      overflow-x: hidden;
      height: 100%;
      background-repeat: no-repeat;
    }

    .card0 {
      box-shadow: 0 4px 8px 0 #cecece;
      border-radius: 0;
    }

    .card2 {
      margin: 50px 40px;
    }

    .image {
      width: 500px;
      height: 500px;
    }

    .border-line {
      border-right: 1px solid #EEEEEE;
    }

    .text-sm {
      font-size: 18px !important;
    }

    ::placeholder {
      color: #BDBDBD;
      opacity: 1;
      font-weight: 300;
    }

    :-ms-input-placeholder {
      color: #BDBDBD;
      font-weight: 300;
    }

    ::-ms-input-placeholder {
      color: #BDBDBD;
      font-weight: 300;
    }

    input,
    textarea {
      padding: 10px 12px;
      border: 1px solid lightgrey;
      border-radius: 2px;
      margin-bottom: 5px;
      margin-top: 2px;
      width: 100%;
      box-sizing: border-box;
      color: #1abac6;
      font-size: 18px;
      letter-spacing: 1px;
    }

    input:focus,
    textarea:focus {
      -moz-box-shadow: none !important;
      -webkit-box-shadow: none !important;
      box-shadow: none !important;
      border: 1px solid #1abac6;
      outline-width: 0;
    }

    button:focus {
      -moz-box-shadow: none !important;
      -webkit-box-shadow: none !important;
      box-shadow: none !important;
      outline-width: 0;
    }

    a {
      color: #1abac6;
      cursor: pointer;
    }

    a:hover {
      color: #43979D;
      cursor: pointer;
    }

    .tit {
      font-size: 36px;
      text-align: center;
      font-family: Arial, Helvetica, sans-serif, "微軟正黑體";
      font-weight: normal;
      color: #666666;
      padding-bottom: 35px;
      letter-spacing: 0.1em;
    }

    .btn-blue {
      background-color: #1abac6;
      width: 150px;
      color: #fff;
      border-radius: 2px;
    }

    .btn-blue:hover {
      background-color: #BDBDBD;
      cursor: pointer;
    }

    .bg-blue {
      color: #fff;
      background-color: #1abac6;
    }

    @media screen and (max-width: 991px) {

      .image {
        width: 350px;
        height: 350px;
      }

      .border-line {
        border-right: none;
      }

      .card2 {
        border-top: 1px solid #EEEEEE !important;
        margin: 0 15px;
      }

      .card0 {
        margin: 0 25px;
      }

    }
  </style>
</head>

<body>
  <div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
    <div class="card card0 border-0">
      <div class="row d-flex">
        <div class="col-lg-6">
          <div class="card1 pb-5">
            <br />
            <br />
            <div class="row px-3 justify-content-center mt-4 mb-5 border-line">
              <div class="login100-pic js-tilt" data-tilt>
                <img src="css/vendor/banner-hospital.png" width="500" height="500" class="image">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6" id="login_form">
          <div class="card2 card border-0 px-4 py-5">
            <div class="mb-0 tit">帳號登入</div>
            <div class="row px-3"></div>
            <div class="row px-3">
              <label class="mb-1">
                <h6 class="mb-0 text-sm">帳號</h6>
              </label>
              <input class="mb-4" type="text" id="account" placeholder="請輸入帳號">
            </div>
            <div class="row px-3">
              <label class="mb-1">
                <h6 class="mb-0 text-sm">密碼</h6>
              </label>
              <input type="password" id="password" placeholder="請輸入密碼">
            </div>
            <div class="row px-3 mb-4">
              <div class="custom-control custom-checkbox custom-control-inline d-none">
                <input id="chk1" type="checkbox" name="chk" class="custom-control-input">
                <label for="chk1" class="custom-control-label text-sm">Remember me</label>
              </div>
              <label class="text-sm text-danger" id="check_login"></label>
              <label class="text-sm text-success" id="pwd_success"></label>
              <a href="#" onclick="return false;" class="ml-auto mb-0 text-sm" id="forgot_pwd">忘記密碼</a>
            </div>
            <div class="row mb-3 px-3" id="btn">
              <button type="submit" class="btn btn-blue text-sm" id="login_btn">登入</button>
            </div>
          </div>
        </div>
        <div class="col-lg-6 d-none" id="forgot_pwd_form">
          <div class="card2 card border-0 px-4 py-5">
            <div class="mb-0 tit">忘記密碼</div>
            <div class="row px-3"></div>
            <div class="row px-3">
              <label class="mb-1">
                <h6 class="mb-0 text-sm">帳號</h6>
              </label>
              <input class="mb-4" type="text" id="forgot_account" placeholder="請輸入帳號">
            </div>
            <div class="row px-3">
              <label class="mb-1">
                <h6 class="mb-0 text-sm">E-Mail</h6>
              </label>
              <input type="email" id="forgot_email" placeholder="請輸入註冊的信箱">
            </div>
            <div class="row px-3 mb-4">
              <label class="text-sm text-danger" id="check_info"></label>

              <a href="" onclick="return false;" class="ml-auto mb-0 text-sm" id="login">我想起來了！</a>
            </div>
            <div class="row mb-3 px-3">
              <button type="submit" class="btn btn-blue text-sm" id="pwd_btn">傳送新密碼</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>

  <script>
    $('.js-tilt').tilt({
      scale: 1.1
    });

    $('#forgot_pwd').click(function() {
      $('#login_form').addClass('d-none');
      $('#forgot_pwd_form').removeClass('d-none');
    });

    $('#login').click(function() {
      $('#login_form').removeClass('d-none');
      $('#forgot_pwd_form').addClass('d-none');
    });

    $('#login_btn').click(function() {
      var account = $('#account').val();
      var password = $('#password').val();

      var request = $.ajax({
        url: "./new_api/horn_login.php",
        method: "POST",
        data: {
          account: account,
          password: password
        },
        dataType: "text"
      });

      request.done(function(dat) {
        var data = JSON.parse(dat);
        var arr = [];
        for (var key in data) {
          arr[key] = data[key];
        }
        // console.log(arr);
        if (!arr['success']) {
          $('#check_login').html(arr['message']);
        } else {
          location.href = arr['location'];
        }
      });

      request.fail(function(jqXHR, textStatus) {
        alert("Requset failed: " + textStatus);
      });
    });

    $('#pwd_btn').click(function() {
      var account = $('#forgot_account').val();
      var email = $('#forgot_email').val();

      var request = $.ajax({
        url: "./new_api/horn_forgot_password.php",
        method: "POST",
        data: {
          account: account,
          email: email
        },
        dataType: "text"
      });

      request.done(function(dat) {
        var data = JSON.parse(dat);
        var arr = [];
        for (var key in data) {
          arr[key] = data[key];
        }
        // console.log(arr);
        if (arr['message']) {
          $('#login').click();
          $('#pwd_success').html(arr['message']);
        } else {
          $('#check_info').html(arr['message']);
        }
      });

      request.fail(function(jqXHR, textStatus) {
        alert("Requset failed: " + textStatus);
      });
    });
  </script>

</body>

</html>