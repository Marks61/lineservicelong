<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="https://www.hncare.cloud/horncloud/new_web_api/">API 線上文件</a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">帳戶
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="account_login.php">會員登入</a></li>
						<li><a href="account_register.php">會員註冊</a></li>
						<li><a href="account_homepage_info.php">主頁資訊</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">密碼</li>
						<li><a href="account_forget_password.php">忘記密碼</a></li>
						<li><a href="account_update_password.php">更新密碼</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">個人</li>
						<li><a href="account_search_personal_info.php">查詢會員資訊</a></li>
						<li><a href="account_update_personal_info.php">更新會員資訊</a></li>
						<li><a href="account_update_img.php">更新大頭貼</a></li>
						<li><a href="account_update_privacy_policy.php">更新隱私權條款</a></li>
						<li><a href="account_update_personal_bp_standard.php">上傳 / 更新血壓警戒值</a></li>
						<li><a href="account_update_personal_bs_standard.php">上傳 / 更新血糖警戒值</a></li>
						<li><a href="account_update_personal_bt_standard.php">上傳 / 更新體溫警戒值</a></li>
						<li><a href="account_update_personal_spo2_standard.php">上傳 / 更新血氧警戒值</a></li>
						<li><a href="account_insert_blood_sugar_measurement_reminder_time.php">上傳血糖測量提醒時間</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">照護</li>
						<li><a href="medical_update_login_and_logout_record.php">照護登入</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">VIP
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li class="dropdown-header">推播</li>
						<li><a href="account_update_token.php">更新Token</a></li>
						<li><a href="account_search_notification_record.php">查詢推播</a></li>
						<li><a href="account_update_notification_read_status.php">更新推播</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">諮詢</li>
						<li><a href="account_insert_consultation.php">新增諮詢</a></li>
						<li><a href="account_search_consultation_record.php">查詢諮詢</a></li>
						<li><a href="medical_insert_consultation.php">回覆諮詢</a></li>
						<li><a href="medical_delete_consultation.php">刪除諮詢</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">康總
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">

						<li><a href="health_update_sleep.php">睡眠</a></li>
						<li><a href="health_update_fatigue.php">疲勞度</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">風險指數</li>
						<li><a href="health_search_BHI_measurement_times.php">查詢剩餘次數</a></li>
						<li><a href="health_search_body_health_index.php">查詢風險指數</a></li>
						<li><a href="health_update_BHI_measurement_times.php">上傳 / 更新測量次數</a></li>
						<li><a href="health_update_body_health_index.php">上傳 / 更新風險指數</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">健康指數</li>
						<li><a href="health_insert_heartbeat_warning.php">新增心搏規律警訊</a></li>
						<li><a href="health_insert_heart_health_warning.php">新增心臟健康警訊</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">心率</li>
						<li><a href="health_update_heart_rate.php">全天心率</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">血氧</li>
						<li><a href="health_update_spo2_24h.php">全天血氧</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">脈波</li>
						<li><a href="health_update_blood_pressure_4.php">全天脈波</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">即時心率</li>
						<li><a href="health_search_heart_rate_real_time.php">查詢即時心率</a></li>
						<li><a href="health_update_heart_rate_real_time.php">上傳 / 更新即時心率</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">活動</li>
						<li><a href="sport_update_all_day_calories.php">上傳 / 更新整天卡路里</a></li>
						<li><a href="sport_update_all_day_steps.php">上傳 / 更新整天步數</a></li>
						<li><a href="sport_update_total_activity_data.php">上傳 / 更新活動資料</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">運動</li>
						<li><a href="sport_insert_sports_record.php">新增運動</a></li>
						<li><a href="sport_search_sports_record.php">查詢運動紀錄</a></li>
						<li class="divider"></li>

						<li><a href="account_search_version.php">取得版本資料</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">創準
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="estab_zenith_binding_device.php">綁定設備</a></li>
						<li><a href="estab_zenith_unbind_device.php">解綁設備</a></li>
						<li><a href="estab_zenith_insert_emerency_btn_warning.php">緊急鈕警訊</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">威聯通
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li class="dropdown-header">動作</li>
						<li><a href="video_call_make_a_video_call.php">( 手機 ⟹ 網頁 ) 撥打視訊通話</a></li>
						<li><a href="video_call_interrupt_dial.php">( 手機 ⟹ 網頁 ) 中斷撥號</a></li>
						<li><a href="video_call_call_ended.php">( 手機 ⟺ 網頁 ) 結束通話</a></li>
						<li><a href="video_call_deny_or_allow_calls.php">( 手機 ⟸ 網頁 ) 拒絕或允許通話</a></li>
						<li><a href="video_call_get_video_call_fcm_data.php">( 手機 ⟺ 手機 ) 發送推播</a></li>
						<li><a href="video_call_determine_the_call_status.php">( 手機 ⟺ 手機 ) 更新資料表</a></li>
						<li class="divider"></li>
						<li class="dropdown-header">紀錄</li>
						<li><a href="account_search_call_record.php">查詢通話紀錄</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">全家寶
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li class="dropdown-header">ECG</li>
						<li><a href="health_search_inventec_ecg.php">查詢ECG</a></li>
						<li><a href="health_insert_inventec_ecg.php">上傳ECG</a></li>
						<li class="divider"></li>
						<li class="dropdown-header">尿酸</li>
						<li><a href="health_search_uric_acid.php">查詢尿酸</a></li>
						<li><a href="health_update_uric_acid.php">上傳 / 更新尿酸</a></li>
						<li class="divider"></li>
						<li class="dropdown-header">膽固醇</li>
						<li><a href="health_search_total_cholesterol.php">查詢膽固醇</a></li>
						<li><a href="health_update_total_cholesterol.php">上傳 / 更新膽固醇</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">共用
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">

						<li class="dropdown-header">血壓</li>
						<li><a href="health_search_blood_pressure.php">查詢血壓</a></li>
						<li><a href="health_update_blood_pressure.php">上傳 / 更新血壓</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">脈波</li>
						<li><a href="health_search_blood_pressure_3.php">查詢脈波</a></li>
						<li><a href="health_update_blood_pressure_3.php">上傳 / 更新脈波</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">居服血壓</li>
						<li><a href="health_update_blood_pressure_2.php">上傳 / 更新居服血壓</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">血糖</li>
						<li><a href="health_search_blood_sugar.php">查詢血糖</a></li>
						<li><a href="health_update_blood_sugar.php">上傳 / 更新血糖</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">血氧</li>
						<li><a href="health_search_spo2.php">查詢血氧</a></li>
						<li><a href="health_update_spo2.php">上傳 / 更新血氧</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">體溫</li>
						<li><a href="health_search_body_temperature.php">查詢體溫</a></li>
						<li><a href="health_update_body_temperature.php">上傳 / 更新體溫</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">即時體溫</li>
						<li><a href="health_search_body_temperature_real_time.php">查詢即時體溫</a></li>
						<li><a href="health_update_body_temperature_real_time.php">上傳 / 更新即時體溫</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">警訊</li>
						<li><a href="health_insert_alarm_mobile_warning.php">新增即時警訊</a></li>

					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">生理
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">

						<li class="dropdown-header">體重</li>
						<li><a href="medical_search_weight.php">查看體重</a></li>
						<!-- <li><a href="health_update_weight.php">上傳 / 更新體重</a></li> -->
						<li class="divider"></li>

						<li class="dropdown-header">異常</li>
						<li><a href="medical_search_alarm.php">查看異常警訊</a></li>
						<li><a href="medical_insert_alarm.php">處置異常警訊</a></li>
						<li><a href="medical_update_alarm_mark.php">追蹤異常警訊</a></li>

					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">西醫
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">

						<li><a href="medicine_search_western_medicines.php">查詢藥品清單</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">藥包</li>
						<li><a href="medicine_delete_western_medicine_package.php">刪除藥包</a></li>
						<li><a href="medicine_insert_western_medicine_package.php">新增藥包</a></li>
						<li><a href="medicine_update_western_medicine_package.php">更新藥包</a></li>
						<li><a href="medicine_search_western_medicine_package.php">查詢藥包</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">藥品</li>
						<li><a href="medicine_update_western_medicine_package_content.php">上傳 / 更新藥包內藥品</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">服藥時間</li>
						<li><a href="medicine_update_western_medicine_package_taking_time.php">上傳 / 更新服藥時間</a></li>
						<li><a href="medicine_search_western_medicine_package_taking_time.php">查詢服藥時間</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">交互作用</li>
						<li><a href="medicine_search_medicine_interaction.php">西醫 ⟺ 中醫</a></li>

					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">中醫
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">

						<li class="dropdown-header">中國醫藥大學</li>
						<li><a href="medicine_search_chu_chinese_medicines.php">查詢藥品清單</a></li>
						<li class="divider"></li>
						<li class="dropdown-header">健保</li>
						<li><a href="medicine_search_nhi_chinese_medicines.php">查詢藥品清單</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">藥包</li>
						<li><a href="medicine_delete_chinese_medicine_package.php">刪除藥包</a></li>
						<li><a href="medicine_insert_chinese_medicine_package.php">新增藥包</a></li>
						<li><a href="medicine_update_chinese_medicine_package.php">更新藥包</a></li>
						<li><a href="medicine_search_chinese_medicine_package.php">查詢藥包</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">藥品</li>
						<li><a href="medicine_update_chinese_medicine_package_content.php">上傳 / 更新藥包內藥品</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">服藥時間</li>
						<li><a href="medicine_update_chinese_medicine_package_taking_time.php">上傳 / 更新服藥時間</a></li>
						<li><a href="medicine_search_chinese_medicine_package_taking_time.php">查詢服藥時間</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">交互作用</li>
						<li><a href="medicine_search_medicine_interaction.php">西醫 ⟺ 中醫</a></li>

					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">紀錄
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">

						<li class="dropdown-header">飲食</li>
						<li><a href="records_update_diet_record.php">上傳 / 更新飲食紀錄</a></li>
						<li><a href="records_search_diet_record.php">查詢飲食紀錄</a></li>
						<li><a href="medical_insert_diet_record.php">處置飲食紀錄</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">照護</li>
						<li><a href="records_search_body_parts.php">查詢身體部位</a></li>
						<li><a href="records_search_types_of_pain_in_body_parts.php">查詢疼痛類型</a></li>
						<li><a href="records_update_care_record.php">上傳 / 更新照護紀錄</a></li>
						<li><a href="records_search_care_record.php">查詢照護紀錄</a></li>
						<li><a href="medical_insert_care_record.php">處置照護紀錄</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">飲水</li>
						<li><a href="records_update_drinking_water.php">上傳 / 更新飲水紀錄</a></li>
						<li><a href="records_search_drinking_water.php">查詢飲水紀錄</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">排尿</li>
						<li><a href="records_update_urine_output.php">上傳 / 更新排尿紀錄</a></li>
						<li><a href="records_search_urine_output.php">查詢排尿紀錄</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">照護諮詢衛教紀錄</li>
						<li><a href="medical_update_carer_log.php">上傳 / 更新照護諮詢衛教紀錄</a></li>
						<li><a href="medical_search_carer_log.php">查詢照護諮詢衛教紀錄</a></li>
						<li><a href="medical_delete_carer_log.php">刪除照護諮詢衛教紀錄</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">值班服務排程</li>
						<li><a href="medical_update_reservation_remind.php">上傳 / 更新值班服務排程</a></li>
						<li><a href="medical_search_reservation_remind.php">查詢值班服務排程</a></li>
						<li><a href="medical_delete_reservation_remind.php">刪除值班服務排程</a></li>
						<li><a href="medical_insert_reservation_remind.php">處置值班服務排程</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">領藥紀錄</li>
						<li><a href="medical_update_drug_response.php">上傳 / 更新領藥紀錄</a></li>
						<li><a href="medical_search_drug_response.php">查詢領藥紀錄</a></li>
						<li><a href="medical_delete_drug_response.php">刪除領藥紀錄</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">聽診器紀錄</li>
						<li><a href="records_update_stethoscope.php">上傳 / 更新聽診器紀錄</a></li>
						<li><a href="records_search_stethoscope.php">查詢聽診器紀錄</a></li>

					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">居服
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">

						<li class="dropdown-header">自費項目</li>
						<li><a href="medical_search_longterm_care_items.php">查詢自費項目</a></li>
						<li class="divider"></li>

						<li class="dropdown-header">打卡</li>
						<li><a href="medical_update_attendance_record.php">上傳打卡紀錄</a></li>
						<li><a href="medical_search_attendance_record.php">查詢打卡紀錄</a></li>
					</ul>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">視訊門診
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="longcare_order_outpatient.php">新增門診預約</a></li>
						<li><a href="medical_search_opd_list.php">醫生查詢門診名單</a></li>
						<li><a href="account_search_opd_list.php">會員查詢門診名單</a></li>
						<li><a href="get_user_views_list.php">會員查詢視訊門診預約歷史</a></li>
						<li><a href="views_get_member_history_data.php">霍恩健康管理中心實名驗證</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Line服務功能
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">

						<li class="dropdown-header">Line服務功能</li>
						<li><a href="line_personal_family_list.php">LINE 家人資料列表</a></li>
						<li><a href="line_personal_family_binding_unlock.php">LINE 解除家人綁定</a></li>
						<li class="divider"></li>

					</ul>
				</li>


			</ul>
		</div>
	</div>
</nav>

<div class="container">

	<p>傳輸方式：POST<br>
		http://114.35.219.208:8080/horncloud/new_api/<br>
		https://www.hncare.cloud/horncloud/new_api/
	</p>

	<!-- <p>傳輸方式：POST<br>
		https://www.hncare.cloud/horncloud/new_api/
	</p> -->

</div>