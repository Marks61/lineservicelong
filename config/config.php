<?php
	date_default_timezone_set("Asia/Taipei");
	function loader($class) {
		$file = 'mod/'.$class.'/class.php';
		if(is_file($file)) {
			require_once($file);
		}
	}
	spl_autoload_register('loader'); 
	spl_autoload_extensions('.php');

	$arr_mod = array('');
	$arr_cot = array('');

	$content = 'mod/homepage/content.php';
	$isVerification = true;		//true 關網頁表登出
	$checkVerification = false;	//檢驗是否OK的帳號

	$ver = isset($_GET['ver'])?$_GET['ver']:0;
	if ($isVerification) {
		if (!$ver) {
			$checkVerification = false;
		} else if ( $ver != $admin['ver']) {
			$checkVerification = false;
		}
	}
	if (!isset($admin['id'])) {
		$checkVerification = false;
	} else if (!$admin['id']) {
		$checkVerification = false;
	}
	if (!$checkVerification) {
		//header("location:un.php");
	} else if ($admin['identity'] != 'admin') {
		//header("location:un.php");
	} else if ($admin['identity'] == 'admin') {
		if (!empty($_GET['content']) && !empty($_GET['mod'])) {
			preg_match('/([\w]+)/',$_GET['mod'],$arr_mod);
			preg_match('/([\w]+)/',$_GET['content'],$arr_cot);
			$content = 'mod/'.$arr_mod[0].'/'.$arr_cot[0].'.php';
		}
	}
	if (!empty($_GET['content']) && !empty($_GET['mod'])) {
		preg_match('/([\w]+)/',$_GET['mod'],$arr_mod);
		preg_match('/([\w]+)/',$_GET['content'],$arr_cot);
		$content = 'mod/'.$arr_mod[0].'/'.$arr_cot[0].'.php';
	}
?>