<?php
	$savePath = './session';
	session_save_path($savePath);
	session_start();
	$lifetime = 604800;	//24小時
	
	session_set_cookie_params($lifetime);
	$admin = '';

	if ( isset($_SESSION['admin']) ) {
		$admin = $_SESSION['admin'];
	} else {
		$admin = '';
	}

	if( !isset($admin['identity']) ) {
		session_unset();
		session_destroy();
	}
?>