<?php
date_default_timezone_set("Asia/Taipei");
function loader($class)
{
	$file = '../mod/' . $class . '/class.php';
	if (is_file($file)) {
		require_once($file);
	}
}

spl_autoload_register('loader');
spl_autoload_extensions('.php');
define('filePath', '../upload/file/');
define('paperPath', '../upload/paper/');
define('serviceEmail', 'k40211148@gcloud.csu.edu.tw');
define('servicePassword', 'mmo745449');
$arr_mod = array('');
$arr_cot = array('');
$content = '../admin/index.php';
$isVerification = true;		//true 關網頁表登出
$checkVerification = true;	//檢驗是否OK的帳號
$ver = isset($_GET['ver']) ? $_GET['ver'] : 0;

if ($isVerification) {
	if (!$ver) {
		$checkVerification = false;
	} else if ($ver != $admin['ver']) {
		$checkVerification = false;
	}
}

$adminPower = array('admin','院所醫生');
if (!$checkVerification) {
	header("location:../unlogin.php");
} else if (!in_array($admin['identity'], $adminPower)) {
	header("location:../unlogin.php");
}

if (!empty($_GET['content']) && !empty($_GET['mod'])) {
	preg_match('/([\w]+)/', $_GET['mod'], $arr_mod);
	preg_match('/([\w]+)/', $_GET['content'], $arr_cot);
	$content = '../mod/' . $arr_mod[0] . '/' . $arr_cot[0] . '.php';
}
