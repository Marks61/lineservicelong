<?php
// ini_set('display_errors','1');
// error_reporting(E_ALL);

use function JmesPath\search;

require_once('class.php');
require_once('session.php');
require_once('config.php');
require_once('../cdb/config.php');
$cla = new menu();

$sql = "select * from account where id = '" . $admin['id'] . "'";
$account_row = $cla->getRow($sql);
$view_id = isset($_GET['view']) ? $_GET['view'] : 0;
$admin['identity_name'] = $cla->getRow('select name from identity where name_en = \'' . $admin['identity'] . '\'')['name'];
$group = $cla->getQuery('select id,name from identity where is_use = 1');
$member = $cla->getQuery2('select id,account,name from account where enable = 1');

if (isset($_GET['mod']) && isset($_GET['content'])) {
	$identity_content = mb_split(',', $cla->getRow('select category from identity where name_en = \'' . $admin['identity'] . '\'')['category']);
	$check_file = $cla->getQuery('select name from feature where mode = \'' . $_GET['mod'] . '\' and content = \'' . $_GET['content'] . '\' and is_use = 1');
	if ($check_file['num'] < 1) {
		header("Location: 404.php?ver=" . $_GET['ver']);
		exit;
	}
	if (!in_array($cla->getRow('select id from feature_class where name_en = \'' . $_GET['mod'] . '\'')['id'], $identity_content)) {
		header("Location: 403.php?ver=" . $_GET['ver']);
		exit;
	}
	$content = '../mod/' . $_GET['mod'] . '/' . $_GET['content'] . '.php';
	$script = '../mod/' . $_GET['mod'] . '/' . $_GET['content'] . '_script.php';
}

ob_start();
require_once('../mod/record/insert.php');
?>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>霍恩關懷雲守護站 | <?php $cla->search_content_name(isset($_GET['mod']) ? $_GET['mod'] : '', isset($_GET['content']) ? $_GET['content'] : ''); ?></title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
	<link href="font-awesome/css/font-awesome.css" rel="stylesheet">

	<link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
	<!-- Toastr style -->
	<link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">
	<!-- ichecks style -->
	<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
	<!--Switchery-->
	<link href="css/plugins/switchery/switchery.css" rel="stylesheet">
	<!-- Gritter -->
	<link href="js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
	<!-- Sweetalert2 -->
	<link rel="stylesheet" type="text/css" href="css/colorlib/vendor/sweetalert2/dist/sweetalert2.min.css">
	<!-- select2 -->
	<link rel="stylesheet" type="text/css" href="css/plugins/select2/select2.min.css">

	<link href="css/animate.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<style>
	.swal2-modal {
		z-index: 99999;
	}

	/*bootstrap new*/
	.modal-checkbox input[type=checkbox] {
		width: 25px;
		height: 25px;
		background-color: #1ab394;
		color: white;
	}

	.modal-checkbox .span {
		font-size: 20pt;
	}

	.new-alert-yellow {
		color: #999c9e;
	}

	.new-alert-red {
		color: #EA0000;
	}

	.select2-close-mask {
		z-index: 2099;
	}

	.select2-dropdown {
		z-index: 3051;
	}
</style>

<body>
	<div id="wrapper">
		<nav class="navbar-default navbar-static-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav metismenu" id="side-menu">
					<li class="nav-header">
						<div class="dropdown profile-element"> <span>
								<img alt="image" class="img-circle" src="img/profile_small.jpg" />
							</span>
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<span class="clear"> <span class="block m-t-xs"> <strong class="font-bold" id="main_name"><?php echo $admin['name'] ?></strong>
									</span> <span class="text-muted text-xs block" id="dropdown-font"><?php echo $admin['identity_name']; ?>
										<!--<b
                                            class="caret"></b>-->
									</span> </span> </a>
							<!--<ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="profile.html">Profile</a></li>
                                <li><a href="contacts.html">Contacts</a></li>
                                <li><a href="mailbox.html">Mailbox</a></li>
                                <li class="divider"></li>
                                <li><a href="login.html">Logout</a></li>
                            </ul>-->
						</div>
						<div class="logo-element">
							<img alt="image" class="img-circle" height="35%" width="35%" src="img/profile_small.jpg" />
						</div>
					</li>
					<?php foreach (mb_split(',', $cla->getRow('select category from identity where name_en = \'' . $admin['identity'] . '\'')['category']) as $category) {
						$data_sub = $cla->getRow('select name,name_en from feature_class where id = \'' . $category . '\'');
						$data_sub_function = $cla->getQuery('select name,mode,content from feature where mode = \'' . $data_sub['name_en'] . '\'');
					?>
						<li class="left-dropdown">
							<a href="#<?php echo $data_sub['name_en']; ?>" data-toggle="collapse" aria-expanded="false"><i class="fa fa-th-large"></i>
								<span class="nav-label"><?php echo $data_sub['name']; ?></span><span class="sub-nav-icon"><i class="fa fa-chevron-right"></i></span></a>
							<ul class="nav nav-second-level collapse" id="<?php echo $data_sub['name_en']; ?>">
								<?php foreach ($data_sub_function['row'] as $function) {
								?>
									<li><a href="?mod=<?php echo $function['mode']; ?>&content=<?php echo $function['content']; ?>&ver=<?php echo $_GET['ver']; ?>"><?php echo $function['name']; ?></a></li>
								<?php
								}
								?>
							</ul>
						</li>
					<?php
					}
					?>
					<HR size="3">
					<li class="left-dropdown">
						<a href="javascript:$('#radio_modal').modal({backdrop: 'static',keyboard: false});"><i class="fa fa-th-large"></i> <span class="nav-label">推播</span></a>
					</li>
				</ul>
			</div>
		</nav>

		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0;background-color:white;">
					<div class="navbar-header">

						<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " style="background-color:#ecb1ac;" href="#"><i class="fa fa-bars"></i> </a>
						<!--
                        <form role="search" class="navbar-form-custom" action="search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="Search for something..." class="form-control"
                                    name="top-search" id="top-search">
                            </div>
                        </form>
                    -->
					</div>
					<ul class="nav navbar-top-links navbar-right">
						<li>
							<!--
                                <span class="m-r-sm text-muted welcome-message">Welcome to INSPINIA+ Admin Theme.</span>
         
                            -->
						</li>
						<!--
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-envelope"></i> <span class="label label-warning">16</span>
                            </a>
                            <ul class="dropdown-menu dropdown-messages">
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-left">
                                            <img alt="image" class="img-circle" src="img/a7.jpg">
                                        </a>
                                        <div class="media-body">
                                            <small class="pull-right">46h ago</small>
                                            <strong>Mike Loreipsum</strong> started following <strong>Monica
                                                Smith</strong>. <br>
                                            <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-left">
                                            <img alt="image" class="img-circle" src="img/a4.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-right text-navy">5h ago</small>
                                            <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica
                                                Smith</strong>. <br>
                                            <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-left">
                                            <img alt="image" class="img-circle" src="img/profile.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-right">23h ago</small>
                                            <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                            <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a href="mailbox.html">
                                            <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell"></i> <span class="label label-primary">8</span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts">
                                <li>
                                    <a href="mailbox.html">
                                        <div>
                                            <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                            <span class="pull-right text-muted small">4 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="profile.html">
                                        <div>
                                            <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                            <span class="pull-right text-muted small">12 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="grid_options.html">
                                        <div>
                                            <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                            <span class="pull-right text-muted small">4 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a href="notifications.html">
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    -->
						<li class="dropdown">
							<a class="dropdown-toggle open-small-chat-mini count-info" style="color:#1ab394">
								<i class="fa fa-comments"></i> <span class="label label-primary">5</span>
							</a>
						</li>
						<li>
							<a><svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
									<path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z" />
									<path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z" />
								</svg><span id="datetime"></span></a>
						</li>
						<li>
							<a href="javascript:logout();">
								<i class="fa fa-sign-out"></i> Log out
							</a>
						</li>
						<li>
							<a class="right-sidebar-toggle">
								<i id="note-button" class="fa fa-gears"></i><span id="new-message" style="display: none;"><b>您有新通知!!</b></span>
							</a>
						</li>
					</ul>

				</nav>
			</div>
			<!--<div class="row  border-bottom white-bg dashboard-header">

				<div class="col-sm-3">
					<h2>Welcome Amelia</h2>
					<small>You have 42 messages and 6 notifications.</small>
					<ul class="list-group clear-list m-t">
						<li class="list-group-item fist-item">
							<span class="pull-right">
								09:00 pm
							</span>
							<span class="label label-success">1</span> Please contact me
						</li>
						<li class="list-group-item">
							<span class="pull-right">
								10:16 am
							</span>
							<span class="label label-info">2</span> Sign a contract
						</li>
						<li class="list-group-item">
							<span class="pull-right">
								08:22 pm
							</span>
							<span class="label label-primary">3</span> Open new shop
						</li>
						<li class="list-group-item">
							<span class="pull-right">
								11:06 pm
							</span>
							<span class="label label-default">4</span> Call back to Sylvia
						</li>
						<li class="list-group-item">
							<span class="pull-right">
								12:00 am
							</span>
							<span class="label label-primary">5</span> Write a letter to Sandra
						</li>
					</ul>
				</div>
				<div class="col-sm-6">
					<div class="flot-chart dashboard-chart">
						<div class="flot-chart-content" id="flot-dashboard-chart"></div>
					</div>
					<div class="row text-left">
						<div class="col-xs-4">
							<div class=" m-l-md">
								<span class="h4 font-bold m-t block">$ 406,100</span>
								<small class="text-muted m-b block">Sales marketing report</small>
							</div>
						</div>
						<div class="col-xs-4">
							<span class="h4 font-bold m-t block">$ 150,401</span>
							<small class="text-muted m-b block">Annual sales revenue</small>
						</div>
						<div class="col-xs-4">
							<span class="h4 font-bold m-t block">$ 16,822</span>
							<small class="text-muted m-b block">Half-year revenue margin</small>
						</div>

					</div>
				</div>
				<div class="col-sm-3">
					<div class="statistic-box">
						<h4>
							Project Beta progress
						</h4>
						<p>
							You have two project with not compleated task.
						</p>
						<div class="row text-center">
							<div class="col-lg-6">
								<canvas id="polarChart" width="80" height="80"></canvas>
								<h5>Kolter</h5>
							</div>
							<div class="col-lg-6">
								<canvas id="doughnutChart" width="78" height="78"></canvas>
								<h5>Maxtor</h5>
							</div>
						</div>
						<div class="m-t">
							<small>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
						</div>

					</div>
				</div>

			</div>-->
			<!--<div class="row">
				<div class="col-lg-12">-->
			<div class="wrapper wrapper-content" id="index">
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-content">
								<div id="myCarousel" class="carousel slide">
									<!-- 轮播 ( carousel) 指标 -->
									<ol class="carousel-indicators">
										<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
										<li data-target="#myCarousel" data-slide-to="1"></li>
										<li data-target="#myCarousel" data-slide-to="2"></li>
										<li data-target="#myCarousel" data-slide-to="3"></li>
										<li data-target="#myCarousel" data-slide-to="4"></li>
										<li data-target="#myCarousel" data-slide-to="5"></li>
									</ol>
									<!-- 轮播 ( carousel ) 项目 -->
									<div class="carousel-inner">
										<div class="item active">
											<img width="1600" height="800" src="img/84634265_p0.png">
										</div>
										<div class="item">
											<img width="1600" height="400" src="img/p2.jpg">
										</div>
										<div class="item">
											<img src="img/p3.jpg">
										</div>
										<div class="item">
											<img src="img/p4.jpg">
										</div>
										<div class="item">
											<img src="img/p5.jpg">
										</div>
										<div class="item">
											<img src="img/p6.jpg">
										</div>
									</div>
									<!-- 轮播( Carousel ) 导航 -->
									<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;
									</a>
									<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<span class="label label-primary pull-right">好友人數</span>
								<h5>Users count</h5>
							</div>
							<div class="ibox-content">
								<h1 class="no-margins">0</h1>
								<!--
								<div class="stat-percent font-bold text-navy">20% <i class="fa fa-level-up"></i></div>-->
								<small>Members</small>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<span class="label label-info pull-right">機器人發送的訊息數量</span>
								<h5>Broadcast</h5>
							</div>
							<div class="ibox-content">
								<h1 class="no-margins">0</h1>
								<div class="stat-percent font-bold text-info">40% <i class="fa fa-level-up"></i></div>
								<small>count</small>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<span class="label label-warning pull-right">一對一聊天發送的訊息數量</span>
								<h5>Message</h5>
							</div>
							<div class="ibox-content">
								<h1 class="no-margins">0</h1>
								<div class="stat-percent font-bold text-warning">16% <i class="fa fa-level-up"></i></div>
								<small>count</small>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12">
						<div class="ibox">
							<div class="ibox-title">
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
											<h2><b>群發訊息一覽</b></h2>

											<!--<select name="classes" id="classes" class="form-control">
												<option value="1" selected="">群發訊息</option>
												<option value="2">貼文</option>
											</select>-->
										</div>
									</div>
									<div class="col-sm-2">
									</div>
									<div class="col-sm-2">
									</div>
									<div class="col-sm-4 text-right">
										<button type="button" class="btn btn-primary" onclick="line_message_modal_open()" id="poster">新增</button>
									</div>

								</div>
							</div>
							<div class="table-responsive ibox-content">
								<table class="table table-striped">
									<thead>
										<th>內容(文字)</th>
										<th>內容(圖片)</th>
										<th>對象</th>
										<th>建立時間</th>
										<th>編輯時間</th>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Master project</td>
											<td>Patrick Smith</td>
											<td>Inceptos Hymenaeos Ltd</td>
											<td><strong>20%</strong></td>
										</tr>
										<tr>
											<td>2</td>
											<td>Alpha project</td>
											<td>Alice Jackson</td>
											<td>Nec Euismod In Company</td>
											<td><strong>40%</strong></td>
										</tr>
										<tr>
											<td>3</td>
											<td>Betha project</td>
											<td>John Smith</td>
											<td>Erat Volutpat</td>
											<td><strong>75%</strong></td>
										</tr>
										<tr>
											<td>4</td>
											<td>Gamma project</td>
											<td>Anna Jordan</td>
											<td>Tellus Ltd</td>
											<td><strong>18%</strong></td>
										</tr>
										<tr>
											<td>2</td>
											<td>Alpha project</td>
											<td>Alice Jackson</td>
											<td>Nec Euismod In Company</td>
											<td><strong>40%</strong></td>
										</tr>
										<tr>
											<td>1</td>
											<td>Master project</td>
											<td>Patrick Smith</td>
											<td>Inceptos Hymenaeos Ltd</td>
											<td><strong>20%</strong></td>
										</tr>
										<tr>
											<td>4</td>
											<td>Gamma project</td>
											<td>Anna Jordan</td>
											<td>Tellus Ltd</td>
											<td><strong>18%</strong></td>
										</tr>
									</tbody>
								</table>
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="wrapper wrapper-content" id="content" style="display: none;">
				<?php require_once($content); ?>
			</div>

			<!--</div>-->
			<div class="footer">
				<div class="pull-right">
					<!--10GB of <strong>250GB</strong> Free.-->
				</div>
				<div>
					<strong>Copyright</strong> 霍恩實業有限公司 &copy; 2021
				</div>
			</div>
		</div>
	</div>

	</div>

	<!--通傳訊息框-->
	<div class="small-chat-box fadeInRight animated">

		<div class="heading" draggable="true">
			<small class="chat-date pull-right">
				02.19.2015
			</small>
			Small chat
		</div>

		<div class="content">

			<div class="left">
				<div class="author-name">
					Monica Jackson <small class="chat-date">
						10:02 am
					</small>
				</div>
				<div class="chat-message active">
					Lorem Ipsum is simply dummy text input.
				</div>

			</div>
			<div class="right">
				<div class="author-name">
					Mick Smith
					<small class="chat-date">
						11:24 am
					</small>
				</div>
				<div class="chat-message">
					Lorem Ipsum is simpl.
				</div>
			</div>
			<div class="left">
				<div class="author-name">
					Alice Novak
					<small class="chat-date">
						08:45 pm
					</small>
				</div>
				<div class="chat-message active">
					Check this stock char.
				</div>
			</div>
			<div class="right">
				<div class="author-name">
					Anna Lamson
					<small class="chat-date">
						11:24 am
					</small>
				</div>
				<div class="chat-message">
					The standard chunk of Lorem Ipsum
				</div>
			</div>
			<div class="left">
				<div class="author-name">
					Mick Lane
					<small class="chat-date">
						08:45 pm
					</small>
				</div>
				<div class="chat-message active">
					I belive that. Lorem Ipsum is simply dummy text.
				</div>
			</div>


		</div>
		<div class="form-chat">
			<div class="input-group input-group-sm"><input type="text" class="form-control"> <span class="input-group-btn"> <button class="btn btn-primary" type="button">Send
					</button> </span></div>
		</div>

	</div>

	<div id="right-sidebar">
		<div class="sidebar-container">

			<ul class="nav nav-tabs navs-3">

				<li class="active"><a data-toggle="tab" href="#tab-1">
						通知
					</a></li>
				<li><a data-toggle="tab" style="display: none;" href="#tab-2">
						設定
					</a></li>
				<li class=""><a data-toggle="tab" href="#tab-3">
						<i class="fa fa-gear"></i>
					</a></li>
			</ul>

			<div class="tab-content">


				<div id="tab-1" class="tab-pane active">

					<div class="sidebar-title">
						<h3> <i class="fa fa-comments-o"></i>通知</h3>
						<small><i class="fa fa-tim"></i>你有<span style="color:#1ab394;" id="message-num"></span>則新通知。</small>
					</div>

					<div id="all-note">
					</div>

				</div>

				<div id="tab-2" class="tab-pane">

					<div class="sidebar-title">
						<h3> <i class="fa fa-cube"></i> Latest projects</h3>
						<small><i class="fa fa-tim"></i> You have 14 projects. 10 not completed.</small>
					</div>

					<ul class="sidebar-list">
						<li>
							<a href="#">
								<div class="small pull-right m-t-xs">9 hours ago</div>
								<h4>Business valuation</h4>
								It is a long established fact that a reader will be distracted.

								<div class="small">Completion with: 22%</div>
								<div class="progress progress-mini">
									<div style="width: 22%;" class="progress-bar progress-bar-warning"></div>
								</div>
								<div class="small text-muted m-t-xs">Project end: 4:00 pm - 12.06.2014</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="small pull-right m-t-xs">9 hours ago</div>
								<h4>Contract with Company </h4>
								Many desktop publishing packages and web page editors.

								<div class="small">Completion with: 48%</div>
								<div class="progress progress-mini">
									<div style="width: 48%;" class="progress-bar"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="small pull-right m-t-xs">9 hours ago</div>
								<h4>Meeting</h4>
								By the readable content of a page when looking at its layout.

								<div class="small">Completion with: 14%</div>
								<div class="progress progress-mini">
									<div style="width: 14%;" class="progress-bar progress-bar-info"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="label label-primary pull-right">NEW</span>
								<h4>The generated</h4>
								<!--<div class="small pull-right m-t-xs">9 hours ago</div>-->
								There are many variations of passages of Lorem Ipsum available.
								<div class="small">Completion with: 22%</div>
								<div class="small text-muted m-t-xs">Project end: 4:00 pm - 12.06.2014</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="small pull-right m-t-xs">9 hours ago</div>
								<h4>Business valuation</h4>
								It is a long established fact that a reader will be distracted.

								<div class="small">Completion with: 22%</div>
								<div class="progress progress-mini">
									<div style="width: 22%;" class="progress-bar progress-bar-warning"></div>
								</div>
								<div class="small text-muted m-t-xs">Project end: 4:00 pm - 12.06.2014</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="small pull-right m-t-xs">9 hours ago</div>
								<h4>Contract with Company </h4>
								Many desktop publishing packages and web page editors.

								<div class="small">Completion with: 48%</div>
								<div class="progress progress-mini">
									<div style="width: 48%;" class="progress-bar"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="small pull-right m-t-xs">9 hours ago</div>
								<h4>Meeting</h4>
								By the readable content of a page when looking at its layout.

								<div class="small">Completion with: 14%</div>
								<div class="progress progress-mini">
									<div style="width: 14%;" class="progress-bar progress-bar-info"></div>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="label label-primary pull-right">NEW</span>
								<h4>The generated</h4>
								<!--<div class="small pull-right m-t-xs">9 hours ago</div>-->
								There are many variations of passages of Lorem Ipsum available.
								<div class="small">Completion with: 22%</div>
								<div class="small text-muted m-t-xs">Project end: 4:00 pm - 12.06.2014</div>
							</a>
						</li>

					</ul>

				</div>

				<div id="tab-3" class="tab-pane">

					<div class="sidebar-title">
						<h3><i class="fa fa-gears"></i> Settings</h3>
						<!--
                                         <small><i class="fa fa-tim"></i> You have 14 projects. 10 not completed.</small>
                            -->
					</div>
					<div class="setings-item">
						<span>
							Profile
						</span>
						<div class="switch">

							<div class="onoffswitch">

								<a href="profile.html" class="btn btn-primary btn-sm">開啟</a>
								<!--<input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox"
                                        id="example">
                                    <label class="onoffswitch-label" for="example">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>-->
							</div>
						</div>
					</div>
					<div class="setings-item">
						<span>
							Contacts
						</span>

						<div class="switch">
							<div class="onoffswitch">
								<a href="contacts.html" class="btn btn-primary btn-sm">開啟</a>
								<!--<input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox"
                                        id="example">
                                    <label class="onoffswitch-label" for="example">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>-->
							</div>
						</div>
					</div>
					<div class="setings-item">
						<span>
							Mailbox
						</span>

						<div class="switch">
							<div class="onoffswitch">
								<a href="mailbox.html" class="btn btn-primary btn-sm">開啟</a>
								<!--<input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox"
                                        id="example">
                                    <label class="onoffswitch-label" for="example">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>-->
							</div>
						</div>
					</div>
					<!--
                        <div class="setings-item">
                            <span>
                                Show notifications
                            </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox"
                                        id="example">
                                    <label class="onoffswitch-label" for="example">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="setings-item">
                            <span>
                                Disable Chat
                            </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="collapsemenu" checked class="onoffswitch-checkbox"
                                        id="example2">
                                    <label class="onoffswitch-label" for="example2">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="setings-item">
                            <span>
                                Enable history
                            </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox"
                                        id="example3">
                                    <label class="onoffswitch-label" for="example3">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="setings-item">
                            <span>
                                Show charts
                            </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox"
                                        id="example4">
                                    <label class="onoffswitch-label" for="example4">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="setings-item">
                            <span>
                                Offline users
                            </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" checked name="collapsemenu" class="onoffswitch-checkbox"
                                        id="example5">
                                    <label class="onoffswitch-label" for="example5">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="setings-item">
                            <span>
                                Global search
                            </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" checked name="collapsemenu" class="onoffswitch-checkbox"
                                        id="example6">
                                    <label class="onoffswitch-label" for="example6">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="setings-item">
                            <span>
                                Update everyday
                            </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox"
                                        id="example7">
                                    <label class="onoffswitch-label" for="example7">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                            -->

					<div class="sidebar-content">
						<!--<h4>Settings</h4>
                            <div class="small">
                                I belive that. Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.
                                And typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever
                                since the 1500s.
                                Over the years, sometimes by accident, sometimes on purpose (injected humour and the
                                like).
                            </div>-->
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="modal alert-modal fade" id="alert_modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<i class="fa fa-exclamation-triangle modal-icon"></i>
					<h4 class="modal-title">警告</h4>
					<small class="font-bold"></small>
				</div>
				<div class="modal-body text-center">
					<h2><b>通知推播功能已經被關閉。<b></br><small>訊息推播功能建議開啟，目前系統功能有所限制。</small></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="modal inmodal fade" id="alert_content_modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<i class="fa fa-info-circle modal-icon"></i>
					<h4 class="modal-title" id="alert-title"></h4>
					<small class="font-bold"></small>
				</div>
				<div class="modal-body" id="alert-content">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal inmodal fade" id="line_message_modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<i class="fa fa-paper-plane modal-icon"></i>
					<h4 class="modal-title">LINE貼文/訊息設定</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>選擇訊息類型</label>
						<select id="sms-type" name="sms-type" onchange="content_set(this.value);" class=" form-control">
							<option value="0">請選擇訊息類別</option>
							<option value="1">圖文簡訊</option>
							<option value="2">文字訊息</option>
							<option value="3">圖片訊息</option>
						</select>
					</div>
					<!--<div class="form-group">
						<label>選擇對象</label>
						<select id="sms-target" name="sms-target" onchange="target_set(this.value);" class="form-control">
							<option value="0">請選擇發送對象</option>
							<option value="1">個人</option>
							<option value="2">群組</option>
							<option value="3">所有人</option>
						</select>
					</div>-->
					<div id="content_edit"></div>
					<div id="target_edit">
						<div class="form-group">
							<label>選擇個人對象</label>
							<select id="target-member" style="display: none;" name="target-member" multiple class=" form-control">
								<?php
								foreach ($member['row'] as $row3) {
								?>
									<option value="<?php echo $row3['id'] ?>"><?php echo $row3['name'] ?>(<?php echo $row3['account'] ?>)</option>
								<?php
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>選擇群組隊象</label>
							<select id="target-group" style="display: none;" name="target-group" multiple class=" form-control">
								<?php
								foreach ($group['row'] as $row2) {
								?>
									<option value="<?php echo $row2['id'] ?>"><?php echo $row2['name'] ?></option>
								<?php
								}
								?>
							</select>

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
					<button type="button" class="btn btn-primary" onclick="add_function();">儲存</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Mainly scripts -->
	<script src="js/jquery-2.1.1.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

	<!-- Flot -->
	<script src="js/plugins/flot/jquery.flot.js"></script>
	<script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
	<script src="js/plugins/flot/jquery.flot.spline.js"></script>
	<script src="js/plugins/flot/jquery.flot.resize.js"></script>
	<script src="js/plugins/flot/jquery.flot.pie.js"></script>

	<!-- Peity -->
	<script src="js/plugins/peity/jquery.peity.min.js"></script>
	<script src="js/demo/peity-demo.js"></script>

	<!-- Custom and plugin javascript -->
	<script src="js/inspinia.js"></script>
	<script src="js/plugins/pace/pace.min.js"></script>

	<!-- jQuery UI -->
	<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

	<!-- GITTER -->
	<script src="js/plugins/gritter/jquery.gritter.min.js"></script>

	<!-- Sparkline -->
	<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

	<!-- Sparkline demo data  -->
	<script src="js/demo/sparkline-demo.js"></script>

	<!-- ChartJS-->
	<script src="js/plugins/chartJs/Chart.min.js"></script>

	<!-- Toastr -->
	<script src="js/plugins/toastr/toastr.min.js"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>

	<script src="js/plugins/dataTables/datatables.min.js"></script>

	<!-- iCheck -->
	<script src="js/plugins/iCheck/icheck.min.js"></script>

	<!-- Switchery -->
	<script src="js/plugins/switchery/switchery.js"></script>

	<!-- Sweetalert2 -->
	<script src="css/colorlib/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

	<!-- select2-->
	<script src="js/plugins/select2/select2.full.min.js"></script>

	<script>
		var ver = '<?php echo $admin['ver'] ?>';
		$('#md_Modal').modal('show');

		function modify_remind(id, remind_enable, sta) {
			var isChecked = ($('.i-checks').prop('checked')) ? 1 : 0;
			// alert(id);
			var request = $.ajax({
				url: "ajax.php?mod=account&content=update_remind&ver=" + ver,
				method: "POST",
				data: {
					id: id,
					remind_enable: remind_enable,
					sta: sta,
					isChecked: isChecked
				},
				dataType: "text"
			});
			request.done(function(dat) {
				var data = JSON.parse(dat);
				var arr = [];
				for (var key in data) {
					arr[key] = data[key];
				}
				if (arr['success']) $('#md_Modal').modal('hide');
				// console.log(arr);
			});
			request.fail(function(jqXHR, textStatus) {
				alert("Request failed: " + textStatus);
			});
			// console.log(isChecked);
		}
	</script>
	<script>
		var isAllSuccess = false;
		var ver = '<?php echo $admin['ver'] ?>';
		var carer = '<?php echo $admin['id']; ?>';
		var identity = '<?php echo $admin['identity']; ?>';
		var windowFeatures = "height=" + screen.availHeight + ", width=" + screen.availWidth + ", toolbar=no, scrollbars=no, resizable=yes, location=no, menubar=no, status=no";
		var ring = new Audio('../sound/Callsound_1.mp3');
	</script>


	<script>
		var edit_textbox = '';
		var alert_catch = function() {

			var request = $.ajax({
				url: "ajax.php?mod=system&content=alert_catch&ver=" + ver,
				method: "POST",
				data: {
					member: '<?php echo $admin['id']; ?>',
					identity: '<?php echo $admin['identity']; ?>'
				},
				dataType: "text"
			});
			request.done(function(dat) {
				var data = JSON.parse(dat);
				var arr = [];
				for (var key in data) {
					arr[key] = data[key];
				}
				if (arr['success']) {
					if (arr['data']['num'] > 0) {
						var code = '';
						$('#new-message').show();
						setInterval(function() {
							$('#note-button').removeClass('new-alert-yellow');
							$('#new-message').removeClass('new-alert-yellow');

							$('#note-button').addClass('new-alert-red');
							$('#new-message').addClass('new-alert-red');

							return;
						}, 1000);
						setInterval(function() {
							$('#note-button').removeClass('new-alert-red');
							$('#new-message').removeClass('new-alert-red');

							$('#note-button').addClass('new-alert-yellow');
							$('#new-message').addClass('new-alert-yellow');
							return;
						}, 3000);

						for (var j = 0; j < arr['data']['row'].length; j++) {
							code += '<div class="sidebar-message">';
							code += '<a href="' + (arr['data']['row'][j]['link'] == null ? 'javascript:alert_content_modal_show(\'' + arr['data']['row'][j]['id'] + '\');' : arr['data']['row'][j]['link']) + '">';
							code += '<div class="pull-left text-center">';
							code += '<img alt="image" class="img-circle message-avatar" src="/horn/lineservice/admin/' + arr['data']['row'][j]['icon'] + '">';
							code += '<div class="m-t-xs">';
							code += '<i class="fa fa-star text-warning"></i>';
							code += '<i class="fa fa-star text-warning"></i>';
							code += '</div>';
							code += '</div>';
							code += '<div class="media-body">';
							code += '<h3><b>' + arr['data']['row'][j]['title'] + '</b></h3>'
							code += arr['data']['row'][j]['content'];
							code += '<br>';
							code += '<small class="text-muted">' + arr['data']['row'][j]['created_at'] + '</small>';
							code += '</div>';
							code += '</a>';
							code += '</div>';
						}
						$('#all-note').html(code);
					} else {
						$('#new-message').hide();
						$('#note-button').removeClass('new-alert-yellow');
						$('#new-message').removeClass('new-alert-yellow');
						$('#note-button').removeClass('new-alert-red');
						$('#new-message').removeClass('new-alert-red');
						$('#all-note').html('');
					}
					$('#message-num').text(arr['data']['num']);
					for (var j = 0; j < arr['data']['row'].length; j++) {
						var notify = new Notification(arr['data']['row'][j]['title'], {
							body: arr['data']['row'][j]['content'],
							icon: '/horn/lineservice/admin/' + arr['data']['row'][j]['icon'],
							tag: '第' + (j + 1) + '則訊息' // 設定標籤
						});
					}
				}
			});
			request.fail(function(jqXHR, textStatus) {
				alert("Request failed: " + textStatus);
			});

			return alert_catch;
		}

		var check_notification_identity = function() {
			if (Notification.permission === 'default' || Notification.permission === 'undefined' || Notification.permission === 'denied') {
				$('#alert_modal').modal({
					backdrop: 'static',
					keyboard: false
				});

				Notification.requestPermission(function(permission) {
					if (permission === 'granted') {
						$('#alert_modal').modal('hide');
						// 使用者同意授權
						//var notification = new Notification('Hi there!'); // 建立通知
					} else if (permission === 'default' || permission === 'undefined' || permission === 'denied') {
						$('#alert_modal').modal({
							backdrop: 'static',
							keyboard: false
						});
					}
				});
			} else {
				$('#alert_modal').modal('hide');
			}

			return check_notification_identity;
		}

		$(document).ready(function() {
			$('#target-group').select2({
				width: '100%'
			});
			$('#target-member').select2({
				width: '100%'
			});
			setInterval(alert_catch(), '30000');
			setInterval(check_notification_identity(), '1000');
			<?php
			if (isset($_GET['mod']) && isset($_GET['content'])) {
				echo '$(\'#index\').hide();';
				echo '$(\'#content\').show();';
			}
			?> $('.i-checkss').iCheck({
				checkboxClass: 'icheckbox_square-green',
				radioClass: 'iradio_square-green',
			});

			if (!('Notification' in window)) {
				console.log('This browser does not support notification');
			} else {
				console.log('This browser does support notification');
			}


			// console.log(isChecked);


		});
		/*$(document).ready(function() {
			/*setTimeout(function () {
			    toastr.options = {
			        closeButton: true,
			        progressBar: true,
			        showMethod: 'slideDown',
			        timeOut: 4000
			    };
			    toastr.success('Responsive Admin Theme', 'Welcome to INSPINIA');

			}, 1300);


			var data1 = [
				[0, 4],
				[1, 8],
				[2, 5],
				[3, 10],
				[4, 4],
				[5, 16],
				[6, 5],
				[7, 11],
				[8, 6],
				[9, 11],
				[10, 30],
				[11, 10],
				[12, 13],
				[13, 4],
				[14, 3],
				[15, 3],
				[16, 6]
			];
			var data2 = [
				[0, 1],
				[1, 0],
				[2, 2],
				[3, 0],
				[4, 1],
				[5, 3],
				[6, 1],
				[7, 5],
				[8, 2],
				[9, 3],
				[10, 2],
				[11, 1],
				[12, 0],
				[13, 2],
				[14, 8],
				[15, 0],
				[16, 0]
			];
			$("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"), [
				data1, data2
			], {
				series: {
					lines: {
						show: false,
						fill: true
					},
					splines: {
						show: true,
						tension: 0.4,
						lineWidth: 1,
						fill: 0.4
					},
					points: {
						radius: 0,
						show: true
					},
					shadowSize: 2
				},
				grid: {
					hoverable: true,
					clickable: true,
					tickColor: "#d5d5d5",
					borderWidth: 1,
					color: '#d5d5d5'
				},
				colors: ["#1ab394", "#1C84C6"],
				xaxis: {},
				yaxis: {
					ticks: 4
				},
				tooltip: false
			});

			var doughnutData = [{
					value: 300,
					color: "#a3e1d4",
					highlight: "#1ab394",
					label: "App"
				},
				{
					value: 50,
					color: "#dedede",
					highlight: "#1ab394",
					label: "Software"
				},
				{
					value: 100,
					color: "#A4CEE8",
					highlight: "#1ab394",
					label: "Laptop"
				}
			];

			var doughnutOptions = {
				segmentShowStroke: true,
				segmentStrokeColor: "#fff",
				segmentStrokeWidth: 2,
				percentageInnerCutout: 45, // This is 0 for Pie charts
				animationSteps: 100,
				animationEasing: "easeOutBounce",
				animateRotate: true,
				animateScale: false
			};

			var ctx = document.getElementById("doughnutChart").getContext("2d");
			var DoughnutChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);

			var polarData = [{
					value: 300,
					color: "#a3e1d4",
					highlight: "#1ab394",
					label: "App"
				},
				{
					value: 140,
					color: "#dedede",
					highlight: "#1ab394",
					label: "Software"
				},
				{
					value: 200,
					color: "#A4CEE8",
					highlight: "#1ab394",
					label: "Laptop"
				}
			];

			var polarOptions = {
				scaleShowLabelBackdrop: true,
				scaleBackdropColor: "rgba(255,255,255,0.75)",
				scaleBeginAtZero: true,
				scaleBackdropPaddingY: 1,
				scaleBackdropPaddingX: 1,
				scaleShowLine: true,
				segmentShowStroke: true,
				segmentStrokeColor: "#fff",
				segmentStrokeWidth: 2,
				animationSteps: 100,
				animationEasing: "easeOutBounce",
				animateRotate: true,
				animateScale: false
			};
			var ctx = document.getElementById("polarChart").getContext("2d");
			var Polarchart = new Chart(ctx).PolarArea(polarData, polarOptions);

		});*/

		function alert_content_modal_show(id) {
			var request = $.ajax({
				url: "ajax.php?mod=system&content=alert_content_catch&ver=" + ver,
				method: "POST",
				data: {
					message: id
				},
				dataType: "text"
			});
			request.done(function(dat) {
				var data = JSON.parse(dat);
				var arr = [];
				for (var key in data) {
					arr[key] = data[key];
				}
				if (arr['success']) {
					$('#alert-title').html(arr['data']['title']);
					$('#alert-content').html('<h3><b>' + arr['data']['content'] + '</b></h3>');
				}

			});
			request.fail(function(jqXHR, textStatus) {
				alert("Request failed: " + textStatus);
			});

			$('#alert_content_modal').modal('show');
		}

		function logout() {
			var request = $.ajax({
				url: "ajax.php?mod=system&content=logout&ver=" + ver,
				method: "POST",
				data: {
					account: '<?php echo $admin['id']; ?>'
				},
				dataType: "text"
			});
			request.done(function(dat) {
				var data = JSON.parse(dat);
				var arr = [];
				for (var key in data) {
					arr[key] = data[key];
				}
				if (arr['success']) {
					location.href = 'index.php?ver=';
				}

			});
			request.fail(function(jqXHR, textStatus) {
				alert("Request failed: " + textStatus);
			});

		}

		function line_message_modal_open() {
			//var mode = document.getElementById('classes').value;
			$('#line_message_modal').modal({
				backdrop: 'static',
				keyboard: false
			});
		}

		function content_set(code) {
			edit_textbox = '';
			if (code == 0) {
				$('#content_edit').html('');
			} else if (code == 1) {
				$('#content_edit').html('');
				edit_textbox += '<div class="form-group"><label>標題</label> <input type="text" placeholder="輸入標題。" id="title" name="title" class="form-control"></div>';
				edit_textbox += '<div class="form-group"><label>內文</label> <input type="text" placeholder="輸入內文。" id="content" name="content" class="form-control"></div>';
				edit_textbox += '<div class="form-group"><label>圖片</label> <input type="file" placeholder="上傳圖片。" id="pic" name="pic" class="form-control"></div>';
			} else if (code == 2) {
				$('#content_edit').html('');
				edit_textbox += '<div class="form-group"><label>內文</label> <input type="text" placeholder="輸入內文。" id="content" name="content" class="form-control"></div>';
			} else if (code == 3) {
				$('#content_edit').html('');
				edit_textbox += '<div class="form-group"><label>圖片</label> <input type="file" placeholder="上傳圖片。" id="pic" name="pic" class="form-control"></div>';
			}
			$('#content_edit').html(edit_textbox);
		}

		function target_set(code) {
			edit_textbox = '';
			if (code == 1) {
				search_member();
				$('#target-member').show();
			} else if (code == 2) {
				search_group();
				$('#target-group').show();
			}
		}
	</script>
	<?php
	if (isset($_GET['mod']) && isset($_GET['content'])) {
		$script = '../mod/' . $_GET['mod'] . '/' . $_GET['content'] . '_script.php';
		include_once($script);
	}
	?>

</body>

</html>