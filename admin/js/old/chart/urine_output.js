//日線圖
function getBT_Line_Day(arr) {
	var lineData = {
		labels: getTimeArray(arr['urine_output_time'],arr['week']),
		datasets: [
			{
				label: "排尿量",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(36,117,188,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(36,117,188,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: getArrayByString(arr['urine_output'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 1000,
					stepSize: 200
				}
			}]
		}
	};

	var ctx = document.getElementById("bt_line_day").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//週線圖
function getBT_Line_Week(arr) {
	var lineData = {
		datasets: [
			{
				label: "排尿量",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(36,117,188,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(36,117,188,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_BT_data(arr['TEM_Week'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 3000,
					stepSize: 500
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("bt_line_week_month").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//月線圖
function getBT_Line_Month(arr) {
	var lineData = {
		datasets: [
			{
				label: "排尿量",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(36,117,188,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(36,117,188,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_BT_data(arr['urine_output'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 3000,
					stepSize: 500
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("bt_line_week_month").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}


//判斷週月季
function bt_Line(arr) {
	var tem={};
	if (arr['month']) {
		tem = getBT_Line_Month(arr);
	} else if (arr['week']) {
		tem = getBT_Line_Week(arr);
	} else {
		tem = getBT_Line_Day(arr);
	}
	return tem;
}

//取得資料
function get_BT_data(arr) {
	var tem = [];
	for (var z in arr) {
		tem.push(arr[z]);
	}
	return tem;
}

function Creat_UO_Chart(arr) {
	var tem = {};
	tem = Draw_UO_Line_Chart(arr);
	return tem;
}

function Draw_UO_Line_Chart(arr) {
	var lineData = {
		datasets: [
			{
				label: "排尿量",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(36,117,188,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(36,117,188,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_BT_data(arr['chart_ArrayData'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 3000,
					stepSize: 500
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['chart_StartDate'],
					max: arr['chart_EndDate']
				}
			}]
		}
	};

	var ctx = document.getElementById("uo_line_chart").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}