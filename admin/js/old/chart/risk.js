//心臟風險日線圖
function getRisk_Day_Line(arr) {
	var lineData = {
		labels: getArrayByString(arr['time']),
		datasets: [
			{
				label: "心臟健康指數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2E9B90",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2E9B90",
				pointRadius: 2,
				borderWidth: 1,
				data: getArrayByString(arr['health'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 100,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 0,
					max: 99,
					stepSize: 33,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 33) label_x = value+'以下正常範圍';
						if (value == 66) label_x = value+'以上高風險';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(255,179,179,1)'],
					borderDash:[10,10]
				}
			}]
		}
	};

	var ctx = document.getElementById("health_risk").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//心臟風險週線圖
function getRisk_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "心臟健康指數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2E9B90",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2E9B90",
				pointRadius: 2,
				borderWidth: 1,
				data: get_RISK_data(arr['health'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 100,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 0,
					max: 99,
					stepSize: 33,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 33) label_x = value+'以下正常範圍';
						if (value == 66) label_x = value+'以上高風險';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(255,179,179,1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("health_risk").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//心臟風險月線圖
function getRisk_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "心臟健康指數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2E9B90",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2E9B90",
				pointRadius: 2,
				borderWidth: 1,
				data: get_RISK_data(arr['health'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 100,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 0,
					max: 99,
					stepSize: 33,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 33) label_x = value+'以下正常範圍';
						if (value == 66) label_x = value+'以上高風險';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(255,179,179,1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("health_risk").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//血管彈性日線圖
function getElasticity_Day_Line(arr) {
	var lineData = {
		labels: getArrayByString(arr['time']),
		datasets: [
			{
				label: "血管彈性指數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2E9B90",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2E9B90",
				pointRadius: 2,
				borderWidth: 1,
				data: getArrayByString(arr['elasticity'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 100,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 0,
					max: 99,
					stepSize: 33,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 33) label_x = value+'以下正常範圍';
						if (value == 66) label_x = value+'以上高風險';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(255,179,179,1)'],
					borderDash:[10,10]
				}
			}]
		}
	};

	var ctx = document.getElementById("elasticity").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//血管彈性週線圖
function getElasticity_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "血管彈性指數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2E9B90",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2E9B90",
				pointRadius: 2,
				borderWidth: 1,
				data: get_RISK_data(arr['elasticity'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 100,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 0,
					max: 99,
					stepSize: 33,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 33) label_x = value+'以下正常範圍';
						if (value == 66) label_x = value+'以上高風險';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(255,179,179,1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("elasticity").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//血管彈性月線圖
function getElasticity_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "血管彈性指數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2E9B90",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2E9B90",
				pointRadius: 2,
				borderWidth: 1,
				data: get_RISK_data(arr['elasticity'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 100,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 0,
					max: 99,
					stepSize: 33,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 33) label_x = value+'以下正常範圍';
						if (value == 66) label_x = value+'以上高風險';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(255,179,179,1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("elasticity").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//心搏規律日線圖
function getFrequency_Day_Line(arr) {
	var lineData = {
		labels: getArrayByString(arr['time']),
		datasets: [
			{
				label: "心搏規律指數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2E9B90",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2E9B90",
				pointRadius: 2,
				borderWidth: 1,
				data: getArrayByString(arr['cycle'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 100,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 0,
					max: 100,
					stepSize: 50,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						var label_y = '';
						if (value == 50) {
							label_x = '51以上高風險';
							label_y = value+'以下低風險';
							var label = new Array(label_x, label_y);
						}
						return label;
					}
				},
				gridLines: {
					color:['rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(0,0,0,0)'],
					borderDash:[10,10]
				}
			}]
		}
	};

	var ctx = document.getElementById("frequency").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//心搏規律週線圖
function getFrequency_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "心搏規律指數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2E9B90",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2E9B90",
				pointRadius: 2,
				borderWidth: 1,
				data: get_RISK_data(arr['cycle'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 100,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 0,
					max: 100,
					stepSize: 50,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						var label_y = '';
						if (value == 50) {
							label_x = '51以上高風險';
							label_y = value+'以下低風險';
							var label = new Array(label_x, label_y);
						}
						return label;
					}
				},
				gridLines: {
					color:['rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(0,0,0,0)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("frequency").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//心搏規律月線圖
function getFrequency_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "心搏規律指數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2E9B90",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2E9B90",
				pointRadius: 2,
				borderWidth: 1,
				data: get_RISK_data(arr['cycle'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 100,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 0,
					max: 100,
					stepSize: 50,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						var label_y = '';
						if (value == 50) {
							label_x = '51以上高風險';
							label_y = value+'以下低風險';
							var label = new Array(label_x, label_y);
						}
						return label;
					}
				},
				gridLines: {
					color:['rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(0,0,0,0)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("frequency").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//心臟風險日週月判斷
function Health_Risk_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getRisk_Month_Line(arr);
	} else if(arr['week']) {
		tem = getRisk_Week_Line(arr);
	} else {
		tem = getRisk_Day_Line(arr);
	}
	return tem;
}

//血管彈性日週月判斷
function Elasticity_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getElasticity_Month_Line(arr);
	} else if(arr['week']) {
		tem = getElasticity_Week_Line(arr);
	} else {
		tem = getElasticity_Day_Line(arr);
	}
	return tem;
}

//心搏規律日週月判斷
function Frequency_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getFrequency_Month_Line(arr);
	} else if(arr['week']) {
		tem = getFrequency_Week_Line(arr);
	} else {
		tem = getFrequency_Day_Line(arr);
	}
	return tem;
}

//取得資料
function get_RISK_data(arr) {
	var tem = [];
	for (var z in arr) {
		tem.push(arr[z]);
	}
	return tem;
}