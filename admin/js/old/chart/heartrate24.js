//日線圖
function getHR24_Day_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "心率",
				backgroundColor: 'rgba(146,163,67,0)',
				borderColor: "#F69679",
				pointRadius: 0,
				borderWidth: 1,
				data: get_HR24_data(arr['hr'])
			}
		]
	};

	var lineOptions = {
		legend: {
    		display: false
        },
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20,
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 160) label_x = '';
						if (value == 100) label_x = '';
						if (value == 40) label_x = '';
						return label_x; 
					}
				},
				gridLines: {
					color:['rgba(255,255,255,0)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'minute',
					parser: 'HH mm',
					unitStepSize: 60,
					displayFormats: {
						minute: 'HH:mm',
						hour: 'HH:mm',
						day: 'HH:mm',
						month: 'HH:mm'
					 },
					 min: arr['Start_Date'],
					 max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//週線圖
function getHR24_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "心率",
				backgroundColor: 'rgba(146,163,67,0)',
				borderColor: "#F69679",
				pointRadius: 0,
				borderWidth: 1,
				data: get_HR24_data(arr['hr'])
			}
		]
	};

	var lineOptions = {
		legend: {
    		display: false
        },
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20,
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 160) label_x = '';
						if (value == 100) label_x = '';
						if (value == 40) label_x = '';
						return label_x; 
					}
				},
				gridLines: {
					color:['rgba(255,255,255,0)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'minute',
					parser:'YYYY M D HH mm',
					unitStepSize: 1440,
					displayFormats: {
						minute:'M-D',
						hour:'M-D',
						day:'YYYY-M-D',
						month: 'YYYY-M-D'
					 },
					 min: arr['Start_Date'],
					 max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//月線圖
function getHR24_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "心率",
				backgroundColor: 'rgba(146,163,67,0)',
				borderColor: "#F69679",
				pointRadius: 0,
				borderWidth: 1,
				data: get_HR24_data(arr['hr'])
			}
		]
	};

	var lineOptions = {
		legend: {
    		display: false
        },
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20,
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 160) label_x = '';
						if (value == 100) label_x = '';
						if (value == 40) label_x = '';
						return label_x; 
					}
				},
				gridLines: {
					color:['rgba(255,255,255,0)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser:'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute:'D',
						hour:'M-D',
						day:'YYYY-M-D',
						month: 'YYYY-M-D'
					 },
					 min: arr['Start_Date'],
					 max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//判斷日週月
function hr_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getHR24_Month_Line(arr);
	} else if (arr['week']) {
		tem = getHR24_Week_Line(arr);
	} else {
		tem = getHR24_Day_Line(arr);
	}
	return tem;
}

//取得資料
function get_HR24_data(arr) {
	var tem = [];
	for (var z in arr) {
		tem.push(arr[z]);
	}
	return tem;
}