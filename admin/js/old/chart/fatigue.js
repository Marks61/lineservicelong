//疲勞度日線圖
function getFatigue_Day_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "疲勞指數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#529AD1",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#529AD1",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['fatigue'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 100,
					stepSize: 20
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser:'YYYY M D HH mm',
					unitStepSize: 1,
					displayFormats: {
						minute:'HH:mm',
						hour:'HH:mm',
						day:'HH:mm',
						month: 'HH:mm'
					},
					min: arr['Start_Time'],
					max: arr['End_Time']
				}
			}]
		}
	};

	var ctx = document.getElementById("fatigue_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//疲勞度週線圖
function getFatigue_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "疲勞指數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#529AD1",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#529AD1",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['fatigue'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 100,
					stepSize: 20
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'M-D',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("fatigue_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//疲勞度月線圖
function getFatigue_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "疲勞指數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#529AD1",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['fatigue'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 100,
					stepSize: 20
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("fatigue_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//里程日柱圖
function getMileage_Day_Bar(arr) {
	var barData = {
		datasets: [
			{
				label: "里程",
				backgroundColor: "#62BB8E",
				borderColor: "#62BB8E",
				data: get_data(arr['mileage'])
			}
		]
	};

	var barOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'day',
					parser:'YYYY M D HH mm',
					unitStepSize: 1,
					displayFormats: {
						minute:'HH:mm',
						hour:'HH',
						day:'M-D',
						month: 'HH:mm'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx2 = document.getElementById("mileage_line").getContext("2d");
	new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});
}

//里程週線圖
function getMileage_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "里程",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#62BB8E",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#62BB8E",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['mileage'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'M-D',
						day: 'M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("mileage_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//里程月線圖
function getMileage_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "里程",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#62BB8E",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['mileage'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("mileage_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//卡路里日柱圖
function getCalories_Day_Bar(arr) {
	var barData = {
		datasets: [
			{
				label: "卡路里",
				backgroundColor: "#F69679",
				borderColor: "#F69679",
				data: get_data(arr['calories'])
			}
		]
	};

	var barOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'day',
					parser:'YYYY M D HH mm',
					unitStepSize: 1,
					displayFormats: {
						minute:'HH:mm',
						hour:'HH',
						day:'M-D',
						month: 'HH:mm'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx2 = document.getElementById("calories_line").getContext("2d");
	new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});
}

//卡路里週線圖
function getCalories_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "卡路里",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#F69679",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#F69679",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['calories'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'M-D',
						day: 'M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("calories_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//卡路里月線圖
function getCalories_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "卡路里",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#F69679",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['calories'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("calories_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//步數日柱圖
function getSteps_Day_Bar(arr) {
	var barData = {
		datasets: [
			{
				label: "步數",
				backgroundColor: "#2E9B90",
				borderColor: "#2E9B90",
				data: get_data(arr['steps'])
			}
		]
	};

	var barOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'day',
					parser:'YYYY M D HH mm',
					unitStepSize: 1,
					displayFormats: {
						minute:'HH:mm',
						hour:'HH',
						day:'M-D',
						month: 'HH:mm'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx2 = document.getElementById("steps_line").getContext("2d");
	new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});
}

//步數週線圖
function getSteps_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "步數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2E9B90",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2E9B90",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['steps'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'M-D',
						day: 'M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("steps_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//步數月線圖
function getSteps_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "步數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2E9B90",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['steps'])
			}
		]
	};

	var lineOptions = {
		legend: {
			labels: {
				fontFamily: "微軟正黑體",
				fontSize: 20
			}
		},
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("steps_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//疲勞度日週月判斷
function fatigue_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getFatigue_Month_Line(arr);
	} else if(arr['week']) {
		tem = getFatigue_Week_Line(arr);
	} else {
		tem = getFatigue_Day_Line(arr);
	}
	return tem;
}

//里程日週月判斷
function mileage_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getMileage_Month_Line(arr);
	} else if(arr['week']) {
		tem = getMileage_Week_Line(arr);
	} else {
		tem = getMileage_Day_Bar(arr);
	}
	return tem;
}

//卡路里日週月判斷
function calories_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getCalories_Month_Line(arr);
	} else if(arr['week']) {
		tem = getCalories_Week_Line(arr);
	} else {
		tem = getCalories_Day_Bar(arr);
	}
	return tem;
}

//步數日週月判斷
function steps_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getSteps_Month_Line(arr);
	} else if(arr['week']) {
		tem = getSteps_Week_Line(arr);
	} else {
		tem = getSteps_Day_Bar(arr);
	}
	return tem;
}
//取得資料
function get_data(arr) {
	var tem = [];
	for (var z in arr) {
		tem.push(arr[z]);
	}
	return tem;
}