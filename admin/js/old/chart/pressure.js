//日線圖
function getBP_Day_Line(arr) {
	var lineData = {
		labels: getTimeArray(arr['time'],arr['week']),
		datasets: [
			{
				label: "收縮壓",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(36,117,188,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(36,117,188,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: getArrayByString(arr['sys'])
			},
			{
				label: "舒張壓",
                backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(85,187,177,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(85,187,177,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: getArrayByString(arr['dia'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 40,
					max: 200,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 40,
					max: 200,
					stepSize: 10,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 130) label_x = '收縮壓過高';
						if (value == 80) label_x = '舒張壓過高';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)',
							'rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(0,0,0,0)','rgba(0,0,0,0)',
							'rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(0,0,0,0)','rgba(0,0,0,0)',
							'rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)'],
					borderDash:[10,10]
				}
			}]
		}
	};

	var ctx = document.getElementById("bp_line_day_week").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//週線圖
function getBP_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "收縮壓",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(36,117,188,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(36,117,188,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_BP_data(arr['SYS_Week'])
			},
			{
				label: "舒張壓",
                backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(85,187,177,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(85,187,177,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_BP_data(arr['DIA_Week'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 40,
					max: 200,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 40,
					max: 200,
					stepSize: 10,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 130) label_x = '收縮壓過高';
						if (value == 80) label_x = '舒張壓過高';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)',
							'rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(0,0,0,0)','rgba(0,0,0,0)',
							'rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(0,0,0,0)','rgba(0,0,0,0)',
							'rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("bp_line_day_week").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//月線圖
function getBP_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "收縮壓",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(36,117,188,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(36,117,188,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_BP_data(arr['SYS_Week'])
			},
			{
				label: "舒張壓",
                backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(85,187,177,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(85,187,177,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_BP_data(arr['DIA_Week'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 40,
					max: 200,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 40,
					max: 200,
					stepSize: 10,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 130) label_x = '收縮壓過高';
						if (value == 80) label_x = '舒張壓過高';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)',
							'rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(0,0,0,0)','rgba(0,0,0,0)',
							'rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(0,0,0,0)','rgba(0,0,0,0)',
							'rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("bp_line_month").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//日週月判斷
function bp_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getBP_Month_Line(arr);
	} else if(arr['week']) {
		tem = getBP_Week_Line(arr);
	} else {
		tem = getBP_Day_Line(arr);
	}
	return tem;
}

//取得資料
function get_BP_data(arr) {
	var tem = [];
	for (var z in arr) {
		tem.push(arr[z]);
	}
	return tem;
}