//日線圖
function getHR_Day_Line(arr) {
	var lineData = {
		labels: getTimeArray(arr['time_day'], 0),
		datasets: [
			{
				label: "靜態心率",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#F69679",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#F69679",
				pointRadius: 2,
				borderWidth: 1,
				data: getArrayByString(arr['heartrate_day'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 160) label_x = '過高';
						if (value == 100) label_x = '';
						if (value == 40) label_x = '過低';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(255,255,255,0)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)'],
					borderDash:[10,10]
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line_chart").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//週線圖
function getHR_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "靜態心率",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(235,147,147,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(235,147,147,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_HR_data(arr['HR_Week'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 160) label_x = '過高';
						if (value == 100) label_x = '';
						if (value == 40) label_x = '過低';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(255,255,255,0)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line_day_week").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//月線圖
function getHR_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "靜態心率",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(235,147,147,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(235,147,147,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_HR_data(arr['HR_Week'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 160) label_x = '過高';
						if (value == 100) label_x = '';
						if (value == 40) label_x = '過低';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(255,255,255,0)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)',
					'rgba(255,255,255,0)','rgba(255,179,179,1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line_month").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//日週月判斷
function hr_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getHR_Month_Line(arr);
	} else if(arr['week']) {
		tem = getHR_Week_Line(arr);
	} else {
		tem = getHR_Day_Line(arr);
	}
	return tem;
}

//取得資料
function get_HR_data(arr) {
	var tem = [];
	for (var z in arr) {
		tem.push(arr[z]);
	}
	return tem;
}

function Creat_HR_Chart(arr) {
	var tem = {};
	if (arr['type'] == 'day') {
		tem = getHR_Day_Line(arr);
	} else {
		tem = Draw_HR_Line_Chart_range(arr);
	}
	return tem;
}

function Draw_HR_Line_Chart_range(arr) {
	var lineData = {
		datasets: [
			{
				label: "靜態心率",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#F69679",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#F69679",
				pointRadius: 2,
				borderWidth: 1,
				data: get_HR_data(arr['chart_ArrayData']['hr_range'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: arr['hr_seq']['chart_min'],
					max: arr['hr_seq']['chart_max'],
					stepSize: arr['hr_seq']['all_d'],
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == arr['hr_seq']['limit_high']) label_x = ' 過高';
						// if (value == 100) label_x = '';
						if (value == arr['hr_seq']['limit_low']) label_x = ' 過低';
						return label_x;
					}
				},
				gridLines: {
					color: arr['hr_Color'],
					// color:['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0)', 'rgba(255, 153, 153, 1)', 'rgba(0, 0, 0, 0)'
					// , 'rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0)', 'rgba(255, 153, 153, 1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['chart_StartDate'],
					max: arr['chart_EndDate']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line_chart").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}