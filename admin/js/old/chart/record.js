//心率日線圖
function getHR_Day_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "心率",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#F69679",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#F69679",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['hr'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20,
					callback: function(value, index, values) {
						var label_x = '';
						if (value==40) label_x = '';
						if (value==160) label_x = '';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(255,255,255,0)','rgba(255,255,255,0)','rgba(255,255,255,0)',
							'rgba(215,3,3,1)','rgba(255,255,255,0)','rgba(255,255,255,0)',
							'rgba(255,255,255,0)','rgba(255,255,255,0)',
							'rgba(255,255,255,0)','rgba(215,3,3,1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'minute',
					parser:'YYYY M D HH mm',
					unitStepSize: arr['minutes'],
					displayFormats: {
						minute:'HH:mm',
						hour:'HH:mm',
						day:'HH:mm',
						month: 'HH:mm'
					},
					min: arr['Start_Time'],
					max: arr['End_Time']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line_"+arr['sport_id']).getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//心率週線圖
function getHR_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "平均心率",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#F69679",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#F69679",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['hr'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'left',
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20,
					callback: function(value, index, values) {
						var label_x = '';
						if (value==40) label_x = '';
						if (value==160) label_x = '';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(255,255,255,0)','rgba(255,255,255,0)','rgba(255,255,255,0)',
							'rgba(215,3,3,1)','rgba(255,255,255,0)','rgba(255,255,255,0)',
							'rgba(255,255,255,0)','rgba(255,255,255,0)','rgba(255,255,255,0)','rgba(215,3,3,1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'minute',
					parser: 'YYYY M D HH mm',
					unitStepSize: 1440,
					displayFormats: {
						minute: 'M-D',
						hour: 'M-D',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line_0").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//心率月線圖
function getHR_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "心率",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#F69679",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['hr'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 20,
					max: 220,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks:{
					min: 20,
					max: 220,
					stepSize: 20,
					callback: function(value, index, values) {
						var label_x = '';
						if (value==40) label_x = '';
						if (value==100) label_x = '';
						if (value==160) label_x = '';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(255,255,255,0)','rgba(255,255,255,0)','rgba(255,255,255,0)',
							'rgba(215,3,3,1)','rgba(255,255,255,0)','rgba(255,255,255,0)',
							'rgba(215,3,3,1)','rgba(255,255,255,0)','rgba(255,255,255,0)','rgba(215,3,3,1)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'D',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line_0").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//卡路里週線圖
function getCalories_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "卡路里",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(235,147,147,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(235,147,147,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['calories'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'minute',
					parser: 'YYYY M D',
					unitStepSize: 1440,
					displayFormats: {
						minute: 'M-D',
						hour: 'M-D',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("calories_line_0").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//卡路里月線圖
function getCalories_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "卡路里",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(235,147,147,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['calories'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'D',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("calories_line_0").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//步數週線圖
function getSteps_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "步數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#ffac80",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#ffac80",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['steps'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'minute',
					parser: 'YYYY M D',
					unitStepSize: 1440,
					displayFormats: {
						minute: 'M-D',
						hour: 'M-D',
						day: 'M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("steps_line_0").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//步數月線圖
function getSteps_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "步數",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#ffac80",
				pointRadius: 2,
				borderWidth: 1,
				data: get_data(arr['steps'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'D',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("steps_line_0").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//心率日週月判斷
function heart_rate_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getHR_Month_Line(arr);
	} else if(arr['week']) {
		tem = getHR_Week_Line(arr);
	} else {
		tem = getHR_Day_Line(arr);
	}
	return tem;
}

//卡路里日週月判斷
function calories_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getCalories_Month_Line(arr);
	} else if(arr['week']) {
		tem = getCalories_Week_Line(arr);
	}
	return tem;
}

//步數日週月判斷
function steps_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getSteps_Month_Line(arr);
	} else if(arr['week']) {
		tem = getSteps_Week_Line(arr);
	}
	return tem;
}
//取得資料
function get_data(arr) {
	var tem = [];
	for (var z in arr) {
		tem.push(arr[z]);
	}
	return tem;
}