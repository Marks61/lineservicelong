//日線圖
function getBO_Day_Line(arr) {
	var lineData = {
		labels: getTimeArray(arr['time_day'], 0),
		datasets: [
			{
				label: "血氧",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#F69679",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#F69679",
				pointRadius: 2,
				borderWidth: 1,
				data: getArrayByString(arr['spo2_day'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 60,
					max: 100,
					stepSize: 5
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 60,
					max: 100,
					stepSize: 10,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 90) label_x = '過低';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)','rgba(255,255,255,0)','rgba(255,255,255,0)'],
					borderDash:[10,10]
				}
			}]
		}
	};

	var ctx = document.getElementById("spo2_line_chart").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//週線圖
function getBO_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "血氧",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(255,172,128,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(255,172,128,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_BO_data(arr['BO_Week'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 60,
					max: 100,
					stepSize: 5
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 60,
					max: 100,
					stepSize: 10,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 90) label_x = '過低';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)','rgba(255,255,255,0)','rgba(255,255,255,0)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("bo_line_day_week").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//月線圖
function getBO_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "血氧",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "rgba(255,172,128,1)",
				pointBackgroundColor: "#fff",
				pointBorderColor: "rgba(255,172,128,1)",
				pointRadius: 2,
				borderWidth: 1,
				data: get_BO_data(arr['BO_Week'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 60,
					max: 100,
					stepSize: 5
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 60,
					max: 100,
					stepSize: 10,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == 90) label_x = '過低';
						return label_x;
					}
				},
				gridLines: {
					color:['rgba(255,255,255,0)','rgba(255,179,179,1)','rgba(255,255,255,0)','rgba(255,255,255,0)','rgba(255,255,255,0)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("bo_line_month").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//日週月判斷
function bo_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getBO_Month_Line(arr);
	} else if(arr['week']) {
		tem = getBO_Week_Line(arr);
	} else {
		tem = getBO_Day_Line(arr);
	}
	return tem;
}

//取得資料
function get_BO_data(arr) {
	var tem = [];
	for (var z in arr) {
		tem.push(arr[z]);
	}
	return tem;
}

function Creat_BO_Chart(arr) {
	var tem = {};
	if (arr['type'] == 'day') {
		tem = getBO_Day_Line(arr);
	} else {
		tem = Draw_BO_Line_Chart_range(arr);
	}
	return tem;
}

function Draw_BO_Line_Chart_range(arr) {
	var lineData = {
		datasets: [
			{
				label: "血氧",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#F69679",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#F69679",
				pointRadius: 2,
				borderWidth: 1,
				data: get_BO_data(arr['chart_ArrayData']['spo2_range'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false,
			callbacks: {
				title: function(tooltipItem, data) {
					var tipArray = tooltipItem[0].xLabel;
					tipArray = tipArray.split("_");
					return tipArray[0];
				}
			}
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 60,
					max: 100,
					stepSize: 5
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: arr['spo2_seq']['chart_min'],
					max: arr['spo2_seq']['chart_max'],
					stepSize: arr['spo2_seq']['all_d'],
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == arr['spo2_seq']['limit_high']) label_x = ' 過低';
						return label_x;
					}
				},
				gridLines: {
					color: arr['spo2_Color'],
					// color:['rgba(0, 0, 0, 0)', 'rgba(255, 153, 153, 1)', 'rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['chart_StartDate'],
					max: arr['chart_EndDate']
				}
			}]
		}
	};

	var ctx = document.getElementById("spo2_line_chart").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}