//日線圖
function getSPO2_24_Day_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "血氧",
				backgroundColor: 'rgba(146,163,67,0)',
				borderColor: "#F69679",
				pointRadius: 0,
				borderWidth: 1,
				data: get_SPO2_24_data(arr['spo2'])
			}
		]
	};

	var lineOptions = {
		legend: {
    		display: false
        },
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 60,
					max: 105,
					stepSize: 5
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: arr['spo2_seq']['chart_min'],
					max: arr['spo2_seq']['chart_max'],
					stepSize: arr['spo2_seq']['all_d'],
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == arr['spo2_seq']['limit_high']) label_x = ' 過低';
						// if (value == 100) label_x = '';
						// if (value == arr['spo2_seq']['limit_low']) label_x = ' 過低';
						return label_x;
					}
				},
				gridLines: {
					color: arr['spo2_Color'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'minute',
					parser: 'HH mm',
					unitStepSize: 60,
					displayFormats: {
						minute: 'HH:mm',
						hour: 'HH:mm',
						day: 'HH:mm',
						month: 'HH:mm'
					 },
					 min: arr['Start_Date'],
					 max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//週線圖
function getSPO2_24_Week_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "血氧",
				backgroundColor: 'rgba(146,163,67,0)',
				borderColor: "#F69679",
				pointRadius: 0,
				borderWidth: 1,
				data: get_SPO2_24_data(arr['spo2'])
			}
		]
	};

	var lineOptions = {
		legend: {
    		display: false
        },
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 60,
					max: 105,
					stepSize: 5
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: arr['spo2_seq']['chart_min'],
					max: arr['spo2_seq']['chart_max'],
					stepSize: arr['spo2_seq']['all_d'],
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == arr['spo2_seq']['limit_high']) label_x = ' 過低';
						// if (value == 100) label_x = '';
						// if (value == arr['spo2_seq']['limit_low']) label_x = ' 過低';
						return label_x;
					}
				},
				gridLines: {
					color: arr['spo2_Color'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'minute',
					parser:'YYYY M D HH mm',
					unitStepSize: 1440,
					displayFormats: {
						minute:'M-D',
						hour:'M-D',
						day:'YYYY-M-D',
						month: 'YYYY-M-D'
					 },
					 min: arr['Start_Date'],
					 max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//月線圖
function getSPO2_24_Month_Line(arr) {
	var lineData = {
		datasets: [
			{
				label: "血氧",
				backgroundColor: 'rgba(146,163,67,0)',
				borderColor: "#F69679",
				pointRadius: 0,
				borderWidth: 1,
				data: get_SPO2_24_data(arr['spo2'])
			}
		]
	};

	var lineOptions = {
		legend: {
    		display: false
        },
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false,
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 60,
					max: 105,
					stepSize: 5
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: arr['spo2_seq']['chart_min'],
					max: arr['spo2_seq']['chart_max'],
					stepSize: arr['spo2_seq']['all_d'],
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_x = '';
						if (value == arr['spo2_seq']['limit_high']) label_x = ' 過低';
						// if (value == 100) label_x = '';
						// if (value == arr['spo2_seq']['limit_low']) label_x = ' 過低';
						return label_x;
					}
				},
				gridLines: {
					color: arr['spo2_Color'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser:'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute:'D',
						hour:'M-D',
						day:'YYYY-M-D',
						month: 'YYYY-M-D'
					 },
					 min: arr['Start_Date'],
					 max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//判斷日週月
function hr_Line(arr) {
	var tem = {};
	if (arr['month']) {
		tem = getSPO2_24_Month_Line(arr);
	} else if (arr['week']) {
		tem = getSPO2_24_Week_Line(arr);
	} else {
		tem = getSPO2_24_Day_Line(arr);
	}
	return tem;
}

//取得資料
function get_SPO2_24_data(arr) {
	var tem = [];
	for (var z in arr) {
		tem.push(arr[z]);
	}
	return tem;
}