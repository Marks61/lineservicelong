//深淺眠柱圖
function getSleep_Bar_Day(arr) {
	var barData = {
        labels: getArrayByString(arr['time']),
        datasets: [
            {
                label: "淺眠",
                stack:'Stack 0',
                backgroundColor: "#62BBAC",
                data: getArrayByString(arr['hour12_1'])
            },
            {
                label: "深睡",
                stack:'Stack 0',
                backgroundColor: "#176E78",
                data: getArrayByString(arr['hour12_2'])
            },
            {
                label: "活動",
                stack:'Stack 0',
                backgroundColor: "#EEF2EE",
                data: getArrayByString(arr['hour12_3'])
            }
        ]
    };
	
    var barOptions = {
    	tooltips: {
	        enabled: false
	    },
    	legend: {
    		display: false
        },
        responsive: true,
        //設置繪製圖大小
		maintainAspectRatio: false,
		scales: {
			xAxes: [{
			  gridLines: {
			    display: false,
			  },
			}],
			yAxes: [{
				ticks: {
					min: 0,
					max: 1,
					stepSize: 1,
					display: false
				}
			}]
		}
    };


    var ctx2 = document.getElementById("sleep_bar").getContext("2d");
    new Chart(ctx2, {type: 'bar', data: barData, options: barOptions});
}

//心率線圖
function getHR_Line_Day(arr) {
	var lineData = {
		datasets: [
			{
				label: "心率",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#F69679",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#F69679",
				pointRadius: 0,
  				borderWidth: 1,
				data: get_data(arr['hr'])
			}
		]
	};
	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		legend: {
    		display: false
        },
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 20,
					max: 200,
					stepSize: 20
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser:'YYYY-M-D HH mm',
					unitStepSize: 1,
					displayFormats: {
						minute:'HH:mm',
						hour:'HH:mm',
						day:'HH:mm',
						month: 'HH:mm',
						year:'HH:mm'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("hr_line").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//取得資料
function get_data(arr) {
	var tem = [];
	for (var z in arr) {
		tem.push(arr[z]);
	}
	return tem;
}