//週線圖
function getW_Line_Week(arr) {
	var lineData = {
			labels: getTimeArray(arr['time'],arr['week']),
			datasets: [
				{
					label: "體重",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#2475BC",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#2475BC",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['weight'])
				}
			]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 160,
					stepSize: 20
				}
			}]
		}
	};

	var ctx = document.getElementById("w_line_week_month").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//月線圖
function getW_Line_Month(arr) {
	var lineData = {
		datasets: [
			{
				label: "體重",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2475BC",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2475BC",
				pointRadius: 2,
				borderWidth: 1,
				data: get_W_data(arr['weight'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 160,
					stepSize: 20
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("w_line_week_month").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//季線圖
function getW_Line_Season(arr) {
	var lineData = {
		datasets: [
			{
				label: "體重",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2475BC",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2475BC",
				pointRadius: 2,
				borderWidth: 1,
				data: get_W_data(arr['weight'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 160,
					stepSize: 20
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				},
				ticks: {
					fontSize:12
				},
				scaleLabel: {
					display: true,
					labelString: arr['label']
				}
			}]
		}
	};

	var ctx = document.getElementById("w_line_season").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//判斷週月季
function w_Line(arr) {
	var tem={};
	if (arr['season']) {
		tem = getW_Line_Season(arr);
	} else if (arr['month']) {
		tem = getW_Line_Month(arr);
	} else if (arr['week']) {
		tem = getW_Line_Week(arr);
	} else {}
	return tem;
}

//取得資料
function get_W_data(arr) {
	var tem = [];
	for (var z in arr) {
		tem.push(arr[z]);
	}
	return tem;
}

function Creat_Weight_Chart(arr) {
	var tem = {};
	tem = Draw_Weight_Line_Chart(arr);
	return tem;
}

function Draw_Weight_Line_Chart(arr) {
	var lineData = {
		datasets: [
			{
				label: "體重",
				backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#2475BC",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#2475BC",
				pointRadius: 2,
				borderWidth: 1,
				data: get_W_data(arr['weight'])
			},
			{
				label: "BMI",
                backgroundColor: 'rgba(230,122,122,0)',
				borderColor: "#17786E",
				pointBackgroundColor: "#fff",
				pointBorderColor: "#17786E",
				pointRadius: 2,
				borderWidth: 1,
				data: get_W_data(arr['bmi'])
			}
		]
	};

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 160,
					stepSize: 20
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['chart_StartDate'],
					max: arr['chart_EndDate']
				}
			}]
		}
	};

	var ctx = document.getElementById("weight_line_chart").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}