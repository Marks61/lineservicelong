//日柱圖
function getBS_Bar_Day(arr) {
	if (arr['diabetes'] != '妊娠型糖尿病') {
		var barData = {
	        labels: ["早上", "中午", "晚上", "睡前"],
	        datasets: [
	        	{
	        	    label: "空腹",
	        	    backgroundColor: "#0A9177",
	        	    borderColor: "#0A9177",
	        	    data: [arr['sugar1']]
	        	},
	            {
	                label: "飯前",
	                backgroundColor: "#66C2A0",
	                borderColor: "#66C2A0",
	                data: ['', arr['sugar3'], arr['sugar5']]
	            },
	            {
	                label: "飯後",
	                backgroundColor: "#F68562",
	                borderColor: "#F68562",
	                data: [arr['sugar2'], arr['sugar4'], arr['sugar6']]
	            },
	            {
	                label: "睡前",
	                backgroundColor: "#529AD1",
	                borderColor: "#529AD1",
	                data: ['', '', '', arr['sugar7']]
	            }
	        ]
	    };
	}
    else {
    	var barData = {
	        labels: ["早上", "中午", "晚上", "睡前"],
	        datasets: [
	        	{
	        	    label: "空腹",
	        	    backgroundColor: "#0A9177",
	        	    borderColor: "#0A9177",
	        	    data: [arr['sugar1']]
	        	},
	            {
	                label: "飯前",
	                backgroundColor: "#66C2A0",
	                borderColor: "#66C2A0",
	                data: ['', arr['sugar3'], arr['sugar5']]
	            },
	            {
	                label: "飯後一小時",
	                backgroundColor: "#F8A974",
	                borderColor: "#F8A974",
	                data: [arr['sugar8'], arr['sugar9'], arr['sugar10']]
	            },
	            {
	                label: "飯後兩小時",
	                backgroundColor: "#F68562",
	                borderColor: "#F68562",
	                data: [arr['sugar2'], arr['sugar4'], arr['sugar6']]
	            },
	            {
	                label: "睡前",
	                backgroundColor: "#529AD1",
	                borderColor: "#529AD1",
	                data: ['', '', '', arr['sugar7']]
	            }
	        ]
	    };
    }

    var barOptions = {
        responsive: true,
        //設置繪製圖大小
		maintainAspectRatio: false,
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 200,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: arr['bs_seq']['chart_min'],
					max: arr['bs_seq']['chart_max'],
					stepSize: arr['bs_seq']['all_d'],
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_X = '';
						if (value == arr['bs_seq']['glu_low']) label_X = ' 過低';
						if (value ==arr['bs_seq']['limit_low']) label_X = ' 飯前過高';
						if (value ==arr['bs_seq']['limit_high']) label_X = ' 飯後過高';
						return label_X;
					}
				},
				gridLines: {
					color: arr['bs_Color'],
					borderDash: [10,10]
				}
			}]
		}
    };


    var ctx2 = document.getElementById("bs_line_chart").getContext("2d");
    new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});
}

//週線圖
function getBS_Line_Week(arr) {
	if (arr['diabetes'] != '妊娠型糖尿病') {
		var lineData = {
			labels: getTimeArray(arr['time'],arr['sugar_week']),
			datasets: [
				{
					label: "空腹",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar1'])
				},
				{
					label: "早飯後",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar2'])
				},
				{
					label: "午飯前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F3A06D",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F3A06D",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar3'])
				},
				{
					label: "午飯後",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar4'])
				},
				{
					label: "晚飯前",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F3A06D",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F3A06D",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar5'])
				},
				{
					label: "晚飯後",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar6'])
				},
				{
					label: "睡前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#529AD1",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#529AD1",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar7'])
				}
			]
		};
	} else {
		var lineData = {
			labels: getTimeArray(arr['time'],arr['sugar_week']),
			datasets: [
				{
					label: "空腹",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar1'])
				},
				{
					label: "早飯後一小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F8A974",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F8A974",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar2'])
				},
				{
					label: "早飯後兩小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar8'])
				},
				{
					label: "午飯前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F3A06D",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F3A06D",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar3'])
				},
				{
					label: "午飯後一小時",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F8A974",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F8A974",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar4'])
				},
				{
					label: "午飯後兩小時",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar9'])
				},
				{
					label: "晚飯前",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F3A06D",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F3A06D",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar5'])
				},
				{
					label: "晚飯後一小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F8A974",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F8A974",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar6'])
				},
				{
					label: "晚飯後兩小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar10'])
				},
				{
					label: "睡前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#529AD1",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#529AD1",
					pointRadius: 2,
					borderWidth: 1,
					data: getArrayByString(arr['sugar7'])
				}
			]
		};
	}

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 200,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 0,
					max: 200,
					stepSize: 20,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_X = '';
						if (value == 80) label_X = '過低';
						if (value == 120) label_X = '飯前過高';
						if (value == 180) label_X = '飯後過高';
						return label_X;
					}
				},
				gridLines: {
					color: ['rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(255,179,179,1)',
							'rgba(0,0,0,0)','rgba(255,159,5,1)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)',
							'rgba(0,0,0,0)'],
					borderDash:[10,10]
				}
			}]
		}
	};

	var ctx = document.getElementById("bs_line_week_month").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//月線圖
function getBS_Line_Month(arr) {
	if (arr['diabetes'] != '妊娠型糖尿病') {
		var lineData = {
			datasets: [
				{
					label: "空腹",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar1'])
				},
				{
					label: "早飯後",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar2'])
				},
				{
					label: "午飯前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F3A06D",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F3A06D",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar3'])
				},
				{
					label: "午飯後",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar4'])
				},
				{
					label: "晚飯前",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F3A06D",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F3A06D",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar5'])
				},
				{
					label: "晚飯後",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar6'])
				},
				{
					label: "睡前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#529AD1",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#529AD1",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar7'])
				}
			]
		};
	} else {
		var lineData = {
			datasets: [
				{
					label: "空腹",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar1'])
				},
				{
					label: "早飯後一小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F8A974",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F8A974",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar2'])
				},
				{
					label: "早飯後兩小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar8'])
				},
				{
					label: "午飯前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F3A06D",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F3A06D",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar3'])
				},
				{
					label: "午飯後一小時",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F8A974",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F8A974",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar4'])
				},
				{
					label: "午飯後兩小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar9'])
				},
				{
					label: "晚飯前",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F3A06D",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F3A06D",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar5'])
				},
				{
					label: "晚飯後一小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F8A974",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F8A974",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar6'])
				},
				{
					label: "晚飯後兩小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar10'])
				},
				{
					label: "睡前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#529AD1",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#529AD1",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar7'])
				}
			]
		};
	}

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 200,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 0,
					max: 200,
					stepSize: 20,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_X = '';
						if (value == 80) label_X = '過低';
						if (value == 120) label_X = '飯前過高';
						if (value == 180) label_X = '飯後過高';
						return label_X;
					}
				},
				gridLines: {
					color: ['rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(255,179,179,1)',
							'rgba(0,0,0,0)','rgba(255,159,5,1)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)',
							'rgba(0,0,0,0)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				}
			}]
		}
	};

	var ctx = document.getElementById("bs_line_week_month").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//季線圖
function getBS_Line_Season(arr) {
	if (arr['diabetes'] != '妊娠型糖尿病') {
		var lineData = {
			datasets: [
				{
					label: "空腹",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar1'])
				},
				{
					label: "早飯後",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar2'])
				},
				{
					label: "午飯前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F3A06D",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F3A06D",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar3'])
				},
				{
					label: "午飯後",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar4'])
				},
				{
					label: "晚飯前",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F3A06D",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F3A06D",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar5'])
				},
				{
					label: "晚飯後",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar6'])
				},
				{
					label: "睡前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#529AD1",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#529AD1",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar7'])
				}
			]
		};
	} else {
		var lineData = {
			datasets: [
				{
					label: "空腹",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar1'])
				},
				{
					label: "早飯後一小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F8A974",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F8A974",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar2'])
				},
				{
					label: "早飯後兩小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar8'])
				},
				{
					label: "午飯前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F3A06D",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F3A06D",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar3'])
				},
				{
					label: "午飯後一小時",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F8A974",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F8A974",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar4'])
				},
				{
					label: "午飯後兩小時",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar9'])
				},
				{
					label: "晚飯前",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F3A06D",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F3A06D",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar5'])
				},
				{
					label: "晚飯後一小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F8A974",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F8A974",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar6'])
				},
				{
					label: "晚飯後兩小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar10'])
				},
				{
					label: "睡前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#529AD1",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#529AD1",
					pointRadius: 0,
					borderWidth: 1,
					data: get_BS_data(arr['sugar7'])
				}
			]
		};
	}

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 200,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: 0,
					max: 200,
					stepSize: 20,
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_X = '';
						if (value == 80) label_X = '過低';
						if (value == 120) label_X = '飯前過高';
						if (value == 180) label_X = '飯後過高';
						return label_X;
					}
				},
				gridLines: {
					color: ['rgba(0,0,0,0)','rgba(255,179,179,1)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(255,179,179,1)',
							'rgba(0,0,0,0)','rgba(255,159,5,1)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)',
							'rgba(0,0,0,0)'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['Start_Date'],
					max: arr['End_Date']
				},
				ticks: {
					fontSize:12
				},
				scaleLabel: {
					display: true,
					labelString: arr['label']
				}
			}]
		}
	};

	var ctx = document.getElementById("bs_line_season").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}

//判斷週月季
function bs_Line(arr) {
	var tem={};
	if (arr['season']) {
		tem = getBS_Line_Season(arr);
	} else if (arr['month']) {
		tem = getBS_Line_Month(arr);
	} else if (arr['sugar_week']) {
		tem = getBS_Line_Week(arr);
	} else {}
	return tem;
}

//取得資料
function get_BS_data(arr) {
	var tem = [];
	for (var z in arr) {
		tem.push(arr[z]);
	}
	return tem;
}

function Creat_BS_Chart(arr) {
	var tem = {};
	if (arr['type']=='day') {
		getBS_Bar_Day(arr);
	} else {
		Draw_BS_Line_Chart_range(arr);
	}
}

function Draw_BS_Line_Chart_range(arr) {
	if (arr['diabetes'] != '妊娠型糖尿病') {
		var lineData = {
			datasets: [
				{
					label: "空腹",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#0A9177",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#0A9177",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar1'])
				},
				{
					label: "早飯後",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar2'])
				},
				{
					label: "午飯前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar3'])
				},
				{
					label: "午飯後",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar4'])
				},
				{
					label: "晚飯前",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar5'])
				},
				{
					label: "晚飯後",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar6'])
				},
				{
					label: "睡前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#529AD1",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#529AD1",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar7'])
				}
			]
		};
	} else {
		var lineData = {
			datasets: [
				{
					label: "空腹",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#0A9177",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#0A9177",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar1'])
				},
				{
					label: "早飯後一小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F8A974",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F8A974",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar2'])
				},
				{
					label: "早飯後兩小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar8'])
				},
				{
					label: "午飯前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar3'])
				},
				{
					label: "午飯後一小時",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F8A974",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F8A974",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar4'])
				},
				{
					label: "午飯後兩小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar9'])
				},
				{
					label: "晚飯前",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#66C2A0",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#66C2A0",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar5'])
				},
				{
					label: "晚飯後一小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F8A974",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F8A974",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar6'])
				},
				{
					label: "晚飯後兩小時",
					backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#F68562",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#F68562",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar10'])
				},
				{
					label: "睡前",
	                backgroundColor: 'rgba(230,122,122,0)',
					borderColor: "#529AD1",
					pointBackgroundColor: "#fff",
					pointBorderColor: "#529AD1",
					pointRadius: 2,
					borderWidth: 1,
					data: get_BS_data(arr['sugar7'])
				}
			]
		};
	}

	var lineOptions = {
		responsive: true,
		//設置繪製圖大小
		maintainAspectRatio: false,
		//線為曲線or直線 0:直線
		elements: {
			line: {
				tension: 0
			}
		},
		//到線上即顯示label_data
		tooltips: {
			mode: 'nearest',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		//x軸&y軸設定
		scales: {
			yAxes: [{
				ticks: {
					min: 0,
					max: 200,
					stepSize: 20
				}
			},
			{
				type: 'linear',
				position: 'right',
				ticks: {
					min: arr['bs_seq']['chart_min'],
					max: arr['bs_seq']['chart_max'],
					stepSize: arr['bs_seq']['all_d'],
					fontColor: "#CE0000",
					callback: function(value, index, values) {
						var label_X = '';
						if (value == arr['bs_seq']['glu_low']) label_X = ' 過低';
						if (value ==arr['bs_seq']['limit_low']) label_X = ' 飯前過高';
						if (value ==arr['bs_seq']['limit_high']) label_X = ' 飯後過高';
						return label_X;
					}
				},
				gridLines: {
					color: arr['bs_Color'],
					borderDash:[10,10]
				}
			}],
			xAxes: [{
				type: 'time',
				time: {
					unit: 'hour',
					parser: 'YYYY M D HH mm',
					unitStepSize: 24,
					displayFormats: {
						minute: 'D',
						hour: 'MM-DD',
						day: 'YYYY-M-D',
						month: 'YYYY-M-D'
					},
					min: arr['chart_StartDate'],
					max: arr['chart_EndDate']
				}
			}]
		}
	};

	var ctx = document.getElementById("bs_line_chart").getContext("2d");
	new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});
}