<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>霍恩關懷雲守護站 |403 Server Error</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body class="red-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>403</h1>
        <h3 class="font-bold">Forbidden</h3>

        <div class="error-desc">
            You don`t have permission to access on this page.
            <div class="form-inline m-t">
                <a style="color:#FBE251;" href="javascript:location.href='index.php?ver=<?php echo $_GET['ver']; ?>';">
                    <h2 ><b><i class="fa fa-home"></i>回首頁</b></h2>
                </a>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>