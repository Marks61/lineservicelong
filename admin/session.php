<?php
	$savePath = '../session';
	session_save_path($savePath);
	session_start();
	$lifetime = 604800;	//24小時
	session_set_cookie_params($lifetime);
	
	$admin = $_SESSION['admin'];
	if (!isset($admin['identity'])) {
		session_unset();
		session_destroy();
		header('location:../index.php');
	}
?>