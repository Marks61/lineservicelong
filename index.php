<?php
$cotString = (isset($_GET['content'])) ? $_GET['content'] : '';
($cotString == 'account_login') ? require_once('../mod/account/account_login.php') : require_once('session.php');
require_once('cdb/config.php'); //連接資料庫
require_once('config/config.php');
?>
<!DOCTYPE html>
<html lang="zh-tw">

<head>
	<title>LineService</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./admin/css/colorlib/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./admin/fonts/colorlib/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./admin/fonts/colorlib/iconic/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./admin/css/colorlib/vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./admin/css/colorlib/vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./admin/css/colorlib/vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./admin/css/colorlib/vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./admin/css/colorlib/vendor/daterangepicker/daterangepicker.css">

	<link rel="stylesheet" type="text/css" href="./admin/css/colorlib/vendor/sweetalert2/dist/sweetalert2.min.css">
	<link rel="stylesheet" type="text/css" href="./admin/css/colorlib/vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./admin/css/colorlib/util.css">
	<link rel="stylesheet" type="text/css" href="./admin/css/colorlib/main.css">
	<!--===============================================================================================-->
</head>


<body id="body">

	<div class="limiter">
		<div class="container-login100" style="background-image: url('./admin/img/colorlib/bg-01.jpg');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				<form class="login100-form validate-form">
					<span class="login100-form-title p-b-49">
						Login
					</span>
					<audio style="display: none;" id="myAudio" controls autoplay="autoplay">
						<source src="sound/Preparing_The_Arena.mp3" type="audio/mpeg">
					</audio>
					<div class="wrap-input100 validate-input m-b-23" data-validate="Username is reauired">
						<span class="label-input100">Username</span>
						<input class="input100" type="text" id="username" name="username" placeholder="Type your username">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" id="pass" name="pass" placeholder="Type your password">
						<span class="focus-input100" data-symbol="&#xf190;"></span>
					</div>

					<div class="text-right p-t-8 p-b-31">
						<a href="#">
							Forgot password?
						</a>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button type="button" class="login100-form-btn" onclick="login();">
								Login
							</button>
						</div>
					</div>

					<!--<div class="txt1 text-center p-t-54 p-b-20">
						<span>
							Or Sign Up Using
						</span>
					</div>

					<div class="flex-c-m">
						<a href="#" class="login100-social-item bg1">
							<i class="fa fa-facebook"></i>
						</a>

						<a href="#" class="login100-social-item bg2">
							<i class="fa fa-twitter"></i>
						</a>

						<a href="#" class="login100-social-item bg3">
							<i class="fa fa-google"></i>
						</a>
					</div>

					<div class="flex-col-c p-t-155">
						<span class="txt1 p-b-17">
							Or Sign Up Using
						</span>

						<a href="#" class="txt2">
							Sign Up
						</a>
					</div>-->
				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>
	<!--===============================================================================================-->
	<script src="./admin/css/colorlib/vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="./admin/css/colorlib/vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="./admin/css/colorlib/vendor/bootstrap/js/popper.js"></script>
	<script src="./admin/css/colorlib/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="./admin/css/colorlib/vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="./admin/css/colorlib/vendor/daterangepicker/moment.min.js"></script>
	<script src="./admin/css/colorlib/vendor/daterangepicker/daterangepicker.js"></script>
	<script src="./admin/css/colorlib/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
	<!--===============================================================================================-->
	<script src="./admin/css/colorlib/vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="./admin/js/colorlib/main.js"></script>

	<script>
		/*$("#password").keyup(function(event) {
			if (event.keyCode === 13) {
				$("#btn_login").click();
			}
		});
		$("#account").keyup(function(event) {
			if (event.keyCode === 13) {
				$("#btn_login").click();
			}
		});*/
	</script>
	<script>
		var isAllSuccess = true;

		function contact_send() {
			var request = $.ajax({
				url: "ajax.php?mod=homepage&content=contact_insert",
				method: "POST",
				data: {
					name: $('#name').val(),
					email: $('#email').val(),
					tel: $('#tel').val(),
					question: $('#question').val()
				},
				dataType: "text"
			});

			request.done(function(dat) {
				var data = JSON.parse(dat);
				var arr = [];
				for (var key in data) {
					arr[key] = data[key];
				}
				// console.log(arr);
				contact_message(arr);
			});

			request.fail(function(jqXHR, textStatus) {
				alert("Requset failed: " + textStatus);
			});
		}

		function contact_message(arr) {
			if (arr['success']) {
				document.getElementById('messages').innerHTML = '<div class="alert alert-success">' + arr['message'] + '</div>';
				document.getElementById('name').value = '';
				document.getElementById('email').value = '';
				document.getElementById('tel').value = '';
				document.getElementById('question').value = '';
			} else {
				document.getElementById('messages').innerHTML = '<div class="alert alert-danger">' + arr['message'] + '</div>';
			}
		}

		function register_account() {
			if (isAllSuccess) {
				var request = $.ajax({
					url: "ajax.php?mod=account&content=account_insert",
					method: "POST",
					data: {
						account1: $('#account2').val(),
						password1: $('#password1').val(),
						password2: $('#password2').val(),
						name1: $('#name1').val(),
						email1: $('#email2').val(),
						birthday1: $('#birthday1').val()
					},
					dataType: "text"
				});

				request.done(function(dat) {
					var data = JSON.parse(dat);
					var arr = [];
					for (var key in data) {
						arr[key] = data[key];
					}
					register_message(arr);
				});

				request.fail(function(jqXHR, textStatus) {
					alert("Requset failed: " + textStatus);
				});
			}
		}

		function playmusic() {
			myAudio.play();
		}

		var myAudio = document.getElementById('myAudio');
		var palyer_button = document.getElementById('body');
		var player_function = body.addEventListener('mouseenter', playmusic);

		$(document).ready(function() {
			setTimeout(player_function, 10000);
			if (myAudio.onended) {
				setTimeout(function() {
					myAudio.play()
				}, 10000);
			}
			$('#account2').blur(function() {
				var account = $(this).val();
				$.ajax({
					url: "ajax.php?mod=account&content=register_check",
					method: "POST",
					data: {
						account: account
					},
					success: function(html) {
						if (html.includes('此帳號已存在')) {
							isAllSuccess = false;
						} else {
							isAllSuccess = true;
						}
						$('#check_account').html(html);
					}
				});
			});
			// $('#email2').blur(function() {
			// 	var email = $(this).val();
			// 	$.ajax({
			// 		url: "ajax.php?mod=account&content=register_check",
			// 		method: "POST",
			// 		data: {
			// 			email: email
			// 		},
			// 		success: function(html) {
			// 			if (html.includes('此E-mail已存在')) {
			// 				isAllSuccess = false;
			// 			} else {
			// 				isAllSuccess = true;
			// 			}
			// 			$('#check_email').html(html);
			// 		}
			// 	});
			// });
		});

		function register_message(arr) {
			if (arr['success']) {
				$('#register').modal('hide');
				$('#login').modal('show');
			} else {
				$('#check_empty2').html(arr['message']);
			}
		}

		function send_password() {
			var request = $.ajax({
				url: "ajax.php?mod=account&content=account_new_password",
				method: "POST",
				data: {
					account: $('#account1').val(),
					email: $('#email1').val()
				},
				dataType: "text"
			});

			request.done(function(dat) {
				var data = JSON.parse(dat);
				var arr = [];
				for (var key in data) {
					arr[key] = data[key];
				}
				password_message(arr);
			});

			request.fail(function(jqXHR, textStatus) {
				alert("Requset failed: " + textStatus);
			});
		}

		function password_message(arr) {
			if (arr['success']) {
				$('#send_ok1').html(arr['message']);
			} else {
				$('#check_empty1').html(arr['message']);
			}
		}

		function login() {
			var request = $.ajax({
				url: "ajax.php?mod=account&content=account_login",
				method: "POST",
				data: {
					account: $('#username').val(),
					password: $('#pass').val()
				},
				dataType: "text"
			});

			request.done(function(dat) {
				var data = JSON.parse(dat);
				var arr = [];
				for (var key in data) {
					arr[key] = data[key];
				}

				login_message(arr);
			});

			request.fail(function(jqXHR, textStatus) {
				alert("Requset failed: " + textStatus);
			});
		}

		function login_message(arr) {
			if (!arr['success']) {
				Swal.fire(
					"登入作業失敗", //標題 
					arr['message'], //訊息內容(可省略)
					arr['message'] == '帳號權限不足' ? "warning" : "error" //圖示(可省略) success/info/warning/error/question
					//圖示範例：https://sweetalert2.github.io/#icons
				);
				//$('#check_login').html(arr['message']);
			} else {
				location.href = arr['url'];
			}
		}
	</script>

</html>