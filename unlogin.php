<?php
	require_once('cdb/config.php');
	require_once('mod/db/class.php');
	$cla = new db();
	date_default_timezone_set("Asia/Taipei");
	$id = isset($_GET['id'])?$_GET['id']:0;
	$savePath = './session';
	session_save_path($savePath);
	session_start();
	$query = "update carer_record SET logout_time='".date('Y-m-d H:i:s')."' where carer_id='".$id."' and device_type = 0 order by login_time desc limit 1";
	$cla->sqlQuery($query);
	session_unset();
	session_destroy();
	header("location:index.php");
?>