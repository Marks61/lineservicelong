<?php
	//允許跨域請求
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept');

	//載入物件
	function loader($class) {
		$file = '/var/www/html/horncloud/api/mod/'.$class.'/class.php';
		if (is_file($file)) {
			require_once($file);
		}
	}

	spl_autoload_register('loader');
	spl_autoload_extensions('.php');

	//資料庫參數
	require_once('/var/www/html/horncloud/cdb/config.php');

	//台北時區
	date_default_timezone_set("Asia/Taipei");

	//會員圖路徑
	define('web_horncloud','https://www.hncare.cloud/horncloud/upload/account/');
	define('diet_care_path','https://www.hncare.cloud/horncloud');
?>