<?php
session_start();
unset($_SESSION['success']);
unset($_SESSION['message']);
require_once('config/config.php');
$realUnion = new RealUnion();
$output = array('success' => 1, 'message' => array('ch' => '', 'en' => ''));
if ($output['success']) $realUnion->setCompanyLogin($output);
$_SESSION['success'] = $output['success'];
$_SESSION['message'] = json_decode(urldecode(json_encode($output)))->message;
if ($_SESSION['success']) {
    header("Location: http://" . $_SERVER['HTTP_HOST'] . '/horn/lineservice/LINE/qrcodeList.php');
} else if ($_SESSION['success'] == 0) {
    header("Location: http://" . $_SERVER['HTTP_HOST'] . '/horn/lineservice/LINE/qrcodeLogin.php');
}
//echo urldecode(json_encode($output));
