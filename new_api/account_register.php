<?php

	require_once('config/config.php');
	$class_account = new account();
	$output = array('success' => 1, 'message' => array('ch' => '', 'en' => ''), 'sid' => '');

	$class_account->Register_InputData($output);

	if ($output['success']) {

		$class_account->Insert_Account($output);

		if ($output['success']) {

			$output['id'] = $class_account->Get_ID_By_Account($output['account']);
			$class_account->Insert_Shopping_Cart_Account($output);

		}

	}

	echo json_encode($output);

 ?>