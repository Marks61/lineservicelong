<?php
class db
{
	private $mMysqli;
	private $mMysqli2;
	function __construct()
	{
		$this->mMysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
		$this->mMysqli2 = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE2);
		$this->mMysqli->query("set names utf8");
		$this->mMysqli2->query("set names utf8");
		$this->db = $this;
	}

	public function sqlQuery1Exeption($sql)
	{
		try {
			$this->mMysqli->query($sql);
			return true;
		} catch (Exception $e) {;
			return false;
		}
	}

	public function sqlInjectionProtect()
	{
		return $this->mMysqli;
	}

	public function __destruct()
	{
		$this->mMysqli->close();
	}

	//取筆數
	public function getRows($sql)
	{
		$num = 0;
		if ($result = $this->mMysqli->query($sql)) {
			$num = $result->num_rows;
			$result->close();
		}
		return $num;
	}

	//取筆數
	public function getRows2($sql)
	{
		$num = 0;
		if ($result = $this->mMysqli2->query($sql)) {
			$num = $result->num_rows;
			$result->close();
		}
		return $num;
	}

	//取一筆資料
	public function getRow($sql)
	{
		$row = array();
		if ($result = $this->mMysqli->query($sql)) {
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$result->close();
		}
		return $row;
	}

	//取一筆資料
	public function getRow2($sql)
	{
		$row = array();
		if ($result = $this->mMysqli2->query($sql)) {
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$result->close();
		}
		return $row;
	}

	//取整個資料
	public function getQuery($sql)
	{
		$rows = array('num' => 0, 'row' => array());
		if ($result = $this->mMysqli->query($sql)) {
			$rows['num'] = $result->num_rows;
			while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
				$rows['row'][] = $row;
			}
			$result->close();
		}
		return $rows;
	}

	//取整個資料
	public function getQuery2($sql)
	{
		$rows = array('num' => 0, 'row' => array());
		if ($result = $this->mMysqli2->query($sql)) {
			$rows['num'] = $result->num_rows;
			while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
				$rows['row'][] = $row;
			}
			$result->close();
		}
		return $rows;
	}

	//執行sql
	public function sqlQuery($sql)
	{
		if ($this->mMysqli->query($sql)) {
			return true;
		} else {
			return false;
		}
	}

	//執行sql
	public function sqlQuery2($sql)
	{
		if ($this->mMysqli2->query($sql)) {
			return true;
		} else {
			return false;
		}
	}

	//過濾
	public function escape($value)
	{
		return $this->mMysqli->real_escape_string($value);
	}

	//傳回insert 的ID
	public function getLastId()
	{
		return $this->mMysqli->insert_id;
	}
}
