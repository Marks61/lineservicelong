<?php

use Symfony\Component\HttpFoundation\Session\Session;

class RealUnion extends db
{
	/**
	 * qrcode申請單
	 * user:申請人姓名
	 * phone:申請人手機
	 * location:申請地點名稱
	 * address:申請地點住址
	 * contact:申請地點電話號碼
	 * number:分店數量
	 */
	public function create(&$output)
	{
		$user = isset($_POST['user']) ? $_POST['user'] : '';
		$phone = isset($_POST['phone']) ? $_POST['phone'] : '';
		$email = isset($_POST['email']) ? $_POST['email'] : '';
		$location = isset($_POST['location']) ? $_POST['location'] : '';
		$address = isset($_POST['address']) ? $_POST['address'] : '';
		$contact = isset($_POST['contact']) ? $_POST['contact'] : '';
		$number = isset($_POST['number']) ? $_POST['number'] : 0;
		$accountId = 0;
		if ($user == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請填寫申請人姓名') : urlencode(', 請填寫申請人姓名');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in user name.' : ', Please fill in user name.';
			$output['success'] = 0;
		}

		if ($phone == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請填寫申請人電話') : urlencode(', 請填寫申請人電話');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the user phone.' : ', Please fill in the user phone.';
			$output['success'] = 0;
		}

		if ($location == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請填寫申請地點名稱') : urlencode(', 請填寫申請地點名稱');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in location name.' : ', Please fill in location name.';
			$output['success'] = 0;
		}

		if ($address == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請填寫申請地點地址') : urlencode(', 請填寫申請地點地址');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the location address.' : ', Please fill in the location address.';
			$output['success'] = 0;
		}

		if ($contact == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請填寫申請地點電話') : urlencode(', 請填寫申請地點電話');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in location telphone.' : ', Please fill in location telphone.';
			$output['success'] = 0;
		}

		if ($email == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請填寫申請人信箱') : urlencode(', 請填寫申請人信箱');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the user email.' : ', Please fill in the user email.';
			$output['success'] = 0;
		}

		if ($number == 0) {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請填寫申請地點分店數量(如果只有一間店請填入1)') : urlencode(', 請填寫申請地點分店數量(如果只有一間店請填入1)');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the location number(If only one ).' : ', Please fill in the location number.';
			$output['success'] = 0;
		}

		if ($output['success']) {
			try {
				$output['success'] = 1;
				$account = $this->getQuery('select id from member where email = \'' . $email . '\' and identity = \'shop\'');
				if ($account['num'] <= 0) {
					$this->sqlQuery('insert into member(account,name,password,identity,email,cellphone,created_at) values(\'' . $email . '\',\'' . $user . '\',\'' . md5($phone) . '\',\'shop\',\'' . $email . '\',\'' . $phone . '\',\'' . date('Y-m-d H:i:s', time()) . '\')');
					$accountId = $this->getLastId();
					$output['userSid'] = $accountId;
					$output['success'] = 2;
				} else {
					$accountId = $account['row'][0]['id'];
				}

				$this->sqlQuery('insert into real_union_location(user,user_name,user_phone,location_name,location,contact,num,email) values(\'' . $accountId . '\',\'' . $user . '\',\'' . md5($phone) . '\',\'' . $location . '\',\'' . $address . '\',\'' . $contact . '\',\'' . $number . '\',\'' . $email . '\')');
				$insert_id = $this->getLastId();
				for ($i = 1; $i <= $number; $i++) {
					$token = '';
					for ($x = 1; $x <= 9999; $x++) {
						$token = $this->unionNumber();
						if ($this->unionNumberCheck($token)) {
							break;
						}
					}
					$this->sqlQuery('insert into real_union(name,address,token,location,created_at) values(\'' . $location . '\',\'' . $address . '\',\'' . $token . '\',\'' . $insert_id . '\',\'' . date('Y-m-d H:i:s', time()) . '\')');
				}
				//$sql = '';
				//$this->sqlQuery($sql);
				//$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	public function updateUserData(&$output)
	{
		$user = isset($_POST['user']) ? $_POST['user'] : '';
		$email = isset($_POST['email']) ? $_POST['email'] : '';
		$password = isset($_POST['password']) ? $_POST['password'] : '';
		$account = isset($_POST['account']) ? $_POST['account'] : '';
		$id = isset($_POST['accountId']) ? $_POST['accountId'] : 0;

		if ($output['success']) {
			try {
				$sql = 'update member set ' . ($user == '' ? '' : 'name = \'' . $user . '\',') . ' ' . ($email == '' ? '' : 'email = \'' . $email . '\',') . ' ' . ($account = '' ? '' : 'account = \'' . $this->escape($account) . '\',') . ' ' . ($password == '' ? '' : 'password = \'' . $this->escape(md5($password)) . '\'') . ' where id = ' . $id;
				$this->sqlQuery($sql);
				$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	public function getUserData(&$output)
	{
		$id = isset($_POST['accountId']) ? $_POST['accountId'] : 0;

		if ($id == 0) {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入帳號編號。') : urlencode(', 請輸入帳號編號。');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the account id.' : ', Please fill in the account id.';
			$output['success'] = 0;
		}

		if ($output['success']) {
			try {
				$sql = 'select name as user,email from member where id = ' . $id;
				$data = $this->getRow($sql);
				$output['user'] = $data['user'];
				$output['email'] = $data['email'];
				$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	public function unionNumber()
	{
		$str = '';
		$number = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
		for ($i = 1; $i <= 16; $i++) {
			$str .= $number[random_int(0, 9)];
		}
		return $str;
	}

	public function unionNumberCheck($str)
	{
		if ($this->getRows('select id from real_union where token = \'' . $str . '\'') <= 0) {
			return true;
		} else {
			return false;
		}
	}

	public function setCompanyLogin(&$output)
	{
		$username = isset($_POST['account']) ? $_POST['account'] : '';
		$password = isset($_POST['password']) ? $_POST['password'] : '';
		if ($username == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請填寫申請人電子信箱') : urlencode(', 請填寫申請人電子信箱');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in user email.' : ', Please fill in user email.';
			$output['success'] = 0;
		}

		if ($password == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請填寫申請人聯絡電話') : urlencode(', 請填寫申請人聯絡電話');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the user phone.' : ', Please fill in the user phone.';
			$output['success'] = 0;
		}

		if ($output['success']) {
			try {
				//$count = $this->getRows('select id from real_union_location where email = \'' . $username . '\' and user_phone = \'' . md5($password) . '\'');
				$count = $this->getRows('select id from member where email = \'' . $username . '\' and password = \'' . $this->escape(md5($password)) . '\' and identity = \'shop\'');
				if ($count <= 0) {
					$output['success'] = 0;
					$output['message']['ch'] = '帳號或密碼有誤';
					$output['message']['en'] = 'Account or password incorrect';
				} else {
					$output['success'] = 1;
					session_start();
					$_SESSION['username'] = $username;
					$_SESSION['password'] = $password;
				}
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	public function qrocdeList()
	{
		session_start();
		$email = $_SESSION['username'];;
		$phone = md5($_SESSION['password']);

		$sql1 = 'select id from real_union_location where email = \'' . $email . '\' and user_phone = \'' . $phone . '\'';
		$location = $this->getQuery($sql1)['row'];
		$qrcode = array('num' => 0, 'data' => array());
		$qrcode_count = 0;
		foreach ($location as $row) {
			foreach ($this->getQuery('select * from real_union where location = \'' . $row['id'] . '\' and deleted_at is null order by id')['row'] as $data) {
				array_push($qrcode['data'], $data);
				$qrcode_count++;
			}
		}
		$qrcode['num'] = $qrcode_count;
		return $qrcode;
	}

	public function getInfo(&$output)
	{
		$id = isset($_POST['id']) ? $_POST['id'] : 0;

		if ($id == 0) {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入檔案編號') : urlencode(', 請輸入檔案編號');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the qrcode id.' : ', Please fill in the qrcode id.';
			$output['success'] = 0;
		}

		if ($output['success']) {
			try {
				$data = $this->getRow('select name,address from real_union where id = \'' . $id . '\'');
				$output['name'] = $data['name'];
				$output['address'] = $data['address'];
				$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	public function editInfo(&$output)
	{
		$name = isset($_POST['name']) ? $_POST['name'] : '';
		$address = isset($_POST['address']) ? $_POST['address'] : '';
		$id = isset($_POST['id']) ? $_POST['id'] : 0;

		if ($name == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請填寫地點名稱') : urlencode(', 請填寫地點名稱');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the location name.' : ', Please fill in the loaction name.';
			$output['success'] = 0;
		}

		if ($address == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請填寫地點地址') : urlencode(', 請填寫地點地址');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the location address.' : ', Please fill in the location address.';
			$output['success'] = 0;
		}

		if ($id == 0) {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入檔案編號') : urlencode(', 請輸入檔案編號');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the qrcode id.' : ', Please fill in the qrcode id.';
			$output['success'] = 0;
		}

		if ($output['success']) {
			try {
				$this->sqlQuery('update real_union set name = \'' . $name . '\',address = \'' . $address . '\',updated_at = \'' . date('Y-m-d H:i:s', time()) . '\' where id = ' . $id);
				$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	public function openQrcode(&$output)
	{
		$id = isset($_POST['id']) ? $_POST['id'] : 0;

		if ($id == 0) {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入檔案編號') : urlencode(', 請輸入檔案編號');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the qrcode id.' : ', Please fill in the qrcode id.';
			$output['success'] = 0;
		}

		if ($output['success']) {
			try {
				$this->sqlQuery('update real_union set is_use = 1,updated_at = \'' . date('Y-m-d H:i:s', time()) . '\' where id = ' . $id);
				$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	public function closeQrcode(&$output)
	{
		$id = isset($_POST['id']) ? $_POST['id'] : 0;

		if ($id == 0) {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入檔案編號') : urlencode(', 請輸入檔案編號');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the qrcode id.' : ', Please fill in the qrcode id.';
			$output['success'] = 0;
		}

		if ($output['success']) {
			try {
				$this->sqlQuery('update real_union set is_use = 0,updated_at = \'' . date('Y-m-d H:i:s', time()) . '\' where id = ' . $id);
				$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	public function deleteQrcode(&$output)
	{
		$id = isset($_POST['id']) ? $_POST['id'] : 0;

		if ($id == 0) {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入檔案編號') : urlencode(', 請輸入檔案編號');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the qrcode id.' : ', Please fill in the qrcode id.';
			$output['success'] = 0;
		}

		if ($output['success']) {
			try {
				$this->sqlQuery('update real_union set deleted_at = \'' . date('Y-m-d H:i:s', time()) . '\' where id = ' . $id);
				$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	public function realUnionSignUp(&$output)
	{
		$token = isset($_POST['token']) ? $_POST['token'] : '';
		$userSid = isset($_POST['userSid']) ? $_POST['userSid'] : '';

		if ($userSid == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入使用者') : urlencode(', 請輸入使用者');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the user id.' : ', Please fill in the user id.';
			$output['success'] = 0;
		}

		if ($token == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入地點編號') : urlencode(', 請輸入地點編號');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the location id.' : ', Please fill in the location id.';
			$output['success'] = 0;
		}

		if ($output['success']) {
			try {
				$data = $this->getQuery('select * from real_union where is_use = 1 and token  = \'' . $token . '\' and deleted_at is null');
				if ($data['num'] <= 0) {
					$output['success'] = -1;
				} else {
					$time = date('Y-m-d H:i:s', time());
					$this->sqlQuery('insert into real_union_history (location,user,in_time,created_at) values((select id from real_union where token = \'' . $token . '\'),(select id from member where line_sid = \'' . $userSid . '\'),\'' . $time . '\',\'' . $time . '\')');
					$output['success'] = 1;
					$output['name'] = $data['row'][0]['name'];
					$output['address'] = $data['row'][0]['address'];
					$output['date'] = date('Y-m-d', strtotime($time));
					$output['time'] = date('H:i:s', strtotime($time));
				}
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	public function realUnionSearch(&$output)
	{
		$startDate = isset($_POST['startdate']) ? $_POST['startdate'] : '';
		$endDate = isset($_POST['endDate']) ? $_POST['endDate'] : '';
		$userSid = isset($_POST['userSid']) ? $_POST['userSid'] : '';

		if ($userSid == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入使用者') : urlencode(', 請輸入使用者');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the user id.' : ', Please fill in the user id.';
			$output['success'] = 0;
		}

		if ($startDate == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入起始日期') : urlencode(', 請輸入起始日期');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the start date.' : ', Please fill in the start date.';
			$output['success'] = 0;
		}

		if ($endDate == '') {
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入結束日期') : urlencode(', 請輸入結束日期');
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the end date.' : ', Please fill in the end date.';
			$output['success'] = 0;
		}

		if ($output['success']) {
			try {
				$data = $this->getQuery('select history.id as id,history.in_time as in_time,location.token as token,location.name as name,location.address as address from real_union_history history,real_union location,member where history.user = member.id and history.location = location.id and history.user in (select id from member where line_sid = \'' . $userSid . '\') and history.in_time between \'' . $startDate . '\' and \'' . $endDate . '\' order by history.in_time asc');
				$output['num'] = $data['num'];
				$output['row'] = $data['row'];
				$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}
}
