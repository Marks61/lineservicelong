<?php

use Aws\ElastiCache\Exception\ElastiCacheException;
use MicrosoftAzure\Storage\Common\Internal\Validate;

class line extends db
{
    public function Check_SID(&$output, $sid = '')
    {
        if ($sid == '') {
            $output['success'] = 0;
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入安全碼') : urlencode(', 請輸入安全碼');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the SID' : ', Please fill in the SID';
        } else if (!$this->Check_SID_Exist($sid)) {

            $output['success'] = 0;
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('安全碼有誤') : urlencode(', 安全碼有誤');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'SID error' : ', SID error';
        } else {

            $output['success'] = 1;
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('安全碼正確') : urlencode(', 安全碼正確');
        }
    }

    public function Check_SID_Exist($sid)
    {
        $sql = "select * from account where sid = '" . $this->escape($sid) . "'";
        return $this->getQuery($sql)['num'];
    }

    public function setBinddingData(&$output)
    {
        $target = isset($_POST['target']) ? $_POST['target'] : '';
        $account = isset($_POST['account']) ? $_POST['account'] : '';
        $URL = '';

        if ($target == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入綁定帳戶類型') : urlencode(', 請輸入綁定帳戶類型');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in bindding account type' : ', Please fill in bindding account type.';
            $output['success'] = 0;
        }

        if ($account == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入霍恩帳號') : urlencode(', 請輸入霍恩帳號');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in content' : ', Please fill in content.';
            $output['success'] = 0;
        } else if (preg_match("/\\s/", $account)) {

            $output['success'] = 0;
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('帳號不能包含空格') : urlencode(', 帳號不能包含空格');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Account cannot contain spaces' : ', Account cannot contain spaces';
        }

        if ((!isset($_POST['target']) && !$_POST['target']) || (!isset($_POST['account']) && !$_POST['account'])) {
            $output['message']['ch'] = urlencode('未收到資料');
            $output['message']['en'] = 'Data not received.';
            $output['success'] = 0;
        }

        if ($output['success']) {
            $URL .= 'https://access.line.me/oauth2/v2.1/authorize?';
            $URL .= 'response_type=code';
            $URL .= '&client_id=' . login_chanel_id;
            $URL .= '&redirect_uri=' . line_url_dress . '/horncloud/new_api/line_login_roll_back.php';
            $URL .= '&state=Horncare.' . $target . '@' . $account;
            $URL .= '&bot_prompt=normal';
            $URL .= '&scope=openid%20profile';

            $output['url'] = $URL;
        }
    }

    public function personalLogin($category, $account, $code)
    {
        $_header = array(
            'Content-Type: application/x-www-form-urlencoded'
        );

        $form_data = array();

        $form_data = [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => line_url_dress . '/horncloud/new_api/line_login_roll_back.php',
            'client_id' => login_chanel_id,
            'client_secret' => login_chanel_secret
        ];

        $r_body = json_encode($form_data);

        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_POST => true
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.line.me/oauth2/v2.1/token');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $_header);
        curl_setopt_array($curl, $options);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($form_data));
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $r_body);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function familyLogin($category, $account, $code)
    {
        $_header = array(
            'Content-Type: application/x-www-form-urlencoded'
        );

        $form_data = array();

        $form_data = [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => line_url_dress . '/horncloud/new_api/line_login_roll_back.php',
            'client_id' => login_chanel_id,
            'client_secret' => login_chanel_secret
        ];

        $r_body = json_encode($form_data);

        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_POST => true
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.line.me/oauth2/v2.1/token');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $_header);
        curl_setopt_array($curl, $options);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($form_data));
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $r_body);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function getLineinfo(&$output, $category, $account, $id_token, $access_token)
    {
        $_header = array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Bearer ' . $access_token
        );

        $form_data = array();

        $form_data = [
            'access_token' => $access_token
        ];

        $r_body = json_encode($form_data);

        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_POST => true
        );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.line.me/v2/profile');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $_header);
        curl_setopt_array($curl, $options);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($form_data));
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $r_body);
        $result = curl_exec($curl);
        curl_close($curl);
        if ($this->setLineBinding($account, $category, json_decode($result))) {
            $output['success'] = 1;
            return true;
        } else {
            $output['success'] = 0;
            $output['message']['ch'] = 'Line 帳號綁定失敗。';
            $output['message']['en'] = 'Line account binddiing failed.';
            return false;
        }
    }

    public function getLineinfo2(&$output, $category, $account, $id_token, $access_token)
    {
        $_header = array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Bearer ' . $access_token
        );

        $form_data = array();

        $form_data = [
            'access_token' => $access_token
        ];

        $r_body = json_encode($form_data);

        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_POST => true
        );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.line.me/v2/profile');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $_header);
        curl_setopt_array($curl, $options);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($form_data));
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $r_body);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    //Line綁定一版
    public function setLineBinding($account, $category, $package)
    {
        $target = $this->getRow("select id,line_sid from account where account='" . $account . "'");
        $line_sid = $target['line_sid'];
        $data = array();
        $query = '';
        $person = 0;
        $family = 0;
        if (count($line_sid) >= 0) {
            if ($category == 'personal') {
                foreach (json_decode($line_sid)->data as $data_info) {
                    array_push($data, array('category' => $data_info->category, 'name' => urlencode($data_info->name), 'picture' => $data_info->picture, 'sid' => $data_info->sid));
                }
                array_push($data, array('category' => $category, 'name' => urlencode($package->displayName), 'picture' => $package->pictureUrl, 'sid' => $package->userId));
                foreach ($data as $row) {
                    if ($row['category'] == 'personal')
                        $person++;
                    else if ($row['category'] == 'family')
                        $family++;
                }
                $query = "update account set line_sid='" . urldecode(json_encode(array('data' => $data, 'personal' => $person, 'family' => $family))) . "' where id = '" . $target['id'] . "'";
            } else if ($category == 'family') {
                foreach (json_decode($line_sid)->data as $data_info) {
                    array_push($data, array('category' => $data_info->category, 'name' => urlencode($data_info->name), 'picture' => $data_info->picture, 'sid' => $data_info->sid));
                }
                array_push($data, array('category' => $category, 'name' => urlencode($package->displayName), 'picture' => $package->pictureUrl, 'sid' => $package->userId));
                foreach ($data as $row) {
                    if ($row['category'] == 'personal')
                        $person++;
                    else if ($row['category'] == 'family')
                        $family++;
                }
                $query = "update account set line_sid='" . urldecode(json_encode(array('data' => $data, 'personal' => $person, 'family' => $family))) . "' where id = '" . $target['id'] . "'";
            }
        }

        if ($this->sqlQuery($query)) {
            return true;
        } else {
            return false;
        }
    }

    //Line綁定api
    public function setBinddingData2(&$output)
    {
        $account = isset($_POST['account']) ? $_POST['account'] : '';
        $category = isset($_POST['category']) ? $_POST['category'] : '';
        $package = isset($_POST['package']) ? $_POST['package'] : '';

        if ($account == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入霍恩帳號') : urlencode(', 請輸入霍恩帳號');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in content' : ', Please fill in content.';
            $output['success'] = 0;
        } else if (preg_match("/\\s/", $account)) {

            $output['success'] = 0;
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? '帳號不能包含空格' : ', 帳號不能包含空格';
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Account cannot contain spaces' : ', Account cannot contain spaces';
        }

        if ($category == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入綁定帳戶類型') : urlencode(', 請輸入綁定帳戶類型');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in bindding account type' : ', Please fill in bindding account type.';
            $output['success'] = 0;
        }

        if ($package == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入 LINE帳戶資料') : urlencode(', 請輸入 LINE帳戶資料');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in Line account data' : ', Please fill in Line account data.';
            $output['success'] = 0;
        }

        if ((!isset($_POST['account']) && !$_POST['accouunt']) || (!isset($_POST['category']) && !$_POST['category']) || (!isset($_POST['package']) && !$_POST['package'])) {
            $output['message']['ch'] = urlencode('未收到資料');
            $output['message']['en'] = 'Data not received.';
            $output['success'] = 0;
        }
        $output['package_data'] = json_decode($package);
        $output['main_data'] = json_decode($package);

        if ($output['success']) return $this->setLineBinding($account, $category, json_decode($package));
    }

    //line 配信
    public function pushMessage(&$output)
    {
        $type = isset($_POST['type']) ? $_POST['type'] : '';
        //$message = isset($_POST['message']) ? $_POST['message'] : '';
        $sid = isset($_POST['sid']) ? $_POST['sid'] : '';
        if ($type == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入推播類型') : urlencode(', 請輸入推播類型');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in message type' : ', Please fill in message type.';
            $output['success'] = 0;
        }

        if ($sid == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請填寫sId') : urlencode(', 請填寫sId');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the sId' : ', Please fill in the sId';
            $output['success'] = 0;
        }

        if ((!isset($_POST['type']) && !$_POST['type'])  || (!isset($_POST['sid']) && !$_POST['sid'])) {
            $output['message']['ch'] = urlencode('未收到資料');
            $output['message']['en'] = 'Data not received.';
            $output['success'] = 0;
        }

        if ($output['success']) return $this->pushMessage_function($output, $type, $sid);
    }

    //https://api.line.me/v2/bot/message/push
    //核心模組，不能刪
    public function pushMessage_function(&$output, $type, $sid)
    {
        $_header = array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . message_chanel_token
        );
        switch ($type) {
            case 'text':
                $content = array(
                    'type' => $type,
                    'text' => urlencode($_POST['message'])
                );
                break;
            case 'image':
                $content = array(
                    'type' => $type,
                    'originalContentUrl' => $_POST['originalContentUrl'],
                    'previewImageUrl' => $_POST['previewImageUrl']
                );
                break;
            case 'video':
                $content = array(
                    'type' => $type,
                    'originalContentUrl' => $_POST['originalContentUrl'],
                    'previewImageUrl' => $_POST['previewImageUrl']
                );
                break;
            case 'location':
                $content = array(
                    'type' => $type,
                    'title' => $_POST['title'],
                    'address' => $_POST['address'],
                    'latitude' => $_POST['latitude'],
                    'longitude' => $_POST['longitube']
                );
                break;
            case 'template':
                if (!isset($_POST['thumbnailImageUrl'])) {
                    $content = array(
                        'type' => $type,
                        'altText' => urlencode($_POST['altText']),
                        'template' => [
                            'type' => $_POST['templateType'],
                            'text' => urlencode($_POST['templateText']),
                            'actions' => $_POST['actions']
                        ]
                    );
                } else {
                    if (!isset($_POST['defaitlPictureActionType'])) {
                        $content = array(
                            'type' => $type,
                            'altText' => urlencode($_POST['altText']),
                            'template' => [
                                'type' => $_POST['templateType'],
                                'imageAspectRadio' => $_POST['imageAspectRadio'], //控制圖片顯示為正方形/長方形?
                                'imageSize' => $_POST['imageSize'], //控制顯示比例
                                'thumbnailImageUrl' => $_POST['thumbnailImageUrl'],
                                'imageBackgroundColor' => $_POST['imageBackgroundColor'], //圖片背景顏色
                                'title' => urlencode($_POST['templeteTitle']),
                                'text' => urlencode($_POST['templateText']),
                                'defaultAction' => null,
                                'actions' => $_POST['actions']
                            ]
                        );
                    } else {
                        $content = array(
                            'type' => $type,
                            'altText' => urlencode($_POST['altText']),
                            'template' => [
                                'type' => $_POST['templateType'],
                                'imageAspectRadio' => $_POST['imageAspectRadio'], //控制圖片顯示為正方形/長方形?
                                'imageSize' => $_POST['imageSize'], //控制顯示比例
                                'thumbnailImageUrl' => $_POST['thumbnailImageUrl'],
                                'imageBackgroundColor' => $_POST['imageBackgroundColor'], //圖片背景顏色
                                'title' => urlencode($_POST['templateTitle']),
                                'text' => urlencode($_POST['templateText']),
                                'defaultAction' => array([
                                    'type' => urlencode($_POST['defaultPictureActionType']),
                                    'label' => urlencode($_POST['defaultPictureActionLabel']),
                                    'text' => urlencode($_POST['defaultPictureActionText'])
                                ]),
                                'actions' => $_POST['actions']
                            ]
                        );
                    }
                }
                break;
        }


        $form_data = array();

        $form_data = [
            'to' => $sid,
            'messages' => array($content)
        ];

        $r_body = urldecode(json_encode($form_data));

        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_POST => true
        );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.line.me/v2/bot/message/push');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $_header);
        curl_setopt_array($curl, $options);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($form_data));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $r_body);
        $result = curl_exec($curl);
        //file_put_contents('log2.txt', $result);
        //var_dump($result);
        curl_close($curl);
        if ($curl) {
            $output['success'] = 1;
            $output['info'] = $result;
            return true;
        } else {
            $output['success'] = 0;
            $output['message']['ch'] = 'Line 推播訊息傳送失敗。';
            $output['message']['en'] = 'Line message push failed.';
            return false;
        }
    }

    //設定大頭貼與LINE同步
    public function changeLinePicture(&$output)
    {
        $account = isset($_POST['account']) ? $_POST['account'] : '';
        $picture = isset($_POST['picture']) ? $_POST['picture'] : '';

        if ($account == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入霍恩帳號') : urlencode(', 請輸入霍恩帳號');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in content' : ', Please fill in content.';
            $output['success'] = 0;
        }

        if ($picture == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入圖片連結') : urlencode(', 請輸入圖片連結');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in picture url' : ', Please fill in picture url.';
            $output['success'] = 0;
        }

        if ((!isset($_POST['account']) && !$_POST['accouunt']) || (!isset($_POST['picture']) && !$_POST['picture'])) {
            $output['message']['ch'] = urlencode('未收到資料');
            $output['message']['en'] = 'Data not received.';
            $output['success'] = 0;
        }
        if ($output['success']) return $this->pictureSetting($output, $account, $picture);
    }

    public function pictureSetting(&$output, $account, $picture)
    {
        if ($this->sqlQuery("update account set image='" . $picture . "' where id='" . $account . "'")) {
            $output['success'] = 1;
            return true;
        } else {
            $output['success'] = 0;
            $output['message']['ch'] = '大頭貼更新失敗。';
            $output['message']['en'] = 'Personal image update failed.';
            return false;
        }
    }

    //解除line綁定
    public function deleteLineBind(&$output)
    {
        $account = isset($_POST['account']) ? $_POST['account'] : '';
        $sid = isset($_POST['sid']) ? $_POST['sid'] : '';

        if ($account == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入霍恩帳號') : urlencode(', 請輸入霍恩帳號');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in content' : ', Please fill in content.';
            $output['success'] = 0;
        }

        if ($sid == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入LINEsId') : urlencode(', 請輸入LINEsId');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in Line sId' : ', Please fill in Line sId.';
            $output['success'] = 0;
        }

        if ((!isset($_POST['account']) && !$_POST['accouunt']) || (!isset($_POST['sid']) && !$_POST['sid'])) {
            $output['message']['ch'] = urlencode('未收到資料');
            $output['message']['en'] = 'Data not received.';
            $output['success'] = 0;
        }

        if ($output['success']) return $this->deleteSid($output, $account, $sid);
    }

    public function deleteSid(&$output, $account, $sid)
    {
        $data = json_decode($this->getRow("select line_sid from account where id='" . $account . "'")['line_sid'])->data;
        $data_copy = array();
        $data_number = 0;
        $person = 0;
        $family = 0;
        foreach ($data as $data_infoamtion) {
            array_push($data_copy, array('category' => $data_infoamtion->category, 'name' => urlencode($data_infoamtion->name), 'picture' => $data_infoamtion->picture, 'sid' => $data_infoamtion->sid));
        }

        foreach ($data_copy as $data_info) {
            if ($data_info['sid'] == $sid) {
                unset($data_copy[$data_number]);
            }
            $data_number++;
        }

        foreach ($data_copy as $row) {
            if ($row['category'] == 'personal')
                $person++;
            else if ($row['category'] == 'family')
                $family++;
        }

        $query = "update account set line_sid='" . urldecode(json_encode(array('data' => $data_copy, 'personal' => $person, 'family' => $family))) . "' where id = '" . $account . "'";

        if ($this->sqlQuery($query)) {
            $output['success'] = 1;
            return true;
        } else {
            $output['success'] = 0;
            $output['message']['ch'] = '解除綁定失敗。';
            $output['message']['en'] = 'Line account unlock bind failed.';
            return false;
        }
    }

    /*//接收文字訊息
       //LINE BOT 核心 可以增修功能，不能刪
    public function setReturnMessage($request)
    {
        $data = json_decode($request);
        if ($data->events[0]->type == 'message') {
            if ($data->events[0]->message->text == '我要查異常紀錄') {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', $this->alertList($data->events[0]->source->userId));
            }else if($data->events[0]->message->text =='請求sid')
            {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', $data->events[0]->source->userId);
            }
             else if ($data->events[0]->message->text == '查詢家屬紀錄') {
                $friend_data = $this->getRows('select account_id from line_fcm_friend where line_token = \'' . $data->events[0]->source->userId . '\' and invite = \'1\'');
                if ($friend_data >= 1) {
		    //IMAHE LINK NEW: https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg
                    //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                    $url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                    $headers = array('Content-Type:  application/x-www-form-urlencoded');
                    $fields = array(
                        'sid' => $data->events[0]->source->userId,
                        'type' => 'template',
                        'altText' => '個人守護站服務',
                        'templateType' => 'buttons',
                        'imageAspectRadio' => 'rectangle',
                        'imageSize' => 'cover',
                        'thumbnailImageUrl' => 'https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg',
                        'imageBackgroundColor' => '#FFFFFF',
                        'templeteTitle' => '守護站個人化服務',
                        'templateText' => '守護站個人化服務選單。',
                        'actions' => array([
                            'type' => 'uri',
                            'label' => urlencode('查詢異常紀錄'),
                            'uri' => line_url_dress . '/horncloud/new_api/family_alert_record.php?userSid=' . $data->events[0]->source->userId
                        ], [
                            'type' => 'uri',
                            'label' => urlencode('查詢血壓/血糖'),
                            'uri' => line_url_dress . '/horncloud/new_api/family_health_record.php?userSid=' . $data->events[0]->source->userId
                        ])
                    );
                } else {
                    //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                    $url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                    $headers = array('Content-Type:  application/x-www-form-urlencoded');
                    $fields = array(
                        'sid' => $data->events[0]->source->userId,
                        'type' => 'template',
                        'altText' => '個人守護站服務',
                        'templateType' => 'buttons',
                        'imageAspectRadio' => 'rectangle',
                        'imageSize' => 'cover',
                        'thumbnailImageUrl' => 'https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg',
                        'imageBackgroundColor' => '#FFFFFF',
                        'templeteTitle' => '守護站個人化服務',
                        'templateText' => '守護站個人化服務選單。',
                        'actions' => array([
                            'type' => 'uri',
                            'label' => urlencode('綁定家人帳號'),
                            'uri' => line_url_dress . '/horncloud/new_api/line_binding_family.php?userSid=' . $data->events[0]->source->userId
                        ])
                    );
                }
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
                $result = curl_exec($ch);
                curl_close($ch);
            }else if($data->events[0]->message->text == '查詢家人列表'){
		    //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                    $url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                    $headers = array('Content-Type:  application/x-www-form-urlencoded');
                    $fields = array(
                        'sid' => $data->events[0]->source->userId,
                        'type' => 'template',
                        'altText' => '個人守護站服務',
                        'templateType' => 'buttons',
                        'imageAspectRadio' => 'rectangle',
                        'imageSize' => 'cover',
                        'thumbnailImageUrl' => 'https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg',
                        'imageBackgroundColor' => '#FFFFFF',
                        'templeteTitle' => '守護站個人化服務',
                        'templateText' => $account_data['name'] . '守護站個人化服務選單。',
                        'actions' => array([
                            'type' => 'uri',
                            'label' => urlencode('查詢家人列表'),
                            'uri' => line_url_dress . '/horncloud/new_api/line_personal_family_list_page.php?userSid=' . $data->events[0]->source->userId
                        ])
                    );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
                $result = curl_exec($ch);

                curl_close($ch);

	    } else if ($data->events[0]->message->text == '我要查本周血糖') {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', $this->bloodSugerList($data->events[0]->source->userId));
            } else if ($data->events[0]->message->text == '我要查本周血壓') {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', $this->bloodPressureList($data->events[0]->source->userId));
            } else if ($data->events[0]->message->text == '開通個人化服務') {
                $account_search = 'select name,account,id from account where line_sid=\'' . $data->events[0]->source->userId . '\'';
                $account_count = $this->getRows($account_search);
                $account_data = $this->getRow($account_search);
                if ($account_count == 0) {
                    //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                    $url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                    $headers = array('Content-Type:  application/x-www-form-urlencoded');
                    $fields = array(
                        'sid' => $data->events[0]->source->userId,
                        'type' => 'template',
                        'altText' => '開通個人守護站服務',
                        'templateType' => 'buttons',
                        'imageAspectRadio' => 'rectangle',
                        'imageSize' => 'cover',
                        'thumbnailImageUrl' => 'https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg',
                        'imageBackgroundColor' => '#FFFFFF',
                        'templeteTitle' => '守護站個人化服務',
                        'templateText' => $account_data['name'] . '守護站個人化服務選單。',
                        'actions' => array([
                            'type' => 'uri',
                            'label' => urlencode('開通個人化服務'),
                            'uri' => line_url_dress . '/horncloud/new_api/line_binding_register.php?userSid=' . $data->events[0]->source->userId
                        ], [
                            'type' => 'uri',
                            'label' => urlencode('綁定家人帳號'),
                            'uri' => line_url_dress . '/horncloud/new_api/line_binding_family.php?userSid=' . $data->events[0]->source->userId
                        ]/*, [
                        'type' => 'uri',
                        'label' => urlencode('解除個人化服務'),
                        'text' => line_url_dress . '/horncloud/new_api/line_binding_register_unlock.php?userSid=' . $data->events[0]->source->userId
                    ]*//*)
                    );
                } else if ($account_count > 0) {
                    //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                    $url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                    $headers = array('Content-Type:  application/x-www-form-urlencoded');
                    $fields = array(
                        'sid' => $data->events[0]->source->userId,
                        'type' => 'template',
                        'altText' => '個人守護站服務',
                        'templateType' => 'buttons',
                        'imageAspectRadio' => 'rectangle',
                        'imageSize' => 'cover',
                        'thumbnailImageUrl' => 'https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg',
                        'imageBackgroundColor' => '#FFFFFF',
                        'templeteTitle' => '守護站個人化服務',
                        'templateText' => $account_data['name'] . '守護站個人化服務選單。',
                        'actions' => array([
                            'type' => 'uri',
                            'label' => urlencode('解除個人化服務'),
                            'uri' => line_url_dress . '/horncloud/new_api/line_binding_register_unlock.php?userSid=' . $data->events[0]->source->userId
                        ], [
                            'type' => 'uri',
                            'label' => urlencode('綁定家人帳號'),
                            'uri' => line_url_dress . '/horncloud/new_api/line_binding_family.php?userSid=' . $data->events[0]->source->userId
                        ], [
                            'type' => 'uri',
                            'label' => urlencode('查詢家人列表'),
                            'uri' => line_url_dress . '/horncloud/new_api/line_personal_family_list_page.php?userSid=' . $data->events[0]->source->userId
                        ])
                    );
                }
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
                $result = curl_exec($ch);

                curl_close($ch);
            } else if ($data->events[0]->message->text == '視訊門診功能') {
                $account_data = $this->getRows('select id from account where line_sid = \'' . $data->events[0]->source->userId . '\' and enable = \'1\'');
                if ($account_data >= 1) {
                    //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                    $url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                    $headers = array('Content-Type:  application/x-www-form-urlencoded');
                    $fields = array(
                        'sid' => $data->events[0]->source->userId,
                        'type' => 'template',
                        'altText' => '個人守護站服務',
                        'templateType' => 'buttons',
                        'imageAspectRadio' => 'rectangle',
                        'imageSize' => 'cover',
                        'thumbnailImageUrl' => 'https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg',
                        'imageBackgroundColor' => '#FFFFFF',
                        'templeteTitle' => '守護站個人化服務',
                        'templateText' => '守護站個人化服務選單。',
                        'actions' => array([
                            'type' => 'uri',
                            'label' => urlencode('查詢門診進度'),
                            'uri' => line_url_dress . '/horncloud/new_api/views_schedule_search.php?userSid=' . $data->events[0]->source->userId
                        ], [
                            'type' => 'uri',
                            'label' => urlencode('查詢掛號紀錄'),
                            'uri' => line_url_dress . '/horncloud/new_api/views_personal_views_history_page.php?userSid=' . $data->events[0]->source->userId
                        ])
                    );
                } else {
                    //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                    $url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                    $headers = array('Content-Type:  application/x-www-form-urlencoded');
                    $fields = array(
                        'sid' => $data->events[0]->source->userId,
                        'type' => 'template',
                        'altText' => '個人守護站服務',
                        'templateType' => 'buttons',
                        'imageAspectRadio' => 'rectangle',
                        'imageSize' => 'cover',
                        'thumbnailImageUrl' => 'https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg',
                        'imageBackgroundColor' => '#FFFFFF',
                        'templeteTitle' => '守護站個人化服務',
                        'templateText' => '守護站個人化服務選單。',
                        'actions' => array([
                            'type' => 'uri',
                            'label' => urlencode('查詢門診進度'),
                            'uri' => line_url_dress . '/horncloud/new_api/views_schedule_search.php?userSid=' . $data->events[0]->source->userId
                        ])
                    );
                }
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
                $result = curl_exec($ch);
                curl_close($ch);
            } else if ($data->events[0]->message->text == '我要查掛號紀錄') {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', $this->view_get_user_list_line($data->events[0]->source->userId));
            } else if ($data->events[0]->message->text == '新增家人訂閱推播') {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', line_url_dress . '/horncloud/new_api/line_binding_family.php?userSid=' . $data->events[0]->source->userId);
            } else {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', '感謝您的訊息！很抱歉，本帳號無法個別回覆用戶的訊息。敬請期待我們下次發送的內容喔。');
            }
        } else if ($data->events[0]->type == 'postback') {
            if (explode('@', $data->events[0]->postback->data)[0] == 'binding_family') {
                $this->binding_family_feedback($data->events[0]->source->userId, explode('#', explode('@', $data->events[0]->postback->data)[1])[0], explode('#', explode('@', $data->events[0]->postback->data)[1])[1]);
            }
        }
    }
    */

    public function setReturnMessage($request)
    {
        $url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/horn/lineservice/new_api/line_push_message.php';
        $url_sche = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
        $data = json_decode($request);
        if ($data->events[0]->type == 'message') {
            if ($data->events[0]->message->text == 'LINE功能綁定群組') {
                //$this->pushMessageLocal($data->events[0]->source->userId, 'text', $url);
                $headers = array('Content-Type:  application/x-www-form-urlencoded');
                $fields = array(
                    'sid' => $data->events[0]->source->userId,
                    'type' => 'template',
                    'altText' => '個人守護站服務',
                    'templateType' => 'buttons',
                    'imageAspectRadio' => 'rectangle',
                    'imageSize' => 'cover',
                    'thumbnailImageUrl' => 'https://scontent.fkhh1-2.fna.fbcdn.net/v/t39.30808-6/274488149_3087539731461022_1077884986092091155_n.png?_nc_cat=109&ccb=1-5&_nc_sid=e3f864&_nc_ohc=zzRPhn7wnJEAX_mPBBe&_nc_ht=scontent.fkhh1-2.fna&oh=00_AT_ygXiDmefy9uUAV7Wvj1KcJZaG6610h7h3kCjY8Nvv6A&oe=62230485',
                    'imageBackgroundColor' => '#FFFFFF',
                    'templeteTitle' => '個人化服務',
                    'templateText' => '個人化服務選單。',
                    'actions' => array([
                        'type' => 'uri',
                        'label' => urlencode('登入帳號綁定'),
                        'uri' =>  $url_sche . '/horn/lineservice/LINE/login.php?userSid=' . $data->events[0]->source->userId
                    ], [
                        'type' => 'uri',
                        'label' => urlencode('忘記密碼'),
                        'uri' =>  $url_sche . '/horn/lineservice/LINE/forget.php?userSid=' . $data->events[0]->source->userId
                    ], [
                        'type' => 'uri',
                        'label' => urlencode('解除帳號綁定'),
                        'uri' => $url_sche . '/horn/lineservice/LINE/bindUnlock.php?userSid=' . $data->events[0]->source->userId
                    ])
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
                $result = curl_exec($ch);
                curl_close($ch);
            } else if ($data->events[0]->message->text == '請求sid') {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', $data->events[0]->source->userId);
            } else if ($data->events[0]->message->text == 'LINE實聯制') {
                $headers = array('Content-Type:  application/x-www-form-urlencoded');
                $fields = array(
                    'sid' => $data->events[0]->source->userId,
                    'type' => 'template',
                    'altText' => '個人守護站服務',
                    'templateType' => 'buttons',
                    'imageAspectRadio' => 'rectangle',
                    'imageSize' => 'cover',
                    'thumbnailImageUrl' => 'https://scontent.fkhh1-2.fna.fbcdn.net/v/t39.30808-6/274488149_3087539731461022_1077884986092091155_n.png?_nc_cat=109&ccb=1-5&_nc_sid=e3f864&_nc_ohc=zzRPhn7wnJEAX_mPBBe&_nc_ht=scontent.fkhh1-2.fna&oh=00_AT_ygXiDmefy9uUAV7Wvj1KcJZaG6610h7h3kCjY8Nvv6A&oe=62230485',
                    'imageBackgroundColor' => '#FFFFFF',
                    'templeteTitle' => 'ALL New!!實聯制服務',
                    'templateText' => '實聯制功能選單。',
                    'actions' => array([
                        'type' => 'uri',
                        'label' => urlencode('實聯制掃碼'),
                        'uri' =>  $url_sche . '/horn/lineservice/LINE/camera.php?userSid=' . $data->events[0]->source->userId
                    ], [
                        'type' => 'postback',
                        'label' => urlencode('實聯制個人紀錄查詢'),
                        'data' => 'searchRealUnion'
                    ])
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
                $result = curl_exec($ch);
                curl_close($ch);
            } else if ($data->events[0]->message->text == 'searchRealUnion') {
                //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                //$url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                $headers = array('Content-Type:  application/x-www-form-urlencoded');
                $fields = array(
                    'sid' => $data->events[0]->source->userId,
                    'type' => 'template',
                    'altText' => '加入家人守護站，異常數據即通知',
                    'templateType' => 'buttons',
                    'imageAspectRadio' => 'rectangle',
                    'imageSize' => 'cover',
                    'thumbnailImageUrl' => 'https://scontent.fkhh1-2.fna.fbcdn.net/v/t39.30808-6/274488149_3087539731461022_1077884986092091155_n.png?_nc_cat=109&ccb=1-5&_nc_sid=e3f864&_nc_ohc=zzRPhn7wnJEAX_mPBBe&_nc_ht=scontent.fkhh1-2.fna&oh=00_AT_ygXiDmefy9uUAV7Wvj1KcJZaG6610h7h3kCjY8Nvv6A&oe=62230485',
                    'imageBackgroundColor' => '#FFFFFF',
                    'templeteTitle' => '實聯制個人歷史查詢',
                    'templateText' => '請選擇查詢天數',
                    'actions' => array(
                        [
                            'type' => 'uri',
                            'label' => urlencode('5日內'),
                            'uri' => line_url_dress . '/horn/lineService/LINE/realUnionHistory.php?userSid=' . $data->events[0]->source->userId . '&day=5'
                        ], [
                            'type' => 'postback',
                            'label' => urlencode('10日內'),
                            'data' => line_url_dress . '/horn/lineService/LINE/realUnionHistory.php?userSid=' . $data->events[0]->source->userId . '&day=10'
                        ], [
                            'type' => 'postback',
                            'label' => urlencode('20日內'),
                            'data' => line_url_dress . '/horn/lineService/LINE/realUnionHistory.php?userSid=' . $data->events[0]->source->userId . '&day=20'
                        ],
                        [
                            'type' => 'postback',
                            'label' => urlencode('28日內(歷史資料保留28日)'),
                            'data' => line_url_dress . '/horn/lineService/LINE/realUnionHistory.php?userSid=' . $data->events[0]->source->userId . '&day=28'
                        ]
                    )
                    /*'actions' => array(
                        /*[
                            'type' => 'postback',
                            'label' => urlencode('今日'),
                            'data' => 'searchRealUnionDay_1'
                        ], [
                            'type' => 'postback',
                            'label' => urlencode('3日內'),
                            'data' => 'searchRealUnionDay_3'
                        ], [
                            'type' => 'postback',
                            'label' => urlencode('5日內'),
                            'data' => 'searchRealUnionDay_5'
                        ], [
                            'type' => 'postback',
                            'label' => urlencode('10日內'),
                            'data' => 'searchRealUnionDay_10'
                        ], [
                            'type' => 'postback',
                            'label' => urlencode('20日內'),
                            'data' => 'searchRealUnionDay_20'
                        ],
                        [
                            'type' => 'postback',
                            'label' => urlencode('28日內(歷史資料保留28日)'),
                            'data' => 'searchRealUnionDay_28'
                        ]
                    )*/
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
                $result = curl_exec($ch);
                file_put_contents('log.txt', $result);
                curl_close($ch);
            } else if ($data->events[0]->message->text == '查詢家人列表') {
                //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                //$url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                $headers = array('Content-Type:  application/x-www-form-urlencoded');
                $fields = array(
                    'sid' => $data->events[0]->source->userId,
                    'type' => 'template',
                    'altText' => '個人守護站服務',
                    'templateType' => 'buttons',
                    'imageAspectRadio' => 'rectangle',
                    'imageSize' => 'cover',
                    'thumbnailImageUrl' => 'https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg',
                    'imageBackgroundColor' => '#FFFFFF',
                    'templeteTitle' => '守護站個人化服務',
                    'templateText' => $account_data['name'] . '守護站個人化服務選單。',
                    'actions' => array([
                        'type' => 'uri',
                        'label' => urlencode('查詢家人列表'),
                        'uri' => line_url_dress . '/horncloud/new_api/line_personal_family_list_page.php?userSid=' . $data->events[0]->source->userId
                    ])
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
                $result = curl_exec($ch);

                curl_close($ch);
            } else if ($data->events[0]->message->text == '我要查本周血糖') {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', $this->bloodSugerList($data->events[0]->source->userId));
            } else if ($data->events[0]->message->text == '我要查本周血壓') {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', $this->bloodPressureList($data->events[0]->source->userId));
            } else if ($data->events[0]->message->text == '開通個人化服務') {
                $account_search = 'select name,account,id from account where line_sid=\'' . $data->events[0]->source->userId . '\'';
                $account_count = $this->getRows($account_search);
                $account_data = $this->getRow($account_search);
                if ($account_count == 0) {
                    //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                    $url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                    $headers = array('Content-Type:  application/x-www-form-urlencoded');
                    $fields = array(
                        'sid' => $data->events[0]->source->userId,
                        'type' => 'template',
                        'altText' => '開通個人守護站服務',
                        'templateType' => 'buttons',
                        'imageAspectRadio' => 'rectangle',
                        'imageSize' => 'cover',
                        'thumbnailImageUrl' => 'https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg',
                        'imageBackgroundColor' => '#FFFFFF',
                        'templeteTitle' => '守護站個人化服務',
                        'templateText' => $account_data['name'] . '守護站個人化服務選單。',
                        'actions' => array([
                            'type' => 'uri',
                            'label' => urlencode('開通個人化服務'),
                            'uri' => line_url_dress . '/horncloud/new_api/line_binding_register.php?userSid=' . $data->events[0]->source->userId
                        ], [
                            'type' => 'uri',
                            'label' => urlencode('綁定家人帳號'),
                            'uri' => line_url_dress . '/horncloud/new_api/line_binding_family.php?userSid=' . $data->events[0]->source->userId
                        ]/*, [
                        'type' => 'uri',
                        'label' => urlencode('解除個人化服務'),
                        'text' => line_url_dress . '/horncloud/new_api/line_binding_register_unlock.php?userSid=' . $data->events[0]->source->userId
                    ]*/)
                    );
                } else if ($account_count > 0) {
                    //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                    $url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                    $headers = array('Content-Type:  application/x-www-form-urlencoded');
                    $fields = array(
                        'sid' => $data->events[0]->source->userId,
                        'type' => 'template',
                        'altText' => '個人守護站服務',
                        'templateType' => 'buttons',
                        'imageAspectRadio' => 'rectangle',
                        'imageSize' => 'cover',
                        'thumbnailImageUrl' => 'https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg',
                        'imageBackgroundColor' => '#FFFFFF',
                        'templeteTitle' => '守護站個人化服務',
                        'templateText' => $account_data['name'] . '守護站個人化服務選單。',
                        'actions' => array([
                            'type' => 'uri',
                            'label' => urlencode('解除個人化服務'),
                            'uri' => line_url_dress . '/horncloud/new_api/line_binding_register_unlock.php?userSid=' . $data->events[0]->source->userId
                        ], [
                            'type' => 'uri',
                            'label' => urlencode('綁定家人帳號'),
                            'uri' => line_url_dress . '/horncloud/new_api/line_binding_family.php?userSid=' . $data->events[0]->source->userId
                        ], [
                            'type' => 'uri',
                            'label' => urlencode('查詢家人列表'),
                            'uri' => line_url_dress . '/horncloud/new_api/line_personal_family_list_page.php?userSid=' . $data->events[0]->source->userId
                        ])
                    );
                }
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
                $result = curl_exec($ch);

                curl_close($ch);
            } else if ($data->events[0]->message->text == '視訊門診功能') {
                $account_data = $this->getRows('select id from account where line_sid = \'' . $data->events[0]->source->userId . '\' and enable = \'1\'');
                if ($account_data >= 1) {
                    //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                    $url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                    $headers = array('Content-Type:  application/x-www-form-urlencoded');
                    $fields = array(
                        'sid' => $data->events[0]->source->userId,
                        'type' => 'template',
                        'altText' => '個人守護站服務',
                        'templateType' => 'buttons',
                        'imageAspectRadio' => 'rectangle',
                        'imageSize' => 'cover',
                        'thumbnailImageUrl' => 'https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg',
                        'imageBackgroundColor' => '#FFFFFF',
                        'templeteTitle' => '守護站個人化服務',
                        'templateText' => '守護站個人化服務選單。',
                        'actions' => array([
                            'type' => 'uri',
                            'label' => urlencode('查詢門診進度'),
                            'uri' => line_url_dress . '/horncloud/new_api/views_schedule_search.php?userSid=' . $data->events[0]->source->userId
                        ], [
                            'type' => 'uri',
                            'label' => urlencode('查詢掛號紀錄'),
                            'uri' => line_url_dress . '/horncloud/new_api/views_personal_views_history_page.php?userSid=' . $data->events[0]->source->userId
                        ])
                    );
                } else {
                    //IMAGE LINK : https://i.imgur.com/PrNxuGH.png
                    $url = 'https://' . $_SERVER['HTTP_HOST'] . '/horncloud/new_api/line_push_message.php';
                    $headers = array('Content-Type:  application/x-www-form-urlencoded');
                    $fields = array(
                        'sid' => $data->events[0]->source->userId,
                        'type' => 'template',
                        'altText' => '個人守護站服務',
                        'templateType' => 'buttons',
                        'imageAspectRadio' => 'rectangle',
                        'imageSize' => 'cover',
                        'thumbnailImageUrl' => 'https://www.hncare.cloud/horncloud/img/line/line_bot_workarea.jpg',
                        'imageBackgroundColor' => '#FFFFFF',
                        'templeteTitle' => '守護站個人化服務',
                        'templateText' => '守護站個人化服務選單。',
                        'actions' => array([
                            'type' => 'uri',
                            'label' => urlencode('查詢門診進度'),
                            'uri' => line_url_dress . '/horncloud/new_api/views_schedule_search.php?userSid=' . $data->events[0]->source->userId
                        ])
                    );
                }
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
                $result = curl_exec($ch);
                curl_close($ch);
            } else if ($data->events[0]->message->text == '我要查掛號紀錄') {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', $this->view_get_user_list_line($data->events[0]->source->userId));
            } else if ($data->events[0]->message->text == '新增家人訂閱推播') {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', line_url_dress . '/horncloud/new_api/line_binding_family.php?userSid=' . $data->events[0]->source->userId);
            } else {
                $this->pushMessageLocal($data->events[0]->source->userId, 'text', '感謝您的訊息！很抱歉，本帳號無法個別回覆用戶的訊息。敬請期待我們下次發送的內容喔。');
            }
        } else if ($data->events[0]->type == 'postback') {
            if (explode('@', $data->events[0]->postback->data)[0] == 'binding_family') {
                $this->binding_family_feedback($data->events[0]->source->userId, explode('#', explode('@', $data->events[0]->postback->data)[1])[0], explode('#', explode('@', $data->events[0]->postback->data)[1])[1]);
            }
        }
    }

    public function pushMessageLocal($sid, $type, $message = '')
    {
        $_header = array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . message_chanel_token
        );
        switch ($type) {
            case 'text':
                $content = array(
                    'type' => $type,
                    'text' => urlencode($message)
                );
                break;
            case 'image':
                $content = array(
                    'type' => $type,
                    'originalContentUrl' => $_POST['originalContentUrl'],
                    'previewImageUrl' => $_POST['previewImageUrl']
                );
                break;
            case 'video':
                $content = array(
                    'type' => $type,
                    'originalContentUrl' => $_POST['originalContentUrl'],
                    'previewImageUrl' => $_POST['previewImageUrl']
                );
                break;
            case 'location':
                $content = array(
                    'type' => $type,
                    'title' => $_POST['title'],
                    'address' => $_POST['address'],
                    'latitude' => $_POST['latitude'],
                    'longitude' => $_POST['longitube']
                );
                break;
        }


        $form_data = array();

        $form_data = [
            'to' => $sid,
            'messages' => array($content)
        ];

        $r_body = urldecode(json_encode($form_data));

        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_POST => true
        );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.line.me/v2/bot/message/push');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $_header);
        curl_setopt_array($curl, $options);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($form_data));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $r_body);
        $result = curl_exec($curl);
        curl_close($curl);
        if ($curl) {
            $output['success'] = 1;
            return true;
        } else {
            $output['success'] = 0;
            $output['message']['ch'] = 'Line 推播訊息傳送失敗。';
            $output['message']['en'] = 'Line message push failed.';
            return false;
        }
    }

    //LINE圖片地圖更新
    public function setImageMap(&$output)
    {
        $content = isset($_POST['content']) ? $_POST['content'] : '';

        if ($content == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入json data') : urlencode(', 請輸入json data');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in json data' : ', Please fill in json data.';
            $output['success'] = 0;
        }

        if ((!isset($_POST['content']) && !$_POST['content'])) {
            $output['message']['ch'] = urlencode('未收到資料');
            $output['message']['en'] = 'Data not received.';
            $output['success'] = 0;
        }

        if ($output['success']) return $this->setImageMapRoute($output, $content);
    }

    public function setImageMapRoute(&$output, $content)
    {
        $_header = array(
            'Content-Type: application/json',
            'authorization: ' . message_chanel_token,
            'cache-control: no-cache'
        );

        $form_data = array();

        $form_data = [
            // 'to' => $sid,
            'messages' => array($content)
        ];

        $r_body = urldecode(json_encode($form_data));

        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_POST => true
        );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.line.me/v2/bot/richmenu');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $_header);
        curl_setopt_array($curl, $options);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($form_data));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $r_body);
        $result = curl_exec($curl);
        curl_close($curl);
        //file_put_contents('logsss.txt', $result);
        if ($curl) {
            $output['success'] = 1;
            return true;
        } else {
            $output['success'] = 0;
            $output['message']['ch'] = 'Line 推播訊息傳送失敗。';
            $output['message']['en'] = 'Line message push failed.';
            return false;
        }
    }

    //上傳圖片地圖
    public function imageMapUpload(&$output)
    {

        $richMenu = isset($_POST['richMenu']) ? $_POST['richMenu'] : '';

        if ($richMenu == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入圖片地圖ID') : urlencode(', 請輸入圖片地圖ID');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in image map ID' : ', Please fill in image map ID.';
            $output['success'] = 0;
        }

        if ((!isset($_POST['richMenu']) && !$_POST['richMenu'])) {
            $output['message']['ch'] = urlencode('未收到資料');
            $output['message']['en'] = 'Data not received.';
            $output['success'] = 0;
        }

        $arr_fid = 'file';
        $name_path = '';                                //父層預覽圖
        $msg_ = '';                                     //父層訊息
        $new_name = '';                                 //新檔名
        $size_limit = 1024 * 1024;                      //圖檔2M以內
        $wh_limit = 270;                                //圖寬高限制
        $img_html = '';

        if ((!isset($_FILES[$arr_fid]) && !$_FILES[$arr_fid])) {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請上傳圖片') : urlencode(', 請上傳圖片');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in image' : ', Please fill in image.';
            $output['success'] = 0;
        } else {
            //限制圖片格式JPG、PNG、GIF
            if (
                $_FILES[$arr_fid]['type'] == 'image/png' || $_FILES[$arr_fid]['type'] == 'image/gif' || $_FILES[$arr_fid]['type'] == 'image/jpeg'
                || $_FILES[$arr_fid]['type'] == 'image/pjpeg' || $_FILES[$arr_fid]['type'] == 'image/x-png'
            ) {
                /*$old_name = $_FILES[$arr_fid[0]]['name'];    //原檔名+副檔名
        $old_name = explode(".", $old_name);    //分割原檔名
        $new_name = date("YmdHis") . "." . $old_name[1]; //新檔名+副檔名*/
                $img_size = $_FILES[$arr_fid]['size'];    //檔大小
                $img_size = number_format($img_size / 1024, 1);    //檔大小單位K
                if ($img_size <= $size_limit) {
                    $output['success'] = 1;
                } else {
                    $new_name = '';
                    $msg_ = '圖超過限制' . $img_size . '<= ' . $size_limit;
                    $output['success'] = 0;
                }
            } else {
                $output['success'] = 0;
                $msg_ = '非可接受的圖片格式';
            }
            $output['message']['ch'] = $msg_;
        }

        //if ($output['success']) return $this->imageUploadToLine($output, $richMenu, $_FILES[$arr_fid]['tmp_name']);
    }

    //Line查異常紀錄
    public function alertList($sid)
    {
        $sql = "select * from account where id in (select account_id from line_fcm_friend where line_token = '" . $sid . "' and invite = '1')";
        //$sql2 = "select friend_id from line_fcm_friend where account_id = (select id from account where line_sid = '" . $sid . "') and invite=1";
        //$personal_num = 0;
        //$personal_count = 0; //個人資料總筆數
        $family_num = 0;
        $family_count = 0; //家屬資料總筆數
        //$personal = '';
        $family = '';
        $id = 0;
        $content = '';
        //file_put_contents('start.txt',$sql);
        //$self_data = $this->getQuery($sql)['row'];
        $family_data = $this->getQuery($sql)['row'];
        //$account_data = $this->getQuery($sql);
        /*foreach ($self_data as $row) {
            $personal_count++;
        }*/

        foreach ($family_data as $row) {
            $family_count++;
        }
        //file_put_contents('start.txt',$sql);
        /*$self_data = $this->getQuery($sql)['row'];
        $family_data = $this->getQuery($sql2)['row'];
        //$account_data = $this->getQuery($sql);
        /*foreach ($self_data as $row) {
            $personal_num++;
        }*/

        /* foreach ($family_data as $row) {
            $family_count++;
        }*/
        //個人紀錄===開始
        //$alert_info_personal = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date');
        /*//file_put_contents('sqlss.txt', 'select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date');
        $alert_info_personal_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date');
        $alert_info_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select friend_id from line_fcm_friend where account_id = ( select id from account where line_sid = \'' . $sid . '\') and invite=1 ) and date_sub(curdate(),interval 1 day) <= date');
        $alert_info_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select friend_id from line_fcm_friend where account_id = ( select id from account where line_sid = \'' . $sid . '\') and invite=1 ) and date_sub(curdate(),interval 1 day) <= date');*/
        /*$physiological_signal_personal = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'高|低|指數\' order by establish desc');
        $physiological_signal_personal_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'高|低|指數\' order by establish desc');
        $btag_sql_personal = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'緊急\' order by establish desc');
        $btag_sql_personal_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'緊急\' order by establish desc');
        $no_measurement_personal = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'未量測\' order by establish desc');
        $no_measurement_personal_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'未量測\' order by establish desc');
        $getstation_sql_personal = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'糖患者\' order by establish desc');
        $getstation_sql_personal_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'糖患者\' order by establish desc');
        $unused_medicine_personal = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'未用藥\' order by establish desc');
        $unused_medicine_personal_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'未用藥\' order by establish desc');*/
        /*$afib_sql_personal = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'疑似|心跳過快|心跳過慢\' order by establish desc');
        $afib_sql_personal_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'疑似|心跳過快|心跳過慢\' order by establish desc');
        $temparater_sql_personal = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'體溫貼\' order by establish desc');
        $temparater_sql_personal_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in ( select id from account where line_sid = \'' . $sid . '\' ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'體溫貼\' order by establish desc');*/
        //個人紀錄===結束
        //家屬紀錄===開始
        $physiological_signal_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'高|低|指數\' order by establish desc');
        $physiological_signal_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'高|低|指數\' order by establish desc');
        $btag_sql_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'緊急\' order by establish desc');
        $btag_sql_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'緊急\' order by establish desc');
        $no_measurement_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'未量測\' order by establish desc');
        $no_measurement_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'未量測\' order by establish desc');
        $getstation_sql_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'糖患者\' order by establish desc');
        $getstation_sql_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'糖患者\' order by establish desc');
        $unused_medicine_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'未用藥\' order by establish desc');
        $unused_medicine_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'未用藥\' order by establish desc');
        /* $afib_sql_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select friend_id from line_fcm_friend where account_id = ( select id from account where line_sid = \'' . $sid . '\') and invite=1 ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'疑似|心跳過快|心跳過慢\' order by establish desc');
        $afib_sql_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select friend_id from line_fcm_friend where account_id = ( select id from account where line_sid = \'' . $sid . '\') and invite=1 ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'疑似|心跳過快|心跳過慢\' order by establish desc');
        $temparater_sql_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select friend_id from line_fcm_friend where account_id = ( select id from account where line_sid = \'' . $sid . '\') and invite=1 ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'體溫貼\' order by establish desc');
        $temparater_sql_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select friend_id from line_fcm_friend where account_id = ( select id from account where line_sid = \'' . $sid . '\') and invite=1 ) and date_sub(curdate(),interval 1 day) <= date and message regexp \'體溫貼\' order by establish desc');*/
        //家屬紀錄===結束
        //$alert_info_personal_count = ($physiological_signal_personal_count + $btag_sql_personal_count + $no_measurement_personal_count + $getstation_sql_personal_count + $unused_medicine_personal_count/*+$afib_sql_personal_count+$temparater_sql_personal_count*/);
        $alert_info_family_count = ($physiological_signal_family_count + $btag_sql_family_count + $no_measurement_family_count + $getstation_sql_family_count + $unused_medicine_family_count/*+$afib_sql_family_count+$temparater_sql_family_count*/);
        /*if ($alert_info_personal_count == 0) {
            $content .= '查無個人資料。\n\n';
        } else {
            $content .= '個人紀錄:\n\n';
            foreach ($physiological_signal_personal['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '-' . $row['date'] . '-' . $row['message'] . '\n\n';
            }
            foreach ($btag_sql_personal['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '-' . $row['date'] . '-' . $row['message'] . '\n\n';
            }
            foreach ($no_measurement_personal['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '-' . $row['date'] . '-' . $row['message'] . '\n\n';
            }
            foreach ($getstation_sql_personal['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '-' . $row['date'] . '-' . $row['message'] . '\n\n';
            }
            foreach ($unused_medicine_personal['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '-' . $row['date'] . '-' . $row['message'] . '\n\n';
            }
            /*foreach ($afib_sql_personal['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '-' . $row['date'] . '-' . $row['message'] . '\n\n';
            }
            foreach ($temparater_sql_personal['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '-' . $row['date'] . '-' . $row['message'] . '\n\n';
            }
        }*/
        if ($alert_info_family_count == 0) {
            $content .= '查無家屬資料。\n\n';
        } else {
            $content .= '家屬紀錄:\n\n';

            foreach ($physiological_signal_family['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '\n' . $row['date'] . '\n' . $row['message'] . '\n\n';
            }
            foreach ($btag_sql_family['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '\n' . $row['date'] . '\n' . $row['message'] . '\n\n';
            }
            foreach ($no_measurement_family['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '\n' . $row['date'] . '\n' . $row['message'] . '\n\n';
            }
            foreach ($getstation_sql_family['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '\n' . $row['date'] . '\n' . $row['message'] . '\n\n';
            }
            foreach ($unused_medicine_family['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '\n' . $row['date'] . '\n' . $row['message'] . '\n\n';
            }
            /*foreach ($afib_sql_family['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '-' . $row['date'] . '-' . $row['message'] . '\n\n';
            }
            foreach ($temparater_sql_family['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '-' . $row['date'] . '-' . $row['message'] . '\n\n';
            }*/
        }

        return $content;
    }

    //LINE查詢個人預約紀錄
    public function view_get_user_list_line($sid)
    {
        $history_data = array();
        $content = '';
        $sql = 'select account.name as member_name,outpatient.outpatient_name,doctor.name as doctor_name,main.number,main.visit_date,main.time_period,main.outpatient_id,main.doctor_id,main.time_start,main.time_end from (select * from account where identity = \'院所醫生\') doctor,outpatient_member_list main,account,outpatient where main.member_id=account.id and main.doctor_id=doctor.id and main.outpatient_id=outpatient.id and main.member_id=account.id and main.member_id = (select id from account where line_sid= \'' . $sid . '\') and (main.visit_date between \'' . date('Y-m-d', time()) . '\' and date_sub(curdate(),interval -30 day)) and main.deleted_at is null';
        if ($this->getRows($sql) == 0) {
            $content = '查不到與會員有關的預約紀錄。';
        } else {
            $data = $this->getQuery($sql)['row'];
            foreach ($data as $row_data) {
                $sub_data = $this->getRow('select outpatient.outpatient_name ,organization.org_name from views_schedule main,outpatient,organization where main.outpatient = outpatient.id and main.org_id = organization.id and main.view_date=\'' . $row_data['visit_date'] . '\' and main.doctor = \'' . $row_data['doctor_id'] . '\' and main.outpatient = \'' . $row_data['outpatient_id'] . '\' and main.time_period = \'' . $row_data['time_period'] . '\' and deleted_at is null');
                switch ($row_data['time_period']) {
                    case '早診':
                        $row_data['time_start'] = ($row_data['time_start'] == null ? '07:30:00' : $row_data['time_start']);
                        $row_data['time_end'] = ($row_data['time_end'] == null ? '10:30:00' : $row_data['time_start']);
                        break;
                    case '午診':
                        $row_data['time_start'] = ($row_data['time_start'] == null ? '12:00:00' : $row_data['time_start']);
                        $row_data['time_end'] = ($row_data['time_start'] == null ? '15:00:00' : $row_data['time_start']);
                        break;
                    case '晚診':
                        $row_data['time_start'] = ($row_data['time_start'] == null ? '17:00:00' : $row_data['time_start']);
                        $row_data['time_end'] = ($row_data['time_start'] == null ? '19:30:00' : $row_data['time_start']);
                        break;
                }
                $content .= '醫療院所:' . $sub_data['org_name'];
                $content .= '\n門診科別:' . $sub_data['outpatient_name'];
                $content .= $row_data['doctor_name'] . ' 醫師';
                $content .= '\n號次 ' . $row_data['number'] . '號';
                $content .= '\n看診時間:' . date('Y-m-d', strtotime($row_data['visit_date']));
                $content .= '\n' . $row_data['time_period'] . ' ' . $row_data['time_start'] . '~' . $row_data['time_end'];
                $content .= '\n';
            }
        }
        return $content;
    }

    //Line查本週血糖
    public function bloodSugerList($sid)
    {
        //$sql = "select * from account where concat(line_sid) like '%\"sid\":\"" . $sid . "\"%'";
        //$sql = "select * from account where line_sid = '" . $sid . "'";
        $sql = "select * from account where id in (select account_id from line_fcm_friend where line_token = '" . $sid . "' and invite = '1')";
        //$sql2 = "select friend_id from line_fcm_friend where account_id = (select id from account where line_sid = '" . $sid . "') and invite=1";
        //$personal_num = 0;
        //$personal_count = 0; //個人資料總筆數
        $family_num = 0;
        $family_count = 0; //家屬資料總筆數
        // $personal = '';
        $family = '';
        $id = 0;
        $content = '';
        //file_put_contents('start.txt',$sql);
        //$self_data = $this->getQuery($sql)['row'];
        $family_data = $this->getQuery($sql)['row'];
        //$account_data = $this->getQuery($sql);
        /*foreach ($self_data as $row) {
            $personal_count++;
        }*/

        foreach ($family_data as $row) {
            $family_count++;
        }

        /*foreach ($account_data['row'] as $row) {
            //file_put_contents('data' . $id . '.txt', 'ddd');
            foreach (json_decode(urldecode($row['line_sid']))->data as $data) {
                if ($data->category == 'personal' && $sid = $data->sid) {
                    $personal_num++;
                    if ($personal_num == $personal_count)
                        $personal .= $row['id'];
                    else
                        $personal = $row['id'] . ',';
                } else  if ($data->category == 'family' && $sid = $data->sid) {
                    $family_num++;
                    if ($family_num == $family_count)
                        $family .= $row['id'];
                    else
                        $family = $row['id'] . ',';
                }
            }
        }*/
        // $blood_sugar_info_personal = $this->getQuery('select blood_sugar.*,account.name as account_name from blood_sugar,account where blood_sugar.account_id=account.id and account_id in (select id from account where line_sid = \'' . $sid . '\') and date_sub(curdate(),interval 7 day) <= record_date');
        //$blood_sugar_info_personal_count = $this->getRows('select blood_sugar.*,account.name as account_name from blood_sugar,account where blood_sugar.account_id=account.id and account_id in (select id from account where line_sid = \'' . $sid . '\') and date_sub(curdate(),interval 7 day) <= record_date');
        $blood_sugar_info_family = $this->getQuery('select blood_sugar.*,account.name as account_name from blood_sugar,account where blood_sugar.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 7 day) <= record_date');
        $blood_sugar_info_family_count = $this->getRows('select blood_sugar.*,account.name as account_name from blood_sugar,account where blood_sugar.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 7 day) <= record_date');
        /*if ($blood_sugar_info_personal_count == 0) {
            $content .= '查無個人資料。\n\n';
        } else {
            $content .= '個人紀錄:\n\n';
            foreach ($blood_sugar_info_personal['row'] as $row) {
                $record_id_index = 0;
                $bs_code_index = 0;
                $record_time_index = 0;
                $record_id_data = explode(',', $row['record_id']);
                $bs_code_data = explode(',', $row['bs_code']);
                $record_time_data = explode(',', $row['record_time']);
                foreach (explode(',', $row['bs_value']) as $bs_value) {
                    switch ($bs_code_data[$bs_code_index]) {
                        case 0:
                            $content .= $row['account_name'] . '-' . $row['record_date'] . '-自訂時段血糖:' . $bs_value . '測量時間 ' . $record_time_data[$record_time_index] . ',測量者:' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 1:
                            $content .= $row['account_name'] . '-' . $row['record_date'] . '-早上空腹:' . $bs_value . '測量時間 ' . $record_time_data[$record_time_index] . ',測量者:' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 8:
                            $content .= $row['account_name'] . '-' . $row['record_date'] . '-早上飯後一小時:' . $bs_value . '測量時間 ' . $record_time_data[$record_time_index] . ',測量者:' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 2:
                            $content .= $row['account_name'] . '-' . $row['record_date'] . '-早上飯後兩小時:' . $bs_value . '測量時間 ' . $record_time_data[$record_time_index] . ',測量者:' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 3:
                            $content .= $row['account_name'] . '-' . $row['record_date'] . '-中午飯前:' . $bs_value . '測量時間 ' . $record_time_data[$record_time_index] . ',測量者:' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 9:
                            $content .= $row['account_name'] . '-' . $row['record_date'] . '-中午飯後一小時:' . $bs_value . '測量時間 ' . $record_time_data[$record_time_index] . ',測量者:' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 4:
                            $content .= $row['account_name'] . '-' . $row['record_date'] . '-中午飯後兩小時:' . $bs_value . '測量時間 ' . $record_time_data[$record_time_index] . ',測量者:' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 5:
                            $content .= $row['account_name'] . '-' . $row['record_date'] . '-晚上飯前:' . $bs_value . '測量時間 ' . $record_time_data[$record_time_index] . ',測量者:' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 10:
                            $content .= $row['account_name'] . '-' . $row['record_date'] . '-晚上飯後一小時:' . $bs_value . '測量時間 ' . $record_time_data[$record_time_index] . ',測量者:' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 6:
                            $content .= $row['account_name'] . '-' . $row['record_date'] . '-晚上飯後兩小時:' . $bs_value . '測量時間 ' . $record_time_data[$record_time_index] . ',測量者:' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 7:
                            $content .= $row['account_name'] . '-' . $row['record_date'] . '-晚上睡前:' . $bs_value . '測量時間 ' . $record_time_data[$record_time_index] . ',測量者:' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                    }
                    $content .= '\n';
                    $record_id_index++;
                    $record_time_index++;
                    $bs_code_index++;
                }
            }
        }*/
        if ($blood_sugar_info_family_count == 0) {
            $content .= '查無家屬資料。\n\n';
        } else {
            $content .= '家屬紀錄:\n\n';

            foreach ($blood_sugar_info_family['row'] as $row) {
                $record_id_index = 0;
                $bs_code_index = 0;
                $record_time_index = 0;
                $record_id_data = explode(',', $row['record_id']);
                $bs_code_data = explode(',', $row['bs_code']);
                $record_time_data = explode(',', $row['record_time']);
                foreach (explode(',', $row['bs_value']) as $bs_value) {
                    switch ($bs_code_data[$bs_code_index]) {
                        case 0:
                            $content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 自訂時段\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 1:
                            $content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 早上空腹\n血糖值: ' . $bs_value  . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 8:
                            $content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] .  '\n測量時段: 早上飯後一小時\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 2:
                            $content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 早上飯後兩小時\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 3:
                            $content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 中午飯前\n血糖值: ' . $bs_value  . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 9:
                            $content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 中午飯後一小時\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 4:
                            $content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 中午飯後兩小時\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 5:
                            $content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 晚上飯前\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 10:
                            $content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 晚上飯後一小時\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 6:
                            $content .= $row['account_name'] . '\n' . $row['record_date'] .  '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 晚上飯後兩小時\n血糖值: ' . $bs_value  . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 7:
                            $content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 晚上睡前\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                    }
                    $content .= '\n';
                    $record_id_index++;
                    $record_time_index++;
                    $bs_code_index++;
                }
            }
        }
        return $content;
    }

    public function record_member($account)
    {
        return $this->getRow('select name from account where id=\'' . $account . '\'')['name'];
    }

    //Line查本週血壓
    public function bloodPressureList($sid)
    {
        //$sql = "select * from account where concat(line_sid) like '%\"sid\":\"" . $sid . "\"%'";
        //$sql = "select * from account where line_sid = '" . $sid . "'";
        $sql = "select * from account where id in (select account_id from line_fcm_friend where line_token = '" . $sid . "' and invite = '1')";
        //$sql2 = "select friend_id from line_fcm_friend where account_id = (select id from account where line_sid = '" . $sid . "') and invite=1";
        //$personal_num = 0;
        //$personal_count = 0; //個人資料總筆數
        $family_num = 0;
        $family_count = 0; //家屬資料總筆數
        //$personal = '';
        $family = '';
        $id = 0;
        $content = '';
        //file_put_contents('start.txt',$sql);
        //$self_data = $this->getQuery($sql)['row'];
        $family_data = $this->getQuery($sql)['row'];
        //$account_data = $this->getQuery($sql);
        /*foreach ($self_data as $row) {
            $personal_count++;
        }*/

        foreach ($family_data as $row) {
            $family_count++;
        }

        /*foreach ($account_data['row'] as $row) {
               //file_put_contents('data' . $id . '.txt', 'ddd');
               foreach (json_decode(urldecode($row['line_sid']))->data as $data) {
                   if ($data->category == 'personal' && $sid = $data->sid) {
                       $personal_num++;
                       if ($personal_num == $personal_count)
                           $personal .= $row['id'];
                       else
                           $personal = $row['id'] . ',';
                   } else  if ($data->category == 'family' && $sid = $data->sid) {
                       $family_num++;
                       if ($family_num == $family_count)
                           $family .= $row['id'];
                       else
                           $family = $row['id'] . ',';
                   }
               }
           }*/

        //$blood_perssure_info_personal = $this->getQuery('select bloodpressure.*,account.name as account_name from bloodpressure,account where bloodpressure.account_id=account.id and account_id in (select id from account where line_sid = \'' . $sid . '\') and date_sub(curdate(),interval 7 day) <= date');
        //$blood_perssure_info_personal_count = $this->getRows('select bloodpressure.*,account.name as account_name from bloodpressure,account where bloodpressure.account_id=account.id and account_id in (select id from account where line_sid = \'' . $sid . '\') and date_sub(curdate(),interval 7 day) <= date');
        $blood_perssure_info_family = $this->getQuery('select bloodpressure.*,account.name as account_name from bloodpressure,account where bloodpressure.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 7 day) <= date');
        $blood_perssure_info_family_count = $this->getRows('select bloodpressure.*,account.name as account_name from bloodpressure,account where bloodpressure.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 7 day) <= date');
        /*if ($blood_perssure_info_personal_count == 0) {
            $content .= '查無個人資料。\n\n';
        } else {
            $content .= '個人紀錄:\n\n';
            foreach ($blood_perssure_info_personal['row'] as $row) {
                $dia_index = 0;
                $hand_parts_index = 0;
                $time_index = 0;
                $heartrate_index = 0;
                $heartrate_data = explode(',', $row['heartrate']);
                $dia_data = explode(',', $row['dia']);
                $hand_parts_data = explode(',', $row['hand_parts']);
                $time_data = explode(',', $row['time']);
                foreach (explode(',', $row['sys']) as $sys) {
                    switch ($hand_parts_data[$hand_parts_index]) {
                        case 0:
                            $content .= $row['account_name'] . '-' . $row['date'] . '-左手收縮壓:' . $sys . ',左手舒張壓:' . $dia_data[$dia_index] . '左手心率:' . $heartrate_data[$heartrate_index] . ',測量時間 ' . $time_data[$time_index] . '。\n';
                            break;
                        case 1:
                            $content .= $row['account_name'] . '-' . $row['date'] . '-右手收縮壓:' . $sys . ',右手舒張壓:' . $dia_data[$dia_index] . '右手心率:' . $heartrate_data[$heartrate_index] . ',測量時間 ' . $time_data[$time_index] . '。\n';
                            break;
                    }
                    $content .= '\n';
                    $heartrate_index++;
                    $dia_index++;
                    $hand_parts_index++;
                    $time_index++;
                }
            }
        }*/
        if ($blood_perssure_info_family_count == 0) {
            $content .= '查無家屬資料。\n\n';
        } else {
            $content .= '家屬紀錄:\n\n';

            foreach ($blood_perssure_info_family['row'] as $row) {
                $dia_index = 0;
                $hand_parts_index = 0;
                $time_index = 0;
                $heartrate_index = 0;
                $heartrate_data = explode(',', $row['heartrate']);
                $dia_data = explode(',', $row['dia']);
                $hand_parts_data = explode(',', $row['hand_parts']);
                $time_data = explode(',', $row['time']);
                foreach (explode(',', $row['sys']) as $sys) {
                    switch ($hand_parts_data[$hand_parts_index]) {
                        case 0:
                            $content .= $row['account_name'] . '-' . $row['date'] . '\n測量時間: ' . $time_data[$time_index] . '\n收縮壓: ' . $sys . ',舒張壓:' . $dia_data[$dia_index] . '。\n';
                            break;
                        case 1:
                            $content .= $row['account_name'] . '-' . $row['date'] . '\n測量時間: ' . $time_data[$time_index] . '\n收縮壓:' . $sys . ',舒張壓:' . $dia_data[$dia_index] . '。\n';
                            break;
                    }
                    $content .= '\n';
                    $heartrate_index++;
                    $dia_index++;
                    $hand_parts_index++;
                    $time_index++;
                }
            }
        }

        return $content;
    }

    //血壓api
    public function return_family_bloodpresure_record(&$output, $sid)
    {
        $sql = "select * from account where id in (select account_id from line_fcm_friend where line_token = '" . $sid . "' and invite = '1')";
        $family_num = 0;
        $family_count = 0; //家屬資料總筆數
        $family = '';
        $id = 0;
        $content = '';
        $family_data = $this->getQuery($sql)['row'];
        $data = array();

        foreach ($family_data as $row) {
            $family_count++;
        }

        $blood_perssure_info_family = $this->getQuery('select bloodpressure.*,account.name as account_name from bloodpressure,account where bloodpressure.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 7 day) <= date');
        $blood_perssure_info_family_count = $this->getRows('select bloodpressure.*,account.name as account_name from bloodpressure,account where bloodpressure.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 7 day) <= date');
        if ($blood_perssure_info_family_count == 0) {
            $output['success'] = 0;
            $output['message']['ch'] = urlencode('查無血壓資料');
            $output['message']['en'] = 'Can`t found blood presure data.';
            //$content .= '查無家屬資料。\n\n';
        } else {
            $content .= '家屬紀錄:\n\n';

            foreach ($blood_perssure_info_family['row'] as $row) {
                $dia_index = 0;
                $hand_parts_index = 0;
                $time_index = 0;
                $heartrate_index = 0;
                $heartrate_data = explode(',', $row['heartrate']);
                $dia_data = explode(',', $row['dia']);
                $hand_parts_data = explode(',', $row['hand_parts']);
                $time_data = explode(',', $row['time']);
                foreach (explode(',', $row['sys']) as $sys) {
                    switch ($hand_parts_data[$hand_parts_index]) {
                        case 0:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['date'], 'time' => $time_data[$time_index], 'sys' => $sys, 'dia' => $dia_data[$dia_index]]);
                            //$content .= $row['account_name'] . '-' . $row['date'] . '\n測量時間: ' . $time_data[$time_index] . '\n收縮壓: ' . $sys . ',舒張壓:' . $dia_data[$dia_index] . '。\n';
                            break;
                        case 1:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['date'], 'time' => $time_data[$time_index], 'sys' => $sys, 'dia' => $dia_data[$dia_index]]);
                            //$content .= $row['account_name'] . '-' . $row['date'] . '\n測量時間: ' . $time_data[$time_index] . '\n收縮壓:' . $sys . ',舒張壓:' . $dia_data[$dia_index] . '。\n';
                            break;
                    }
                    $content .= '\n';
                    $heartrate_index++;
                    $dia_index++;
                    $hand_parts_index++;
                    $time_index++;
                }
            }
            $output['success'] = 1;
            $output['blood_presure'] = $data;
        }
        $output['success'] == 0 ? http_response_code('400') : http_response_code('200');

        echo urldecode(json_encode($output));
        //return true;
    }

    //血糖api
    public function return_family_bloodsuger_record(&$output, $sid)
    {
        $sql = "select * from account where id in (select account_id from line_fcm_friend where line_token = '" . $sid . "' and invite = '1')";
        $family_num = 0;
        $family_count = 0; //家屬資料總筆數
        $family = '';
        $id = 0;
        $content = '';
        $family_data = $this->getQuery($sql)['row'];
        $data = array();
        foreach ($family_data as $row) {
            $family_count++;
        }

        $blood_sugar_info_family = $this->getQuery('select blood_sugar.*,account.name as account_name from blood_sugar,account where blood_sugar.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 7 day) <= record_date');
        $blood_sugar_info_family_count = $this->getRows('select blood_sugar.*,account.name as account_name from blood_sugar,account where blood_sugar.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 7 day) <= record_date');
        if ($blood_sugar_info_family_count == 0) {
            $output['success'] = 0;
            $output['message']['ch'] = urlencode('查無血糖資料');
            $output['message']['en'] = 'Can`t found blood suger data.';
            //$content .= '查無家屬資料。\n\n';
        } else {
            $content .= '家屬紀錄:\n\n';

            foreach ($blood_sugar_info_family['row'] as $row) {
                $record_id_index = 0;
                $bs_code_index = 0;
                $record_time_index = 0;
                $record_id_data = explode(',', $row['record_id']);
                $bs_code_data = explode(',', $row['bs_code']);
                $record_time_data = explode(',', $row['record_time']);

                foreach (explode(',', $row['bs_value']) as $bs_value) {
                    switch ($bs_code_data[$bs_code_index]) {
                        case 0:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['record_date'], 'time' => '自訂時段: ' . $record_time_data[$record_time_index], 'vlaue' => $bs_value]);
                            //$content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 自訂時段\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 1:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['record_date'], 'time' => '早上空腹', 'vlaue' => $bs_value]);
                            //$content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 早上空腹\n血糖值: ' . $bs_value  . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 8:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['record_date'], 'time' => '早上飯後一小時', 'vlaue' => $bs_value]);
                            //$content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] .  '\n測量時段: 早上飯後一小時\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 2:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['record_date'], 'time' => '早上飯後兩小時', 'vlaue' => $bs_value]);
                            //$content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 早上飯後兩小時\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 3:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['record_date'], 'time' => '中午飯前', 'vlaue' => $bs_value]);
                            //$content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 中午飯前\n血糖值: ' . $bs_value  . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 9:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['record_date'], 'time' => '中午飯後一小時', 'vlaue' => $bs_value]);
                            ///$content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 中午飯後一小時\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 4:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['record_date'], 'time' => '中午飯後一小時', 'vlaue' => $bs_value]);
                            //$content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 中午飯後兩小時\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 5:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['record_date'], 'time' => '晚上飯前', 'vlaue' => $bs_value]);
                            //$content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 晚上飯前\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 10:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['record_date'], 'time' => '晚上飯後一小時', 'vlaue' => $bs_value]);
                            //$content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 晚上飯後一小時\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 6:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['record_date'], 'time' => '晚上飯後兩小時', 'vlaue' => $bs_value]);
                            //$content .= $row['account_name'] . '\n' . $row['record_date'] .  '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 晚上飯後兩小時\n血糖值: ' . $bs_value  . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                        case 7:
                            array_push($data, ['name' => $row['account_name'], 'date' => $row['record_date'], 'time' => '晚上睡前', 'vlaue' => $bs_value]);
                            //$content .= $row['account_name'] . '\n' . $row['record_date'] . '\n測量時間: ' . $record_time_data[$record_time_index] . '\n測量時段: 晚上睡前\n血糖值: ' . $bs_value . '\n測量者: ' . $this->record_member($record_id_data[$record_id_index]) . '。\n';
                            break;
                    }
                    $content .= '\n';
                    $record_id_index++;
                    $record_time_index++;
                    $bs_code_index++;
                }
            }
            $output['success'] = 1;
            $output['blood_suger'] = $data;
        }
        $output['success'] == 0 ? http_response_code('400') : http_response_code('200');
        echo urldecode(json_encode($output));
        //return true;
    }

    //異常警訊api
    public function return_family_alert_record(&$output, $sid)
    {
        $sql = "select * from account where id in (select account_id from line_fcm_friend where line_token = '" . $sid . "' and invite = '1')";
        $family_num = 0;
        $family_count = 0; //家屬資料總筆數
        $family = '';
        $id = 0;
        $content = '';
        $family_data = $this->getQuery($sql)['row'];
        $data = array();

        foreach ($family_data as $row) {
            $family_count++;
        }
        //家屬紀錄===開始
        $physiological_signal_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'高|低|指數\' order by establish desc');
        $physiological_signal_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'高|低|指數\' order by establish desc');
        $btag_sql_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'緊急\' order by establish desc');
        $btag_sql_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'緊急\' order by establish desc');
        $no_measurement_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'未量測\' order by establish desc');
        $no_measurement_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'未量測\' order by establish desc');
        $getstation_sql_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'糖患者\' order by establish desc');
        $getstation_sql_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'糖患者\' order by establish desc');
        $unused_medicine_family = $this->getQuery('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'未用藥\' order by establish desc');
        $unused_medicine_family_count = $this->getRows('select alarm_mobile.*,account.name as account_name from alarm_mobile,account where alarm_mobile.account_id=account.id and account_id in (select account_id from line_fcm_friend where line_token = \'' . $sid . '\' and invite = \'1\') and date_sub(curdate(),interval 1 day) <= date and message regexp \'未用藥\' order by establish desc');
        //家屬紀錄===結束
        $alert_info_family_count = ($physiological_signal_family_count + $btag_sql_family_count + $no_measurement_family_count + $getstation_sql_family_count + $unused_medicine_family_count/*+$afib_sql_family_count+$temparater_sql_family_count*/);
        if ($alert_info_family_count == 0) {
            $output['success'] = 0;
            $output['message']['ch'] = urlencode('查無異常紀錄');
            $output['message']['en'] = 'Can`t found alert data.';
            //$content .= '查無家屬資料。\n\n';
        } else {
            $content .= '家屬紀錄:\n\n';

            foreach ($physiological_signal_family['row'] as $row) {
                array_push($data, ['name' => $row['account_name'], 'datetime' => $row['establish'], 'message' => explode('；', $row['message'])]);
                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                //$content .= $row['account_name'] . '\n' . $row['date'] . '\n' . $row['message'] . '\n\n';
            }
            foreach ($btag_sql_family['row'] as $row) {
                array_push($data, ['name' => $row['account_name'], 'datetime' => $row['establish'], 'message' => explode('；', $row['message'])]);
                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                //$content .= $row['account_name'] . '\n' . $row['date'] . '\n' . $row['message'] . '\n\n';
            }
            foreach ($no_measurement_family['row'] as $row) {
                array_push($data, ['name' => $row['account_name'], 'datetime' => $row['establish'], 'message' => explode('；', $row['message'])]);
                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                //$content .= $row['account_name'] . '\n' . $row['date'] . '\n' . $row['message'] . '\n\n';
            }
            foreach ($getstation_sql_family['row'] as $row) {
                array_push($data, ['name' => $row['account_name'], 'datetime' => $row['establish'], 'message' => explode('；', $row['message'])]);
                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                //$content .= $row['account_name'] . '\n' . $row['date'] . '\n' . $row['message'] . '\n\n';
            }
            foreach ($unused_medicine_family['row'] as $row) {
                array_push($data, ['name' => $row['account_name'], 'datetime' => $row['establish'], 'message' => explode('；', $row['message'])]);
                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                //$content .= $row['account_name'] . '\n' . $row['date'] . '\n' . $row['message'] . '\n\n';
            }
            /*foreach ($afib_sql_family['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '-' . $row['date'] . '-' . $row['message'] . '\n\n';
            }
            foreach ($temparater_sql_family['row'] as $row) {

                //$content .= $row['account_name'] . '-' . $row['record_day'] . '-' . $row['message'] . '\n\n';
                $content .= $row['account_name'] . '-' . $row['date'] . '-' . $row['message'] . '\n\n';
            }*/

            $output['success'] = 1;
            $output['alert'] = $data;
        }
        $output['success'] == 0 ? http_response_code('400') : http_response_code('200');
        echo urldecode(json_encode($output));

        //return $content;
    }
    //視訊門診掛號紀錄api
    public function return_personal_viewshistory_record(&$output, $sid)
    {
        $history_data = array();
        $content = '';
        $view_data = array();
        $sql = 'select account.name as member_name,outpatient.outpatient_name,doctor.name as doctor_name,main.number,main.visit_date,main.time_period,main.outpatient_id,main.doctor_id,main.time_start,main.time_end from (select * from account where identity = \'院所醫生\') doctor,outpatient_member_list main,account,outpatient where main.member_id=account.id and main.doctor_id=doctor.id and main.outpatient_id=outpatient.id and main.member_id=account.id and main.member_id = (select id from account where line_sid= \'' . $sid . '\') and (main.visit_date between \'' . date('Y-m-d', time()) . '\' and date_sub(curdate(),interval -30 day)) and main.deleted_at is null';
        if ($this->getRows($sql) == 0) {
            $output['success'] = 0;
            $output['message']['ch'] = urlencode('查無視訊門診預約紀錄');
            $output['message']['en'] = 'Can`t found views history data.';
            //$content = '查不到與會員有關的預約紀錄。';
        } else {
            $data = $this->getQuery($sql)['row'];
            foreach ($data as $row_data) {
                $sub_data = $this->getRow('select outpatient.outpatient_name ,organization.org_name from views_schedule main,outpatient,organization where main.outpatient = outpatient.id and main.org_id = organization.id and main.view_date=\'' . $row_data['visit_date'] . '\' and main.doctor = \'' . $row_data['doctor_id'] . '\' and main.outpatient = \'' . $row_data['outpatient_id'] . '\' and main.time_period = \'' . $row_data['time_period'] . '\' and deleted_at is null');
                switch ($row_data['time_period']) {
                    case '早診':
                        $row_data['time_start'] = ($row_data['time_start'] == null ? '07:30:00' : $row_data['time_start']);
                        $row_data['time_end'] = ($row_data['time_end'] == null ? '12:30:00' : $row_data['time_start']);
                        break;
                    case '午診':
                        $row_data['time_start'] = ($row_data['time_start'] == null ? '13:00:00' : $row_data['time_start']);
                        $row_data['time_end'] = ($row_data['time_start'] == null ? '17:30:00' : $row_data['time_start']);
                        break;
                    case '晚診':
                        $row_data['time_start'] = ($row_data['time_start'] == null ? '18:00:00' : $row_data['time_start']);
                        $row_data['time_end'] = ($row_data['time_start'] == null ? '21:30:00' : $row_data['time_start']);
                        break;
                }
                array_push($view_data, ['org' => $sub_data['org_name'], 'outpatient' => $sub_data['outpatient_name'], 'doctor' => $row_data['doctor_name'], 'number' => $row_data['number'], 'visit_date' => date('Y/m/d', strtotime($row_data['visit_date'])), 'time_period' => $row_data['time_period'], 'time_start' => $row_data['time_start'], 'time_end' => $row_data['time_end']]);
                /*$content .= '醫療院所:' . $sub_data['org_name'];
                $content .= '\n門診科別:' . $sub_data['outpatient_name'];
                $content .= $row_data['doctor_name'] . ' 醫師';
                $content .= '\n號次 ' . $row_data['number'] . '號';
                $content .= '\n看診時間:' . date('Y-m-d', strtotime($row_data['visit_date']));
                $content .= '\n' . $row_data['time_period'] . ' ' . $row_data['time_start'] . '~' . $row_data['time_end'];
                $content .= '\n';*/
            }
            $output['success'] = 1;
            $output['views_history'] = $view_data;
        }
        $output['success'] == 0 ? http_response_code('400') : http_response_code('200');

        echo urldecode(json_encode($output));
        //return true;
    }


    //line fcm message
    public function line_fcm_message(&$output, $row, $message, $self)
    {
        $self_data = $this->getRow('select name from account where id = ' . $self);
        $friend_data = $this->getQuery('select line_token,friend_id from line_fcm_friend where account_id=\'' . $self . '\' and invite = 1')['row'];

        foreach ($friend_data as $friend) {
            if ($friend['line_token'] != '') {

                $this->pushMessageLocal($friend['line_token'], 'text', $self_data['name'] . ' 的家屬您好:\n' . $message);
            } else {
                $email = $this->getRow('select email,name from account where id=' . $friend['friend_id']);
                set_time_limit(180); //限180秒
                require_once('mod/PHPMailer-5.2.9/PHPMailerAutoload.php'); //phpmailer class的位置
                $mail = new PHPMailer(); //建立新物件
                $mail->IsSMTP(); //設定使用SMTP方式寄信
                $mail->SMTPAuth = true; //設定SMTP需要驗證
                $mail->SMTPSecure = 'ssl'; // Gmail的SMTP主機需要使用SSL連線
                //$mail->Host = 'smtp-mail.outlook.com';
                //$mail->Port = '587';
                $mail->Host = "smtp.gmail.com"; //Gamil的SMTP主機
                $mail->Port = 465; //Gamil的SMTP主機的埠號(Gmail為465)。
                $mail->CharSet = "utf-8"; //郵件編碼
                //$mail->Username = 'horncare.longcare@outlook.com';
                //$mail->Password = 'qw5978978';
                $mail->Username = "horncare.cloud@gmail.com"; //Gamil帳號
                $mail->Password = "Horn521446"; //Gmail密碼
                $mail->From = "horncare.cloud@gmail.com"; //寄件者信箱
                $mail->FromName = "霍恩關懷雲"; //寄件者姓名
                $mail->Subject = 'Docter 霍恩關懷雲 異常警訊通知';     //郵件標題
                $mail->Body = $self_data['name'] . '的守護站發出異常通知，點選連結加入<a href="https://line.me/ti/p/@295quxfl">霍恩關懷雲LINE官方帳號</a>並開啟個人化服務瞭解。';        //郵件內容
                $mail->IsHTML(true); //郵件內容為html
                $mail->AddAddress($email['email'], $email['name']); //收件者郵件及名稱
                $mail->AddBCC(" "); //設定 密件副本收件者
                $mail->send();
            }
        }
    }

    //LINE binding family feedback line ver
    public function binding_family_feedback($family, $self, $feedback)
    {
        $account_data = $this->getRow('select * from account where id = \'' . $self . '\'');
        //file_put_contents('jsonlog.txt', $this->getRow('select * from account where id = (select id from account where line_sid=\'' . $family . '\')')['name'] . '已經同意您的守護站好友邀請。');
        try {
            if ($feedback == 1) {
                $this->sqlQuery('update line_fcm_friend set invite=\'' . $feedback . '\',updated_at=\'' . date('Y-m-d h:i:s', time()) . '\' where line_token = \'' . $self . '\' and account_id = (select id from account where line_sid=\'' . $family . '\')');
                $this->pushMessageLocal($self, 'text', $this->getRow('select * from account where id = (select id from account where line_sid=\'' . $family . '\')')['name'] . '已經同意您的守護站好友邀請。');
            } else if ($feedback == 0) {
                $this->sqlQuery('update line_fcm_friend set invite=\'' . $feedback . '\',updated_at=\'' . date('Y-m-d h:i:s', time()) . '\' where line_token = \'' . $self . '\' and account_id = (select id from account where line_sid=\'' . $family . '\')');
                $this->pushMessageLocal($self, 'text', $this->getRow('select * from account where id = (select id from account where line_sid=\'' . $family . '\')')['name'] . '拒絕了您的守護站好友邀請。');
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    //search person`s Line family list
    public function search_personal_family(&$output)
    {
        $account = isset($_POST['account']) ? $_POST['account'] : 0;

        if ($output['message']['ch'] == urlencode('安全碼正確')) {
            $output['message']['ch'] = '';
            $output['message']['en'] = '';
        }

        if ($account == 0) {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入帳號編號') : urlencode(', 請輸入帳號編號');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in account data number' : ', Please fill in account data number.';
            $output['success'] = 0;
        }

        if ((!isset($_POST['account']) && !$_POST['account'])) {
            $output['message']['ch'] = urlencode('未收到資料');
            $output['message']['en'] = 'Data not received.';
            $output['success'] = 0;
        }

        if ($output['success']) return $this->return_family_list($output, $account);
    }

    public function return_family_list(&$output, $account = '')
    {
        try {
            if ($output['message']['ch'] == urlencode('安全碼正確')) {
                $output['message']['ch'] = '';
                $output['message']['en'] = '';
            }

            $friend_data_sql = 'select account_id,friend_id,line_fcm_friend.id as data_id,account.name as friend_name,account.account as friend_account from line_fcm_friend,account where account.id=line_fcm_friend.friend_id and line_fcm_friend.account_id=(select id from account where sid=\'' . $account . '\') and line_fcm_friend.invite = 1';
            $friend_data = $this->getQuery($friend_data_sql)['row'];
            $friend_data_count = $this->getRows($friend_data_sql);
            $friend_data_array = array();
            foreach ($friend_data as $rows) {
                array_push($friend_data_array, [
                    'friend_name' => urlencode($rows['friend_name']),
                    'friend_account' => $rows['friend_account'],
                    'friend_id' => $rows['friend_id'],
                    'account_id' => $rows['account_id'],
                    'data_id' => $rows['data_id']
                ]);
            }
            $output['success'] = 1;
            $output['data'] = $friend_data_array;
            $output['data_count'] = $friend_data_count;
        } catch (Exception $e) {
            $output['success'] = 0;
            $output['message']['ch'] = urlencode('程式執行錯誤。');
            $output['message']['en'] = 'function error.';
        }
    }

    //Line unlock family binding
    public function family_binding_unlock(&$output)
    {
        $data_id = isset($_POST['data_id']) ? $_POST['data_id'] : 0;

        if ($output['message']['ch'] == urlencode('安全碼正確')) {
            $output['message']['ch'] = '';
            $output['message']['en'] = '';
        }

        if ($data_id == 0) {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入資料編號') : urlencode(', 請輸入資料編號');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in data number' : ', Please fill in data number.';
            $output['success'] = 0;
        }

        if ((!isset($_POST['data_id']) && !$_POST['data_id'])) {
            $output['message']['ch'] = urlencode('未收到資料');
            $output['message']['en'] = 'Data not received.';
            $output['success'] = 0;
        }

        if ($output['success'] == 1) return $this->return_family_unlock($output, $data_id);
    }

    public function return_family_unlock(&$output, $data_id)
    {
        $friend_data = $this->getRow('select account_id,friend_id from line_fcm_friend where id = \'' . $data_id . '\'');

        try {
            $this->sqlQuery('update line_fcm_friend set invite=\'0\',updated_at=\'' . date('Y-m-d h:i:s', time()) . '\' where id = \'' . $data_id . '\'');
            $output['success'] = 1;
            $output['message']['ch'] = urlencode('好友狀態更新成功。');
            $output['message']['en'] = 'Success.';
        } catch (Exception $e) {
            $output['success'] = 0;
            $output['message']['ch'] = urlencode('程式執行錯誤。');
            $output['message']['en'] = 'function error.';
        }
    }

    //line 功能-透過sid查詢霍恩關懷雲會員Sid
    public function get_sid_with_line(&$output)
    {
        $sid = isset($_POST['sid']) ? $_POST['sid'] : '';
        if ($sid == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入LINE sid') : urlencode(', 請輸入LINE sid');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in LINE sid' : ', Please fill in LINE sid.';
            $output['success'] = 0;
        }

        if ((!isset($_POST['sid']) && !$_POST['sid'])) {
            $output['message']['ch'] = urlencode('未收到資料');
            $output['message']['en'] = 'Data not received.';
            $output['success'] = 0;
        }

        if ($output['success'] == 1) {
            $this->return_get_sid_with_line($output, $sid);
            echo urldecode(json_encode($output));
        }
    }

    public function return_get_sid_with_line(&$output, $sid)
    {
        try {
            $data = $this->getQuery('select sid from account where line_sid = \'' . $sid . '\' and enable = 1');
            if (intval($data['num']) >= 1) {
                $output['success'] = 1;
                $output['user_sid'] = $data['row'][0]['sid'];
                $output['find_status'] = 1;
            } else {
                $output['success'] = 0;
                $output['find_status'] = 0;
                $output['message']['ch'] = urlencode('找不到資料');
                $output['message']['en'] = 'Can`t found data.';
            }
        } catch (Exception $e) {
            $output['success'] = 0;
            $output['message']['ch'] = urlencode('程式執行錯誤。');
            $output['message']['en'] = 'function error.';
        }
    }

    public function lineNotifyMessageePush(&$output)
    {
        $fields = null;
        $user = isset($_POST['user']) ? $_POST['user'] : '';
        $type = isset($_POST['type']) ? $_POST['type'] : '';

        if ($type == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入推播類型。') : urlencode(', 請輸入推播類型。');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in broadcast category.' : ', Please fill in broadcast category.';
            $output['success'] = 0;
        }

        if ($user == '') {
            $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入推播目標。') : urlencode(', 請輸入推播目標。');
            $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in broadcast target.' : ', Please fill in broadcast target.';
            $output['success'] = 0;
        } else {
            $access_token = $this->getRow('select notify_sid from member where account = \'' . $user . '\'')['notify_sid'];
            if ($access_token == '') {
                $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('找不到access token。') : urlencode(', 找不到 access token。');
                $output['message']['en'] .= ($output['message']['en'] == '') ? 'Can`t found access token.' : ', Can`t found access token.';
                $output['success'] = 0;
            }
        }

        if ($output['success']) {
            if ($type == 'text') {
                $message = isset($_POST['message']) ? $_POST['message'] : '';
                if ($message == '') {
                    $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入推播訊息內容。') : urlencode(', 請輸入推播訊息內容。');
                    $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in broadcast message content.' : ', Please fill in broadcast message content.';
                    $output['success'] = 0;
                }
                $fields = array(
                    'access_token'=>$access_token,
                    'type' => $type,
                    'message' => $message
                );
            }
            if ($type == 'image') {
                $imageSmail = isset($_POST['imageSmail']) ? $_POST['imageSmail'] : '';
                $imageFull = isset($_POST['imageFull']) ? $_POST['imageFull'] : '';
                $imageFile = isset($_POST['imageFile']) ? $_POST['imageFile'] : null;
                if ($imageFile == null) {
                    if ($imageSmail == '') {
                        $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入圖片縮圖Url。') : urlencode(', 請輸入圖片縮圖Url。');
                        $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in picture small size` url.' : ', Please fill in picture small size`s url.';
                        $output['success'] = 0;
                    }
                    if ($imageFull == '') {
                        $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入圖片Url。') : urlencode(', 請輸入圖片Url。');
                        $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in picture url.' : ', Please fill in picture url.';
                        $output['success'] = 0;
                    }
                }

                if (($imageSmail == '' || $imageFull == '') && $imageFile == null) {
                    $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入圖片檔案。') : urlencode(', 請輸入圖片檔案。');
                    $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in picture file.' : ', Please fill in picture file.';
                    $output['success'] = 0;
                }

                $fields = array(
                    'access_token'=>$access_token,
                    'type' => $type,
                    'imageSmail' => $imageSmail,
                    'imageFull' => $imageFull,
                    'imageFile' => $imageFile
                );
            }

            if ($type == 'sticker') {
                $stickerPacID = isset($_POST['stickerPacID']) ? $_POST['stickerPacID'] : 0;
                $stickerID = isset($_POST['stickerID']) ? $_POST['stickerPacID'] : 0;
                if ($stickerPacID == '') {
                    $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入表情包ID。') : urlencode(', 請輸入表情包ID。');
                    $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in sticker Package ID.' : ', Please fill in sticker Package ID.';
                    $output['success'] = 0;
                }
                if ($stickerID == '') {
                    $output['message']['ch'] .= ($output['message']['ch'] == '') ? urlencode('請輸入表情符號ID。') : urlencode(', 請輸入表情符號ID。');
                    $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in sticker ID.' : ', Please fill in sticker ID.';
                    $output['success'] = 0;
                }

                $fields = array(
                    'access_token'=>$access_token,
                    'type' => $type,
                    'stickerPacID' => $stickerPacID,
                    'stickerID' => $stickerID,
                );
            }
        }
        if ($output['success']) {
            $url = (!isset($_SERVER['HTTPS']) ? 'http' : 'https') . '://' . $_SERVER['HTTP_HOST'] . '/horn/lineservice/new_api/line_notify_push_message.php';
            $headers = array('Content-Type:  application/x-www-form-urlencoded');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
            $result = curl_exec($ch);
            var_dump($result);
            exit;
            curl_close($ch);
        }
    }

    //LINE Notify Kernel
    public function lineNotifyKernel($access_token, $type, $imageSmail = '', $imageFull = '', $image = null, $message = '', $stickerPacID = 0, $stickerID = 0)
    {
        $_header = array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Bearer ' . $access_token
        );
        switch ($type) {
            case 'text':
                $content = array(
                    'message' => $message
                );
                break;
            case 'image':
                if ($image == null) {
                    $content = array(
                        'imageThumbnail' => $imageSmail,
                        'imageFullsize' => $imageFull
                    );
                } else {
                    $content = array(
                        'imageFile' => $image
                    );
                }
                break;
            case 'sticker':
                $content = array(
                    'stickerPackageId' => $stickerPacID,
                    'stickerId' => $stickerID
                );
                break;
        }

        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_POST => true
        );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $_header);
        curl_setopt_array($curl, $options);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($form_data));
        curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($content));
        $result = curl_exec($curl);
        curl_close($curl);
        if ($curl) {
            $output['success'] = 1;
            return true;
        } else {
            $output['success'] = 0;
            $output['message']['ch'] = 'Line Notify 訊息傳送失敗。';
            $output['message']['en'] = 'Line Notify message push failed.';
            return false;
        }
    }
}
