<?php

class account extends db
{

	// 新增帳號
	public function Insert_Account(&$output)
	{

		$today = date('Y-m-d');
		$sql = "select * from account where account = '" . $account . "'";

		$sql = "insert into account (account, password, identity, email, sid, starting_date, end_date, extension, clause, consent) values ('" . $this->escape($output['account']) . "', '" . md5($output['password']) . "', '一般會員', '" . $this->escape($output['email']) . "', '" . md5($output['account'] . $output['password']) . "', '" . $today . "', '" . $today . "', '" . $this->Get_Extension($today, '天數加2個月') . "', '" . $output['clause'] . "', '" . $output['consent'] . "')";

		if ($this->sqlQuery($sql)) {

			$output['message']['ch'] = '註冊成功';
			$output['sid'] = md5($output['account'] . $output['password']);
			$id = $this->getLastId();
			$this->Add_history($id, $id, '手機註冊');

			if (strpos($output['account'], 'KMUHC') !== false) {
				$sql2 = "update account set identity = '付費會員', org_id = 2, extension = '0000-00-00' where account = '" . $this->escape($output['account']) . "'";
				$this->sqlQuery($sql2);
			} else if (strpos($output['account'], 'KMUH') !== false) {
				$sql2 = "update account set identity = '關懷員', org_id = 2 where account = '" . $this->escape($output['account']) . "'";
				$this->sqlQuery($sql2);
			} else if (strpos($output['account'], 'SGLFW') !== false) {
				$sql2 = "update account set identity = '付費會員', org_id = 17, group_id = 10, extension = '0000-00-00' where account = '" . $this->escape($output['account']) . "'";
				$this->sqlQuery($sql2);
			} else if (strpos($output['account'], 'SGLFL') !== false) {
				$sql2 = "update account set identity = '付費會員', org_id = 17, group_id = 11, extension = '0000-00-00' where account = '" . $this->escape($output['account']) . "'";
				$this->sqlQuery($sql2);
			} else if (strpos($output['account'], 'SGLFK') !== false) {
				$sql2 = "update account set identity = '付費會員', org_id = 17, group_id = 9, extension = '0000-00-00' where account = '" . $this->escape($output['account']) . "'";
				$this->sqlQuery($sql2);
			} else if (strpos($output['account'], 'SGLF') !== false) {
				$sql2 = "update account set identity = '關懷員', org_id = 17 where account = '" . $this->escape($output['account']) . "'";
				$this->sqlQuery($sql2);
			}
		} else {

			$output['success'] = 0;
			$output['message']['ch'] = '註冊失敗';
			$output['message']['en'] = 'Registration failed.';
		}
	}

	// 取得會員帳號
	public function Get_Account_By_ID($account_id)
	{

		$row = '無帳號';
		$sql = "select * from account where id = '" . $account_id . "'";

		if ($this->getRows($sql)) {

			$account = $this->getRow($sql);
			$row = $account['account'];
		}

		return $row;
	}

	// 取得會員資料
	public function Get_Account_By_ID_2($account_id)
	{

		$sql = "select * from account where id = '" . $account_id . "'";

		if ($this->getRows($sql)) {

			$account = $this->getRow($sql);
		}

		return $account;
	}

	// 取得會員ID
	public function Get_ID_By_Account($account)
	{

		$sql = "select * from account where account = '" . $account . "'";

		if ($this->getQuery($sql)['num']) {
			$row = $this->getRow($sql);
		}

		return $row['id'];
	}

	// 取得會員安全碼
	public function Get_SID_By_Account($account)
	{

		$sql = "select * from account where account = '" . $account . "'";

		if ($this->getRows($sql)) {

			$row = $this->getRow($sql);
		}

		return $row['sid'];
	}

	// 驗證Sid是否存在 不存在傳回0
	public function Check_SID_Exist($sid)
	{

		$sql = "select * from account where sid = '" . $this->escape($sid) . "'";
		return $this->getQuery($sql)['num'];
	}

	public function Check_SID($sid, $output)
	{

		if ($sid == '') {

			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入安全碼' : ', 請輸入安全碼';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the SID' : ', Please fill in the SID';
		} else if (!$this->Check_SID_Exist($sid)) {

			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '安全碼有誤' : ', 安全碼有誤';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'SID error' : ', SID error';
		} else {

			$output['success'] = 1;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '安全碼正確' : ', 安全碼正確';
		}

		return $output;
	}

	// 使用Sid取會員資料
	public function Get_Account_By_SID($sid)
	{

		$sql = "select * from account where sid = '" . $this->escape($sid) . "'";
		return $this->getRow($sql);
	}

	public function rs($f1, $f2)
	{
		$sql = "select * from account where account = '" . $this->escape($f1) . "' and password = '" . md5($f2) . "' and enable = 1";
		return ($this->getRows($sql));
	}

	public function mbr($f1, $f2)
	{
		$sql = "select * from account where account = '" . $this->escape($f1) . "' and password = '" . md5($f2) . "' and enable = 1";
		return ($this->getRow($sql));
	}

	public function isEnableView($row)
	{
		return (strtotime($row['extension']) >= strtotime(date('Y-m-d'))) ? 1 : 0; //過期傳回0
	}

	//單次型qrcode登入驗證
	public function setQrcodeLogin(&$output)
	{
		$qrcode = md5(time());
		try {
			$sql = 'insert into qrcode_vail(token,created_at,fail_time) values(\'' . $qrcode . '\',\'' . date('Y-m-d H:i:s', time()) . '\',\'' . date('Y-m-d H:i:s', time() + 300) . '\')';
			$this->sqlQuery($sql);
			$output['success'] = 1;
			$output['token'] = $qrcode;
			$output['url'] = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/horn/lineservice/LINE/vail.php?token=' . $qrcode /*. '&userSid=' . $_GET['userSid']*/;
		} catch (Exception $e) {
			$output['success'] = 0;
			$output['message'] = $e->getMessage();
		}
	}

	//單次型qrcode登入驗證超時
	public function qrcodeTimeOut(&$output)
	{
		$token = isset($_POST['token']) ? $_POST['token'] : '';
		if ($token == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入qrcode' : ', 請輸入qrcode';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the token' : ', Please fill in the token';
		}

		if ($output['success']) {

			try {
				$sql = 'update qrcode_vail set faild = 1 where token = \'' . $token . '\'';
				$this->sqlQuery($sql);
				$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	//單次型qrcode登入驗證狀態
	public function qrcodeCheck(&$output)
	{
		$token = isset($_POST['token']) ? $_POST['token'] : '';
		if ($token == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入qrcode' : ', 請輸入qrcode';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the token' : ', Please fill in the token';
		}

		if ($output['success']) {

			try {
				$sql = 'select faild,id,use_id,is_use from qrcode_vail where fail_time > \'' . date('Y-m-d H:i:s', time()) . '\' and token = \'' . $token . '\'';
				$data = $this->getRow($sql);
				if ($data['faild'] == 1) {
					$output['success'] = -1;
				} else if ($data['use_id'] != '') {
					$member_data = $this->getRow2('select email,name,id from account where id = ' . $data['use_id']);
					$output['success'] = 1;
					$output['email'] = $member_data['email'];
					$output['name'] = $member_data['name'];
					$output['id'] = $member_data['id'];
				} else {
					$output['success'] = 0;
				}
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	//單次型qrcode登入驗證狀態
	public function qrcodeStatus(&$output)
	{
		$token = isset($_POST['token']) ? $_POST['token'] : '';
		$sid = isset($_POST['sid']) ? $_POST['sid'] : '';
		if ($token == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入qrcode' : ', 請輸入qrcode';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the token' : ', Please fill in the token';
		}
		if ($sid == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '無法登入' : ', 無法登入';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the LINE Sid' : ', Please fill in the LINE Sid';
		}

		if ($output['success']) {

			try {
				$sql = 'select * from qrcode_vail where fail_time > \'' . date('Y-m-d H:i:s', time()) . '\' and token = \'' . $token . '\' and is_use = 0';
				$data = $this->getQuery($sql);
				if ($data['num'] <= 0)
					$output['success'] = -2;
				else {
					if ($data['row'][0]['faild'] == 1) {
						$output['success'] = -2;
					} else {
						$sql2 = 'select * from member where line_sid = \'' . $sid . '\'';
						$member_data = $this->getQuery($sql2);
						if ($member_data['num'] <= 0) {
							$output['success'] = -1;
						} else {
							$sql3 = 'update qrcode_vail set use_id = \'' . $member_data['row'][0]['id'] . '\',is_use = 1 where token = \'' . $token . '\'';
							$this->sqlQuery($sql3);
							$output['success'] = 1;
						}
					}
				}
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	//line 綁定
	public function setLineBinding(&$output)
	{
		$account = isset($_POST['account']) ? $_POST['account'] : '';
		$password = isset($_POST['password']) ? $_POST['password'] : '';
		$line_sid = isset($_POST['sid']) ? $_POST['sid'] : '';
		if ($account == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入帳號' : ', 請輸入帳號';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the account' : ', Please fill in the account';
		}
		if ($password == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入密碼' : ', 請輸入密碼';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the password' : ', Please fill in the password';
		}
		if ($line_sid == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '網址無效。' : ', 網址無效';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the LINE Sid' : ', Please fill in the LINE Sid';
		}

		if ($output['success']) {

			try {
				$sql = "select * from account where account = '" . $this->escape($account) . "' and password = '" . md5($password) . "'";
				$account_horn = $this->getQuery2($sql);
				if (!$account_horn['num']) {
					$output['success'] = 0;
					$output['message']['ch'] = '帳號或密碼有誤';
					$output['message']['en'] = 'Account or password incorrect';
				} else {
					$curl = curl_init();
					curl_setopt_array($curl, array(
						CURLOPT_URL => "https://api.line.me/v2/bot/profile/" . $line_sid,
						CURLOPT_SSL_VERIFYHOST => false,
						CURLOPT_SSL_VERIFYPEER => false,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_HTTPGET => true,
						CURLOPT_HTTPHEADER => array(
							"Authorization: Bearer " . message_chanel_token
						),
					));
					$json_data = curl_exec($curl);
					$account_data = json_decode($json_data);
					if (isset($account_data->message)) {
						$output['success'] = 0;
						$output['message']['ch'] = 'LINE Sid有誤';
						$output['message']['en'] = 'LINE Sid incorrect';
					} else {
						$line_bind_status = ($this->getRows('select id from member where line_sid = \'' . $line_sid . '\'') >= 1 ? true : false);
						if ($line_bind_status) {
							$output['success'] = 0;
							$output['message']['ch'] = 'LINE Sid已經綁定過帳號。';
							$output['message']['en'] = 'LINE Sid used.';
						} else {
							$sql4 = 'insert into member(line_sid,name,account,created_at) values(\'' . $line_sid . '\',\'' . $account_data->displayName . '\',\'' . $account_horn['row'][0]['account'] . '\',\'' . date('Y-m-d H:i:s', time()) . '\')';
							$this->sqlQuery($sql4);
							$output['success'] = 1;
						}
					}
				}

				//$account_name = json_decode($json_data)->displayName;

				//$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	//line notify 綁定
	public function setLineNotifyBinding(&$output)
	{
		$line_sid = isset($_POST['Uid']) ? $_POST['Uid'] : '';
		$notify_sid = isset($_POST['token']) ? $_POST['token'] : '';

		if ($notify_sid == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入通知系統編號' : ', 請輸入通知系統編號';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the notify token.' : ', Please fill in the notify token.';
		}
		if ($line_sid == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '網址無效。' : ', 網址無效';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the LINE Sid' : ', Please fill in the LINE Sid';
		}

		if ($output['success']) {

			try {
				$sql = "select id from member where line_sid = '" . $line_sid . "'";
				$data = $this->getQuery($sql);
				if ($data['num'] <= 0) {
					$output['success'] = 0;
					$output['message']['ch'] = 'LINE Sid有誤';
					$output['message']['en'] = 'LINE Sid incorrect';
				} else {
					$notify_bind_status = ($data['row'][0]['notify_sid'] == '' ? false : true);
					if ($notify_bind_status) {
						$output['success'] = 2;
						$output['message']['ch'] = 'LINE notify 服務已經綁定過，若要更新請填寫更新表單。';
						$output['message']['en'] = 'LINE notify Sid used.';
					} else {
						$sql4 = 'update member set notify_sid=\'' . $notify_sid . '\' where id = \'' . $data['row'][0]['id'] . '\'';
						$this->sqlQuery($sql4);
						$output['success'] = 1;
					}
				}
				//$account_name = json_decode($json_data)->displayName;

				//$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	//line notify 綁定重設
	public function setLineNotifyBindingReset(&$output)
	{
		$line_sid = isset($_POST['Uid']) ? $_POST['Uid'] : '';
		$notify_sid = isset($_POST['token']) ? $_POST['token'] : '';

		if ($notify_sid == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入通知系統編號' : ', 請輸入通知系統編號';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the notify token.' : ', Please fill in the notify token.';
		}
		if ($line_sid == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '網址無效。' : ', 網址無效';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the LINE Sid' : ', Please fill in the LINE Sid';
		}

		if ($output['success']) {

			try {
				$sql = "select id from member where line_sid = '" . $line_sid . "'";
				$data = $this->getQuery($sql);
				if ($data['num'] <= 0) {
					$output['success'] = 0;
					$output['message']['ch'] = 'LINE Sid有誤';
					$output['message']['en'] = 'LINE Sid incorrect';
				} else {

					$sql4 = 'update member set notify_sid=\'' . $notify_sid . '\' where id = \'' . $data['row'][0]['id'] . '\'';
					$this->sqlQuery($sql4);
					$output['success'] = 1;
				}

				//$account_name = json_decode($json_data)->displayName;

				//$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	public function resetTargetSearch(&$output)
	{
		$email = isset($_POST['email']) ? $_POST['email'] : '';
		if ($email == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入電子信箱' : ', 請輸入電子信箱';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the email' : ', Please fill in the email';
		}

		if ($output['success']) {

			try {
				$account_data = $this->getQuery2('select account,id,name from account where email = \'' . $email . '\'');
				if ($account_data['num'] <= 0) {
					$output['success'] = 0;
					$output['message']['ch'] .= ($output['message']['ch'] == '') ? '電子信箱找不到會員資料' : ', 電子信箱找不到會員資料';
					$output['message']['en'] .= ($output['message']['en'] == '') ? 'This email address cannot be used to verify any member accounts.' : ',This email address cannot be used to verify any member accounts.';
				} else if ($account_data['num'] >= 1) {
					$output['success'] = 1;
					$output['id'] = $account_data['row'][0]['id'];
					$output['name'] = $account_data['row'][0]['name'];
				}
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	public function resetMail(&$output)
	{
		$email = isset($_POST['email']) ? $_POST['email'] : '';
		$name = isset($_POST['name']) ? $_POST['name'] : '';
		$account = isset($_POST['account']) ? $_POST['account'] : 0;
		if ($email == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入電子信箱' : ', 請輸入電子信箱';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the email' : ', Please fill in the email';
		}
		if ($name == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入會員名稱' : ', 請輸入會員名稱';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the name' : ', Please fill in the name';
		}
		if ($account == 0) {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入會員障號' : ', 請輸入會員帳號';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the member ID' : ', Please fill in the member ID';
		}
		if ($output['success']) {

			try {
				$token = $this->forgetToken();
				$sql = 'insert into forget_token(token,use_id,created_at,fail_time) values(\'' . $token . '\',\'' . $account . '\',\'' . date('Y-m-d H:i:s', time()) . '\',\'' . date('Y-m-d H:i:s', time() + 10800) . '\')';
				$this->sqlQuery($sql);
				$mail_status = $this->postEmail($email, $name, '會員密碼重設信函', '<h2>會員密碼重設信函</h2><p>請點選連結重設密碼。https://' . $_SERVER['HTTP_HOST'] . '/horn/lineservice/LINE/resetPassword.php?token=' . $token . '</p>');
				//$output['success'] = 1;
				if (!$mail_status['status']) {
					$output['success'] = 0;
					$output['message']['ch'] .= ($output['message']['ch'] == '') ? '服務中斷，請稍後再試' : ', 服務中斷，請稍後再試';
					$output['mailService'] = $mail_status['message'];
					$output['message']['en'] .= ($output['message']['en'] == '') ? 'Service is die.' : ', Service is die.';
				} else if ($mail_status) {
					$output['success'] = 1;
				}
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	//寄信模組
	public function postEmail($email, $name, $subject, $mailcontent)
	{
		set_time_limit(180); //限180秒
		require_once('mod/PHPMailer-5.2.9/PHPMailerAutoload.php'); //phpmailer class的位置
		$mail = new PHPMailer(); //建立新物件
		$mail->IsSMTP(); //設定使用SMTP方式寄信
		$mail->SMTPAuth = true; //設定SMTP需要驗證
		$mail->SMTPSecure = 'tsl'; //outlook需要tsl連線
		//$mail->SMTPDebug = 4;  // debugging: 1 = errors and messages, 2 = messages only
		//$mail->SMTPAutoTLS = true;
		//$mail->SMTPSecure = 'ssl'; // Gmail的SMTP主機需要使用SSL連線
		//$mail->SMTPKeepAlive = true; //chen
		$mail->Mailer = 'smtp'; // chen
		$mail->Host = 'smtp.gmail.com'; //outlook
		$mail->Port = '465'; //outlook port
		//$mail->Host = 'smtp.gmail.com';
		//$mail->Port = '465';
		//$mail->Port = 2525; //Gamil的SMTP主機的埠號(Gmail為465)。
		$mail->CharSet = "utf-8"; //郵件編碼
		$mail->Username = 'smaacg2011@gmail.com';
		$mail->Password = 'mmo357653';
		//$mail->Username = "c38e0c64617b41"; //Gamil帳號
		//$mail->Password = "7c3fdd9dcb44b7"; //Gmail密碼
		$mail->From = "smaacg2011@gmail.com"; //寄件者信箱
		$mail->FromName = "黃柏翰"; //寄件者姓名
		$mail->Subject = $subject;     //郵件標題
		$mail->Body = $mailcontent;        //郵件內容
		$mail->IsHTML(true); //郵件內容為html
		$mail->AddAddress($email, $name); //收件者郵件及名稱
		$mail->AddBCC(" "); //設定 密件副本收件者

		if (!$mail->Send()) {
			//$mail->Send();
			//return false;
			return array(
				"message" => $mail->ErrorInfo,
				"status" => false
			);
			/* $output = array(
            );*/
		} else {
			return true;
			/*$output = array(
                "message" => "警告訊息已寄出",
                "success" => 1
            );*/
		}
	}

	//forget_token 6單位
	public function forgetToken()
	{

		$string_array = array(
			'0', '1', '2', '3', '4',
			'5', '6', '7', '8', '9',
			'A', 'B', 'C', 'D', 'E',
			'F', 'G', 'H', 'I', 'J',
			'K', 'L', 'M', 'N', 'O',
			'P', 'Q', 'R', 'S', 'T',
			'U', 'V', 'W', 'X', 'Y',
			'Z', 'a', 'b', 'c', 'd',
			'e', 'f', 'g', 'h', 'i',
			'j', 'k', 'l', 'm', 'n',
			'o', 'p', 'q', 'r', 's',
			't', 'u', 'v', 'w', 'x',
			'y', 'z'
		);

		$str = '';

		for ($i = 1; $i <= 6; $i++) {
			$str .= $string_array[random_int(0, count($string_array) - 1)];
		}

		return $str;
	}

	//忘記密碼驗證碼狀態
	public function resetTokenCheck(&$output)
	{
		$token = isset($_POST['token']) ? $_POST['token'] : '';
		if ($token == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入驗證碼' : ', 請輸入驗證碼';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the token' : ', Please fill in the token';
		}

		if ($output['success']) {
			try {
				$sql = 'select faild,id,use_id,is_use from forget_token where fail_time > \'' . date('Y-m-d H:i:s', time()) . '\' and token = \'' . $token . '\'';
				$data = $this->getQuery($sql);
				if ($data['num'] <= 0) {
					$output['success'] = -1;
					$this->sqlQuery('update forget_token set faild = 1 where token = \'' . $token . '\'');
				} else {
					if ($data['row'][0]['faild'] == 1) {
						$output['success'] = -1;
					} else if ($data['row'][0]['use_id'] != '') {
						$member_data = $this->getRow2('select email,name,id from account where id = ' . $data['row'][0]['use_id']);
						$output['success'] = 1;
						$output['email'] = $member_data['email'];
						$output['name'] = $member_data['name'];
						$output['id'] = $member_data['id'];
					} else {
						$output['success'] = 0;
					}
				}
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}

	//更新用戶密碼
	public function resetPassword(&$output)
	{
		$password = isset($_POST['password']) ? $_POST['password'] : '';
		$password_check = isset($_POST['password_check']) ? $_POST['password_check'] : '';
		$account =  isset($_POST['account']) ? $_POST['account'] : 0;

		if ($password == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入新密碼。' : ', 請輸入新密碼。';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the new password.' : ', Please fill in the new password.';
		}
		if ($password_check == '') {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請再次輸入新的密碼。' : ', 請再次輸入新的密碼。';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the new password again.' : ', Please fill in the new password again.';
		} else if ($password_check != $password) {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '確認密碼必須與新密碼相同。' : ', 確認密碼必須與新密碼相同。';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Check word should be same new password.' : ', Check word should be same new password.';
		}

		if ($account == 0) {
			$output['success'] = 0;
			$output['message']['ch'] .= ($output['message']['ch'] == '') ? '請輸入會員。' : ', 請輸入會員。';
			$output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the user id.' : ', Please fill in the user id.';
		}

		if ($output['success']) {
			try {
				$this->sqlQuery2('update account set password = \'' . md5($password) . '\' where id = ' . $account);
				$output['success'] = 1;
			} catch (Exception $e) {
				$output['success'] = 0;
				$output['message'] = $e->getMessage();
			}
		}
	}
}
