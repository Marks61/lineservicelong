<?php
require_once('config/config.php');
$realUnion = new RealUnion();
$output = array('success' => 1, 'message' => array('ch' => '', 'en' => ''));
//Google機器人驗證 
$token = isset($_POST['recaptcha']) ? $_POST['recaptcha'] : '';
$secret_key = '6Lcc0ekeAAAAAAk-9RWfeDSbofcBYaOs9vNGftmr';
if ($token == '') {
    $output['success'] = 0;
    $output['message']['ch'] .= ($output['message']['ch'] == '') ? '機器人驗證-未驗證' : ', 機器人驗證-未驗證';
    $output['message']['en'] .= ($output['message']['en'] == '') ? 'Please fill in the google recapcha v3.' : ',Please fill in the google recapcha v3.';
}

$response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret_key . '&response=' . $token);
$response_data = json_decode($response, true);
if ($response_data["success"]) {
    $output['success'] = 1;
    $output['message']['ch'] .= ($output['message']['ch'] == '') ? '機器人驗證-已通過' : ', 機器人驗證-已通過';
    $output['message']['en'] .= ($output['message']['en'] == '') ? 'Verified with Google recaptcha v3.' : ',Verified with Google recaptcha v3.';
} else {
    $output['success'] = 0;
    $output['message']['ch'] .= ($output['message']['ch'] == '') ? '機器人驗證-未通過' : ', 機器人驗證-未通過';
    $output['message']['en'] .= ($output['message']['en'] == '') ? 'Verified fail with Google recaptcha v3.' : ',Verified fail with Google recaptcha v3.';
}

if ($output['success']){
    $realUnion->create($output);
}
echo urldecode(json_encode($output));
