<?php
	date_default_timezone_set("Asia/Taipei");
	$ip='';
	if (!empty($_SERVER["HTTP_CLIENT_IP"])){
		$ip = $_SERVER["HTTP_CLIENT_IP"];
	}elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
		$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
	}else{
		$ip = $_SERVER["REMOTE_ADDR"];
	}
	$record=new record();
	$record->include(isset($_GET['mod'])?$_GET['mod']:'',isset($_GET['content'])?$_GET['content']:'',$admin['id'],$ip);
