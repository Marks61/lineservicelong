<?php
class record extends db
{
	public function include($mod, $content, $user, $ip)
	{
		try {
			$category = array();
			if ($mod == '' && $content == '') {
				$category['id'] = 0;
				$category['name'] = '首頁';
			} else {
				$category = $this->getRow('select id,name from feature where mode = \'' . $mod . '\' and content = \'' . $content . '\'');
			}
			$sql = 'insert into log_file (category,function_name,users_id,ip,record,created_at) values(\'' . $category['id'] . '\',\'' . $category['name'] . '\',\'' . $user . '\',\'' . $ip . '\',\'進入功能首頁\',\'' . date('Y-m-d H:i:s', time()) . '\')';
			$this->sqlQuery($sql);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function insert($mod, $content, $record, $user, $ip)
	{
		try {
			$category = array();
			if ($mod == '' && $content == '') {
				$category['id'] = 0;
				$category['name'] = '首頁';
			} else {
				$category = $this->getRow('select id,name from feature where mode = \'' . $mod . '\' and content = \'' . $content . '\'');
			}
			$sql = 'insert into log_file (category,function_name,users_id,ip,record,created_at) values(\'' . $category['id'] . '\',\'' . $category['name'] . '\',\'' . $user . '\',\'' . $ip . '\',\'' . $record . '\',\'' . date('Y-m-d H:i:s', time()) . '\')';
			$this->sqlQuery($sql);
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function userInfo($user)
	{
		$user_str = '';
		foreach ($user as $row) {
			$user_str .= $row;
		}
		$sql = 'select id,account,name,identity from account where id in (' . $user_str . ')';
		$data = $this->getQuery2($sql)['row'];
		$user_info = array();
		foreach ($data as $row) {
			$user_info[$row['id']]['account'] = $row['account'];
			$user_info[$row['id']]['name'] = $row['name'];
			$user_info[$row['id']]['identity'] = $row['identity'];
		}
		return $user_info;
	}

	public function index($key = '')
	{
		$user = array();
		$sql = 'select log_file.*,log_file.created_at as created_at from log_file';
		$data = $this->getQuery($sql);
		foreach ($data['row'] as $row) {
			if (!in_array($row['users_id'], $user))
				array_push($user, $row['users_id']);
		}
		$userInfo = $this->userInfo($user);
		foreach ($data['row'] as $key => $row) {
			$data['row'][$key]['userinfo'] = $userInfo[$row['users_id']];
		}
		return $data;
	}
}
