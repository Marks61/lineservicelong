<?php
date_default_timezone_set("Asia/Taipei");
$ip = '';
if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
	$ip = $_SERVER["HTTP_CLIENT_IP"];
} elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
	$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
} else {
	$ip = $_SERVER["REMOTE_ADDR"];
}
$record = new record();
$record->insert(isset($_POST['mod']) ? $_POST['mod'] : '', isset($_POST['content']) ? $_POST['content'] : '', $_POST['key'], $admin['id'], $ip);
