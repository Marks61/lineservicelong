<?php
class system extends db
{
	public function featureIndex()
	{
		$sql = 'select * from feature';
		$data = $this->getQuery($sql);
		return $data;
	}

	public function featureClassIndex()
	{
		$sql = 'select * from feature_class';
		$data = $this->getQuery($sql);
		return $data;
	}

	public function featureClassList()
	{
		$sql = 'select id,name,name_en from feature_class where is_use = 1';
		$data = $this->getQuery($sql);
		return $data;
	}

	public function identityIndex()
	{
		$sql = 'select * from identity';
		$data = $this->getQuery($sql);
		return $data;
	}

	public function identityFeatureCategory()
	{
		$sql = 'select name,id from feature_class where is_use = 1';
		$data = $this->getQuery($sql);
		return $data;
	}
}
