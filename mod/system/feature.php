<?php
$system = new system();
$data = $system->featureIndex();
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Dashboard</h5>
                <!--
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>-->
            </div>
            <div class="ibox-content">
                <div class="">
                    <a onclick="" href="javascript:$('#add_modal').modal({backdrop:'static', keyboard: false});" class="btn btn-primary " id="add">新增</a>
                    <a onclick="" href="?mod=system&content=featureClass&ver=<?php echo $admin['ver']; ?>" class="btn btn-primary " id="class_edit">系統分類</a>
                    <a onclick="get_edit_target()" href="javascript:void(0);" class="btn btn-primary " id="editor">編輯</a>
                    <a onclick="control_function()" href="javascript:void(0);" class="btn btn-primary " id="status">啟動/關閉</a>
                    <a onclick="delete_function();" href="javascript:void(0);" class="btn btn-primary" id="deleted">刪除</a>
                </div>
                <table class="table table-striped table-bordered table-hover" id="editable">
                    <thead>
                        <tr>
                            <th></th>
                            <th>功能名稱</th>
                            <th>功能分類</th>
                            <th>狀態</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data['row'] as $row) {
                        ?>
                            <tr>
                            <td><input type="checkbox" onclick="add_item('<?php echo $row['id']; ?>');" class="i-checks" name="input[]" id="check_<?php echo $row['id']; ?>" value="<?php echo $row['id']; ?>"></td>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php echo $row['mode']; ?></td>
                                <td><?php echo $row['is_use'] == 0 ? '<span class="label label-danger text-center pull-center">Closeing</span>' : '<span class="label label-primary text-center pull-center">Opening</span>'; ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="add_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-plus modal-icon"></i>
                <h4 class="modal-title">新增系統功能</h4>
                <small class="font-bold">寫好的網頁程式必須完成登錄才可以被使用。</small>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>選擇分類</label>
                    <select id="class" name="class" class="form-control">
                        <option value="0">請選擇功能分類</option>
                        <?php
                        foreach ($system->featureClassList()['row'] as $data) {
                            echo '<option value="' . $data['id'] . '">' . $data['name'] . '(' . $data['name_en'] . ')</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group"><label>功能名稱</label> <input type="text" id="name" name="name" placeholder="輸入功能中文名稱。" class="form-control"></div>
                <div class="form-group"><label>功能名稱(英文)</label> <input type="text" id="ename" name="ename" placeholder="輸入英文檔案名稱，例如程式功能的檔案名稱為system.php，就在此欄位輸入system。" class="form-control"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary" onclick="add_function();">儲存</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="edit_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-edit modal-icon"></i>
                <h4 class="modal-title">編輯系統功能</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
            <div class="form-group">
                    <label>選擇分類</label>
                    <select id="class" name="class" class="form-control">
                        <?php
                        foreach ($system->featureClassList()['row'] as $data) {
                            echo '<option value="' . $data['id'] . '">' . $data['name'] . '(' . $data['name_en'] . ')</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group"><label>功能名稱</label> <input type="text" placeholder="輸入分類中文名稱。" id="name" name="name" class="form-control"></div>
                <div class="form-group"><label>功能名稱(英文)</label> <input type="text" placeholder="輸入分類英文名稱" id="ename" name="ename" readonly name="ename" class="form-control"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="javascript:$('#check_'+checked_values[0]).click();check_status();" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary" onclick="edit_function();">儲存</button>
            </div>
        </div>
    </div>
</div>