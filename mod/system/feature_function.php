<?php
require_once('../mod/db/class.php');
date_default_timezone_set("Asia/Taipei");
class feature_function extends db
{
    public function checkClassExist(&$mode, $mod, $content)
    {
        $mod = $this->getRow('select name_en from feature_class where id = ' . $mod)['name_en'];
        $mode = $mod;
        $sql = 'select * from feature where content = \'' . $content . '\' and mode = \'' . $mod . '\'';
        $number = $this->getRows($sql);

        if ($number > 0)
            return true;
        else
            return false;
    }

    public function functionFileExist($mod, $content)
    {
        $mod = $this->getRow('select name_en from feature_class where id = ' . $mod)['name_en'];
        if (file_exists('../mod/' . $mod . '/' . $content . '.php')) {
            return true;
        } else {
            return false;
        }
    }

    public function alerUpdate(&$output, $ch, $en)
    {
        $output['success'] = 0;
        $output['message']['ch'] = $ch;
        $output['message']['en'] = $en;
    }

    public function getIp()
    {
        $ip = '';
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
    }
}

$db = new db();
$featureFunction = new feature_function();
$record = new record();
$output = ['success' => 1];
if ($_GET['function'] == 'add') {

    try {
        $category = isset($_POST['category']) ? $_POST['category'] : 0;
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $ename = isset($_POST['ename']) ? $_POST['ename'] : '';
        $mode = '';
        $message_ch = '';
        $message_en = '';
        if ($category == 0) {
            $message_ch == '' ? $message_ch = '必須設定程式類別。' : $message_ch .= ';必須設定程式類別。';
            $message_en == '' ? $message_en = 'Please fill in the feature class.' : $message_en .= 'Please fill in the feature class.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($name == '') {
            $message_ch == '' ? $message_ch = '必須輸入程式名稱。' : $message_ch .= ';必須輸入程式名稱。';
            $message_en == '' ? $message_en = 'Please fill in the feature name.' : $message_en .= 'Please fill in the feature name.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($ename == '') {
            $message_ch == '' ? $message_ch = '必須輸入程式名稱(英文)。' : $message_ch .= ';必須輸入程式名稱(英文)。';
            $message_en == '' ? $message_en = 'Please fill in the feature function name.' : $message_en .= 'Please fill in the feature function name.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($featureFunction->checkClassExist($mode, $category, $ename)) {
            $message_ch == '' ? $message_ch = '無法新增已經存在的程式功能。' : $message_ch .= ';無法新增已經存在的程式功能。';
            $message_en == '' ? $message_en = 'Can`t add same system function .Operation failed.' : $message_en .= 'Can`t add same system function .Operation failed.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if (!$featureFunction->functionFileExist($category, $ename)) {
            $message_ch == '' ? $message_ch = '需要上傳功能檔案。' : $message_ch .= ';需要上傳功能檔案。';
            $message_en == '' ? $message_en = 'Can`t add same system function .Operation failed.' : $message_en .= 'Can`t add same system function .Operation failed.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($output['success']) {
            $sql = 'insert into feature(name,mode,content) values(\'' . $name . '\',\'' . $mode . '\',\'' . $ename . '\')';
            $record->insert('system', 'feature', '新增系統功能:' . $ename, $admin['id'], $featureFunction->getIp());
            $db->sqlQuery($sql);
        }
    } catch (Exception $e) {
        $featureClassFunction->alerUpdate($output, '新增資料失敗', 'Add feature class is failed.');
    }

    echo json_encode($output);
}
if ($_GET['function'] == 'edit') {
    try {
        $category = isset($_POST['category']) ? $_POST['category'] : 0;
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $feature = isset($_POST['mod']) ? $_POST['mod'] : '';
        $message_ch = '';
        $message_en = '';
        if ($feature == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the feature.' : $message_en .= 'Please fill in the feature.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        }

        if ($name == '') {
            $message_ch == '' ? $message_ch = '必須輸入程式名稱。' : $message_ch .= ';必須輸入程式名稱。';
            $message_en == '' ? $message_en = 'Please fill in the feature name.' : $message_en .= 'Please fill in the feature name.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($category == 0) {
            $message_ch == '' ? $message_ch = '必須設定程式類別。' : $message_ch .= ';必須設定程式類別。';
            $message_en == '' ? $message_en = 'Please fill in the feature class.' : $message_en .= 'Please fill in the feature class.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        }

        if ($output['success']) {
            $sql = 'update feature set mode = (select name_en from feature_class where id = ' . $category . '),name = \'' . $name . '\' where id = ' . $feature;
            $record->insert('system', 'feature', '編輯功能名稱:' . $name, $admin['id'], $featureFunction->getIp());
            $db->sqlQuery($sql);
        }
    } catch (Exception $e) {
        $featureFunction->alerUpdate($output, '編輯資料失敗', 'Edit feature class is failed.');
    }

    echo json_encode($output);
}
if ($_GET['function'] == 'setting') {
    try {
        $feature = isset($_POST['mod']) ? $_POST['mod'] : '';
        $feature_open = '';
        $feature_close = '';
        $feature_text = '';
        $feature_open_array = [];
        $feature_close_array = [];
        $message_ch = '';
        $message_en = '';
        if ($feature == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the feature.' : $message_en .= 'Please fill in the feature.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        } else {
            for ($i = 0; $i < count($feature); $i++) {
                if ($i == (count($feature) - 1)) {
                    $feature_text .= $feature[$i];
                } else {
                    $feature_text .= $feature[$i] . ',';
                }
            }

            $sql_open = 'select id from feature where id in (' . $feature_text . ') and is_use = 0';
            $sql_close = 'select id from feature where id in (' . $feature_text . ') and is_use = 1';

            $data_open = $db->getQuery($sql_open)['row'];
            $data_close = $db->getQuery($sql_close)['row'];

            for ($k = 0; $k < count($data_open); $k++) {
                if ($k == (count($data_open) - 1)) {
                    $feature_open .= $data_open[$k]['id'];
                } else {
                    $feature_open .= $data_open[$k]['id'] . ',';
                }
            }

            for ($j = 0; $j < count($data_close); $j++) {
                if ($j == (count($data_close) - 1)) {
                    $feature_close .= $data_close[$j]['id'];
                } else {
                    $feature_close .= $data_close[$j]['id'] . ',';
                }
            }
        }

        if ($output['success']) {
            $sql1 = 'update feature set is_use = 1 where id in (' . $feature_open . ')';
            $sql2 = 'update feature set is_use = 0 where id in (' . $feature_close . ')';
            $db->sqlQuery($sql1);
            $db->sqlQuery($sql2);
            $record->insert('system', 'feature', '編輯系統功能狀態:', $admin['id'], $featureFunction->getIp());
        }
    } catch (Exception $e) {
        $featureClassFunction->alerUpdate($output, '編輯資料失敗', 'Edit feature class is failed.');
    }

    echo json_encode($output);
}
if ($_GET['function'] == 'delete') {
    try {
        $feature = isset($_POST['mod']) ? $_POST['mod'] : '';
        $feature_text = '';
        $message_ch = '';
        $message_en = '';
        if ($feature == '') {
            $message_ch == '' ? $message_ch = '必須選擇刪除項目。' : $message_ch .= ';必須選擇刪除項目。';
            $message_en == '' ? $message_en = 'Please fill in the feature.' : $message_en .= 'Please fill in the feature.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        } else {
            for ($i = 0; $i < count($feature); $i++) {
                if ($i == (count($feature) - 1)) {
                    $feature_text .= $feature[$i];
                } else {
                    $feature_text .= $feature[$i] . ',';
                }
            }
        }
        if ($output['success']) {
            $sql = 'delete from feature where id in ( ' . $feature_text . ')';
            $record->insert('system', 'feature', '刪除系統功能。', $admin['id'], $featureFunction->getIp());
            $db->sqlQuery($sql);
        }
    } catch (Exception $e) {
        $featureFunction->alerUpdate($output, '刪除資料失敗', 'Delete feature is failed.');
    }

    echo json_encode($output);
}
if ($_GET['function'] == 'searchEditData') {
    try {
        $feature = isset($_POST['mod']) ? $_POST['mod'] : '';
        $message_ch = '';
        $message_en = '';
        if ($feature == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the feature.' : $message_en .= 'Please fill in the feature.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($output['success']) {
            $sql = 'select category.id as category,feature.name as name,feature.content as content from feature,feature_class category where feature.mode = category.name_en and feature.id = ' . $feature;
            $output['data'] = $db->getRow($sql);
        }
    } catch (Exception $e) {
        $featureFunction->alerUpdate($output, '編輯資料失敗', 'Edit feature class is failed.');
    }

    echo json_encode($output);
}

if ($_GET['function'] == 'searchDeleteData') {
    try {
        $feature = isset($_POST['mod']) ? $_POST['mod'] : '';
        $feature_text = '';
        $message_ch = '';
        $message_en = '';
        if ($feature == '') {
            $message_ch == '' ? $message_ch = '必須設定刪除項目。' : $message_ch .= ';必須設定刪除項目。';
            $message_en == '' ? $message_en = 'Please fill in the feature.' : $message_en .= 'Please fill in the feature.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        } else {
            for ($i = 0; $i < count($feature); $i++) {
                if ($i == (count($feature) - 1)) {
                    $feature_text .= $feature[$i];
                } else {
                    $feature_text .= $feature[$i] . ',';
                }
            }
        }

        if ($output['success']) {
            $sql = 'select name,mode,content from feature where id in (' . $feature_text . ')';
            $output['data'] = $db->getQuery($sql);
        }
    } catch (Exception $e) {
        $featureFunction->alerUpdate($output, '刪除資料查詢失敗', 'Delete feature is failed.');
    }

    echo json_encode($output);
}

if ($_GET['function'] == 'searchControlData') {
    try {
        $feature = isset($_POST['mod']) ? $_POST['mod'] : '';
        $feature_text = '';
        $message_ch = '';
        $message_en = '';
        if ($feature == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the feature.' : $message_en .= 'Please fill in the feature.';
            $featureFunction->alerUpdate($output, $message_ch, $message_en);
        } else {
            for ($i = 0; $i < count($feature); $i++) {
                if ($i == (count($feature) - 1)) {
                    $feature_text .= $feature[$i];
                } else {
                    $feature_text .= $feature[$i] . ',';
                }
            }
        }

        if ($output['success']) {
            $sql = 'select name,mode,content,is_use from feature where id in (' . $feature_text . ')';
            $output['data'] = $db->getQuery($sql);
            for ($i = 0; $i < count($output['data']['row']); $i++) {
                if ($output['data']['row'][$i]['is_use'] == 1)
                    $output['data']['row'][$i]['status'] = '停用';
                else if ($output['data']['row'][$i]['is_use'] == 0)
                    $output['data']['row'][$i]['status'] = '啟用';
            }
        }
    } catch (Exception $e) {
        $featureFunction->alerUpdate($output, '查詢失敗', 'Edit feature is failed.');
    }

    echo json_encode($output);
}

return json_encode($output);
