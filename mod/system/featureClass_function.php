<?php
require_once('../mod/db/class.php');
date_default_timezone_set("Asia/Taipei");
class featureClass_function extends db
{
    public function checkClassExist($system)
    {
        $sql = 'select * from feature_class where name_en = \'' . $system . '\'';
        $number = $this->getRows($sql);

        if ($number > 0)
            return true;
        else
            return false;
    }

    public function alerUpdate(&$output, $ch, $en)
    {
        $output['success'] = 0;
        $output['message']['ch'] = $ch;
        $output['message']['en'] = $en;
    }

    public function getIp()
    {
        $ip = '';
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
    }
}

$db = new db();
$featureClassFunction = new featureClass_function();
$record = new record();
$output = ['success' => 1];
if ($_GET['function'] == 'add') {

    try {
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $ename = isset($_POST['ename']) ? $_POST['ename'] : '';
        $message_ch = '';
        $message_en = '';
        if ($name == '') {
            $message_ch == '' ? $message_ch = '必須輸入程式分類名稱。' : $message_ch .= ';必須輸入程式分類名稱。';
            $message_en == '' ? $message_en = 'Please fill in the feature class name.' : $message_en .= 'Please fill in the feature class name.';
            $featureClassFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($ename == '') {
            $message_ch == '' ? $message_ch = '必須輸入程式分類名稱(英文)。' : $message_ch .= ';必須輸入程式分類名稱(英文)。';
            $message_en == '' ? $message_en = 'Please fill in the feature class model name.' : $message_en .= 'Please fill in the feature class model name.';
            $featureClassFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($featureClassFunction->checkClassExist($ename)) {
            $message_ch == '' ? $message_ch = '無法新增已經存在的系統分類。' : $message_ch .= ';無法新增已經存在的系統分類。';
            $message_en == '' ? $message_en = 'Can`t add same system category .Operation failed.' : $message_en .= 'Can`t add same system category .Operation failed.';
            $featureClassFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($output['success']) {
            $sql = 'insert into feature_class(name,name_en,identity) values(\'' . $name . '\',\'' . $ename . '\',\'admin\')';
            $db->sqlQuery($sql);
            $insertData = $db->getLastId();
            $identity = '';
            $identityData = mb_split(',', $db->getRow('select category from identity where name_en = \'admin\'')['category']);
            for ($i = 0; $i < count($identityData); $i++) {
                if ($i == (count($identityData) - 1)) {
                    $identity .= $identityData[$i] . ',' . $insertData;
                } else {
                    $identity .= $identityData[$i] . ',';
                }
            }
            $db->sqlQuery('update identity set category = \'' . $identity . '\' where name_en = \'admin\'');
            $record->insert('system', 'featureClass', '新增系統類別:' . $ename, $admin['id'], $featureClassFunction->getIp());
        }
    } catch (Exception $e) {
        $featureClassFunction->alerUpdate($output, '新增資料失敗', 'Add feature class is failed.');
    }

    echo json_encode($output);
}
if ($_GET['function'] == 'edit') {
    try {
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $mod = isset($_POST['mod']) ? $_POST['mod'] : '';
        $message_ch = '';
        $message_en = '';
        if ($mod == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the feature class.' : $message_en .= 'Please fill in the feature class.';
            $featureClassFunction->alerUpdate($output, $message_ch, $message_en);
        }

        if ($name == '') {
            $message_ch == '' ? $message_ch = '必須輸入程式分類名稱。' : $message_ch .= ';必須輸入程式分類名稱。';
            $message_en == '' ? $message_en = 'Please fill in the feature class name.' : $message_en .= 'Please fill in the feature class name.';
            $featureClassFunction->alerUpdate($output, $message_ch, $message_en);
        }

        if ($output['success']) {
            $sql = 'update feature_class set name = \'' . $name . '\' where id = ' . $mod;
            $record->insert('system', 'featureClass', '編輯系統類別名稱:' . $name, $admin['id'], $featureClassFunction->getIp());
            $db->sqlQuery($sql);
        }
    } catch (Exception $e) {
        $featureClassFunction->alerUpdate($output, '編輯資料失敗', 'Edit feature class is failed.');
    }

    echo json_encode($output);
}
if ($_GET['function'] == 'setting') {
    try {
        $mod = isset($_POST['mod']) ? $_POST['mod'] : '';
        $mod_open = '';
        $mod_close = '';
        $mod_text = '';
        $mod_open_array = [];
        $mod_close_array = [];
        $message_ch = '';
        $message_en = '';
        if ($mod == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the feature class.' : $message_en .= 'Please fill in the feature class.';
            $featureClassFunction->alerUpdate($output, $message_ch, $message_en);
        } else {
            for ($i = 0; $i < count($mod); $i++) {
                if ($i == (count($mod) - 1)) {
                    $mod_text .= $mod[$i];
                } else {
                    $mod_text .= $mod[$i] . ',';
                }
            }

            $sql_open = 'select id from feature_class where id in (' . $mod_text . ') and is_use = 0';
            $sql_close = 'select id from feature_class where id in (' . $mod_text . ') and is_use = 1';

            $data_open = $db->getQuery($sql_open)['row'];
            $data_close = $db->getQuery($sql_close)['row'];

            for ($k = 0; $k < count($data_open); $k++) {
                if ($k == (count($data_open) - 1)) {
                    $mod_open .= $data_open[$k]['id'];
                } else {
                    $mod_open .= $data_open[$k]['id'] . ',';
                }
            }

            for ($j = 0; $j < count($data_close); $j++) {
                if ($j == (count($data_close) - 1)) {
                    $mod_close .= $data_close[$j]['id'];
                } else {
                    $mod_close .= $data_close[$j]['id'] . ',';
                }
            }
        }

        if ($output['success']) {
            $sql1 = 'update feature_class set is_use = 1 where id in (' . $mod_open . ')';
            $sql2 = 'update feature_class set is_use = 0 where id in (' . $mod_close . ')';
            $db->sqlQuery($sql1);
            $db->sqlQuery($sql2);
            $record->insert('system', 'featureClass', '編輯系統狀態:', $admin['id'], $featureClassFunction->getIp());
        }
    } catch (Exception $e) {
        $featureClassFunction->alerUpdate($output, '編輯資料失敗', 'Edit feature class is failed.');
    }

    echo json_encode($output);
}
if ($_GET['function'] == 'delete') {
    try {
        $mod = isset($_POST['mod']) ? $_POST['mod'] : '';
        $mod_text = '';
        $message_ch = '';
        $message_en = '';
        if ($mod == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the feature class.' : $message_en .= 'Please fill in the feature class.';
            $featureClassFunction->alerUpdate($output, $message_ch, $message_en);
        } else {
            for ($i = 0; $i < count($mod); $i++) {
                if ($i == (count($mod) - 1)) {
                    $mod_text .= $mod[$i];
                } else {
                    $mod_text .= $mod[$i] . ',';
                }
            }
        }
        if ($output['success']) {
            $sql = 'delete from feature_class where id in ( ' . $mod_text . ')';
            $record->insert('system', 'featureClass', '刪除系統類別。', $admin['id'], $featureClassFunction->getIp());
            $db->sqlQuery($sql);
        }
    } catch (Exception $e) {
        $featureClassFunction->alerUpdate($output, '刪除資料失敗', 'Delete feature class is failed.');
    }

    echo json_encode($output);
}
if ($_GET['function'] == 'searchEditData') {
    try {
        $mod = isset($_POST['mod']) ? $_POST['mod'] : '';
        $message_ch = '';
        $message_en = '';
        if ($mod == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the feature class.' : $message_en .= 'Please fill in the feature class.';
            $featureClassFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($output['success']) {
            $sql = 'select name,name_en from feature_class where id = ' . $mod;
            $output['data'] = $db->getRow($sql);
        }
    } catch (Exception $e) {
        $featureClassFunction->alerUpdate($output, '編輯資料失敗', 'Edit feature class is failed.');
    }

    echo json_encode($output);
}

if ($_GET['function'] == 'searchDeleteData') {
    try {
        $mod = isset($_POST['mod']) ? $_POST['mod'] : '';
        $mod_text = '';
        $message_ch = '';
        $message_en = '';
        if ($mod == '') {
            $message_ch == '' ? $message_ch = '必須設定刪除項目。' : $message_ch .= ';必須設定刪除項目。';
            $message_en == '' ? $message_en = 'Please fill in the feature class.' : $message_en .= 'Please fill in the feature class.';
            $featureClassFunction->alerUpdate($output, $message_ch, $message_en);
        } else {
            for ($i = 0; $i < count($mod); $i++) {
                if ($i == (count($mod) - 1)) {
                    $mod_text .= $mod[$i];
                } else {
                    $mod_text .= $mod[$i] . ',';
                }
            }
        }

        if ($output['success']) {
            $sql = 'select name,name_en from feature_class where id in (' . $mod_text . ')';
            $output['data'] = $db->getQuery($sql);
        }
    } catch (Exception $e) {
        $featureClassFunction->alerUpdate($output, '刪除資料失敗', 'Delete feature class is failed.');
    }

    echo json_encode($output);
}

if ($_GET['function'] == 'searchControlData') {
    try {
        $mod = isset($_POST['mod']) ? $_POST['mod'] : '';
        $mod_text = '';
        $message_ch = '';
        $message_en = '';
        if ($mod == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the feature class.' : $message_en .= 'Please fill in the feature class.';
            $featureClassFunction->alerUpdate($output, $message_ch, $message_en);
        } else {
            for ($i = 0; $i < count($mod); $i++) {
                if ($i == (count($mod) - 1)) {
                    $mod_text .= $mod[$i];
                } else {
                    $mod_text .= $mod[$i] . ',';
                }
            }
        }

        if ($output['success']) {
            $sql = 'select name,name_en,is_use from feature_class where id in (' . $mod_text . ')';
            $output['data'] = $db->getQuery($sql);
            for ($i = 0; $i < count($output['data']['row']); $i++) {
                if ($output['data']['row'][$i]['is_use'] == 1)
                    $output['data']['row'][$i]['status'] = '停用';
                else if ($output['data']['row'][$i]['is_use'] == 0)
                    $output['data']['row'][$i]['status'] = '啟用';
            }
        }
    } catch (Exception $e) {
        $featureClassFunction->alerUpdate($output, '查詢失敗', 'Edit feature class is failed.');
    }

    echo json_encode($output);
}

return json_encode($output);
