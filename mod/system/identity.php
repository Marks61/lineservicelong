<?php
$system = new system();
$data = $system->identityIndex();
$category = $system->identityFeatureCategory();
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Dashboard</h5>
                <!--
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>-->
            </div>
            <div class="ibox-content">
                <div class="">
                    <a onclick="" href="javascript:$('#add_modal').modal({backdrop:'static', keyboard: false});" class="btn btn-primary " id="add">新增</a>
                    <a onclick="get_edit_target();" class="btn btn-primary " id="editor">編輯</a>
                    <a onclick="more_setting();" class="btn btn-warning " id="more_setting">更多設定</a>
                    <a onclick="control_function();" href="javascript:void(0);" class="btn btn-primary " id="status">啟動/關閉</a>
                    <a onclick="delete_function();" class="btn btn-primary" id="deleted">刪除</a>
                </div>
                <table class="table table-striped table-bordered table-hover" id="editable">
                    <thead>
                        <tr>
                            <th></th>
                            <th>使用者權限名稱</th>
                            <th>狀態</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data['row'] as $row) {
                        ?>
                            <tr>
                                <td><?php echo $row['importance'] == 1 ? '' : '<input type="checkbox" onclick="add_item(\'' . $row['id'] . '\');" class="i-checks" name="input[]" id="check_' . $row['id'] . '" value="' . $row['id'] . '">'; ?></td>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php echo $row['is_use'] == 0 ? '<span class="label label-danger text-center pull-center">Closeing</span>' : '<span class="label label-primary text-center pull-center">Opening</span>'; ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="add_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-plus modal-icon"></i>
                <h4 class="modal-title">新增使用者權限</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <div class="form-group"><label>權限名稱</label> <input type="text" placeholder="輸入權限中文名稱。" id="name" name="name" class="form-control"></div>
                <div class="form-group"><label>權限名稱(英文)</label> <input type="text" placeholder="輸入權限英文名稱" id="ename" name="ename" class="form-control"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal" onclick="javascript:re='add';more_setting_modal();">更多設定</button>
                <button type="button" class="btn btn-primary" onclick="add_function();">儲存</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="edit_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-plus modal-icon"></i>
                <h4 class="modal-title">編輯使用者權限</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <div class="form-group"><label>權限名稱</label> <input type="text" placeholder="輸入權限中文名稱。" id="name" name="name" class="form-control"></div>
                <div class="form-group"><label>權限名稱(英文)</label> <input type="text" placeholder="輸入權限英文名稱" id="ename" readonly name="ename" class="form-control"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="javascript:$('#check_'+checked_values[0]).click();check_status();" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal" onclick="javascript:re='edit';more_setting_modal(checked_values[0]);">更多設定</button>
                <button type="button" class="btn btn-primary" onclick="edit_function();">儲存</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="more_setting_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-list-ul modal-icon"></i>
                <h4 class="modal-title">權限可用功能設定</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <div class="checkbox checkbox-danger">
                    <?php
                    foreach ($category['row'] as $row) {
                    ?>
                        <input type="checkbox" class="i-checks" name="category[]" id="category_<?php echo $row['id']; ?>" value="<?php echo $row['id']; ?>" onclick="add_category('<?php echo $row['id']; ?>');">
                        <label>
                            <sapn for="category_<?php echo $row['id']; ?>" class="modal-checkbox span"><?php echo $row['name']; ?></sapn>
                        </label></br>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="more_setting_reback(re);" data-dismiss="modal">儲存</button>
            </div>
        </div>
    </div>
</div>