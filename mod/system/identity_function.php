<?php
require_once('../mod/db/class.php');
date_default_timezone_set("Asia/Taipei");
class identity_function extends db
{
    public function checkIdentityExist($system)
    {
        $sql = 'select * from identity where name_en = \'' . $system . '\'';
        $number = $this->getRows($sql);

        if ($number > 0)
            return true;
        else
            return false;
    }

    public function alerUpdate(&$output, $ch, $en)
    {
        $output['success'] = 0;
        $output['message']['ch'] = $ch;
        $output['message']['en'] = $en;
    }

    public function getIp()
    {
        $ip = '';
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
    }

    public function category($data)
    {
        $text = '';
        for ($i = 0; $i < count($data); $i++) {
            if ($i == (count($data) - 1)) {
                $text .= $data[$i];
            } else {
                $text .= $data[$i] . ',';
            }
        }

        return $text;
    }

    public function alert_message($category, $identity)
    {
        try {
            $category_group = $this->getQuery('select id,name from feature_class where id in (\'' . $category . '\')')['row'];
            $identity = $this->getRow('select id from identity where name_en  = \'' . $identity . '\'')['id'];
            $category_text = '';
            for ($i = 0; $i < count($category_group); $i++) {
                if ($i == (count($category_group) - 1)) {
                    $category_text .= $category_group[$i]['name'];
                } else {
                    $category_text .= $category_group[$i]['name'] . ',';
                }
            }

            $this->sqlQuery('insert into notification(identity_group,title,content,icon,created_at) values(\'' . $identity . '\',\'功能開放通知:\',\'「' . $category_text . '」功能已經開通。\',\'img/alert.png\',\'' . date('Y-m-d H:i:s', date_timestamp_get(new DateTime())) . '\')');
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}

$db = new db();
$identityFunction = new identity_function();
$record = new record();
$output = ['success' => 1];
if ($_GET['function'] == 'add') {

    try {
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $ename = isset($_POST['ename']) ? $_POST['ename'] : '';
        $category = isset($_POST['category']) ? $identityFunction->category($_POST['category']) : '';
        $message_ch = '';
        $message_en = '';
        if ($name == '') {
            $message_ch == '' ? $message_ch = '必須輸入使用者權限名稱。' : $message_ch .= ';必須輸入使用者權限名稱名稱。';
            $message_en == '' ? $message_en = 'Please fill in the user identity name.' : $message_en .= 'Please fill in the user identity name.';
            $identityFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($ename == '') {
            $message_ch == '' ? $message_ch = '必須輸入使用者權限名稱(英文)。' : $message_ch .= ';必須輸入使用者權限名稱(英文)。';
            $message_en == '' ? $message_en = 'Please fill in the user identity name(EN).' : $message_en .= 'Please fill in the user identity name(EN).';
            $identityFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($identityFunction->checkIdentityExist($ename)) {
            $message_ch == '' ? $message_ch = '無法新增已經存在的使用者權限。' : $message_ch .= ';無法新增已經存在的使用者權限。';
            $message_en == '' ? $message_en = 'Can`t add same user identity .Operation failed.' : $message_en .= 'Can`t add same user identity .Operation failed.';
            $identityFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($output['success']) {
            $sql = 'insert into identity(name,name_en,category) values(\'' . $name . '\',\'' . $ename . '\',\'' . ($category != '' ? $category : '') . '\')';
            $record->insert('system', 'identity', '新增使用者權限:' . $ename, $admin['id'], $identityFunction->getIp());
            $db->sqlQuery($sql);
            if ($category != '')
                $identityFunction->alert_message($category, $ename);
        }
    } catch (Exception $e) {
        $idenityFunction->alerUpdate($output, '新增資料失敗', 'Add user identity is failed.');
    }

    echo json_encode($output);
}
if ($_GET['function'] == 'edit') {
    try {
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $category = isset($_POST['category']) ? $identityFunction->category($_POST['category']) : '';
        $data = isset($_POST['mod']) ? $_POST['mod'] : '';
        $message_ch = '';
        $message_en = '';
        if ($data == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the user identity.' : $message_en .= 'Please fill in the user identity.';
            $identityFunction->alerUpdate($output, $message_ch, $message_en);
        }

        if ($name == '') {
            $message_ch == '' ? $message_ch = '必須輸入使用者權限名稱。' : $message_ch .= ';必須輸入使用者權限名稱。';
            $message_en == '' ? $message_en = 'Please fill in the user identity name.' : $message_en .= 'Please fill in the user identity name.';
            $identityFunction->alerUpdate($output, $message_ch, $message_en);
        }

        if ($output['success']) {
            $sql = 'update identity set name = \'' . $name . '\'' . ($category != '' ? ',category = \'' . $category . '\'' : '') . ' where id = ' . $data;
            $record->insert('system', 'identity', '編輯使用者權限:' . $name, $admin['id'], $identityFunction->getIp());
            $db->sqlQuery($sql);
        }
    } catch (Exception $e) {
        $identityFunction->alerUpdate($output, '編輯資料失敗', 'Edit user identity is failed.');
    }

    echo json_encode($output);
}
if ($_GET['function'] == 'setting') {
    try {
        $data = isset($_POST['mod']) ? $_POST['mod'] : '';
        $data_open = '';
        $data_close = '';
        $data_text = '';
        $data_open_array = [];
        $data_close_array = [];
        $message_ch = '';
        $message_en = '';
        if ($data == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the user identity.' : $message_en .= 'Please fill in the user identity.';
            $identityFunction->alerUpdate($output, $message_ch, $message_en);
        } else {
            for ($i = 0; $i < count($data); $i++) {
                if ($i == (count($data) - 1)) {
                    $data_text .= $data[$i];
                } else {
                    $data_text .= $data[$i] . ',';
                }
            }

            $sql_open = 'select id from identity where id in (' . $data_text . ') and is_use = 0';
            $sql_close = 'select id from identity where id in (' . $data_text . ') and is_use = 1';

            $row_open = $db->getQuery($sql_open)['row'];
            $row_close = $db->getQuery($sql_close)['row'];

            for ($k = 0; $k < count($row_open); $k++) {
                if ($k == (count($row_open) - 1)) {
                    $data_open .= $row_open[$k]['id'];
                } else {
                    $data_open .= $row_open[$k]['id'] . ',';
                }
            }

            for ($j = 0; $j < count($row_close); $j++) {
                if ($j == (count($row_close) - 1)) {
                    $data_close .= $row_close[$j]['id'];
                } else {
                    $data_close .= $row_close[$j]['id'] . ',';
                }
            }
        }

        if ($output['success']) {
            $sql1 = 'update identity set is_use = 1 where id in (' . $data_open . ')';
            $sql2 = 'update identity set is_use = 0 where id in (' . $data_close . ')';
            $db->sqlQuery($sql1);
            $db->sqlQuery($sql2);
            $record->insert('system', 'identity', '編輯使用者權限狀態:', $admin['id'], $identityFunction->getIp());
        }
    } catch (Exception $e) {
        $identityFunction->alerUpdate($output, '編輯資料失敗', 'Edit user identity is failed.');
    }

    echo json_encode($output);
}
if ($_GET['function'] == 'delete') {
    try {
        $data = isset($_POST['mod']) ? $_POST['mod'] : '';
        $data_text = '';
        $message_ch = '';
        $message_en = '';
        if ($data == '') {
            $message_ch == '' ? $message_ch = '必須設定刪除項目。' : $message_ch .= ';必須設定刪除項目。';
            $message_en == '' ? $message_en = 'Please fill in the user identity.' : $message_en .= 'Please fill in the user identity.';
            $identityFunction->alerUpdate($output, $message_ch, $message_en);
        } else {
            for ($i = 0; $i < count($data); $i++) {
                if ($i == (count($data) - 1)) {
                    $data_text .= $data[$i];
                } else {
                    $data_text .= $data[$i] . ',';
                }
            }
        }
        if ($output['success']) {
            $sql = 'delete from identity where id in ( ' . $data_text . ')';
            $record->insert('system', 'identity', '刪除使用者權限。', $admin['id'], $identityFunction->getIp());
            $db->sqlQuery($sql);
        }
    } catch (Exception $e) {
        $identityFunction->alerUpdate($output, '刪除資料失敗', 'Delete user identity is failed.');
    }

    echo json_encode($output);
}
if ($_GET['function'] == 'searchEditData') {
    try {
        $data = isset($_POST['mod']) ? $_POST['mod'] : '';
        $message_ch = '';
        $message_en = '';
        if ($data == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the user identity.' : $message_en .= 'Please fill in the user identity.';
            $identityFunction->alerUpdate($output, $message_ch, $message_en);
        }
        if ($output['success']) {
            $sql = 'select name,name_en,category from identity where id = ' . $data;
            $output['data'] = $db->getRow($sql);
            $output['data']['category'] = mb_split(',', $output['data']['category']);
        }
    } catch (Exception $e) {
        $idenityFunction->alerUpdate($output, '編輯資料失敗', 'Edit user identity is failed.');
    }

    echo json_encode($output);
}

if ($_GET['function'] == 'searchDeleteData') {
    try {
        $data = isset($_POST['mod']) ? $_POST['mod'] : '';
        $data_text = '';
        $message_ch = '';
        $message_en = '';
        if ($data == '') {
            $message_ch == '' ? $message_ch = '必須設定刪除項目。' : $message_ch .= ';必須設定刪除項目。';
            $message_en == '' ? $message_en = 'Please fill in the user identity.' : $message_en .= 'Please fill in the user identity.';
            $identityFunction->alerUpdate($output, $message_ch, $message_en);
        } else {
            for ($i = 0; $i < count($data); $i++) {
                if ($i == (count($data) - 1)) {
                    $data_text .= $data[$i];
                } else {
                    $data_text .= $data[$i] . ',';
                }
            }
        }

        if ($output['success']) {
            $sql = 'select name,name_en from identity where id in (' . $data_text . ')';
            $output['data'] = $db->getQuery($sql);
        }
    } catch (Exception $e) {
        $identityFunction->alerUpdate($output, '刪除資料失敗', 'Delete user identity is failed.');
    }

    echo json_encode($output);
}

if ($_GET['function'] == 'searchControlData') {
    try {
        $data = isset($_POST['mod']) ? $_POST['mod'] : '';
        $data_text = '';
        $message_ch = '';
        $message_en = '';
        if ($data == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定刪除項目。';
            $message_en == '' ? $message_en = 'Please fill in the user identity.' : $message_en .= 'Please fill in the user identity.';
            $identityFunction->alerUpdate($output, $message_ch, $message_en);
        } else {
            for ($i = 0; $i < count($data); $i++) {
                if ($i == (count($data) - 1)) {
                    $data_text .= $data[$i];
                } else {
                    $data_text .= $data[$i] . ',';
                }
            }
        }

        if ($output['success']) {
            $sql = 'select name,name_en,is_use from identity where id in (' . $data_text . ')';
            $output['data'] = $db->getQuery($sql);
            for ($i = 0; $i < count($output['data']['row']); $i++) {
                if ($output['data']['row'][$i]['is_use'] == 1)
                    $output['data']['row'][$i]['status'] = '停用';
                else if ($output['data']['row'][$i]['is_use'] == 0)
                    $output['data']['row'][$i]['status'] = '啟用';
            }
        }
    } catch (Exception $e) {
        $identityFunction->alerUpdate($output, '查詢失敗', 'Edit user identity is failed.');
    }

    echo json_encode($output);
}

if ($_GET['function'] == 'edit_category') {
    try {
        $data = isset($_POST['mod']) ? $_POST['mod'] : '';
        $category = isset($_POST['category']) ? $identityFunction->category($_POST['category']) : '';
        $message_ch = '';
        $message_en = '';
        if ($data == '') {
            $message_ch == '' ? $message_ch = '必須設定編輯項目。' : $message_ch .= ';必須設定編輯項目。';
            $message_en == '' ? $message_en = 'Please fill in the user identity.' : $message_en .= 'Please fill in the user identity.';
            $identityFunction->alerUpdate($output, $message_ch, $message_en);
        }

        if ($output['success']) {
            $sql = 'update identity set ' . ($category != '' ? 'category = \'' . $category . '\'' : 'category = NULL') . ' where id = ' . $data;
            $db->sqlQuery($sql);
        }
    } catch (Exception $e) {
        $identityFunction->alerUpdate($output, '資料編輯失敗', 'Edit user identity is failed.');
    }

    echo json_encode($output);
}

return json_encode($output);
