<?php
require_once('../mod/db/class.php');
date_default_timezone_set("Asia/Taipei");
class alertCatch_function extends db
{
    public function alerUpdate(&$output, $ch, $en)
    {
        $output['success'] = 0;
        $output['message']['ch'] = $ch;
        $output['message']['en'] = $en;
    }

    public function getIp()
    {
        $ip = '';
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
    }

    public function identity($identity)
    {
        return $this->getRow('select id from identity where name_en = \'' . $identity . '\'')['id'];
    }
}

$db = new db();
$alertCatchFunction = new alertCatch_function();
$output = ['success' => 1];
try {
    $member = isset($_POST['member']) ? $_POST['member'] : '';
    $identity = isset($_POST['identity']) ? $alertCatchFunction->identity($_POST['identity']) : '';

    if ($output['success']) {
        $sql = 'select id,title,content,icon,link,created_at from notification where poster = 0 and (member = \'' . $member . '\' or identity_group = \'' . $identity . '\') order by created_at desc';
        $output['data'] = $db->getQuery($sql);
    }
} catch (Exception $e) {
    $alertCatchFunction->alerUpdate($output, '找不到資料', 'Find alert data is failed.');
}

echo json_encode($output);
