<script>
    var checked_values = [];
    var function_mod = '<?php echo $_GET['mod']; ?>';
    var function_content = '<?php echo $_GET['content']; ?>';

    $(document).ready(function() {
        $('#editor').hide();
        $('#status').hide();
        $('#deleted').hide();
        $("input[name='input[]']").click(function() {
            if ($("input[name='input[]']").is(':checked')) {
                //console.log(checked_values);
                editer_show();
            } else {
                checked_values = [];
                editer_hide();
            }
        });

        $('input[type=search]').keyup(function() {
            var request = $.ajax({
                url: "ajax.php?mod=record&content=memory&ver=<?php echo $admin['ver']; ?>",
                method: "POST",
                data: {
                    mod: '<?php echo $_GET['mod']; ?>',
                    content: '<?php echo $_GET['content']; ?>',
                    key: '關鍵字查詢:' + $('input[type=search]').val()
                },
                dataType: "text"
            });

            request.done(function(dat) {
                /*var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }*/
                console.log('側錄中請微笑');
            });

            request.fail(function(jqXHR, textStatus) {
                alert("Requset failed: " + textStatus);
            });

        });
    });


    $('.dataTables-example').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
                extend: 'copy'
            },
            {
                extend: 'csv'
            },
            {
                extend: 'excel',
                title: 'ExampleFile'
            },
            {
                extend: 'pdf',
                title: 'ExampleFile'
            },

            {
                extend: 'print',
                customize: function(win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]

    });

    /* Init DataTables */
    var oTable = $('#editable').DataTable();

    /* Apply the jEditable handlers to the table */
    oTable.$('td').editable('../example_ajax.php', {
        "callback": function(sValue, y) {
            var aPos = oTable.fnGetPosition(this);
            oTable.fnUpdate(sValue, aPos[0], aPos[1]);
        },
        "submitdata": function(value, settings) {
            return {
                "row_id": this.parentNode.getAttribute('id'),
                "column": oTable.fnGetPosition(this)[2]
            };
        },

        "width": "90%",
        "height": "100%",
        "type": "readonly"
    });

    function fnClickAddRow() {
        $('#editable').dataTable().fnAddData([
            "Custom row",
            "New row",
            "New row",
            "New row",
            "New row",
            "New row"
        ]);

    }

    function editer_show() {
        $('#editor').show();
        $('#status').show();
        $('#deleted').show();
        if (checked_values.length >= 2) {
            $('#editor').hide();
        } else {
            $('#editor').show();
        }
    }

    function editer_hide() {
        $('#editor').hide();
        $('#status').hide();
        $('#deleted').hide();
    }

    function add_function() {
        $("#add_modal select").removeClass('alert_textbox');
        $("#add_modal input[type='text']").removeClass('alert_textbox');
        var request = $.ajax({
            url: "ajax.php?mod=system&content=feature_function&function=add&ver=<?php echo $admin['ver']; ?>",
            method: "POST",
            data: {
                category: $('#add_modal #class').val(),
                name: $('#add_modal #name').val(),
                ename: $('#add_modal #ename').val()
            },
            dataType: "text"
        });

        request.done(function(dat) {
            var data = JSON.parse(dat);
            var message = '';
            var arr = [];
            for (var key in data) {
                arr[key] = data[key];
            }
            if (arr['success']) {
                location.href = 'index.php?mod=' + function_mod + '&content=' + function_content + '&ver=<?php echo $admin['ver']; ?>';
            } else {
                var message_data = arr['message']['ch'].split(';');
                for (var i = 1; i <= message_data.length; i++) {
                    if (i == message_data.length) {
                        message += message_data[i - 1];
                    } else {
                        message += message_data[i - 1] + '</br>';
                    }
                    if (message_data[i - 1] == '必須設定程式類別。') {
                        $('#add_modal #class').addClass('alert_textbox');
                    } else if (message_data[i - 1] == '必須輸入程式名稱。') {
                        $('#add_modal #name').addClass('alert_textbox');
                    } else if (message_data[i - 1] == '必須輸入程式名稱(英文)。' || message_data[i - 1] == '無法新增已經存在的程式功能。' || message_data[i - 1] == '需要上傳功能檔案。') {
                        $('#add_modal #ename').addClass('alert_textbox');
                    }
                }
                $('#add_modal').modal('hide');
                Swal.fire(
                    "新增失敗", //標題 
                    message, //訊息內容(可省略)
                    "error" //圖示(可省略) success/info/warning/error/question
                    //圖示範例：https://sweetalert2.github.io/#icons
                ).then(function() {
                    $('#add_modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                });
            }
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requset failed: " + textStatus);
        });

    }

    function edit_function() {
        $("#edit_modal input[type='text']").removeClass('alert_textbox');
        $("#edit_modal select").removeClass('alert_textbox');
        var request = $.ajax({
            url: "ajax.php?mod=system&content=feature_function&function=edit&ver=<?php echo $admin['ver']; ?>",
            method: "POST",
            data: {
                category: $('#edit_modal #class').val(),
                name: $('#edit_modal #name').val(),
                mod: checked_values[0]
            },
            dataType: "text"
        });

        request.done(function(dat) {
            var data = JSON.parse(dat);
            var message = '';
            var arr = [];
            for (var key in data) {
                arr[key] = data[key];
            }
            if (arr['success']) {
                location.href = 'index.php?mod=' + function_mod + '&content=' + function_content + '&ver=<?php echo $admin['ver']; ?>';
            } else {
                var message_data = arr['message']['ch'].split(';');
                for (var i = 1; i <= message_data.length; i++) {
                    if (i == message_data.length) {
                        message += message_data[i - 1];
                    } else {
                        message += message_data[i - 1] + '</br>';
                    }
                    if (message_data[i - 1] == '必須設定程式類別。') {
                        $('#edit_modal #class').addClass('alert_textbox');
                    } else if (message_data[i - 1] == '必須輸入程式名稱。') {
                        $('#edit_modal #name').addClass('alert_textbox');
                    }
                }
                $('#edit_modal').modal('hide');
                Swal.fire(
                    "編輯失敗", //標題 
                    message, //訊息內容(可省略)
                    "error" //圖示(可省略) success/info/warning/error/question
                    //圖示範例：https://sweetalert2.github.io/#icons
                ).then(function() {
                    $('#edit_modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                });
            }
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requset failed: " + textStatus);
        });

    }

    function add_item(id) {
        if (!checked_values.includes(id)) {
            checked_values.push(id);
        } else {
            var checked_values2 = [];
            for (var i = 0; i < checked_values.length; i++) {
                if (checked_values[i] != id) {
                    checked_values2.push(checked_values[i]);
                }
            }
            checked_values = checked_values2;
        }
        console.log(checked_values);
    }

    function get_edit_target() {
        if (checked_values.length <= 1) {
            var request = $.ajax({
                url: "ajax.php?mod=system&content=feature_function&function=searchEditData&ver=<?php echo $admin['ver']; ?>",
                method: "POST",
                data: {
                    mod: checked_values[0]
                },
                dataType: "text"
            });

            request.done(function(dat) {
                var data = JSON.parse(dat);
                var arr = [];
                for (var key in data) {
                    arr[key] = data[key];
                }
                if (!arr['success']) {
                    Swal.fire(
                        "警告", //標題 
                        arr['message']['ch'], //訊息內容(可省略)
                        "warning" //圖示(可省略) success/info/warning/error/question
                        //圖示範例：https://sweetalert2.github.io/#icons
                    )
                } else {
                    $('#edit_modal #class').val(arr['data']['category']);
                    $('#edit_modal #name').val(arr['data']['name']);
                    $('#edit_modal #ename').val(arr['data']['content']);
                    $('#edit_modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });

                }
            });

            request.fail(function(jqXHR, textStatus) {
                alert("Requset failed: " + textStatus);
            });
        } else if (checked_values.length > 1) {
            Swal.fire(
                "警告", //標題 
                '不得編輯多個項目', //訊息內容(可省略)
                "warning" //圖示(可省略) success/info/warning/error/question
                //圖示範例：https://sweetalert2.github.io/#icons
            )

        }
    }

    function check_status() {
        if ($("input[name='input[]']").is(':checked')) {
            this.click();
        }
    }

    function delete_function() {
        var item = '<ol>';
        var request = $.ajax({
            url: "ajax.php?mod=system&content=feature_function&function=searchDeleteData&ver=<?php echo $admin['ver']; ?>",
            method: "POST",
            data: {
                mod: checked_values
            },
            dataType: "text"
        });
        request.done(function(dat) {
            var data = JSON.parse(dat);
            var arr = [];
            for (var key in data) {
                arr[key] = data[key];
            }
            if (arr['success']) {
                for (var i = 0; i < arr['data']['num']; i++) {
                    item = item + '<li>' + arr['data']['row'][i]['mode'] + '-' + arr['data']['row'][i]['name'] + '(' + arr['data']['row'][i]['content'] + ')</li>';
                }
                Swal.fire({
                    title: '確認刪除操作',
                    html: '<span>您所選的資料共' + arr['data']['num'] + '筆，將會永久刪除，而且此操作無法復原:' + item + '</ol></span>',
                    showCancelButton: true,
                    icon: 'question'
                }).then(function(result) {
                    if (result.value) {
                        delect_action();
                    }
                });
            }
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requset failed: " + textStatus);
        });
    }

    function delect_action() {
        var request = $.ajax({
            url: "ajax.php?mod=system&content=feature_function&function=delete&ver=<?php echo $admin['ver']; ?>",
            method: "POST",
            data: {
                mod: checked_values
            },
            dataType: "text"
        });
        request.done(function(dat) {
            var data = JSON.parse(dat);
            var arr = [];
            for (var key in data) {
                arr[key] = data[key];
            }
            if (arr['success']) {
                location.href = 'index.php?mod=' + function_mod + '&content=' + function_content + '&ver=<?php echo $admin['ver']; ?>';
            }
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requset failed: " + textStatus);
        });

    }

    function control_function() {
        var item = '<ol>';
        var request = $.ajax({
            url: "ajax.php?mod=system&content=feature_function&function=searchControlData&ver=<?php echo $admin['ver']; ?>",
            method: "POST",
            data: {
                mod: checked_values
            },
            dataType: "text"
        });
        request.done(function(dat) {
            var data = JSON.parse(dat);
            var arr = [];
            for (var key in data) {
                arr[key] = data[key];
            }
            if (arr['success']) {
                for (var i = 0; i < arr['data']['num']; i++) {
                    item = item + '<li><b>「' + arr['data']['row'][i]['mode'] + '-' + arr['data']['row'][i]['name'] + '」</b>狀態即將設定成' + arr['data']['row'][i]['status'] + '。</li>';
                }
                Swal.fire({
                    title: '確認狀態變更',
                    html: '<span>您所選的資料共' + arr['data']['num'] + '筆，正在變更狀態:' + item + '</ol></span>',
                    showCancelButton: true,
                    icon: 'question'
                }).then(function(result) {
                    if (result.value) {
                        control_action();
                    }
                });
            }
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requset failed: " + textStatus);
        });
    }

    function control_action() {
        var request = $.ajax({
            url: "ajax.php?mod=system&content=feature_function&function=setting&ver=<?php echo $admin['ver']; ?>",
            method: "POST",
            data: {
                mod: checked_values
            },
            dataType: "text"
        });
        request.done(function(dat) {
            var data = JSON.parse(dat);
            var arr = [];
            for (var key in data) {
                arr[key] = data[key];
            }
            if (arr['success']) {
                location.href = 'index.php?mod=' + function_mod + '&content=' + function_content + '&ver=<?php echo $admin['ver']; ?>';
            }
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requset failed: " + textStatus);
        });

    }
</script>