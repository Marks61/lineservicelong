<?php
$record = new record();
$data = $record->index();
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Dashboard</h5>
                <!--
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>-->
            </div>
            <div class="ibox-content">
                <div class="">
                    <!--<a onclick="fnClickAddRow();" href="javascript:void(0);" class="btn btn-primary ">Add a new row</a>-->
                </div>
                <table class="table table-striped table-bordered table-hover " id="editable">
                    <thead>
                        <tr>
                            <th>操作說明</th>
                            <th>功能分類</th>
                            <th>功能(名稱)</th>
                            <th>操作者</th>
                            <th>IP</th>
                            <th>紀錄時間</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data['row'] as $row) {
                        ?>
                            <tr>
                                <td><?php echo $row['record']; ?></td>
                                <td><?php echo $row['category'] == 0 ? '首頁' : '' ?></td>
                                <td><?php echo $row['function_name']; ?></td>
                                <td class="center"><?php echo $row['userinfo']['account'] . '(' . $row['userinfo']['name'] . ')'; ?></td>
                                <td class="center"><?php echo $row['ip']; ?></td>
                                <td class="center"><?php echo date('Y/m/d H:i:s', strtotime($row['created_at'])); ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
</div>