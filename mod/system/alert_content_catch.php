<?php
require_once('../mod/db/class.php');
date_default_timezone_set("Asia/Taipei");
class alertContentCatch_function extends db
{
    public function alerUpdate(&$output, $ch, $en)
    {
        $output['success'] = 0;
        $output['message']['ch'] = $ch;
        $output['message']['en'] = $en;
    }

    public function getIp()
    {
        $ip = '';
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
    }
}

$db = new db();
$alertContentCatchFunction = new alertContentCatch_function();
$output = ['success' => 1];
try {
    $message = isset($_POST['message']) ? $_POST['message'] : 0;

    if ($output['success']) {
        $sql = 'select title,content,icon,created_at from notification where id = ' . $message;
        $db->sqlQuery('update notification set poster = 1 where id = ' . $message);
        $output['data'] = $db->getRow($sql);
    }
} catch (Exception $e) {
    $alertContentCatchFunction->alerUpdate($output, '找不到資料', 'Find alert data is failed.');
}

echo json_encode($output);
