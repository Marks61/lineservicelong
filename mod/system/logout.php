<?php
require_once('../mod/db/class.php');
date_default_timezone_set("Asia/Taipei");
class logout_function extends db
{
    public function alerUpdate(&$output, $ch, $en)
    {
        $output['success'] = 0;
        $output['message']['ch'] = $ch;
        $output['message']['en'] = $en;
    }

    public function getIp()
    {
        $ip = '';
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
    }
}

$db = new db();
$logoutFunction = new logout_function();
$record = new record();
$output = ['success' => 1];
try {
    $account = isset($_POST['account']) ? $_POST['account'] : '';

    if ($output['success']) {
        $record->insert('system', 'identity', '系統登出:', $account, $logoutFunction->getIp());
    }
} catch (Exception $e) {
    $logoutFunction->alerUpdate($output, '登出作業失敗', 'Logout is failed.');
}

echo json_encode($output);
