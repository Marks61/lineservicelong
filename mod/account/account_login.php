<?php
date_default_timezone_set("Asia/Taipei");

$savePath = './session';
session_save_path($savePath);
// session_start();
// $lifetime = 604800;
// session_set_cookie_params($lifetime);

$lifetime = 604800;	//24小時
session_set_cookie_params($lifetime);

if (isset($_SESSION)) {
	session_start();
}

$admin = isset($_SESSION['admin']) ? $_SESSION['admin'] : array();
$ver = isset($_GET['ver']) ? $_GET['ver'] : 0;

$isVerification = true;
if ($isVerification) {
	if ($ver && $ver == $admin['ver'] && isset($admin['identity'])) {	//有登入
		header('location:admin/index.php?ver=' . $admin['ver']);
	}
} else {
}
$output = array("success" => 1, "message" => "", "ver" => "");
$account = isset($_POST['account']) ? $_POST['account'] : '';
$password = isset($_POST['password']) ? $_POST['password'] : '';
$today = date('Y-m-d');

$cla = new account();

$num = $cla->rs($account, $password);

if ($num == 0) {
	$output['success'] = 0;
	$output['message'] = '帳號或密碼不符合！';
}
if ($num == 1) {
	unset($_SESSION['admin']);
	$row = $cla->mbr($account, $password);

	$row['ver'] = substr(md5(time()), 0, 12);
	$output['url'] = $cla->getRow('select url from identity where name_en = \'' . $row['identity'] . '\'')['url'] . '?ver=' . $row['ver'];
	/*if ($row['identity'] == '一般會員' || $row['identity'] == '付費會員')
		$output['url'] = 'admin/index.php?mod=account&content=personal_modify&ver=' . $row['ver'];
	else if ($row['identity'] == '院所主管' || $row['identity'] == '關懷員' || $row['identity'] == '保全')
		$output['url'] = 'admin/index.php?mod=alert&content=real_time&ver=' . $row['ver'];
	else if ($row['identity'] == '藥局' || $row['identity'] == '診所' || $row['identity'] == '主管機關' || $row['identity'] == '居服員' || $row['identity'] == '醫療院所')
		$output['url'] = 'admin/index.php?mod=account&content=member_data&ver=' . $row['ver'];
	else if ($row['identity'] == '院所醫生')
		$output['url'] = 'admin/index.php?mod=longcare&content=longcare_order_outpatient&ver=' . $row['ver'];
	else if ($row['identity'] == '商家')
		$output['url'] = 'admin/index.php?mod=account&content=sale_record&ver=' . $row['ver'];
	else
		$output['url'] = 'admin/index.php?ver=' . $row['ver'];*/

	if ($row['identity'] == 'admin') {
		$_SESSION['admin'] = $row;
		$output['ver'] = $row['ver'];
	} else {
		$row['pidIsTrue'] = $cla->getCheckPid($row['pid']);
		$row['remind'] = $cla->getRemind($row);
		$row['view'] = $cla->isEnableView($row);

		// if ($row['identity'] == '一般會員' && !$row['pidIsTrue']) {
		// 	$output['success'] = 0;
		// 	$output['message'] = '您尚無綁定設備';
		// }
		/*if ($row['identity'] == '一般會員' && $row['end_date'] < $today) {
			$output['success'] = 0;
			$output['message'] = '您的使用期限已到期，<br>如要繼續使用請至購物車購買服務或與公司人員聯繫！';
		} else if ($row['identity'] == '付費會員' && $row['extension'] == '0000-00-00') {
			$output['success'] = 0;
			$output['message'] = '帳號尚未授權，請等待審核！';
		} else {
			$_SESSION['admin'] = $row;
			if ($row['identity'] == '院所主管' or $row['identity'] == '醫療院所' or $row['identity'] == '關懷員' or $row['identity'] == '診所' or $row['identity'] == '藥局' or $row['identity'] == '居服員' or $row['identity'] == '醫生') {
				$sql = "insert into carer_record (carer_id, login_date, login_time) values ('" . $row['id'] . "', '" . date('Y-m-d') . "', '" . date('Y-m-d H:i:s') . "')";
				$cla->sqlQuery($sql);
			}
			$output['identity'] = $row['identity'];
			$output['ver'] = $row['ver'];
		}*/
		if ($row['identity'] == 'admin') {
		} else if ($row['identity'] == '院所醫生' || $row['identity'] == '關懷員') {
		} else {
			$output['success'] = 0;
			$output['message'] = '帳號權限不足';
		}
	}
}
echo json_encode($output);
