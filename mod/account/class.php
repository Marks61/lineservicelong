<?php
class account extends db
{

	public function rs($f1, $f2)
	{
		$sql = "select * from account where account = '" . $this->escape($f1) . "' and password = '" . md5($f2) . "' and enable = 1";
		return ($this->getRows2($sql));
	}

	public function mbr($f1, $f2)
	{
		$sql = "select * from account where account = '" . $this->escape($f1) . "' and password = '" . md5($f2) . "' and enable = 1";
		return ($this->getRow2($sql));
	}

	//檢驗pid
	public function getCheckPid($pid)
	{
		$ispid = 0;
		if ($pid == '') {
		} else {
			$ispid = $this->checkPidCode($pid);
		}
		return $ispid;
	}

	//檢查pid 不存在傳回0
	public function checkPidCode($pid)
	{
		$sql = "select * from pid where pid = '" . $this->escape($pid) . "'";
		return $this->getQuery($sql)['num'];
	}

	//領藥門診提醒
	public function getRemind($row)
	{
		$arr = array('taking' => 0, 'outpatient' => 0, 'taking_id' => 0, 'outpatient_id' => 0);
		if ($row['identity'] == '一般會員' or $row['identity'] == '付費會員') {
			$arr['taking'] = $this->getTakingDays($row['id'], $arr);		//取領藥提醒天數
			$arr['outpatient'] = $this->getOutpatientDays($row['id'], $arr); //取門診提醒天數
		}
		return $arr;
	}

	//取領藥提醒天數
	public function getTakingDays($id, &$arr)
	{
		$days = 0;
		$today = date("Y-m-d");
		$taking_end = date("Y-m-d", strtotime("$today -7 day"));
		$sql = "select * from medicine_reminder where account_id = '" . $id . "' and taking_date >= '" . $taking_end . "' and taking_date <= '" . $today . "' and taking_enable = 1 order by id desc";

		if ($this->getRows($sql)) {
			$row = $this->getRow($sql);
			$arr['taking_id'] = $row['id'];
			$taking_date = $row['taking_date'];
			$taking_end_date = date("Y-m-d", strtotime("$taking_date +7 day"));
			$takingEndStr = strtotime($taking_end_date);
			$todatStr = strtotime($today);

			$days = round(($takingEndStr - $todatStr) / 3600 / 24);
		}
		return $days;
	}

	//取門診提醒天數
	public function getOutpatientDays($id, &$arr)
	{
		$days = 0;
		$today = date("Y-m-d");
		$outpatient_end = date("Y-m-d", strtotime("$today -15 day"));
		$sql = "select * from medicine_reminder where account_id = '" . $id . "' and outpatient_date >= '" . $outpatient_end . "' and outpatient_date <= '" . $today . "' and outpatient_enable = 1 order by id desc";

		if ($this->getRows($sql)) {
			$row = $this->getRow($sql);
			$arr['outpatient_id'] = $row['id'];
			$outpatient_date = $row['outpatient_date'];
			$outpatient_end_date = date("Y-m-d", strtotime("$outpatient_date +15 day"));
			$outpatientEndStr = strtotime($outpatient_end_date);
			$todatStr = strtotime($today);

			$days = round(($outpatientEndStr - $todatStr) / 3600 / 24);
		}
		return $days;
	}

	public function isEnableView($row)
	{
		return (strtotime($row['extension']) >= strtotime(date('Y-m-d'))) ? 1 : 0; //過期傳回0
	}

}
