<?php
	class language extends db {
		public function menu($key) {
			$arr = array(
			'購物車'			=>'Shopping cart',
			'關於霍恩'		=>'About',
			'關於產品'		=>'Product',
			'聯絡我們'		=>'Contact',
			'最新消息'		=>'News',
			'問與答'			=>'Q & A',
			'會員登入'		=>'Login',
			'語系'			=>'Language',
			'應用程式'		=>'Application'
			);
			return $arr[$key];
		}
		public function login($key) {
			$arr = array(
			'請輸入帳號'						=>'Enter your account',
			'請輸入密碼'						=>'Enter your password',
			'再次輸入密碼'					=>'Confirm Password',
			'請輸入姓名'						=>'Enter your name',
			'請輸入E-mail'					=>'Enter your E-mail',
			'請輸入出生年月日'				=>'Birthday',
			'輸入完畢，並驗證信箱'			=>'Enter and verify the mailbox',
			'完成註冊'						=>'Confirm',
			'帳號'							=>'Account',
			'密碼'							=>'Password',
			'登入'							=>'Login',
			'忘記密碼'						=>'Forget Password',
			'傳送新密碼'						=>'Send New Password',
			'會員登入'						=>'Login',
			'還沒註冊？'						=>'',
			'趕快申請一個帳號吧！'			=>'Apply for an account!',
			'註冊'							=>'Register',
			'英數組合6~10個字'				=>'Account must be between 6 and 10 characters',
			'英數組合6~20個字'				=>'Password must be between 6 and 20 characters',
			'※ 註冊信箱及密碼等同Horngo帳號密碼。'		=>'The registered mailbox and password are the same as the Horngo account password.',
			);
			return $arr[$key];
		}
		public function buy($key) {
			$arr = array(
			'購買地點'						=>'Place of Purchase',
			'Dr.Phone達特豐通訊'				=>'Dr.Phone',
			'高雄市鳳山區文龍路17號'			=>'No.17, Wenlong Rd., Fengshan Dist., Kaohsiung City 830, Taiwan (R.O.C.)',
			'漢藥局'							=>'HAN Pharmacy',
			'台北市北投區吉利街146號'			=>'No.146, Jili St., Beitou Dist., Taipei City 112, Taiwan (R.O.C.)'
			);
			return $arr[$key];
		}
		public function portfile($key) {
			$arr = array(
			'產品項目'					=>'Prduct List',
			'產品概覽'					=>'Overview',
			'技術規格'					=>'Tech Specs',
			'安規認證'					=>'Certification',
			'產品手冊'					=>'Manual'
			);
			return $arr[$key];
		}
	}
?>