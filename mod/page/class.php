<?php
	class page extends db {
		public function pagination($limit, $initial_page, $page, $start_page, $total_data_num) {
			$total_links = ceil($total_data_num / $limit);
			$previous_link = '';
			$next_link = '';
			$page_link = '';

			if ($total_links > 4) {
				if ($initial_page < 5) {
					for ($i = 1; $i <= 5; $i++) {
						$page_arr[] = $i;
					}

					$page_arr[] = '...';
					$page_arr[] = $total_links;
				} else {
					$end_limit = $total_links - 5;

					if ($initial_page > $end_limit) {
						$page_arr[] = 1;
						$page_arr[] = '...';
						for ($i = $end_limit; $i <= $total_links; $i++) {
							$page_arr[] = $i;
						}
					} else {
						$page_arr[] = 1;
						$page_arr[] = '...';

						for ($i = $initial_page - 1; $i <= $initial_page + 1; $i++) {
							$page_arr[] = $i;
						}

						$page_arr[] = '...';
						$page_arr[] = $total_links;
					}
				}
			} else {
				for ($i = 1; $i <= $total_links; $i++) {
					$page_arr[] = $i;
				}
			}

			for ($i = 0; $i < count($page_arr); $i++) {
				if ($initial_page == $page_arr[$i]) {
					$page_link .= '
						<li class="page-item active">
							<a class="page-link" href="#">'.$page_arr[$i].'
								<span class="sr-only">(current)</span>
							</a>
						</li>';

					$previous_id = $page_arr[$i] - 1;

					if ($previous_id > 0) {
					$previous_link = '
						<li class="page-item">
							<a class="page-link" href="javascript:void(0)" data-page_number="'.$previous_id.'">上一頁</a>
						</li>';
					} else {
					$previous_link = '
						<li class="page-item disabled">
							<a class="page-link" href="#">上一頁</a>
						</li>';
					}

					$next_id = $page_arr[$i] + 1;

					if ($next_id >= $total_links) {
					$next_link = '
						<li class="page-item disabled">
							<a class="page-link" href="#">下一頁</a>
						</li>';
					} else {
					$next_link = '
						<li class="page-item">
							<a class="page-link" href="javascript:void(0)" data-page_number="'.$next_id.'">下一頁</a>
						</li>';
					}
				} else {
					if ($page_arr[$i] == '...') {
					$page_link .= '
						<li class="page-item disabled">
							<a class="page-link" href="#">...</a>
						</li>';
					} else {
					$page_link .= '
						<li class="page-item">
							<a class="page-link" href="javascript:void(0)" data-page_number="'.$page_arr[$i].'">'.$page_arr[$i].'</a>
						</li>';
					}
				}
			}
			return $previous_link . $page_link . $next_link;
		}
	}
?>