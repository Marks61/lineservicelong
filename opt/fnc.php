<?php
function spost($f1,$f2,$f3,&$sta_arr,$min,$max){	//輸入過濾
	$p=$_POST[$f1];
	$p=trim($p);
	if($f3){//欄位為必填時
		if(strlen($p)===0){//欄位無資料
			$sta_arr[0]=$f1;
			$sta_arr[1]='empty';
			$sta_arr[2]='必填欄位不可為空';
			return $p;
		}else if(strlen($p)>$max||strlen($p)<$min){//字數限制不符合
			$sta_arr[0]=$f1;
			$sta_arr[1]='not';
			$sta_arr[2]=strlen($p).'字數不符';
			return $p;
		}else{//欄位格式檢查
			if($f2=='z9')preg_match('/[\w]+/',$p,$arr);		//英數組合
			if($f2=='int')preg_match('/[\d]+/',$p,$arr);		//數字組合
			if($f2=='txt'){
				$arr[0]=stripslashes($p);
				//$arr[0]=mysql_real_escape_string($p);		//預防資料庫攻擊
				$arr[0]=htmlentities($arr[0], ENT_COMPAT, 'utf-8');  //停用HTML語法
			}
			if($f2=='cot'){
				//$arr[0]=mysql_real_escape_string($p);		//預防資料庫攻擊
				$arr[0]=htmlentities($p, ENT_COMPAT, 'utf-8'); 	//停用HTML語法
			}
			if($f2=='password')return $p;				//密碼不檢查
			if($f2=='email'){					//email檢查
				if(preg_match('/([\w]+.[\w]+)@([\w]+.[\w]+)\.([\w]{2,4})/',$p,$arr)){
					return $p;
				}else{
					$sta_arr[0]=$f1;
					$sta_arr[1]='not';
					$sta_arr[2]='E-mail有誤';
					return $p;	
				}
			}
			if(strlen($arr[0])==strlen($p)&&strlen($arr[0])>0){
				return $arr[0];
			}else{//欄位型態值不符合
				$sta_arr[0]=$f1;
				$sta_arr[1]='not';
				$sta_arr[2]='值有誤'.strlen($arr[0]).'='.strlen($p);
				return $arr[0];
			}
		}
	}else{//欄位為非必填時
			if($f2=='int')preg_match('/[\d]+/',$p,$arr);		//數字組合
			if($f2=='txt'){
				$arr[0]=stripslashes($p);
				//$arr[0]=mysql_real_escape_string($p);		//預防資料庫攻擊
				$arr[0]=htmlentities($arr[0], ENT_COMPAT, 'utf-8');  //停用HTML語法
			}
			if($f2=='cot'){
				//$arr[0]=mysql_real_escape_string($p);	//預防資料庫攻擊
				$arr[0]=htmlentities($p, ENT_COMPAT, 'utf-8'); 
			}
			if(strlen($arr[0])==strlen($p)&&strlen($arr[0])>0){
				return $arr[0];
			}else if(strlen($arr[0])===0){
				return '';
			}else{//欄位型態值不符合
				$sta_arr[0]=$f1;
				$sta_arr[1]='not';
				$sta_arr[2]='值有誤!';
				return $arr[0];
			}	
	}
	return $arr[0];
}

function post_digit($f1,$f2,$f3){//頁數	取值(網址參數, 預設值, 預設值有效)
	if($f3){//有預設值
		preg_match('/[\d]+/',$_POST[$f1],$arr);
		if(strlen($arr[0])==0)$arr[0]=$f2;
	}else{
		preg_match('/[\d]+/',$_POST[$f1],$arr);
		if(strlen($arr[0])==0)$arr[0]=-1;
	}
	return $arr[0];
}
function get_digit($f1,$f2,$f3){//頁數	取值(網址參數, 預設值, 預設值有效)
	if($f3){//有預設值
		$arr=array();
		if(isset($_GET[$f1])){
			preg_match('/([\d]+)/',$_GET[$f1],$arr);
			if(strlen($arr[0])==0)$arr[0]=$f2;
		}else{
			$arr[0]=$f2;
		}
	}else{
		$arr=array();
		if(isset($_GET[$f1])){
			preg_match('/([\d]+)/',$_GET[$f1],$arr);
			if(strlen($arr[0])==0)$arr[0]=-1;
		}else{
			$arr[0]=-1;
		}
	}
	return $arr[0];
}

function img_resize($img_w, $img_h, $img_limit){
	if($img_w > $img_limit || $img_h > $img_limit){
		if($img_w>$img_h){ //比較寬
			$new_h = round($img_h * $img_limit / $img_w);
			$new_w = $img_limit;
			$new_size = array(1=>$new_w, 2=>$new_h);
			return $new_size;
		}else if($img_w<$img_h){ //比較高
			$new_w = round($img_w * $img_limit / $img_h);
			$new_h = $img_limit;
			$new_size = array(1=>$new_w, 2=>$new_h);
			return $new_size;
		}else{
			$new_h = $img_limit;
			$new_w = $img_limit;
			$new_size = array(1=>$new_w, 2=>$new_h);
			return $new_size;
		}
	}else{
		$new_size = array(1=>$img_w, 2=>$img_h);
		return $new_size;
	}

}
function power_check($id,$m2){
	$flg=false;
	if($m2==''){
	
	}else if($m2=='all'){
		$flg=true;
	}else{
		$arr_m2=explode(",",$m2);
		for($i=0;$i<count($arr_m2);$i++){
			if($id==$arr_m2[$i])$flg=true;
		}
	}
	return $flg;
}
function power_check2($pstr,$arrp){
	$flg=false;
		if(in_array($pstr,$arrp)){
			$flg=true;
		}
	return $flg;
}
?>