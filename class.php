<?php
	require_once ("cdb/config.php");
	require_once ("opt/error_handler.php");

	class login {
		private $mMysqli;

		function __construct() {
			$this->mMysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_DATABASE);
			$this->mMysqli->query("set names utf8");
		}

		public function __destruct() {
			$this->mMysqli->close();
		}

		public function escape($value) {
			return $this->mMysqli->real_escape_string($value);
		}

		public function rs($f1,$f2) {
			$num = 0;
			$sql = "select * from account where account = '".$this->escape($f1)."' and password = '".md5($f2)."' and enable = 1";
			if ($result = $this->mMysqli->query($sql)) {
				$num = $result->num_rows;
				$result->close();
			}
			return $num;
		}

		public function mbr($f1,$f2) {
			$row = array();
			$sql = "select * from account where account = '".$this->escape($f1)."' and password = '".md5($f2)."' and enable = 1";
			if ($result = $this->mMysqli->query($sql)) {
				$row = $result->fetch_array(MYSQLI_ASSOC);
				$result->close();
			}
			return $row;
		}

		public function getRows($sel) {
			$num = -1;
			if ($result1 = $this->mMysqli->query($sel)) {
				$num = $result1->num_rows;
			}
			$result1->close();
			return $num;
		}

		public function getRow($str) {
			if ($result = $this->mMysqli->query($str)) {
				$row = $result->fetch_array(MYSQLI_ASSOC);
			}
			$result->close();
			return $row;
		}

		public function getrows_b($str) {
			$output = false;
			$num = -1;
			if ($result1 = $this->mMysqli->query($str)) {
				$num = $result1->num_rows;
			}
			$result1->close();
			($num >= 1)?$output = true:$output = false;
			return $output;
		}

		public function getQuery($sql) {
			$rows = array('num'=>0,'row'=>array());
			if ($result = $this->mMysqli->query($sql)) {
				$rows['num'] = $result->num_rows;
				while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
					$rows['row'][] = $row;
				}
				$result->close();
			}
			return $rows;
		}

		//取新日期
		public function getnewdate($date_added,$timeminute) {
			$tem = explode(' ',$date_added);
			$temdate = explode('-',$tem[0]);
			$temtime = explode(':',$tem[1]);
			return date("Y-m-d H:i:s",mktime($temtime[0],$temtime[1]+(int)$timeminute,$temtime[2],$temdate[1],$temdate[2],$temdate[0]));
		}

		public function sqlQuery($sql) {
			if ($this->mMysqli->query($sql)) {
				return true;
			} else {
				return false;
			}
		}

		public function get_client_ip() {
			$output = '';
			foreach (array(
	                'HTTP_CLIENT_IP',
	                'HTTP_X_FORWARDED_FOR',
	                'HTTP_X_FORWARDED',
	                'HTTP_X_CLUSTER_CLIENT_IP',
	                'HTTP_FORWARDED_FOR',
	                'HTTP_FORWARDED',
	                'REMOTE_ADDR',
	                'HTTP_VIA') as $key) {
				if (array_key_exists($key, $_SERVER)) {
					if($output=='') {
						$output=$key.'='.$_SERVER[$key];
					} else {
						$output.=','.$key.'='.$_SERVER[$key];
					}
				}
			}
			return $output;
		}

		public function isEnableView($row) {
			return (strtotime($row['extension'])>=strtotime(date('Y-m-d')))?1:0;//過期傳回0
		}

		//檢查pic 不存在傳回0
		public function checkPidCode($pid) {
			$sql = "select * from pid where pid = '".$this->escape($pid)."'";
			return $this->getQuery($sql)['num'];
		}

		//檢驗pid
		public function getCheckPid($pid) {
			$ispid = 0;
			if ($pid=='') {

			} else {
				$ispid = $this->checkPidCode($pid);
			}
			return $ispid;
		}

		//領藥門診提醒
		public function getRemind($row) {
			$arr = array('taking'=>0,'outpatient'=>0,'taking_id'=>0,'outpatient_id'=>0);
			if ($row['identity'] == '一般會員') {
				$arr['taking'] = $this->getTakingDays($row['id'],$arr);		//取領藥提醒天數
				$arr['outpatient'] = $this->getOutpatientDays($row['id'],$arr);//取門診提醒天數
			}
			return $arr;
		}

		//取領藥提醒天數
		public function getTakingDays($id,&$arr) {
			$days = 0;
			$today = date("Y-m-d");
			$taking_end = date("Y-m-d",strtotime("$today -7 day"));
			$sql = "select * from medicine_reminder where account_id = '".$id."' and taking_date >= '".$taking_end."' and taking_date <= '".$today."' and taking_enable = 1 order by id desc";

			if ($this->getrows($sql)) {
				$row = $this->getrow($sql);
				$arr['taking_id'] = $row['id'];
				$taking_date = $row['taking_date'];
				$taking_end_date = date("Y-m-d",strtotime("$taking_date +7 day"));
				$takingEndStr = strtotime($taking_end_date);
				$todatStr = strtotime($today);

				$days = round(($takingEndStr-$todatStr)/3600/24) ;
			}
			return $days;
		}

		//取門診提醒天數
		public function getOutpatientDays($id,&$arr) {
			$days = 0;
			$today = date("Y-m-d");
			$outpatient_end = date("Y-m-d",strtotime("$today -15 day"));
			$sql = "select * from medicine_reminder where account_id = '".$id."' and outpatient_date >= '".$outpatient_end."' and outpatient_date <= '".$today."' and outpatient_enable = 1 order by id desc";

			if ($this->getrows($sql)) {
				$row = $this->getrow($sql);
				$arr['outpatient_id'] = $row['id'];
				$outpatient_date = $row['outpatient_date'];
				$outpatient_end_date = date("Y-m-d",strtotime("$outpatient_date +15 day"));
				$outpatientEndStr = strtotime($outpatient_end_date);
				$todatStr = strtotime($today);

				$days = round(($outpatientEndStr-$todatStr)/3600/24) ;
			}
			return $days;
		}
	}
?>